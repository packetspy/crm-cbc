/**
 * Created by ludio on 05/08/16.
 */

var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
var productArrayAdd = [];

$("#adicionarProduto").on('click', function(e) {
    e.preventDefault();

    var states = $("#estadoSelect").val();
    var productSelect = $("#productSelect option:selected");
    var product = productSelect.val();
    var productName = productSelect.text();
    var product_type = $("#productType").val();

    //$("#estadoSelect").val("").selectpicker("refresh");
    //$("#productSelect").val("").selectpicker("refresh");
    //$("#productType").val("").selectpicker("refresh");
    if (addProduct(product, product_type, states, productName)) {
        notifyMsg('Produto adicionado com sucesso', 'success');
    }
});

$("#adicionarPacote").on('click', function(e) {
    e.preventDefault();

    var states = $("#estadopacoteSelect").val();
    var packageSelect = $("#packageSelect option:selected");
    var productsFromPackage = packageSelect.data('products-id');
    var packageType = $("#packageType").val();
    $("#estadopacoteSelect").val("").selectpicker("refresh");
    $("#packageSelect").val("").selectpicker("refresh");
    $("#packageType").val("").selectpicker("refresh");

    if(typeof(productsFromPackage) != 'number') {
        productsFromPackage = productsFromPackage.split(",");
    } else {
        productsFromPackage = [productsFromPackage];
    }

    for(product in productsFromPackage) {
        var productId = productsFromPackage[product].toString();
        var productName = products[productsFromPackage[product]];
        var response = addProduct(productId, packageType, states, productName);
    }
    if (response) {
        notifyMsg('Produtos do pacote adicionados com sucesso', 'success');
    }


});

var notifyMsg = function(msg, type = 'info')
{
    jQuery.notify({
        // options
        message: msg
    },{
        // settings
        "type": type,
        "z_index": 10000,
        "placement": {
            "from": "top",
            "align": "center"
        }
    });
};

var validateProduct = function (product_id, product_type, states)
{
    if (product_id == undefined || product_id == '') {
        notifyMsg('Selecione ao menos um produto!', 'danger');
        return false;
    }
    if (states == '' || states == undefined) {
        notifyMsg('Selecione ao menos um estado!', 'danger');
        return false;
    }
    if (product_type == undefined || product_type == '') {
        notifyMsg('Selecione a finalidade do produto', 'danger');
        return false;
    }

    if(productArrayAdd.indexOf(product_id.toString()) > -1) {
        notifyMsg("Produto já inserido", 'danger');
        return false;
    }

    return true;
};

var uniqueID = function (){
    function chr4(){
        return Math.random().toString(16).slice(-4);
    }
    return chr4() + chr4() +
        '-' + chr4() +
        '-' + chr4() +
        '-' + chr4() +
        '-' + chr4() + chr4() + chr4();
};


var addProduct = function (product_id, product_type, states, product_name)
{
    if (!validateProduct(product_id, product_type, states))
        return false;

    productArrayAdd.push(product_id);

    var productUniqueId = uniqueID();
    var productItem  = '<tr id="'+ productUniqueId +'">';
    productItem     += '<td>' + product_type.toUpperCase() + '<input type="hidden" name="productType[' + productUniqueId + ']" value="'+ product_type +'" readonly="readonly"></td>';
    productItem     += '<td>'+ product_name +' <input type="hidden" name="productID[' + productUniqueId + ']" value="'+ product_id +'" readonly="readonly"></td>';
    productItem     += '<td><input type="hidden" name="productStates[' + productUniqueId + ']" value="' + states + '">';
    productItem     += states + '</td>';
    productItem     += '<td><a class="removeProduct btn btn-xs btn-danger btn-fill" data-id="' + productUniqueId + '"><i class="fa fa-trash"></i></a></td>';
    productItem     += '</tr>';

    $("#productList").append(productItem);
    return true;
};



$(document).on('click', '.removeProduct', function(e) {
    e.preventDefault();
    var id = $(this).data('id');

    productArrayAdd.splice(productArrayAdd.indexOf(id), 1);

    var remo_db = $(this).data('db');
    if (remo_db == true) {
        var state = $(this).data('state');
        var product_id = $(this).data('product-id');
        var contact_id = $(this).data('contact-id');
        var transaction = $(this).data('transaction');

        var ajaxRequest = $.ajax({
           url: url_remove,
           type: "post",
           data: {
               contact_id: contact_id,
               product_id: product_id,
               transacao: transaction,
               estado: state,
               _token: CSRF_TOKEN
           }
        });

        ajaxRequest.done(function (response, textStatus, jqXHR){
            // show successfully for submit message
            $('#'+id).remove();
            notifyMsg('Produto excluído', 'info');
        });

        /* On failure of request this function will be called  */
        ajaxRequest.fail(function (){

            // show error
            notifyMsg('Falha na requisição, tente novamente', 'danger');
           // $("#result").html('There is error while submit');
        });

        return false;
    }


    $('#'+id).remove();
    notifyMsg('Produto excluído', 'info');
    return false;
});
