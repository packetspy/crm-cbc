$(document).ready(function() {

    $('.moveContactForm').submit(function () {
        var client_id = $('#client_id').length;
        if (client_id == 0)
        {
            notifyMsg('Escolha uma empresa para mover o contato', 'danger');
            return false;
        }
        return true;
    });
});


$(document).on('change', '.slct-tipo', function(e) {
    console.log(e);
    var inputElemento =  $(this).parent('div').parent('div').find('.col-sm-8 .input-contact').first();

    var value = e.target.value;
    if (value == 'telefone')
    {
        var masks = ['(00) 00000-0000', '(00) 0000-00009'], maskBehavior = function(val, e, field, options) {
            return val.length > 14 ? masks[0] : masks[1];
        };
        inputElemento.mask(maskBehavior, {
            onKeyPress : function(val, e, field, options) {
                field.mask(maskBehavior(val, e, field, options), options);
            }
        });
    }
    else {
        inputElemento.unmask();
    }

});


$(document).on('click', '#checaContato', function(e) {
   e.preventDefault();

    var productItems = $('#productList >tbody >tr').length - 1;
   // console.log('Produtos ' + productItems);

    if (productItems < 1)
    {
        notifyMsg('Escolha ao menos um produto de interesse', 'danger');
        return false;
    }

    $("#contactForm").submit();
    return true;
});


var novaFormaDeContato = function()
{
    var hideElement = $('.contact-form-elements-hide').clone();
    var qtd = $('.contact-form-append .col-sm-12').length;
    hideElement.show();
    $('.contact-form-append .col-sm-12').append(hideElement.html());
    var element = $('.contact-form-single:last-child');
    var classElement = 'contact-form-element' + qtd;
    element.addClass(classElement);
    element.append();
}

$('#more-contact-forms').on('click', function(){
    novaFormaDeContato();
});

var removerContatoHtml = function(el) {
    //$(el).parent().html("")
    $(el).closest('.row').remove();
}

var show_product = function()
{
    $(".btn-show-packages").removeClass("btn-fill");
    $(".btn-show-products").addClass("btn-fill");
    $(".package-show").each(function(){
        $(this).hide();
    });
    $(".product-show").each(function() {
        $(this).show();
    });
}

var show_package = function()
{
    $(".btn-show-products").removeClass("btn-fill");
    $(".btn-show-packages").addClass("btn-fill");
    $(".product-show").each(function(){
        $(this).hide();
    });
    $(".package-show").each(function() {
        $(this).show();
    });
}

var neverMind = function()
{
  $('#contact-form').hide();
  $('#contact-new').show();
}

var handleTipoContato = function() {
    var e = document.getElementById("tipocontato");
    var str = e.options[e.selectedIndex].dataset.label;
    document.getElementById('label_tipo_contato').innerHTML = str;
  }

var loadContact = function(contact) {
    productArrayAdd = [];
    $.get(contact, function(data){
        $('#contact-form').html(data);
        $('#estadoSelect').selectpicker();
        $('#productSelect').selectpicker();
        $('#productType').selectpicker();
        $("#estadopacoteSelect").selectpicker();
        $("#packageSelect").selectpicker();
        $("#packageType").selectpicker();
        $('#contact-form').show();
        $('#contact-new').hide();
        show_product();
        //products_or_group();
    });
}
