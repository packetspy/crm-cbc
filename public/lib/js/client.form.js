/**
 * Created by Lúdio on 07/07/2016.
 */

var maskTipoPessoa = function(value) {
    if (value == 1)
    {
        $("#label-documento").html("CPF");
        $('#documento').mask('000.000.000-00', {reverse: false});
    }
    else {
        $("#label-documento").html("CNPJ");
        $("#documento").mask("00.000.000/0000-00");
    }
}

$('#tipo_documento').on('change', function(e) {
   console.log(e);
   var value = e.target.value;
   console.log(value);
   maskTipoPessoa(value);
});

$("#contact-new").on("click", function() {
    productArrayAdd = [];
    $('#contact-form').show();
});

$(document).ready(function() {
    //telefone mascaras de são paulo
    aplicaMascaraTelefone('.mask-telefone');
    maskTipoPessoa($("#tipo_documento").val());
    $('#contact-form').hide();

});