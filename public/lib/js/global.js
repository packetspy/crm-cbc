var Global = function() {

    var rootHost = function(){
      return document.location.origin;
    }

    var removeElement = function(id = null, type = null, contact_id = null, product_id = null, transacao = null){
      switch(type) {
          case 'contato':
              elementToRemove = '#panel'+id;
              //remove item com animação
              $(elementToRemove).fadeOut(300, function(){ $(this).remove();});
              totalElement = $(elementToRemove).parent('.accordion').find('.panel-collapse').length - 1;
              console.log(totalElement);
              if (totalElement == 0){
                //exibe mensagem de vazio
                $("#empty-contato").removeClass("hide");
              }
              break;
          case 'produto':
              elementToRemove = '#tr_p'+contact_id+'-'+product_id+'-'+transacao;
              //remove item com animação
              $(elementToRemove).fadeOut(300, function(){ $(elementToRemove).remove();});
              totalElement = $(elementToRemove).parent('tbody').find('tr').length - 1;
              console.log(totalElement);
              if (totalElement == 0){
                //exibe mensagem de vazio
                $("#empty-produto").removeClass("hide");
                $(elementToRemove).parent('tbody').parent('table').addClass("hide");
              }
              break;
          case 'email':
              elementToRemove = '#li'+id;
              //remove item com animação
              $(elementToRemove).fadeOut(300, function(){ $(this).remove();});
              totalElement = $(elementToRemove).parent('ul').find('li').length - 1;
              console.log(totalElement);
              if (totalElement == 0){
                //exibe mensagem de vazio
                $("#empty-email").removeClass("hide");
              }
              break;
          case 'telefone':
              elementToRemove = '#li'+id;
              //remove item com animação
              $(elementToRemove).fadeOut(300, function(){ $(this).remove();});
              totalElement = $(elementToRemove).parent('ul').find('li').length - 1;
              console.log(totalElement);
              if (totalElement == 0){
                //exibe mensagem de vazio
                $("#empty-telefone").removeClass("hide");
              }
              break;
          case 'skype':
              elementToRemove = '#li'+id;
              //remove item com animação
              $(elementToRemove).fadeOut(300, function(){ $(this).remove();});
              totalElement = $(elementToRemove).parent('ul').find('li').length - 1;
              console.log(totalElement);
              if (totalElement == 0){
                //exibe mensagem de vazio
                $("#empty-skype").removeClass("hide");
              }
              break;
      }
    }

    // Handles Bootstrap Tooltips.
    var handleTooltips = function() {
        // global tooltips
        $('.tooltips').tooltip();

        //Folow de the documentation
        $('[data-toggle="tooltip"]').tooltip()
    };

  var handleConfirmation = function () {
      if (!$().confirmation) {
          return;
      }
      $('[data-toggle=confirmation]').confirmation({
        rootSelector: '[data-toggle=confirmation]',
        // other options
      });
  }
  var handleDisableClieck = function () {
      //Ainda não esta funcionando
      $('disableonclick').click(function(event){
        $('disableonclick').prop('disabled', true);
      });
  }

  var handleDelete = function () {
      $(document).on('click', '.delete', function (e) {
        e.preventDefault();
        var id = $(this).data('id');
        var type = $(this).data('type');
        var token = $(this).data('token');
        var titledelete = $(this).data('titledelete');
        var textdelete = $(this).data('textdelete');
        var titlesuccess = $(this).data('titlesuccess');
        var url = $(this).data('url');
        swal({
                title: titledelete,
                text: textdelete,
                type: "error",
                confirmButtonClass: "btn-fill btn-danger",
                confirmButtonText: "Sim!",
                showCancelButton: true,
                cancelButtonClass: "btn-fill",
                cancelButtonClass: "btn-fill",
                cancelButtonText: "Cancelar",
                html: true
            },
            function() {
                $.ajax({
                    type: "DELETE",
                    url: rootHost()+'/'+url+'/'+id,
                    data: {'id':id, '_token': token},
                    success: function (data){
                        //swal(titlesuccess, data.message, data.type);
                        swal({
                                title: titlesuccess,
                                type: "success",
                                confirmButtonClass: "btn-fill btn-success",
                                html: true
                            });
                        removeElement(id, type);
                    }
                });
            });
      });
  }

  var handleDeleteProduto = function () {
      $(document).on('click', '.delete-produto', function (e) {
        e.preventDefault();
        var contact_id = $(this).data('contactid');
        var product_id = $(this).data('productid');
        var transacao = $(this).data('transacao');
        var type = $(this).data('type');
        var token = $(this).data('token');
        var titledelete = $(this).data('titledelete');
        var textdelete = $(this).data('textdelete');
        var titlesuccess = $(this).data('titlesuccess');
        var url = $(this).data('url');
        swal({
                title: titledelete,
                text: textdelete,
                type: "error",
                confirmButtonClass: "btn-fill btn-danger",
                confirmButtonText: "Sim!",
                showCancelButton: true,
                cancelButtonClass: "btn-fill",
                cancelButtonClass: "btn-fill",
                cancelButtonText: "Cancelar",
                html: true
            },
            function() {
                $.ajax({
                    type: "DELETE",
                    //url: rootHost()+'/'+url+'/'+id,
                    url: rootHost()+'/'+url+'/'+product_id,
                    data: {'contact_id':contact_id, 'product_id':product_id, 'transacao':transacao, '_token': token},
                    success: function (data){
                        swal({
                                title: titlesuccess,
                                type: "success",
                                confirmButtonClass: "btn-fill btn-success",
                                html: true
                            });
                        removeElement(null, type, contact_id, product_id, transacao);
                    }
                });
            });
      });
  }

  var handleSure = function () {
      $(document).on('click', '.sure', function (e) {
        e.preventDefault();
        var form = $(this).data('form');
        var type = $(this).data('type');
        var titledelete = $(this).data('titledelete');
        var textdelete = $(this).data('textdelete');
        var titlesuccess = $(this).data('titlesuccess');
        var url = $(this).data('url');
        swal({
                title: titledelete,
                text: textdelete,
                type: "warning",
                confirmButtonClass: "btn-fill btn-danger",
                confirmButtonText: "Sim!",
                showCancelButton: true,
                cancelButtonClass: "btn-fill",
                cancelButtonText: "Cancelar",
                html: true
            },
            function(isConfirm) {
              //if (isConfirm) $('.form-delete').submit();
              if (isConfirm) $(form).submit();
              });
      });
  }

  var handleTransfer = function () {
      $(document).on('click', '.transfer', function (e) {
        //e.preventDefault();
        var id = $(this).data('id');
        var type = $(this).data('type');
        var token = $(this).data('token');
        var titletransfer = $(this).data('titletransfer');
        var titleok = $(this).data('titleok');
        var titlesuccess = $(this).data('titlesuccess');
        var url = $(this).data('url');
        swal({
                title: titletransfer,
                type: "warning",
                confirmButtonClass: "btn-fill btn-warning",
                confirmButtonText: "Sim!",
                showCancelButton: true,
                cancelButtonClass: "btn-fill",
                cancelButtonText: "Cancelar",
                html: true
            },
            function() {
                $.ajax({
                    type: "GET",
                    url: rootHost()+'/'+url+'/'+id,
                    success: function (data){
                        //swal(titleok, titlesuccess, "success");
                        swal({
                                title: titleok,
                                type: "success",
                                confirmButtonClass: "btn-fill btn-success",
                                html: true
                            });
                        removeElement(id, type);
                    }
                });
            });
      });
  }

  var handleClipboard = function (){
    var clipboard = new Clipboard('.clipboard');
    clipboard.on('success', function(e) {
        console.log(e);
    });
    clipboard.on('error', function(e) {
        console.log(e);
    });
  }

    return {
        //main function to initiate the module
        init: function() {
            handleTooltips();
            handleConfirmation();
            handleDisableClieck();
            handleDelete();
            handleDeleteProduto();
            handleSure();
            handleTransfer();
            handleClipboard();
        }
    };
}();

jQuery(document).ready(function() {
    Global.init();
});
