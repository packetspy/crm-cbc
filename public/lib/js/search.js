$.typeahead({
    //input: '.js-typeahead-busca',
    input: '.js-typeahead-busca',
    minLength: 1,
    maxItem:500,
    order: "asc",
    hint: true,
    dynamic: true,
    //dynamicFilter: ['id','documento','nome','empresa','status_id','status.id'],
    display: ['id','documento','nome','empresa','status.id','status.status'],
    accent: true,
    //maxItemPerGroup: 30,
    backdrop: false,
    href: "/cliente/resumo/{{id}}",
    emptyTemplate: 'Nenhum resultado encontrado para "{{query}}"',
    template: function(){
      if (status.id === '1'){
        return 'não passa por aqui =/ Why?!'
      }else{
        return '<span class="label-title">Nome Fantasia:</span> {{nome}} </br>'
                + '<span class="label-title">Razão Social:</span> {{empresa}} '
                + '<span class="label label-info pull-right">{{status.status}} </span>'
                //+ '<a class="btn btn-fill btn-xs btn-default" onclick="abreModal(\'https://crm.cbcnegocios.com.br/clientes/nova-interacao/1746\', \'Nova interação\', \'modal_escolha_autocomplete\');">+ Interação</a>'
      }
    },
    source: {
        "Clientes": {
            ajax: {
                url: "/busca/clientes",
                dataType: "json",
                path: "",
                data: {
                  query: '{{query}}', type: 'cliente'
              },
            }
        }
    },
    callback: {
        onClickAfter: function (node, a, item, event) {
              //event.preventDefault;
              // href key gets added inside item from options.href configuration
              //alert(item.href);
          }
        },
      debug: false
});
