$(document).ready(function() {
    $('.select').selectpicker({
      allowClear: true,
      placeholder: "Selecione"
    });

    $(function() {
    $('.daterange').daterangepicker({
        timePicker: false,
        singleDatePicker: true,
        showDropdowns: true,
        locale: {
            //format: 'DD/MM/YYYY',
            format: 'YYYY-MM-DD',
        }
    });
});

    //Desabilita campo caso não seja responsavel do cliente ou Gertores/Atendimento
    readOnly();
});

function readOnly(){
  isResponsavel = document.getElementById("isResponsavel");
  form = document.getElementById($(isResponsavel).closest('form').attr('id'));
  if (isResponsavel){
    if (isResponsavel.value == 'false') {
      $('#'+form.id+' input').attr('readonly','readonly');
      $('#'+form.id+' select').attr('disabled',true);
      $('#'+form.id+' button').attr('disabled',true);
    }
  }

}

var abreModal = function(url, titulo, classes = "") {
    if (classes == undefined)
        classes = "";

    var $url = url;
    BootstrapDialog.show({
        title: titulo,
        cssClass: classes,
        message: function(dialog) {
            var $message = $('<div></div>');
            var pageToLoad = dialog.getData('pageToLoad');
            $message.load(pageToLoad);
            return $message;
        },
        data: {
            'pageToLoad': $url
        },
        buttons: [ {
            label: 'Fechar',
            class: "btn-fill btn_close_modal",
            action: function(dialogItself){
                dialogItself.close();
            }
        }]
    });
};

function aplicaMascaraTelefone(elemento)
{
    var masks = ['(00) 00000-0000', '(00) 0000-00009'], maskBehavior = function(val, e, field, options) {
        return val.length > 14 ? masks[0] : masks[1];
    };
    $(elemento).mask(maskBehavior, {
        onKeyPress : function(val, e, field, options) {
            field.mask(maskBehavior(val, e, field, options), options);
        }
    });
};
