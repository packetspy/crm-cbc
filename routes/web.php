<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// \Event::listen('Illuminate\Database\Events\QueryExecuted', function ($query) {
//     echo'<pre>';
//     var_dump($query->sql);
//     var_dump($query->bindings);
//     var_dump($query->time);
//     echo'</pre>';
// });

// ******** Migração de rotas novas ******** //

//Proteçao para caracteres numericos, na versão 5.4 muda forma de implementar
Route::pattern('id', '[0-9]+');
Route::pattern('client_id', '[0-9]+');

//Rotas de autenticaçao
//Route::auth();

/*
********************** Autenticação **********************
*/
//Route::auth();
Route::group(['middleware' => ['web']], function() {

// Login Routes...
    Route::get('login', ['as' => 'login', 'uses' => 'Auth\LoginController@showLoginForm']);
    Route::post('login', ['as' => 'login.post', 'uses' => 'Auth\LoginController@login']);
    Route::post('logout', ['as' => 'logout', 'uses' => 'Auth\LoginController@logout']);
    Route::get('logout', ['as' => 'logout', 'uses' => 'Auth\LoginController@logout']);
    Route::post('reset', ['as' => 'reset.post', 'uses' => 'Auth\LoginController@reset']);

// Registration Routes...
    //Route::get('register', ['as' => 'register', 'uses' => 'Auth\RegisterController@showRegistrationForm']);
    //Route::post('register', ['as' => 'register.post', 'uses' => 'Auth\RegisterController@register']);

// Password Reset Routes...
    Route::get('password/reset', ['as' => 'password.reset', 'uses' => 'Auth\ForgotPasswordController@showLinkRequestForm']);
    Route::post('password/email', ['as' => 'password.email', 'uses' => 'Auth\ForgotPasswordController@sendResetLinkEmail']);
    Route::get('password/reset/{token}', ['as' => 'password.reset.token', 'uses' => 'Auth\ResetPasswordController@showResetForm']);
    //Route::post('password/reset', ['as' => 'password.reset.post', 'uses' => 'Auth\ResetPasswordController@reset']);
    Route::post('password/reset', ['as' => 'password.reset.post', 'uses' => 'Crm\USerController@postAlterarSenha']);
});

//Gerenciamento de perfil pelo usuário
Route::group(array('middleware' => 'auth', 'namespace' => 'Crm', 'prefix' => 'perfil'), function()
{
  Route::get('/', 'UserController@getPerfil');
  //Route::get('alterar-senha', 'UserController@alterarSenha');
  Route::post('alterar-senha', 'UserController@postAlterarSenha');
});


//Busca
Route::group(array('middleware' => ['auth','needsRoleOrPermission'], 'namespace' => 'Crm', 'prefix' => 'busca'), function()
{
  Route::get('avancada', 'BuscaController@getAvancada');
  Route::get('clientes', 'BuscaController@getClientes');

});

//Dashboard
Route::group(array('middleware' => 'auth', 'namespace' => 'Crm'), function()
{
  Route::get('/', 'DashboardController@index');
  Route::resource('dashboard', 'DashboardController');
});

//Lista de clientes
Route::group(array('middleware' => 'auth', 'namespace' => 'Crm', 'prefix' => 'clientes'), function()
{
  Route::get('/', ['as' => 'clientes' ,'middleware' => ['auth','needsRoleOrPermission'], 'is' => ['superuser'], 'shield' => 'client.create', 'any' => true, 'uses' => 'ClientController@getIndex']);
  Route::get('selecionar-responsavel/{client_id?}/{resposavel_id?}', 'ClientController@getViewSelectResponsible');
  Route::post('selecionar-responsavel/', ['as' => 'clientes' ,'middleware' => ['auth','needsRoleOrPermission'],'is' => ['superuser'], 'shield' => 'client.edit', 'any' => true, 'uses' => 'ClientController@changeClientResponsible']);
  Route::get('meus-clientes', ['as' => 'clientes' ,'middleware' => ['auth','needsRoleOrPermission'], 'is' => ['consultores'], 'shield' => 'client.create', 'any' => true, 'uses' => 'ClientController@getMeusClientes']);
});

//Cliente -> Recursos
Route::resource('cliente', 'Crm\Cliente\ClienteController', ['only' => ['store','destroy']]);
Route::group(array('middleware' => 'auth', 'namespace' => 'Crm\Cliente', 'prefix' => 'cliente'), function()
{
  //Cliente
  Route::get('/novo-cliente', 'ClienteController@getNovoCliente');
  Route::get('/novo-lead', 'ClienteController@getNovoLead');

  //Resumo
  Route::resource('resumo', 'ResumoController');

  //Dados
  Route::resource('dados', 'DadosController');

  //Contatos
  Route::get('/contatos/{contato}/novo-contato', 'ContatosController@getNovoContato');
  Route::get('/contatos/lista-simples/{contato}', 'ContatosController@getListaSimples');
  Route::get('/contatos/mover/{cliente}/{contato}', 'ContatosController@getMoverContato');
  Route::put('/contatos/mover/{contato}', 'ContatosController@putMoverContato');
  Route::resource('contatos', 'ContatosController');

  //Formas de Contatos
  Route::resource('formas-de-contato', 'FormasContatoController', ['only' => ['store', 'destroy']]);

  //Produtos de um contato
  Route::resource('produtos', 'ProdutosContatoController', ['only' => ['store', 'destroy']]);

  //Historico de interações
  Route::resource('interacoes', 'InteracoesController');

  //Financeiro
  Route::put('/financeiro/dados-cobranca/{client_id?}', 'FinanceiroController@putDadosCobranca');
  Route::put('/financeiro/endereco-cobranca/{client_id?}', 'FinanceiroController@putEnderecoCobranca');
  Route::post('/financeiro/historico-cobranca/{client_id?}', 'FinanceiroController@postHistoricoCobranca');
  Route::resource('financeiro', 'FinanceiroController', ['only' => ['index', 'show']]);

  //Relevancia
  Route::resource('relevancia', 'RelevanciaController', ['only' => ['index', 'show']]);

});

//Rotas para interações via modals
Route::get('interacao/nova-interacao/{client_id?}/{father_id?}', 'Crm\ClientController@getEscolheInteracao');
Route::get('interacao/visualizar/{client_id?}/{interaction_id?}', 'Crm\ClientController@getViewInteracao');
Route::get('interacao/mover/{client_id?}/{interaction_id?}', 'Crm\ClientController@getMoveInteraction');
Route::put('interacao/mover/{client_id?}/{interaction_id?}', 'Crm\ClientController@putMoveInteraction');
Route::post('interacao/salvar-interacao/simples/{id?}', 'Crm\ClientController@postInteracaoSimples');

/*//Rotas de Importação
Route::group(['middleware' => ['auth', 'needsRoleOrPermission'],'is' => ['superuser'], 'shield' => 'import.index', 'any' => true], function () {
  Route::get('importar', 'Crm\ImportController@index');
  Route::post('importar/clientes', 'Crm\ImportController@postClientes');
  Route::post('importar/contatos', 'Crm\ImportController@postContatos');
  Route::post('importar/produtos', 'Crm\ImportController@postProdutos');
  Route::post('importar/relevancia', 'Crm\ImportController@postRelevancia');
  Route::post('importar/ultimas-interacoes', 'Crm\ImportController@postOrigemLead');
  Route::post('importar/login', 'Crm\ImportController@postLogin');
});

//Rotas de Exportação
Route::group(['middleware' => ['auth', 'needsPermission'], 'shield' => 'export.index', 'any' => true], function () {
  Route::get('exportar', 'Crm\ExportController@index');
  Route::get('exportar/clientes', 'Crm\ExportController@getClientes');
  Route::get('exportar/contatos', 'Crm\ExportController@getContatos');
  Route::get('exportar/produtos', 'Crm\ExportController@getProdutos');
  Route::get('exportar/pagamentos', 'Crm\ExportController@getPagamentos');
  Route::get('exportar/ultimas-interacoes', 'Crm\ExportController@getUltimasInteracoes');
});*/

Route::group(array('middleware' => ['auth','needsPermission'], 'shield' => 'import.index', 'namespace' => 'Crm\Database', 'prefix' => 'database'), function()
{
  Route::get('importar', 'ImportController@index');
  Route::post('importar/clientes', 'ImportController@postClientes');
  Route::post('importar/contatos', 'ImportController@postContatos');
  Route::post('importar/produtos', 'ImportController@postProdutos');
  Route::post('importar/relevancia', 'ImportController@postRelevancia');
  Route::post('importar/ultimas-interacoes', 'ImportController@postOrigemLead');
  Route::post('importar/login', 'ImportController@postLogin');

});

Route::group(array('middleware' => ['auth','needsPermission'], 'shield' => 'export.index', 'namespace' => 'Crm\Database', 'prefix' => 'database'), function()
{
  Route::get('exportar', 'ExportController@index');
  Route::get('exportar/clientes', 'ExportController@getClientes');
  Route::get('exportar/contatos', 'ExportController@getContatos');
  Route::post('exportar/leads', 'ExportController@postLeads');
  Route::get('exportar/produtos', 'ExportController@getProdutos');
  Route::post('exportar/financeiro', 'ExportController@postFinanceiro');
  Route::get('exportar/ultimas-interacoes', 'ExportController@getUltimasInteracoes');
});

Route::group(array('middleware' => ['auth','needsPermission'], 'shield' => 'configuracoes.index', 'namespace' => 'Crm\Configuracoes', 'prefix' => 'configuracoes'), function()
{
  //Usuários
  Route::resource('usuarios', 'UsuarioController');
  //Grupos
  Route::resource('grupos', 'GrupoController');
  //Permissões
  Route::resource('permissoes', 'PermissaoController');
  //Escritórios
  Route::resource('escritorios', 'EscritorioController');

});

// **************** Rotas Antigas **************** //

Route::group(['middleware' => ['auth'], 'prefix' => 'gestao'], function() {
  Route::controller('tipo_interacao', 'Crm\InteractionTypeController');

  // CALENDARIO

  // carrega view do calendario
  Route::get('calendario', ['as' => 'usuarios' ,'middleware' => ['auth','needsRoleOrPermission'],
  'is' => ['superuser'], 'shield' => 'calendario.index', 'any' => true,
  'uses' => 'Crm\CalendarioController@getIndex']);

  // carrega os contatos para exibir no calendario
  Route::get('getContatos', ['as' => 'getContatos', 'middleware' => ['auth','needsRoleOrPermission'],
  'is' => ['superuser'], 'shield' => 'calendario.index', 'any' => true, 'uses' => 'Crm\CalendarioController@getContatos']);


  /*
  * Estatísticas
  */
  Route::controller('estatisticas', 'Crm\StatisticsController');

  /*
  * Relatorio individual
  */

  Route::get('relatorios/individuais', ['as' => 'relatorios' ,'middleware' => ['auth','needsRoleOrPermission'],
  'is' => ['superuser'], 'shield' => 'user.relatorios', 'any' => true,
  'uses' => 'Crm\RelatorioController@getIndexIndividual']);

  Route::post('relatorios/individuais', ['as' => 'relatorios' ,'middleware' => ['auth','needsRoleOrPermission'],
  'is' => ['superuser'], 'shield' => 'user.relatorios', 'any' => true,
  'uses' => 'Crm\RelatorioController@postGerarIndividual']);

});

Route::group(['middleware' => ['auth', 'needsRoleOrPermission'],'is' => ['superuser'], 'shield' => 'user.relatorios', 'any' => true], function () {
  //Route::controller('relatorios', 'Crm\RelatorioController');
  Route::get('relatorios', 'Crm\RelatorioController@getIndex');
  Route::post('relatorios/gerar-total', 'Crm\RelatorioController@postGerar');
});

/**
*
* Módulo - Usuários
*
*/

Route::group(['prefix' => 'usuarios','middleware' => ['auth']], function() {

  Route::group(['middleware' => ['needsRoleOrPermission'],'is' => ['superuser'], 'shield' => 'user.index', 'any' => true], function () {
    Route::get('', 'Crm\UserController@getIndex');
  });

  Route::group(['middleware' => ['needsRoleOrPermission'],'is' => ['superuser'], 'shield' => 'user.create.all', 'any' => true], function () {
    Route::get('novo', 'Crm\UserController@getCreate');
    Route::post('salvar', 'Crm\UserController@postStore');
  });

  Route::group(['middleware' => ['needsRoleOrPermission'],'is' => ['superuser'], 'shield' => 'user.edit', 'any' => true], function () {
    Route::get('editar/{id}', 'Crm\UserController@getEdit');
    Route::post('atualizar/{id}', 'Crm\UserController@postUpdate');
    Route::get('deletar/{id}', 'Crm\UserController@getDeletar');
  });

  Route::group(['middleware' => ['needsRoleOrPermission'],'is' => ['superuser'], 'shield' => 'user.logs', 'any' => true], function () {
    Route::get('logs/{id}', 'Crm\UserController@getLogs');
    Route::get('estatisticas/{id}', 'Crm\UserController@getStatistics');
  });

});


/*
* Módulo - Produtos
*/

Route::group(['prefix' => 'produtos','middleware' => ['auth']], function() {

  Route::get('', ['as' => 'produtos' ,'middleware' => ['needsRoleOrPermission'],
  'is' => ['superuser'], 'shield' => 'product.index', 'any' => true,
  'uses' => 'Crm\ProductController@getIndex']);

  Route::group(['as' => 'produtos' ,'middleware' => ['needsRoleOrPermission'],
  'is' => ['superuser'], 'shield' => 'product.create', 'any' => true], function () {
    Route::get('novo', 'Crm\ProductController@getCreate');
    Route::post('salvar', 'Crm\ProductController@postSaveProduct');
    Route::get('lista', 'Crm\ProductController@getViewLists');
    Route::get('lista/novo', 'Crm\ProductController@getCreateList');
    Route::get('lista/editar/{id}', 'Crm\ProductController@getListEdit');
    Route::get('lista/excluir/{id}', 'Crm\ProductController@getListDelete');
    Route::get('lista/search', 'Crm\ProductController@getListSearch');
    Route::post('lista', 'Crm\ProductController@postCreateList');
  });

  Route::group(['as' => 'produtos' ,'middleware' => ['needsRoleOrPermission'],
  'is' => ['superuser'], 'shield' => 'product.edit', 'any' => true], function () {
    // {id?} - id condicional
    Route::get('editar/{id?}', 'Crm\ProductController@getEdit');
    Route::post('atualizar/{id}', 'Crm\ProductController@postUpdateProduct');
  });

  Route::get('excluir/{id}', ['as' => 'produtos' ,'middleware' => ['needsRoleOrPermission'],
  'is' => ['superuser'], 'shield' => 'product.destroy', 'any' => true,
  'uses' => 'Crm\ProductController@getExclude']);

});

Route::get('contatos/clientes/delete-contact-info/{id}', 'Crm\ContactClientController@deleteContactInfo');
Route::controller('contatos/clientes', 'Crm\ContactClientController');
