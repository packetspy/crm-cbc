<?php

namespace App\Exceptions;


use Artesaos\Defender\Handlers\ForbiddenHandler;
use Artesaos\Defender\Exceptions\ForbiddenException;
use Artesaos\Defender\Contracts\ForbiddenHandler as ForbiddenHandlerContract;

class DefenderHandler extends ForbiddenHandler
{
    public function handle()
    {
        if (\Request::ajax())
        {
            return response()->make('Acesso não liberado!');
        }
        return response()->view('errors.forbidden');
    }
}