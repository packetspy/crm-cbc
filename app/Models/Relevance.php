<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Relevance extends Model
{
    protected $table = 'relevance';
    protected $fillable = [
        'id_plataforma',
        'indicacoes',
        'contraindicacoes',
        'negocios',
        'data_referencia'
    ];

    public $exportFields = [
        'id_plataforma',
        'indicacoes',
        'contraindicacoes',
        'negocios',
        'data_referencia'
    ];

    protected $hidden = [];

    public function client()
    {
        return $this->belongsTo('App\Models\Client', 'id_plataforma');
    }

    /**
    * Mutators
    */
    public function getDataCriacaoAttribute()
    {
        $data = $this->attributes['data_referencia'];
        return date('d/m/Y', strtotime($data));
    }

}
