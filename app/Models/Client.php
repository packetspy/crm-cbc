<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'id',
        'documento',
        'ie',
        'tipo_documento',
        'tipoempresa_id',
        'nome',
        'empresa',
        'endereco',
        'complemento',
        'cidade',
        'estado',
        'cep',
        'segmento',
        'site',
        'user_id',
        'escritorio_id',
        'responsavel_id',
        'status_id',
        'origem_id',
        'id_plataforma',
        'datacadastro_plataforma'
    ];

    public $exportFields = [
        'clients.id',
        'status.status',
        'clients.status_id',
        'clients.documento',
        'clients.tipo_documento',
        'tipoempresa.tipo',
        'clients.tipoempresa_id',
        'clients.nome',
        'clients.empresa',
        'clients.segmento',
        'clients.site',
        'clients.user_id',
        'users.name as criadopor',
        'clients.responsavel_id',
        'user_responsavel.name as responsavel_nome',
        'clients.escritorio_id',
        'escritorios.nome as escritorio',
        'clients.origem_id',
        'origem.origem as origem',
        'clients.id_plataforma',
        'clients.created_at as criadoem',
    ];

    protected $hidden = [];

    //observe this model being deleted and delete the child events
    public static function boot ()
    {
        parent::boot();

        //Grava indice de registros que precisam ser atualizados no IBM WAtson
        self::updated(function (Client $client) {
            $updateIbm = new UpdateIbm();
            $updateIbm->create(['client_id'=>$client->id, 'contact_id'=>null]);
        });
        self::created(function (Client $client) {
            $updateIbm = new UpdateIbm();
            $updateIbm->create(['client_id'=>$client->id, 'contact_id'=>null]);
        });

        //Mapeia dados relacionados e executa delete
        self::deleting(function (Client $client) {
            foreach ($client->interactions as $interaction){
                $interaction->delete();
            }
        });
        self::deleting(function (Client $client) {
            foreach ($client->contacts as $contact){
                $contact->delete();
            }
        });
    }

    public function interactions()
    {
        return $this->hasMany('App\Models\Interaction')->withTrashed();
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id')->withTrashed();
    }

    public function responsavel()
    {
        return $this->belongsTo('App\Models\User','responsavel_id','id')->withTrashed();
    }

    public function escritorio()
    {
        return $this->belongsTo('App\Models\Escritorio');
    }

    public function contacts()
    {
        return $this->hasMany('App\Models\Contact');
    }

    public function status()
    {
        return $this->belongsTo('App\Models\Status');
    }

    public function origem()
    {
        return $this->belongsTo('App\Models\Origem');
    }


    public function getTipoPessoaAttribute()
    {
        $tipo = $this->attributes['tipo_documento'];
        return $tipo == 1 ? 'Pessoa Física' : 'Pessoa Jurídica';
    }

    public function getTipoPessoaShortAttribute()
    {
        $tipo = $this->attributes['tipo_documento'];
        return $tipo == 1 ? 'PF' : 'PJ';
    }

    public function getDocumentoAttribute()
    {
        return $this->attributes['documento'] == null ? '' : $this->attributes['documento'];
    }

    public function getDocumentocleanAttribute()
    {
        return preg_replace( '/[^0-9]/', '', $this->attributes['documento'] );
    }

    public function getDataCadastroPlataformaAttribute()
    {
        $data = $this->attributes['datacadastro_plataforma'];
        //if ($data == null ? $data = 'Não cadastrado' : $data = date('d/m/Y \à\s H:i', strtotime($data)) );
        if ($data == null ? $data = '' : $data = date('d/m/Y \à\s H:i', strtotime($data)) );
        return $data;
    }

}
