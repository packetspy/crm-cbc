<?php

namespace App\Models;

use App\Scopes\EmailScope;
use Illuminate\Database\Eloquent\Model;

class Responsavel extends Model
{
  use SoftDeletes;

  protected $table = 'users';

  public function clients()
  {
      return $this->hasOne('App\Models\Client','id','responsavel_id');
  }
}
