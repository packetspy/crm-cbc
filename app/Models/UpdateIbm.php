<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UpdateIbm extends Model
{
    protected $table = 'update_ibm';
    protected $fillable = [
        'client_id',
        'contact_id',
    ];
}
