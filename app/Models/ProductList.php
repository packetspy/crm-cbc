<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductList extends Model
{
    protected $fillable = [
        'id',
        'name'
    ];

    protected $hidden = [];


    public $products_id = [];

    public function products()
    {
        return $this->hasMany('App\Models\ProductsInList', 'list_id', 'id');
    }

    public function loadProductsIdInArray()
    {
        foreach($this->products as $conectionBetweenProduct) {
            $this->products_id[] = $conectionBetweenProduct->product_id;
        }
    }
}
