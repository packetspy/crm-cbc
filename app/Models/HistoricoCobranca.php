<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HistoricoCobranca extends Model
{
    protected $table = 'historico_cobranca';
    protected $fillable = [
        'amount',
        'free',
        'day',
        'observacao',
        'payment_type_id',
        'client_id',
        'user_id'
    ];

    public $exportFields = [
        'client_id as id_crm',
        'id_plataforma as id_plataforma',
        'clients.status_id',
        'clients.nome as cliente_nomefantasia',
        'clients.empresa as cliente_razaosocial',
        'amount as valor',
        'historico_cobranca.created_at',
        'description as observacao'
    ];

    protected $hidden = [];

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function client()
    {
        return $this->belongsTo('App\Models\Client', 'client_id');
    }

    public function payment_type()
    {
        return $this->belongsTo('App\Models\PaymentType', 'payment_type_id');
    }

    public function getTypeId()
    {
        return $this->payment_type->id;
    }

    public function getTipoInteracaoAttribute()
    {
        return $this->payment_type->type();
    }

    /**
    * Mutators
    */
    public function getAmountAttribute()
    {
        return 'R$ '.number_format($this->attributes['amount'], 2, ',' ,'.');
    }
    public function setAmountAttribute($value)
    {
        $value = str_replace("R$", "", $value);
        $value = str_replace(".", "", $value);
        $value = str_replace(",", ".", $value);
        $this->attributes['amount'] = $value;
    }

    public function getFreeAttribute()
    {
        $data = $this->attributes['free'];
        if($data){
          return 'Sim';
        }else{
          return 'Não';
        }
    }

    public function setDayAttribute($value)
    {
        $value = ltrim($value, "0");
        $this->attributes['day'] = $value;
    }

    public function getDataCriacaoAttribute()
    {
        $data = $this->attributes['created_at'];
        return date('d/m/Y \à\s H:i', strtotime($data));
    }
}
