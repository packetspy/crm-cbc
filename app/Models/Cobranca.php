<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cobranca extends Model
{
    protected $table = 'cobranca';
    protected $fillable = [
    ];

    public function client()
    {
        return $this->belongsTo('App\Models\Client');
    }

    public function status()
    {
        return $this->belongsTo('App\Models\Status');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    /**
    * Mutators
    */
    public function getValorAttribute()
    {
        return 'R$ '.number_format($this->attributes['valor'], 2, ',' ,'.');
    }
    public function setValorAttribute($value)
    {
        $value = str_replace("R$", "", $value);
        $value = str_replace(".", "", $value);
        $value = str_replace(",", ".", $value);
        $this->attributes['valor'] = $value;
    }

    public function setVencimentoAttribute($value)
    {
        $value = ltrim($value, "0");
        $this->attributes['vencimento'] = $value;
    }

    public function getDataCriacaoAttribute()
    {
        $data = $this->attributes['created_at'];
        return date('d/m/Y \à\s H:i', strtotime($data));
    }
}
