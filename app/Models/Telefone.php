<?php

namespace App\Models;

use App\Scopes\TelefoneScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Telefone extends Model
{
    use SoftDeletes;
    protected $table = 'contact_infos';
    protected $fillable = [
      'contato',
      'tipo',
      'contact_id',
      'created_at',
      'updated_at'
    ];

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new TelefoneScope);

        //Grava indice de registros que precisam ser atualizados no IBM WAtson
        self::created(function (Telefone $telefone) {
            $updateIbm = new UpdateIbm();
            $updateIbm->create(['client_id'=>null, 'contact_id'=>$telefone->contact_id]);
        });
    }

    public function contact()
    {
        return $this->hasOne('App\Models\Contact');
    }

    public function getContatoAttribute()
    {
      if (ctype_digit($this->attributes['contato']) ? $value = self::mascara_telelefone($this->attributes['contato']) : $value = $this->attributes['contato']);
      return $value;
    }

    //Arrumar um lugar melhor para colocar isso
    static public function mascara_telelefone($TEL) {
      $tam = strlen(preg_replace("/[^0-9]/", "", $TEL));
        if ($tam == 13) { // COM CÓDIGO DE ÁREA NACIONAL E DO PAIS e 9 dígitos
        return "+".substr($TEL,0,$tam-11)."(".substr($TEL,$tam-11,2).") ".substr($TEL,$tam-9,5)."-".substr($TEL,-4);
        }
        if ($tam == 12) { // COM CÓDIGO DE ÁREA NACIONAL E DO PAIS
        return "+".substr($TEL,0,$tam-10)."(".substr($TEL,$tam-10,2).") ".substr($TEL,$tam-8,4)."-".substr($TEL,-4);
        }
        if ($tam == 11) { // COM CÓDIGO DE ÁREA NACIONAL e 9 dígitos
        return "(".substr($TEL,0,2).") ".substr($TEL,2,5)."-".substr($TEL,7,11);
        }
        if ($tam == 10) { // COM CÓDIGO DE ÁREA NACIONAL
        return "(".substr($TEL,0,2).") ".substr($TEL,2,4)."-".substr($TEL,6,10);
        }
        if ($tam <= 9) { // SEM CÓDIGO DE ÁREA
        return substr($TEL,0,$tam-4)."-".substr($TEL,-4);
        }
    }
}
