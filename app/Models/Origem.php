<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Origem extends Model
{
    protected $table = 'origem';

    public function client()
    {
        return $this->hasMany('App\Models\Client');
    }
    public function contact()
    {
        return $this->hasMany('App\Models\Contact');
    }
}
