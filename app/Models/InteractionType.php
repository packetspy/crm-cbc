<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InteractionType extends Model
{
    protected $fillable = [
        'exibir_formulario',
        'ativo',
        'user_id',
        'nome',
        'father_id',
        'observacao_obrigatoria',
        'funcao',
        'exibe_apenas_contraparte'
    ];

    protected $hidden = [];

    public function nome()
    {
        if($this->father_id == null)
            return $this->nome;

        return $this->father()->first()->nome() . "/" . $this->nome;
    }

    public function countChildrens()
    {
        $this->fillable[]="childrens_number";
        $this->childrens_number = $this->childrens()->getResults()->count();
    }

    public function nomePai()
    {
        if($this->father_id == null)
            return '';

        return $this->father()->first()->nome();        
    }

    public function interactions()
    {
        return $this->hasMany('App\Models\Interaction', 'interaction_type_id');
    }

    public function interaction()
    {
        return $this->belongsTo('App\Models\Interaction');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function father()
    {
        return $this->belongsTo('App\Models\InteractionType', 'father_id');
    }

    public function childrens()
    {
        return $this->hasMany('App\Models\InteractionType', 'father_id', 'id');
    }
}
