<?php

namespace App\Models;

use App\Scopes\SkypeScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Skype extends Model
{
    use SoftDeletes;
    protected $table = 'contact_infos';
    protected $fillable = [
      'contato',
      'tipo',
      'contact_id',
      'created_at',
      'updated_at'
    ];

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new SkypeScope);

        //Grava indice de registros que precisam ser atualizados no IBM WAtson
        self::created(function (Skype $skype) {
            $updateIbm = new UpdateIbm();
            $updateIbm->create(['client_id'=>null, 'contact_id'=>$skype->contact_id]);
        });
    }

    public function contact()
    {
        return $this->hasOne('App\Models\Contact');
    }
}
