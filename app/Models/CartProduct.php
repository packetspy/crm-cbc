<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CartProduct extends Model
{
    protected $fillable = [
        'quantidade',
        'product_id'
    ];

    protected $hidden = [];

    public function cart()
    {
        return $this->hasOne('App\Models\Cart');
    }

    public function product()
    {
        return $this->belongsTo('App\Models\Product', 'product_id');
    }

    public function getValorTotalAttribute()
    {
        $produto = $this->product;
        $qtd = $this->attributes['quantidade'];
        $valor = $produto->valor * $qtd;
        return $valor;
    }
}
