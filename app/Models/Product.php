<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'id',
        'nome',
        'descricao',
        'valor',
        'ativo',
        'id_plataforma'
    ];

    public $exportFields = [
        'id',
        'nome',
        'descricao',
        'valor',
        'ativo',
        'id_plataforma'
    ];

    protected $hidden = [];

    public function request_product()
    {
        return $this->hasMany('App\Models\CartProduct');
    }

    public function intermediate_contact()
    {
        return $this->hasMany('App\Models\ContactProduct');
    }

}
