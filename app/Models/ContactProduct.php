<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContactProduct extends Model
{
    protected $fillable = [
        'id',
        'estado',
        'transacao',
        'product_id',
        'contact_id'
    ];

    public $exportFields = [
        'id',
        'nome',
        'descricao'
    ];

    protected $hidden = [];

    public function product()
    {
        return $this->belongsTo('App\Models\Product');
    }

    public function contact()
    {
        return $this->belongsTo('App\Models\Contact');
    }

    public function getTipoTransacaoAttribute()
    {
        $transaction = $this->attributes['transacao'];
        switch($transaction)
        {
            case 'compra_venda':
                return 'Compra & Venda';
            default:
                return ucfirst($transaction);
        }
    }


}
