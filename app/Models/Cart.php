<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $fillable = ['interaction_id'];

    protected $hidden = [];

    public function interaction()
    {
        return $this->belongsTo('App\Models\Interaction');
    }

    public function cart_products()
    {
        return $this->hasMany('App\Models\CartProduct');
    }


}
