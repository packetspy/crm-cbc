<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Interaction extends Model
{
    protected $fillable = [
        'observacao',
        'data_contato',
        'user_id',
        'client_id',
        'interaction_type_id',
        'father_id'
    ];

    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $hidden = [];

    public function author()
    {
        return $this->belongsTo('App\Models\User', 'user_id')->withTrashed();
    }

    public function client()
    {
        return $this->belongsTo('App\Models\Client', 'client_id');
    }

    public function interaction_type()
    {
        return $this->belongsTo('App\Models\InteractionType', 'interaction_type_id');
    }

    public function father()
    {
        return $this->belongsTo('App\Models\Interaction', 'father_id');
    }

    public function children()
    {
        return $this->hasOne('App\Models\Interaction', 'father_id');
    }

    public function cart()
    {
        return $this->hasOne('App\Models\Cart');
    }

    public function getDataCriacaoAttribute()
    {
        $data = $this->attributes['created_at'];
        return date('d/m/Y \à\s H:i', strtotime($data));
    }

    public function dataFormatada()
    {
        return date('d/m/Y H:i', strtotime($this->data_contato));
    }

    public function getTituloAttribute()
    {
        $tipo = $this->interaction_type->id;
        $data = $this->attributes['data_contato'];
        if($data == null)
            return $this->inicioObservacao();
      //  if ($tipo == 1)
       //     return 'Entrar em contato em ' . $this->dataFormatada();

        return 'Data: ' . $this->dataFormatada() . " - ". $this->inicioObservacao();
    }

    public function inicioObservacao()
    {
        return $this->observacao;
    }

    public function getTipoId()
    {
        return $this->interaction_type->id;
    }


    public function getTipoInteracaoClassAttribute()
    {
        switch($this->interaction_type->id){
            case 1:
                return 'label-default';
                break;
            case 2:
                return 'label-info';
                break;
            default:
                return 'label-warning';
                break;
        }
    }

    public function getTipoInteracaoAttribute()
    {
        return $this->interaction_type->nome();
    }

    public function getColorAttribute()
    {
      if(!is_null($this->children)){
        return $this->attributes['color'] = "#2ecc71";
      }
    }
}
