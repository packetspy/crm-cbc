<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductsInList extends Model
{
    protected $table = "list_products";

    protected $fillable = [
        'product_id',
        'list_id',
    ];

    protected $hidden = [];

    public function list_belong()
    {
        return $this->belongsTo('App\Models\ProductList', 'list_id');
    }

    public function product()
    {
        return $this->belongsTo('App\Models\Product', 'product_id');
    }
}
