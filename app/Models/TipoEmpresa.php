<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoEmpresa extends Model
{
    protected $table = 'tipoempresa';

    public function client()
    {
        return $this->hasOne('App\Models\Client');
    }
}
