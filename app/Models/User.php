<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Artesaos\Defender\Traits\HasDefender;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasDefender;
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'fname',
        'lname',
        'email',
        'password',
        'escritorio_id',
        'remember_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function clients()
    {
        return $this->hasMany('App\Models\Client');
    }

    public function interactions()
    {
        return $this->hasMany('App\Models\Interaction');
    }

    public function products()
    {
        return $this->hasMany('App\Models\Product');
    }

    public function interaction_types()
    {
        return $this->hasMany('App\Models\InteractionType');
    }

    public function contacts()
    {
        return $this->hasMany('App\Models\Contact');
    }

    public function escritorio()
    {
        return $this->belongsTo('App\Models\Escritorio');
    }

    public function getArrayPermissions()
    {
        $permissions = $this->getAllPermissions();
        $return = [];
        foreach ($permissions as $permission)
            $return[ $permission->name ] = '1';

        return $return;
    }

    public function isAdmin(){
      $this->middleware('needsRole:Atendimento');
      return (\Auth::check() && $this->role == 1);
    }

}
