<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HistoricoLead extends Model
{
    protected $table = 'historico_lead';
    protected $fillable = [
      'client_id',
      'user_id',
      'responsavel_id',
      'escritorio_id',
      'status_id',
      'origem_id',
      'created_at'
    ];

    public $exportFields = [
        'client_id as crm_id',
        'clients.nome as cliente_nome',
        'clients.empresa as cliente_empresa',
        'users.name as criadopor',
        'user_responsavel.name as responsavel',
        'escritorios.nome as escritorio',
        'status.status as status_original',
        'origem.origem as origem',
    ];

}
