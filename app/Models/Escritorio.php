<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Escritorio extends Model
{
    protected $table = 'escritorios';

    public function clients()
    {
        return $this->hasMany('App\Models\Client');
    }

    public function user()
    {
        return $this->hasOne('App\Models\User');
    }
}
