<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\UpdateIbm;

class Contact extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'nome',
        'departamento',
        'user_id',
        'escritorio_id',
        'client_id',
        'origem_id',
        'id',
        'id_plataforma'
    ];

    public $exportFields = [
        'client_id as cliente_id',
        'clients.id_plataforma as empresa_plataforma_id',
        'clients.status_id as status_id',
        'status.status as status_empresa',
        'escritorios.nome as escritorio',
        'user_responsavel.id as responsavel_id',
        'user_responsavel.name as responsavel_nome',
        'clients.origem_id as origem_id',
        'origem.origem as origem',
        'clients.nome as nomefantasia',
        'clients.empresa as razaosocial',
        'clients.tipo_documento as tipo_documento',
        'clients.documento as documento',
        'clients.ie as ie',
        'clients.tipoempresa_id as tipoempresa_id',
        'tipoempresa.tipo as tipo_empresa',
        'clients.cidade as cidade',
        'clients.estado as estado',
        'contacts.id as contato_id',
        'contacts.id_plataforma as contato_plataforma_id',
        'contacts.nome as nome',
        'contacts.departamento',
        'e1.contato as Email',
        'e2.contato as email2',
        'e3.contato as email3',
        't1.contato as telefone1',
        't2.contato as telefone2',
        't3.contato as telefone3',
        'historico_login.data_acesso as data_ultimo_login'
    ];


    protected $hidden = [];

    public static function boot()
    {
        parent::boot();

        //Grava indice de registros que precisam ser atualizados no IBM WAtson
        self::updated(function (Contact $contact) {
            $updateIbm = new UpdateIbm();
            $updateIbm->create(['client_id'=>null, 'contact_id'=>$contact->id]);
        });
        self::created(function (Contact $contact) {
            $updateIbm = new UpdateIbm();
            $updateIbm->create(['client_id'=>null, 'contact_id'=>$contact->id]);
        });

        //Mapeia dados relacionados e executa delete
        self::deleting(function (Contact $contact) {
            foreach ($contact->intermediate_products as $products){
                $products->delete();
            }
        });
        self::deleting(function (Contact $contact) {
            foreach ($contact->email as $email){
                $email->delete();
            }
        });
        self::deleting(function (Contact $contact) {
            foreach ($contact->telefone as $telefone){
                $telefone->delete();
            }
        });
        self::deleting(function (Contact $contact) {
            foreach ($contact->skype as $skype){
                $skype->delete();
            }
        });
    }

    public function author()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function client()
    {
        return $this->belongsTo('App\Models\Client');
    }

    public function origem()
    {
        return $this->belongsTo('App\Models\Origem');
    }

    public function intermediate_products()
    {
        return $this->hasMany('App\Models\ContactProduct');
    }

    public function intermediate_products_group()
    {
        return $this->hasMany('App\Models\ContactProduct');
    }

    public function contact_infos()
    {
        return $this->hasMany('App\Models\ContactInfo');
    }

    public function email()
    {
        return $this->hasMany('App\Models\Email');
    }

    public function telefone()
    {
        return $this->hasMany('App\Models\Telefone');
    }

    public function skype()
    {
        return $this->hasMany('App\Models\Skype');
    }

    public function getDataCriacaoAttribute()
    {
        $data = $this->attributes['created_at'];
        return date('d/m/Y H:i', strtotime($data));
    }
}
