<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Services\IbmService;


class UpdateIbm extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $cliente_id;
    protected $contato_id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($cliente_id, $contato_id)
    {
        $this->cliente_id = $cliente_id;
        $this->contato_id = $contato_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(IbmService $ibm)
    {
        $ibm->singleUpdateIBM($this->cliente_id, $this->contato_id);
    }
}
