<?php

namespace App\Helpers;

Class FormaPagamento
{
    protected static $formapagamento = [
        "1"=>"Boleto",
        "2"=>"Cartão de Crédito",
        "3"=>"Paypal",
        "4"=>"Depósito/Transferência",
    ];

    public static function getAll()
    {
        return self::$formapagamento;
    }

    public static function getKeys()
    {
        return array_keys(self::$formapagamento);
    }
}
