<?php

namespace App\Helpers;

use Exception;
use App\Helpers\ArrayToXML;

/**
 * SilverpopAPI Class for PHP.
 *
 */

// Set this to TRUE to enable printing of debug messages
DEFINE('SP_PRINT_DEBUG_MESSAGES', FALSE);

// Set TRUE to have curl verify HTTPS certificates. This works fine, but you may
//	have to set up root certificates for curl.
DEFINE('SP_API_VERIFY_SSL_CERT', FALSE);

class SilverpopException extends Exception { }
class SilverpopNoUserException extends SilverpopException { }
class SilverpopUserExistsException extends SilverpopException { }

class SilverpopAPI {
	public $api_url = 'https://api8.silverpop.com/XMLAPI';
	//public $http_timeout = 20; // in seconds
	public $http_timeout = 0; // never timeout
	public $sessionid; // contains current session ID if logged in (i.e. after call to login())
	public $last_request;  // XML string of the most recent request.

	function login($username,$password) {
		$data = array('USERNAME' => $username, 'PASSWORD' => $password);
		$response = $this->call_api('Login',$data);
		$this->sessionid = (string)$response->SESSIONID;
		return $response;
	}

	function logout(){
		$response = $this->call_api('Logout');
		$this->sessionid = NULL;
		return $response;
	}

	function add_recipient($list_id, $lead = array(), $contact_lists = array(), $visitor_key, $created_from = 2, $send_autoreply = true,
									$update_if_found = true, $sync_fields = true, $allow_html = true, $opt_back_in = true) {

		// TODO: sanity check list_id and other parameters

		// translate parameter options to "true"/"false" strings
		$update_if_found = $update_if_found ? "true" : "false";
		$sync_fields = $sync_fields ? "true" : "false";
		$send_autoreply = $send_autoreply ? "true" : "false";
		$allow_html = $allow_html ? "true" : "false";

		if($opt_back_in) {
			$lead['OPT_OUT'] = 'false';
		}

		$data = array('LIST_ID'=> $list_id, 'CREATED_FROM'=> $created_from, 'SEND_AUTOREPLY' => $send_autoreply,
		'UPDATE_IF_FOUND'=> $update_if_found, 'SYNC_FIELDS'=> $sync_fields, 'ALLOW_HTML'=> $allow_html, 'VISITOR_KEY'=> $visitor_key);

		// now build the lead into a silly "column" format
		$data['COLUMN'] = array();
		foreach($lead as $key => $value) {
			$data['COLUMN'][] = array('NAME'=> $key, 'VALUE' => $value);
		}
		//if we want to add contact to a CL...
		$data['CONTACT_LISTS'] = array();
		foreach($contact_lists as $key => $value) {
			$data['CONTACT_LISTS'][] = array('CONTACT_LIST_ID'=> $value);
		}

		$data['SYNC_FIELDS'] = array();
			$data['SYNC_FIELDS']['SYNC_FIELD'] = array('NAME'=>'contato_id', 'VALUE'=>$lead['contato_id']);
			//$data['SYNC_FIELDS']['SYNC_FIELD'] = array('NAME'=>'Email', 'VALUE'=>$lead['email']);

		$response = $this->call_api('AddRecipient',$data);

		$data = array();
		$data['recipient_id'] = (string)$response->RecipientId;

		return $data;
	}

	function select_recipient($list_id, $recipient_email) {

		$xml = '<?xml version="1.0" encoding="UTF-8"?>
		<Envelope>
		<Body>
		<SelectRecipientData>
		<LIST_ID>'.$list_id.'</LIST_ID>
		<EMAIL>'.$recipient_email.'</EMAIL>
		<SYNC_FIELDS>
		</SelectRecipientData>
		</Body>
		</Envelope>';

		$response = $this->call_api_ready($xml);

		$data = array();
		$data['recipient_id'] = (string)$response->RecipientId;

		return $data;

	}

	function add_recipient_contact_list($contact_list,$recipient_id) {

		$xml = '<?xml version="1.0" encoding="UTF-8"?>
		<Envelope>
		<Body>
		<AddContactToContactList>
		<CONTACT_LIST_ID>'.$contact_list.'</CONTACT_LIST_ID>
		<CONTACT_ID>'.$recipient_id.'</CONTACT_ID>
		</AddContactToContactList>
		</Body>
		</Envelope>';
		//echo $xml;
		$response = $this->call_api_ready($xml);

	}

	function add_recipient_program($program_id, $recipient_id) {

		$xml = '<?xml version="1.0" encoding="UTF-8"?>
		<Envelope>
		<Body>
		<AddContactToProgram>
		<PROGRAM_ID>'.$program_id.'</PROGRAM_ID>
		<CONTACT_ID>'.$recipient_id.'</CONTACT_ID>
		</AddContactToProgram>
		</Body>
		</Envelope>';

		$response = $this->call_api_ready($xml);

		return $response;

	}

	function send_mailing($mailing_id, $recipient_email){

		$xml = '<?xml version="1.0" encoding="UTF-8"?>
		<Envelope>
		<Body>
		<SendMailing>
		<MailingId>'.$mailing_id.'</MailingId>
		<RecipientEmail>'.$recipient_email.'</RecipientEmail>
		</SendMailing>
		</Body>
		</Envelope>';

		$response = $this->call_api_ready($xml);

		return $response;

	}

	public function update_relational_table($relationaltable_id, $params) {

		$xml = '<?xml version="1.0" encoding="UTF-8"?>
		<Envelope>
		<Body>
		<InsertUpdateRelationalTable>
		<TABLE_ID>'.$relationaltable_id.'</TABLE_ID>
		<ROWS>
		<ROW>
		'.$params.'
		</ROW>
		</ROWS>
		</InsertUpdateRelationalTable>
		</Body>
		</Envelope>';


		$response = $this->call_api_ready($xml);

		return $response;

	}

	/**
	 * ReplaceDCRuleset API call
	 */
	public function replace_DC_Ruleset($xml) {

		$response = $this->call_api_ready($xml);

		$data = array();
		$data['ruleset_id'] = (string)$response->RULESET_ID;

		return $data;

	}

	/**
	 * ScheduleMailing API call
	 */
	public function schedule_mailing($xml) {

		$response = $this->call_api_ready($xml);

		$data = array();
		$data['mailing_id'] = (string)$response->MAILING_ID;

		return $data;

	}


	/**
	 * call_api()  takes API function name and array of parameters, turns it into formatted
	 * XML and then actually calls the HTTP API (using call_api_xml).
	 */
	protected function call_api($function_name, $parameters = array()) {

		// ArrayToXML converts array of arrays to string of XML. See xmltools.php.
		$xml_data_string = ArrayToXML::toXML(array('Body' => array($function_name => $parameters )), 'Envelope');

		// call_api_xml() actually talks to SP server.
		$response_payload = $this->call_api_xml($xml_data_string);

		// return XML data enclosed in response
		return $response_payload;
	}

	/**
	 * call_api_ready()  takes API function name and array of parameters, turns it into formatted
	 * XML and then actually calls the HTTP API (using call_api_xml).
	 */
	protected function call_api_ready($xml) {

		// Received XML
		$xml_data_string = $xml;

		// call_api_xml() actually talks to SP server.
		$response_payload = $this->call_api_xml($xml_data_string);

		// return XML data enclosed in response
		return $response_payload;
	}

	/**
	 * Takes an already formatted XML string and calls Silverpop API. Also used
	 * directly by SilverpopQueue
	 *
	 */
	public function call_api_xml($xml_data_string) {
		$post_variables = array();

		$url = $this->api_url;
		if($this->sessionid) {
		  $url .= ';jsessionid='.$this->sessionid;
		}

		$this->last_request = $xml_data_string;
		$post_variables['xml'] = $xml_data_string;

		if(SP_PRINT_DEBUG_MESSAGES)
			var_dump($post_variables);
		$result = $this->http_post_form($url,$post_variables);
		if(SP_PRINT_DEBUG_MESSAGES)
			print htmlentities($result);
		$xml = simplexml_load_string($result);
		$response_payload = $xml->Body->RESULT;
		$success = in_array(strtolower($response_payload->SUCCESS), array('true','success','yes'));
		if(SP_PRINT_DEBUG_MESSAGES)
		if(!$success) {
			$error_message = @(string)$xml->Body->Fault->FaultString;
			$error_num = @(int)$xml->Body->Fault->detail->error->errorid;
			if((!$error_message) && (!$error_num)) {
				throw new SilverpopException("Unknown error");
			} else {
				if(($error_num == 128) || ($error_num == 155))
					throw new SilverpopNoUserException($error_message,$error_num);
				elseif($error_num == 122)
					throw new SilverpopUserExistsException($error_message,$error_num);
				//Errores de programas
				//651: Contacto agregado anteriormente
				//653: programa terminado
				elseif ($error_num == 651 || $error_num==653)
					return $error_num;
					//throw new SilverpopException($error_message,$error_num);
				else
					throw new SilverpopException($error_message,$error_num);
			}

		}
		return $response_payload;
	}

	/**
	 * POST data to an HTTP server. Requires the PHP CuRL extension.
	 * $data is an array of data like array('key'=>'value')
	 */
	protected function http_post_form($url,$data = array()) {
		if(!function_exists("curl_exec")) {
			trigger_error("ERROR: PHP/cURL is not installed on this server");
		}

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$url); // set url to post to
		curl_setopt($ch, CURLOPT_FAILONERROR, 1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); // allow redirects
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1); // return into a variable
		curl_setopt($ch, CURLOPT_TIMEOUT, $this->http_timeout); // times out after $timeout secs
		curl_setopt($ch, CURLOPT_POST, 1); // set POST method

		// uncomment following two lines to skip SSL certificate validation
		if(!SP_API_VERIFY_SSL_CERT) {
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		}

		// Translate $data hash into form encoded string (e.g. "param1=val&param2=val&param3=val")
		$values = array();
		foreach($data as $key=>$value)
		$values[]="$key=".urlencode($value);
		$data_string=implode("&",$values);

		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string); // add POST fields
		$result = curl_exec($ch); // run the whole process
		if($result === false) {
			throw New SilverpopException("CURL Error: ".curl_error($ch) . " " . curl_errno($ch));
		}
		curl_close($ch);
		return $result;
	}

}

?>
