<?php

  /*
  |--------------------------------------------------------------------------
  | Detect Active Route
  |--------------------------------------------------------------------------
  |
  | Compare given route with current route and return output if they match.
  | Very useful for navigation, marking if the link is active.
  |
  */
  function isActiveRoute($route, $output = "active")
  {
      if (Route::current()->getUri() == $route) return $output;
  }

  /*
  |--------------------------------------------------------------------------
  | Detect Active Routes
  |--------------------------------------------------------------------------
  |
  | Compare given routes with current route and return output if they match.
  | Very useful for navigation, marking if the link is active.
  |
  */
  function areActiveRoutes(Array $routes, $output = "active")
  {
      foreach ($routes as $route)
      {
          if (Route::current()->getUri() == $route) return $output;
      }
  }

  /*
  |--------------------------------------------------------------------------
  | Verifica se está dentro de um cliente
  |--------------------------------------------------------------------------
  |
  | Faz verificação para checar se o consultor é responsavel pelo cliente.
  |
  */
  function isOnCliente()
  {
      if (Request::segment(3)) {
        return true;
      }else{
        return false;
      }
  }

  /*
  |--------------------------------------------------------------------------
  | Verifica se é o responsavel
  |--------------------------------------------------------------------------
  |
  | Faz verificação para checar se o consultor é responsavel pelo cliente.
  |
  */
  function isResponsavel($responsavel_id)
  {
      if ($responsavel_id == Auth::user()->id) {
        return true;
      }else{
        return false;
      }
  }

  /*
  |--------------------------------------------------------------------------
  | Verifica quantidade de dias sem login, formata data e aplica cor
  |--------------------------------------------------------------------------
  |
  */
  function checkLoginTime($login)
  {
    if (empty($login)) {
      $label = '<span class="label label-login animated infinite bounce label-danger">Nunca logou!</span>';
      return $label;
    }

    $data = Carbon\Carbon::parse($login);
    $data_formatada = $data->format('d/m/Y \à\s H:i');
    $now = Carbon\Carbon::now();
    $dias = $now->diffInDays($data);
    $color = '';

    switch (true) {
      case ($dias < 30):
      //Tá Ok
      $color = 'label-success';
      break;

      case ($dias < 60):
      //Foco no cliente
      $color = 'label-primary';
      break;

      case ($dias < 90):
      //Atenção!!
      $color = 'label-warning';
      break;

      case ($dias >= 90):
      //Fudeu, abandonaram o cliente =(
      $color = 'label-danger';
      break;
    }

    $dias_data = '('.$dias.' dias) '.$data_formatada;
    $label = '<span class="label label-login animated infinite bounce '.$color.'">'.$dias_data.'</span>';
    return $label;
  }
