<?php

namespace App\Helpers;

Class TipoDocumento
{
    protected static $tipodocumento = [
        "1"=>"CPF",
        "2"=>"CNPJ",
    ];

    public static function getAll()
    {
        return self::$tipodocumento;
    }

    public static function getKeys()
    {
        return array_keys(self::$tipodocumento);
    }
}
