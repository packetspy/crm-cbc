<?php

namespace App\Helpers;


class InteractionType
{
    protected static $negocioFechado=[6];
    protected static $negocioNaoFechado=[8,9,10,11];
    protected static $funcoes=[1,2,3,4,12,13];
    protected static $acharContraparte = [];

    public static function contraparte()
    {
        return array_merge(self::$negocioFechado, self::$negocioNaoFechado);
    }

    public static function negocioFechado()
    {
        return self::$negocioFechado;
    }

    public static function negocioNaoFechado()
    {
        return self::$negocioNaoFechado;
    }

    public static function funcoes()
    {
        return self::$funcoes;
    }
}
