<?php

namespace App\Helpers;

Class Recorrencia
{
    protected static $recorrencia = [
        "1"=>"Mensal",
        "2"=>"Trimestral",
        "3"=>"Semestral",
        "4"=>"Anual",
    ];

    public static function getAll()
    {
        return self::$recorrencia;
    }

    public static function getKeys()
    {
        return array_keys(self::$recorrencia);
    }
}
