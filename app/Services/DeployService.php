<?php
namespace App\Services;

use App\Services\Service;

use App\Models\User;
use App\Models\Client;
use App\Models\Role;
use App\Models\Status;
use App\Models\Contact;
use App\Models\ContactInfo;


Class DeployService extends Service
{
    protected $rules = [
        '' => ''
    ];

    public function __construct()
    {
      //
    }

    /**
     * Executa atualizaçao de ambiente.
     * Deve ser executada apena uma vez na implementação.
     * @param null
     * @return messagem de sucesso/erro.
     */
    public static function UpdateRolesAndStatus()
    {
        $message = null;
        // ***** Adiciona nova role se não existe *****
        $role = Role::where('name','=','Gestores')->first();
        if(!$role){
          $newrole = new Role();
          $newrole->name = 'Gestores';
          $newrole->save;
          $message .= 'Role Adicionada. \n ';
        }
        $message .= 'Role já existe. \n ';

        // ***** Adiciona role a usuários especificados *****
        $role = Role::where('name','=','Gestores')->first();

        //Anderson
        User::find(3)->roles()->attach($role->id);
        //Eder
        User::find(4)->roles()->attach($role->id);
        //Kelvin
        User::find(10)->roles()->attach($role->id);
        $message .= 'Usuários associados a role! \n';

        // ***** Adiciona Status a tabela *****
        $novos_status = [];
        $novos_status[] = 'Sem Perfil';
        $novos_status[] = 'Em prospecção';
        $novos_status[] = 'Lead frio';
        $novos_status[] = 'Relevante';
        $novos_status[] = 'Meta Relevante';
        $novos_status[] = 'Desabilitado Pagamento';
        $novos_status[] = 'Não tem interesse';
        $novos_status[] = 'Ativo/Cadastrado';

        foreach ($novos_status as $status) {
          $existe = Status::where('status','=',$status)->first();
          if(!$existe){
            $st = new Status();
            $st->status = $status;
            $st->save();
          }
        }
        $message .= 'Novos status Adicionados! \n';

        return $message;
    }

    /**
     * Executa atualizaçao de ambiente.
     * Deve ser executada apena uma vez na implementação.
     * @param null
     * @return messagem de sucesso/erro.
     */
    public static function UpdateClientStatus()
    {
        $message = null;

        // ***** Atualiza status_id (1) de Clientes Ativos *****
        $clientes_ativos = Client::where('ativo', '=', '1')->get();
        foreach ($clientes_ativos as $cliente) {
          $cliente_selecionado = Client::find($cliente->id);
          if($cliente_selecionado->ativo == 1){
            $cliente_selecionado->status_id = 8;
            $cliente_selecionado->save();
          }
        }

        // ***** Atualiza status_id (1) de Clientes Ativos *****
        $clientes_inativos = Client::where('ativo', '=', '0')->get();
        foreach ($clientes_inativos as $cliente) {
          $cliente_selecionado = Client::find($cliente->id);
          if($cliente_selecionado->ativo == 0){
            $cliente_selecionado->status_id = 2;
            $cliente_selecionado->save();
          }
        }

        // ***** Atualiza status_id (0) de Pre-Cadastro *****
        $clientes_pre = Client::where('empresa', 'LIKE', '(Pré)%')->get();
        foreach ($clientes_pre as $cliente) {
          $cliente_selecionado = Client::find($cliente->id);
            $cliente_selecionado->status_id = 2;
            $cliente_selecionado->save();
        }

        $message = 'Status atualizado com sucesso!';
        return $message;
    }

    public static function migrateContactJsonToTable()
    {
        $contacts = Contact::all();

        foreach ($contacts as $contact) {
            self::migrateContactValue($contact);
        }
    }

    public static function migrateContactValue($contact)
    {
        $contactsInJson = json_decode($contact->contatos, true);
        foreach($contactsInJson as $oneContactToMigrate) {
            $exist = ContactInfo::where('contact_id', $contact->id)
                                ->where('tipo', $oneContactToMigrate['tipo'])
                                ->where('contato', $oneContactToMigrate['contato'])
                                ->exists();
            if ($exist) {
              //echo "\nJá existe\n";
                continue;
            }
            //echo "\nRecem criado\n";
            $contact->contact_infos()->create($oneContactToMigrate);
        }
    }

    public static function limpaInvalidos()
    {
        //Limpa emails
          \DB::table('clients')->where('email', 0)->update(['email' => null]);
          \DB::table('clients')->where('email', '0')->update(['email' => null]);
          \DB::table('clients')->where('email', '')->update(['email' => null]);
          \DB::table('clients')->where('email', 'eu@eu.com')->update(['email' => null]);

        //Limpa telefones
          \DB::table('clients')->where('telefone', 0)->update(['telefone' => null]);
          \DB::table('clients')->where('telefone', '0')->update(['telefone' => null]);
          \DB::table('clients')->where('telefone', '')->update(['telefone' => null]);
          \DB::table('clients')->where('telefone', '(11) 9999-9999')->update(['telefone' => null]);
          \DB::table('clients')->where('telefone', '(99) 9999-9999')->update(['telefone' => null]);
          \DB::table('clients')->where('telefone', '(11) 99999-9999')->update(['telefone' => null]);
          \DB::table('clients')->where('telefone', '(99) 99999-9999')->update(['telefone' => null]);
          \DB::table('clients')->where('telefone', '(11)9999-9999')->update(['telefone' => null]);
          \DB::table('clients')->where('telefone', '(99)9999-9999')->update(['telefone' => null]);
          \DB::table('clients')->where('telefone', '(11)99999-9999')->update(['telefone' => null]);
          \DB::table('clients')->where('telefone', '(99)99999-9999')->update(['telefone' => null]);

        //Limpa celular
          \DB::table('clients')->where('celular', 0)->update(['celular' => null]);
          \DB::table('clients')->where('celular', '0')->update(['celular' => null]);
          \DB::table('clients')->where('celular', '')->update(['celular' => null]);
          \DB::table('clients')->where('celular', '(11) 9999-9999')->update(['celular' => null]);
          \DB::table('clients')->where('celular', '(99) 9999-9999')->update(['celular' => null]);
          \DB::table('clients')->where('celular', '(11) 99999-9999')->update(['celular' => null]);
          \DB::table('clients')->where('celular', '(99) 99999-9999')->update(['celular' => null]);
          \DB::table('clients')->where('celular', '(11)9999-9999')->update(['celular' => null]);
          \DB::table('clients')->where('celular', '(99)9999-9999')->update(['celular' => null]);
          \DB::table('clients')->where('celular', '(11)99999-9999')->update(['celular' => null]);
          \DB::table('clients')->where('celular', '(99)99999-9999')->update(['celular' => null]);


        //get em todos os clientes
        $clientes = Client::all();

        //Remove Celular se for duplicado com o Telefone
        foreach ($clientes as $cliente) {
          if ($cliente->celular == $cliente->telefone) {
            \DB::table('clients')->where('id', $cliente->id)->update(['celular' => null]);
          }
        }

    }

    public static function nuloSeExiste()
    {
      //get em todos os clientes
      $clientes = Client::all();

      foreach ($clientes as $cliente) {

        //verifica se há email em cliente
        if($cliente->email != null){
          //procura email na tabela de contact_infos
          $emaillinha = ContactInfo::where('contato', '=', $cliente->email)->first();
          if (isset($emaillinha->contato)) {
            if ($emaillinha->contato == $cliente->email) {
              echo '=';
              //seta email como null na tabela de clientes
              \DB::table('clients')->where('id', $cliente->id)->update(['email' => null]);
            }
          }
        }

        //verifica se há telefone em cliente
        if($cliente->telefone != null){
          //procura telefone na tabela de contact_infos
          $telefonelinha = ContactInfo::where('contato', '=', $cliente->telefone)->first();
          if (isset($telefonelinha->contato)) {
            if ($telefonelinha->contato == $cliente->telefone) {
              echo '=';
              //seta email como null na tabela de clientes
              \DB::table('clients')->where('id', $cliente->id)->update(['telefone' => null]);
            }
          }
        }

        //verifica se há celular em cliente
        if($cliente->celular != null){
          //procura celular na tabela de contact_infos
          $celularlinha = ContactInfo::where('contato', '=', $cliente->celular)->first();

          if (isset($celularlinha->contato)) {
            if ($celularlinha->contato == $cliente->celular) {
              echo '=';
              //seta email como null na tabela de clientes
              \DB::table('clients')->where('id', $cliente->id)->update(['celular' => null]);
            }
          }
        }
    }
  }

  public static function migrateEmailsTableClients()
  {
    //get em todos os clientes
    $clientes = Client::all();

      foreach ($clientes as $cliente) {
        if (($cliente->email != null) || ($cliente->telefone != null) || ($cliente->celular != null)) {
          $nome = 'Atulizar nome do Contato';
          if($cliente->email != null){
            $antesdoarroba = explode("@", $cliente->email);
            $nome = $antesdoarroba[0];
          }else{
            if ($cliente->nome != '') {
              $nome = $cliente->nome;
            }else{
              $nome = $cliente->empresa;
            }
          }
            $novocontato = new Contact();
            $novocontato->nome = ucfirst($nome);
            $novocontato->departamento = null ;
            $novocontato->user_id = 1;
            $novocontato->client_id = $cliente->id;
            $success = $novocontato->save();

            if ($success) {
              if ($cliente->email) {
                \DB::table('contact_infos')->insert(['tipo' => 'email', 'contato' => $cliente->email, 'contact_id' => $novocontato->id]);
                \DB::table('clients')->where('id', $cliente->id)->update(['email' => null]);
              }
              if ($cliente->telefone) {
                \DB::table('contact_infos')->insert(['tipo' => 'telefone', 'contato' => $cliente->telefone, 'contact_id' => $novocontato->id]);
                \DB::table('clients')->where('id', $cliente->id)->update(['telefone' => null]);
              }
              if ($cliente->celular) {
                \DB::table('contact_infos')->insert(['tipo' => 'telefone', 'contato' => $cliente->celular, 'contact_id' => $novocontato->id]);
                \DB::table('clients')->where('id', $cliente->id)->update(['celular' => null]);
              }
            }
            echo "=";
        }//fecha if
      }//fecha segundo foreach
      echo "\r \n ** Processo finalizado **";
    }


    public static function NormalizeData()
    {
      \DB::table('contact_infos')
        ->select(\DB::raw('update set contato = LOWER(contato)'))
        ->where('tipo', '=', 'email');

      \DB::table('clients')->select(\DB::raw('update set site = LOWER(site)'));
      \DB::table('clients')->select(\DB::raw('update set email = LOWER(email)'));
      \DB::table('clients')->select(\DB::raw('update set telefone = LOWER(telefone)'));
      \DB::table('clients')->select(\DB::raw('update set celular = LOWER(celular)'));

      \DB::table('clients')->select(\DB::raw('update set site = replace(site, "\r", "")'));
      \DB::table('clients')->select(\DB::raw('update set email = replace(email, "\r", "")'));
      \DB::table('clients')->select(\DB::raw('update set telefone = replace(telefone, "\r", "")'));
      \DB::table('clients')->select(\DB::raw('update set celular = replace(celular, "\r", "")'));

      \DB::table('clients')->select(\DB::raw('update set site = replace(site, "\n", "")'));
      \DB::table('clients')->select(\DB::raw('update set email = replace(email, "\n", "")'));
      \DB::table('clients')->select(\DB::raw('update set telefone = replace(telefone, "\n", "")'));
      \DB::table('clients')->select(\DB::raw('update set celular = replace(celular, "\n", "")'));

      \DB::table('clients')->select(\DB::raw('update set site = replace(site, " ", "")'));
      \DB::table('clients')->select(\DB::raw('update set email = replace(email, " ", "")'));
      \DB::table('clients')->select(\DB::raw('update set telefone = replace(telefone, " ", "")'));
      \DB::table('clients')->select(\DB::raw('update set celular = replace(celular, " ", "")'));
    }

    public static function transferContatos()
    {
      self::limpaInvalidos();
      self::NormalizeData();
      self::nuloSeExiste();
      self::migrateEmailsTableClients();
    }
}
