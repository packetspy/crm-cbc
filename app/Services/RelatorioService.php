<?php

namespace App\Services;

use App\Services\XlsService;
use App\Helpers\InteractionType;
use App\Models\Interaction;


class RelatorioService
{

    public static function create($functionName, $where, $type='xlsx')
    {
        $xls = new XlsService();

        if(method_exists(RelatorioService::class, $functionName))
            return $xls->createReport($type, RelatorioService::organize($functionName, RelatorioService::$functionName($where)));

        return $xls->createReport($type, RelatorioService::organize($functionName, []));
    }

    public static function contatosVendedor($where)
    {
        $query = \DB::table('users')
            ->join('interactions', 'users.id','=', 'interactions.user_id')
            ->join('interaction_types','interactions.interaction_type_id', '=', 'interaction_types.id')
            ->join('clients', 'interactions.client_id', '=', 'clients.id')
            ->join('status', 'clients.status_id', '=', 'status.id');

            $query = self::interactionsWhere($query, $where);

            return $query->select(
                'users.name as Vendedor',
                'clients.nome as Cliente',
                'status.status as Status',
                'interactions.created_at AS data_de_criacao',
                'interactions.data_contato AS data_de_contato',
                'interaction_types.nome AS tipo_de_interacao',
                'clients.id_plataforma as id_plataforma',
                'interactions.observacao AS observacao'
            )
            ->get();
    }

    public static function indicadoresVendedoresMaisInteracoes($where)
    {
        $query = \DB::table('users')
            ->join('interactions', 'users.id','=', 'interactions.user_id');

        $query = self::interactionsWhere($query, $where);
        return $query->select(
            'users.name as Vendedor',
            \DB::raw('count(interactions.id) AS Numero_de_interacoes')
        )->groupBy('Vendedor')->orderBy(\DB::raw('count(interactions.id)'), 'DESC')->get();
    }

    public static function indicadoresMelhoresVendedores($where)
    {
        $query = \DB::table('users')
            ->join('interactions', 'users.id','=', 'interactions.user_id')
            ->join('interaction_types','interactions.interaction_type_id', '=', 'interaction_types.id');

            $query = self::interactionsWhere($query, $where);
            $query = $query->whereIn('interactions.interaction_type_id', InteractionType::negocioFechado());

            return $query->select(
                'users.name as Vendedor',
                \DB::raw('count(interactions.id) AS Numero_de_negocio_fechado')
            )->groupBy('Vendedor')
             ->orderBy(\DB::raw('count(interactions.id)'), 'DESC')
             ->get();
    }

    public static function indicadoresGeraisInteracoes($where)
    {
        $qtdAtualizados = self::indicadorInteracaoAtualizacao($where);
        $qtdTotalCriados = count(self::indicadorTotalInteracoesInicial($where));
        $qtdNegocioFechado = self::indicadorNegocioFechado($where);
        $qtdNegocioNaoFechado = self::indicadorNegocioNaoFechado($where);
        $interacoesAtualizacao = self::indicadorInteracaoTempestiva($where);
        return [
            [
                'interacoes_iniciadas_total'   => $qtdTotalCriados,
                'interacoes_iniciadas_atualizadas' => $qtdAtualizados,
                'interacoes_com_data_tempestivas' => $interacoesAtualizacao['tempestiva'],
                'interacoes_com_data_no_horario'  => $interacoesAtualizacao['no_horario'],
                'negocios_gerados' => $qtdNegocioFechado,
                'negocios_nao_fechados' => $qtdNegocioNaoFechado
            ]
        ];
    }

    public static function indicadorTotalInteracoesInicial($where)
    {
        $query = \DB::table('interactions')
            ->join('users', 'interactions.user_id','=', 'users.id');
        $query = self::interactionsWhere($query, $where);
        $query = $query->whereNull('interactions.father_id');

        return  $query->select(
            'interactions.id AS id'
        )->get();
    }

    public static function indicadorInteracaoAtualizacao($where)
    {
        $resultadoPais = self::indicadorTotalInteracoesInicial($where);
        $interactions = [];
        foreach($resultadoPais as $intFather) {
            $interactions[] = $intFather->id;
        }

        $resultadoFilhas = \DB::table('interactions')

                            ->join('users', 'interactions.user_id','=', 'users.id')
                            ->whereIn('father_id', $interactions)
                            ->select('interactions.id AS id')
                            ->get();

        return count($resultadoFilhas);
    }

    public static function indicadorNegocioFechado($where)
    {
        $query = \DB::table('interactions')
            ->join('users', 'interactions.user_id','=', 'users.id');
        $query = self::interactionsWhere($query, $where);
        $query = $query->whereIn('interactions.interaction_type_id', InteractionType::negocioFechado());

        return  $query->select(
            'interactions.id AS id'
        )->count();
    }

    public static function indicadorNegocioNaoFechadoDetalhado($where)
    {
        $query = Interaction::join('users', 'interactions.user_id', '=', 'users.id')
            ->select('interactions.*', \DB::raw('count(interactions.id) as total'));
        $query = self::interactionsWhere($query, $where);
        $query = $query->whereIn('interactions.interaction_type_id', InteractionType::negocioNaoFechado());
        return $query->get();
    }

    public static function indicadorNegocioNaoFechado($where)
    {
        $query = \DB::table('interactions')
            ->join('users', 'interactions.user_id','=', 'users.id');
        $query = self::interactionsWhere($query, $where);
        $query = $query->whereIn('interactions.interaction_type_id', InteractionType::negocioNaoFechado());

        return  $query->select(
            'interactions.id AS id'
        )->count();
    }

    public static function indicadorInteracaoTempestiva($where)
    {
        $query = \DB::table('interactions')
            ->join('users', 'interactions.user_id','=', 'users.id');
        if(isset($where['start']) && $where['start'] != '')
            $query = $query->where('interactions.data_contato', '>=', $where['start']);

        if(isset($where['end']) && $where['end'] != '')
            $query = $query->where('interactions.data_contato', '<', $where['end']);

        if(isset($where['user_id']) && $where['user_id'] != '')
            $query = $query->where('users.id', '=', $where['user_id']);


        $query = $query->whereNotNull('interactions.data_contato');

        $interactionsWithDate = $query->select(
            'interactions.id AS id',
            'interactions.data_contato AS data_contato'
        )->get();

        $interactions = [];
        foreach($interactionsWithDate as $interaction){
            $interactions[] = $interaction->id;
            $dataContato[$interaction->id] = $interaction->data_contato;
        }

        $interactionsChild = \DB::table('interactions')

                    ->join('users', 'interactions.user_id','=', 'users.id')
                    ->whereIn('interactions.father_id', $interactions)
                    ->select(
                        'interactions.id AS id',
                        'interactions.father_id AS father_id',
                        'interactions.created_at AS created'
                    )->get();

        $retorno = [];
        $retorno['no_horario'] = $retorno['tempestiva'] = 0;
        foreach($interactionsChild AS $element)
        {
            $horaContato = new \DateTime($element->created);
            $horaMarcada = new \Datetime($dataContato[$element->father_id]);
            $horaMarcada->add(new \DateInterval('PT12H'));
            if($horaMarcada < $horaContato) {
                $retorno['tempestiva']++;
            }else
            {
                $retorno['no_horario']++;
            }
        }
        $retorno['sem_lancamento'] = ($retorno['tempestiva'] + $retorno['no_horario']) - count($interactionsWithDate);
        $retorno['sem_lancamento'] *= -1;
        return $retorno;
    }

    protected static function interactionsWhere($query, $where)
    {
        if(isset($where['start']) && $where['start'] != ''){
            $query = $query->where('interactions.created_at', '>=', $where['start']);
        }

        if(isset($where['end']) && $where['end'] != '')
            $query = $query->where('interactions.created_at', '<', $where['end']);

        if(isset($where['user_id']) && $where['user_id'] != '')
            $query = $query->where('users.id', '=', $where['user_id']);

        if(isset($where['group_by']) && $where['group_by'] != '')
            $query = $query->groupBy($where['group_by']);


        return $query;
    }

    public static function organize($name, $dados)
    {
        $retorno = [];
        $retorno["nome"] = $name;
        $retorno["dados"] = $dados;

        $retorno["cabecalho"] = ["Não foram encontrados dados para pesquisa"];

        if(empty($dados))
            return $retorno;

        $retorno["cabecalho"] = [];
        foreach($dados[0] as $key => $value)
            $retorno["cabecalho"][] = ucfirst(str_replace("_", " ", $key));

        return $retorno;
    }
}
