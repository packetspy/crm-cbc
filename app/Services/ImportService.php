<?php

namespace App\Services;

use App\Services\XlsService;

class ImportService
{
    protected $xls;

    public function __construct(XlsService $xls)
    {
        $this->xls = $xls;
    }

    public function import($file)
    {
        return $this->xls->readFile($file);
    }

    public function sync($file, $sheet = null)
    {
        return $this->xls->readFile($file, $sheet);
    }
}
