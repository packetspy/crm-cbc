<?php

namespace App\Services;

use \Artesaos\Defender\Facades\Defender;
use \App\Models\Permission;

class PermissionService extends Service
{
  public function getPermissoes($limit = 50)
  {
    return Permission::orderBy('name', 'asc')->paginate($limit);
  }

  public function getListPermissoes()
  {
    return Permission::all();
  }
}
