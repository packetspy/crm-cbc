<?php

namespace App\Services;

use App\Services\Service;
use App\Models\Status;

Class StatusService extends Service
{
    public function __construct()
    {
       //
    }

    /**
     * Gets the status.
     * @param      string  $order_by  the column
     * @param      string  $order direction
     * @return     array   The status.
     */
    public function getStatus($order_by = null, $order = null)
    {
      if(!$order_by){
        return $status = Status::orderBy('order', 'asc')->get();
      }else{
        return $status = Status::orderBy($order_by, $order)->get();
      }
      return $status = Status::all();
    }

    public function getStatusFinanceiro($order_by = null, $order = null)
    {
      if(!$order_by){
        return $status = Status::where('financeiro', '=', 1)->orderBy('order', 'asc')->get();
      }else{
        return $status = Status::where('financeiro', '=', 1)->orderBy($order_by, $order)->get();
      }
      return $status = Status::where('financeiro', '=', 1)->get();
    }
}
