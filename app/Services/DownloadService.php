<?php

namespace App\Services;

use App\Models\ContactInfo;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use Mail;
use GuzzleHttp;

use App\Services\Service;
use App\Services\HistoricoLoginService;
use App\Services\ImportService;

use App\Models\Client;
use App\Models\Contact;
use App\Models\Email;
use App\Models\Telefone;

Class DownloadService extends Service
{
    protected $historicoLoginService = null;
    protected $importService = null;

    protected $username = null;
    protected $password = null;
    protected $data_inicio_plataforma = null;
    protected $data_inicial = null;
    protected $data_final = null;
    protected $timestamp = null;
    protected $extension = null;
    protected $pagina_login = null;
    protected $pagina_poslogin = null;
    protected $postlogin = null;
    protected $postdata = null;

    public function __construct()
    {
        $this->historicoLoginService = \App::make('App\Services\HistoricoLoginService');
        $this->historicoOperacaoService = \App::make('App\Services\HistoricoOperacaoService');
        $this->clientService = \App::make('App\Services\ClientService');
        $this->contactService = \App::make('App\Services\ContactService');
        $this->importService = \App::make('App\Services\ImportService');

        $this->username = 'admin';
        $this->password = 'CBC#relatorio';
        $this->data_inicio_plataforma = '01/07/2015 00:00'; //Sim, a CMA só aceita a data nesse formato Oo
        $this->data_inicial = Carbon::now('America/Sao_Paulo')->yesterday();
        $this->data_final = Carbon::tomorrow('America/Sao_Paulo')->subSecond()->format('d/m/Y H:i'); //Sim, a CMA só aceita a data nesse formato Oo
        $this->timestamp = Carbon::now('America/Sao_Paulo');
        $this->extension = 'xls';
        $this->pagina_login = 'https://demo.cbcnegocios.com.br/cma/login.php';
        $this->pagina_poslogin = 'https://demo.cbcnegocios.com.br/cma/reports.php';
        $this->postlogin = null;
        $this->postdata = null;
    }

    public function getReportCma($data_inicial, $data_final)
    {
        //to infinity
        set_time_limit(0);

        //logica para baixar arquivo em CURL
        // Inicia o cURL e faz Login
        $this->postlogin = "inputUser=".$this->username."&inputPassword=".$this->password;
        $this->postdata = "dtIni=".$data_inicial."&dtFim=".$data_final;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->pagina_login);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,  $this->postlogin);
        //curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie.txt');
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $login = curl_exec ($ch);

        //$info = curl_getinfo($ch);
        //$http_result = $info ['http_code'];

        // Define uma nova URL para ser chamada (após o login)
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,0); //Use 0 to wait indefinitely.
        curl_setopt($ch, CURLOPT_TIMEOUT, 1800); //timeout in seconds
        curl_setopt($ch, CURLOPT_URL, $this->pagina_poslogin);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,  $this->postdata);
        curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie.txt');
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");
        $content = curl_exec ($ch);

        // Encerra o cURL
        curl_close ($ch);
        return $content;
    }

    public function getReportCmaHourly()
    {
        $filename = 'relatorio-'.$this->timestamp.'.'.$this->extension;
        $content = self::getReportCma($this->data_inicio_plataforma, $this->data_final);
        if ($content) {
            Storage::disk('base_cma_hourly')->put($filename, $content);
            $realfile = storage_path('report/cma/hourly/').$filename;
            $this->historicoLoginService->importLoginsPlataforma(false, $this->importService->sync($realfile, 'Usuarios'));
            //$this->historicoOperacaoService->importOperacoesPlataforma(false, $this->importService->sync($realfile, 'OfertasContraofertasNegocios'));
            $this->clientService->sincronizaInfoCliente(false, $this->importService->sync($realfile, 'Empresas'));
            $this->contactService->sincronizaInfoContato(false, $this->importService->sync($realfile, 'Usuarios'));
        }
    }

    public function getReportCmaFull()
    {
        $filename = 'relatorio-'.$this->timestamp.'.'.$this->extension;
        $content = self::getReportCma($this->data_inicio_plataforma, $this->data_final);
        if ($content) {
            Storage::disk('base_cma_full')->put($filename, $content);
            $realfile = storage_path('report/cma/full/').$filename;

            $data = [
                'from' => 'no-reply@cbcnegocios.com.br',
                'from-name' => 'CRM - CBC Negócios',
                'to' => 'relatorios@cbcnegocios.com.br',
                'to-name' => 'Grupo Relátorios CMA',
                'priority' => 1,
                'subject' => 'Base CBC '.$this->timestamp,
            ];

            Mail::send('emails.base-automatica', ['data' => $data, 'realfile' => $realfile], function ($mail) use ($data, $realfile) {
                $mail->from($data['from'], $data['from-name']);
                $mail->to($data['to'], $data['to-name']);
                $mail->priority($data['priority']);
                $mail->attach($realfile);
                $mail->subject($data['subject']);
            });
        }
    }

    public function getCadastrosFromApiCma()
    {
        $now = Carbon::now('America/Sao_Paulo');
        $cadastros = new GuzzleHttp\Client();
        $res = $cadastros->request('GET','http://exchange.cbcnegocios.com.br:8080/api/v1/registerusercompany');
        $cadastros = json_decode($res->getBody(), true);

        foreach ($cadastros as $cadastro) {
            $empresa = Client::where('id_plataforma', '=', $cadastro['empresa']['emp_IntEmpresa'])->first();

            if (!$empresa) {
                $novaempresa = new Client();
                $novaempresa->id_plataforma = $cadastro['empresa']['emp_IntEmpresa'];
                $novaempresa->tipoempresa_id = $this->clientService->discoverTipoEmpresa($cadastro['empresa']['emp_ChaTipoEmpresa']);
                $novaempresa->user_id = 1; // "Automático/CRM"
                $novaempresa->status_id = 8; // "Ativo/Cadastrado"
                $novaempresa->origem_id = 10; // "Site: BotaoCadastre-se"
                $novaempresa->empresa = $cadastro['empresa']['emp_ChaRazaoSocial'];
                $novaempresa->nome = $cadastro['empresa']['emp_ChaNomeFantasia'];
                $novaempresa->site = $cadastro['empresa']['emp_ChaWebsiteLinkedin'];
                $novaempresa->endereco = $cadastro['empresa']['emp_ChaEndereco'];
                $novaempresa->bairro = $cadastro['empresa']['emp_ChaBairro'];
                $novaempresa->cidade = $cadastro['empresa']['emp_ChaCidade'];
                $novaempresa->estado = $cadastro['empresa']['emp_ChaEstado'];
                $novaempresa->cep = $cadastro['empresa']['emp_ChaCEP'];
                $novaempresa->datacadastro_plataforma = date('Y-m-d H:i:s', $cadastro['empresa']['emp_DtdDtaInclusao'] /1000);
                $success_empresa = $novaempresa->save();

                if($success_empresa){

                    $novocontato = new Contact();
                    $novocontato->client_id = $novaempresa->id;
                    $novocontato->user_id =  1; // "Automático/CRM"
                    $novocontato->origem_id = 10; // "Site: BotaoCadastre-se"
                    $novocontato->id_plataforma =  $cadastro['usuario']['uss_IntUsuario'];
                    $novocontato->nome = $cadastro['usuario']['uss_ChaNomeUsuario'];
                    $novocontato->nome_plataforma = $cadastro['usuario']['uss_ChaNomeUsuario'];
                    $novocontato->perfil_plataforma = $cadastro['usuario']['uss_ChaPerfil'];
                    $novocontato->datacadastro_plataforma = date('Y-m-d H:i:s', $cadastro['usuario']['uss_DtdDtaInclusao'] /1000);

                    $success_contato = $novocontato->save();

                    if ($success_contato) {
                        //Cria EMAIL contato para o lead
                        if ($cadastro['usuario']['uss_ChaEmail'] != null) {
                            $email = [
                                'contato' => $cadastro['usuario']['uss_ChaEmail'],
                                'tipo' => 'email',
                                'contact_id' => $novocontato->id,
                                'created_at' => $now,
                            ];
                            Email::create($email);
                        }

                        //Cria CELULAR do contato
                        if ($cadastro['usuario']['uss_ChaCelular'] != null) {
                            $telefone = [
                                'contato' => $cadastro['usuario']['uss_ChaCelular'],
                                'tipo' => 'telefone',
                                'contact_id' => $novocontato->id,
                                'created_at' => $now,
                            ];
                            Telefone::create($telefone);
                        }
                        //Cria TELEFONE contato
                        if ($cadastro['usuario']['uss_ChaTelefoneComercial'] != null) {
                            $telefone = [
                                'contato' => $cadastro['usuario']['uss_ChaTelefoneComercial'],
                                'tipo' => 'telefone',
                                'contact_id' => $novocontato->id,
                                'created_at' => $now,
                            ];
                            Telefone::create($telefone);
                        }
                    }
                }
            }else{
                $novocontato = new Contact();
                $novocontato->client_id = $empresa->id;
                $novocontato->user_id =  1; // "Automático/CRM"
                $novocontato->origem_id = 10; // "Site: BotaoCadastre-se"
                $novocontato->id_plataforma =  $cadastro['usuario']['uss_IntUsuario'];
                $novocontato->nome = $cadastro['usuario']['uss_ChaNomeUsuario'];
                $novocontato->nome_plataforma = $cadastro['usuario']['uss_ChaNomeUsuario'];
                $novocontato->perfil_plataforma = $cadastro['usuario']['uss_ChaPerfil'];
                $novocontato->datacadastro_plataforma = date('Y-m-d H:i:s', $cadastro['usuario']['uss_DtdDtaInclusao'] /1000);

                $checaNovoContatoInfo = ContactInfo::where('contato','like','%'.$cadastro['usuario']['uss_ChaEmail'].'%')->first();

                if(!$checaNovoContatoInfo){

                    $success_contato = $novocontato->save();

                    if ($success_contato) {
                        //Cria EMAIL contato para o lead
                        if ($cadastro['usuario']['uss_ChaEmail'] != null) {
                            $email = [
                                'contato' => $cadastro['usuario']['uss_ChaEmail'],
                                'tipo' => 'email',
                                'contact_id' => $novocontato->id,
                                'created_at' => $now,
                            ];
                            Email::create($email);
                        }
                        //Cria CELULAR do contato
                        if ($cadastro['usuario']['uss_ChaCelular'] != null) {
                            $telefone = [
                                'contato' => $cadastro['usuario']['uss_ChaCelular'],
                                'tipo' => 'telefone',
                                'contact_id' => $novocontato->id,
                                'created_at' => $now,
                            ];
                            Telefone::create($telefone);
                        }
                        //Cria TELEFONE contato
                        if ($cadastro['usuario']['uss_ChaTelefoneComercial'] != null) {
                            $telefone = [
                                'contato' => $cadastro['usuario']['uss_ChaTelefoneComercial'],
                                'tipo' => 'telefone',
                                'contact_id' => $novocontato->id,
                                'created_at' => $now,
                            ];
                            Telefone::create($telefone);
                        }

                    }

                }
            }
        };

    }
}
