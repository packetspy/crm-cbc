<?php

namespace App\Services;

use App\Services\Service;
use App\Models\TipoEmpresa;

Class TipoEmpresaService extends Service
{
    public function __construct()
    {
       //
    }

    /**
     * Gets the status.
     * @param      string  $order_by  the column
     * @param      string  $order direction
     * @return     array   The status.
     */
    public function getTipoEmpresa($order_by = null, $order = null)
    {
      if(!$order_by){
        return $status = TipoEmpresa::orderBy('tipo', 'asc')->get();
      }else{
        return $status = TipoEmpresa::orderBy($order_by, $order)->get();
      }
      return $status = TipoEmpresa::all();
    }
}
