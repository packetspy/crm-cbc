<?php

namespace App\Services;

use App\Services\Service;
use Illuminate\Database\QueryException;
use App\Models\HistoricoLead;

Class HistoricoLeadService extends Service
{
    public function __construct()
    {
        //
    }

    public function getLeadsToExport($data_inicial, $data_final)
    {
        $result = \DB::select('
              SELECT client_id as crm_id,
              clients.nome as cliente_nome, clients.empresa as cliente_empresa,
              users.name as criadopor,
              user_responsavel.name as responsavel,
              escritorios.nome as escritorio,
              status.status as status_original,
              status_client.status as status_atual,
              origem.origem as origem,
              historico_lead.email as email,
              DATE_FORMAT(historico_lead.created_at, "%Y-%m-%d") as criadoem
              FROM historico_lead
              LEFT JOIN clients on historico_lead.client_id = clients.id
              LEFT JOIN escritorios on historico_lead.escritorio_id = escritorios.id
              LEFT JOIN status on historico_lead.status_id = status.id
              LEFT JOIN status as status_client on clients.status_id = status_client.id
              LEFT JOIN origem on historico_lead.origem_id = origem.id
              LEFT JOIN users on historico_lead.user_id = users.id
              LEFT JOIN users as user_responsavel on historico_lead.responsavel_id = user_responsavel.id
              WHERE cast(historico_lead.created_at as date) BETWEEN "'.$data_inicial.'" AND "'.$data_final.'" order by criadoem desc'
            );

        return $result;
    }

}
