<?php
namespace App\Services;

use App\Services\Service;
use App\Services\ClientService;
use App\Services\ContactService;

use App\Models\Contact;
use App\Models\ContactInfo;
use App\Models\Email;
use App\Models\Telefone;
use App\Models\Skype;

Class ContactInfoService extends Service
{
    protected $rules = [
        'contato' => 'required',
        'tipocontato' => 'required'
    ];
    protected $messages = [
        'contact_not_found' => 'Contato não encontrado.',
        'contato' => 'O campo "contato" é necessário.'
    ];

    public $clientId = null;
    public $contactId = null;

    public function __construct($userId = null)
    {
        $this->userId = $userId;
        $this->clientService = new ClientService($userId);
        $this->contactService = new ContactService($userId);
        $this->contactProduct = new ContactProductRepository();
    }

    public function getClienteId()
    {
        return $this->clientId;
    }
    public function getContatoId()
    {
        return $this->contactId;
    }

    /**
     * Validate if the function data match with the rules.
     *
     * @return     boolean  true when everything is ok. False when not.
     */
    protected function validateContato()
    {
        return $this->validate($this->data, $this->rules, $this->messages);
    }

    public function sanitizeData($data = array())
    {
        foreach ($data as $key => $value)
        {
            if ($key != '_token'){
                $data[$key] = trim(preg_replace('/\t+/', '', $value));
              }
        }
        return $data;
    }

    public function BuscaFormasDeContato($query, $limit, $status, $order_by, $order, $type)
    {
      if ($query){
      $cliente = ContactInfo::select('id', 'contato', 'contact_id');
      $cliente = $cliente->orWhere('contato', 'LIKE', '%'. $query .'%');
      $cliente = $cliente->where('tipo', '=', $type);
      $cliente = $cliente->orderBy($order_by, $order);
      $cliente = $cliente->with('contact.client');
      return $cliente->paginate($limit);
      }
    }

    public function insert($data)
    {
        $this->data = $data;

        //Define qual model utilizar
        $formadecontato = new ContactInfo();
        if ($data['tipocontato'] == 'email') {
          $formadecontato = new Email();
        }elseif($data['tipocontato'] == 'telefone'){
          $formadecontato = new Telefone();
        }elseif($data['tipocontato'] == 'skype'){
          $formadecontato = new Skype();
        }

        $contato = $this->contactService->Exist($data['contato_id']);

        if(!$contato) {
            $this->setError("contato", $this->messages['contact_not_found']);
            return false;
        }else{
          $this->clientId = $data['cliente_id'];
          $this->contactId = $data['contato_id'];
        }

        if($this->validateContato()) {
            $data = $this->sanitizeData($data);
            $this->clientId = $data['cliente_id'];
            $this->contactId = $data['contato_id'];

            $formadecontato->contact_id = $data['contato_id'];
            $formadecontato->tipo = $data['tipocontato'];
            $formadecontato->contato = $data['contato'];
            $formadecontato->created_at = $this->getNow();
            $formadecontato_success = $formadecontato->save();
            return true;
        }
        return false;
    }

    public function delete($formadecontato_id)
    {
      $formadecontato = ContactInfo::destroy($formadecontato_id);
      if(!$formadecontato) {
          return false;
      }
      return true;
    }


}
