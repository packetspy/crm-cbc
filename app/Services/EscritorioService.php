<?php

namespace App\Services;

use App\Services\Service;
use App\Models\Escritorio;

Class EscritorioService extends Service
{
    public function __construct()
    {
       //
    }

    public function getEscritorios($limit = 50)
    {
      return Escritorio::paginate($limit);
    }
    public function getListEscritorios()
    {
      return Escritorio::all();
    }
}
