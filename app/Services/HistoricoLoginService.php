<?php

namespace App\Services;

use App\Services\Service;
use Illuminate\Database\QueryException;
use App\Models\HistoricoLogin;

Class HistoricoLoginService extends Service
{
    public function __construct()
    {
        //
    }

    public function getUltimoLogin($cliente_id)
    {
      $login = HistoricoLogin::where('id_empresa_plataforma', '=', $cliente_id)
        ->orderBy('data_acesso', 'desc')
        ->first();
      $dados =  array(
        'ultimo_login'=>$login['data_acesso'],
      );
      return $dados;
    }

    public static function importLoginsPlataforma($response = true, Array $elements)
    {
        $resultado = [];
        $success = false;

        foreach($elements as $element) {
          $acesso = HistoricoLogin::where('id_empresa_plataforma', '=', $element['id_empresa'])
            ->where('email', '=', $element['email'])
            ->orderBy('data_acesso', 'desc')
            ->first();

            if (($element['data_ultimo_acesso'] > '1970-01-01') && ($element['data_ultimo_acesso'] > $acesso['data_acesso'])) {
              $novo_acesso = new HistoricoLogin();
              $novo_acesso->id_empresa_plataforma = $element['id_empresa'];
              $novo_acesso->id_usuario_plataforma = $element['id_usuario'];
              $novo_acesso->email = strtolower($element['email']);
              $novo_acesso->data_acesso = $element['data_ultimo_acesso'];
              $success = $novo_acesso->save();
            }

            if($success)
                $resultado[] = array_merge($element, ['status' => 'Atualizado com sucesso no DB.']);
            else
                $resultado[] = array_merge($element, ['status' => 'Data de acesso igual ou mais antiga que o DB.']);
        }

        if ($response) {
          return $resultado;
        }
        return true;
    }

}
