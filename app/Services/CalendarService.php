<?php

namespace App\Services;

use Carbon\Carbon;

use App\Services\Service;
use App\Models\User;
use App\Models\Interaction;
Class CalendarService extends Service
{

    protected $clientId = null;

    protected $rules = [];

    protected $messages = [];


    public function __construct($userId=null)
    {
        $this->userId = $userId;
    }

    public function getInfoCalendarioUsuario($start = null, $end = null)
    {
        if($start == null) {
            $start = Carbon::today();
            $end = clone $start;
            $end->add(new \DateInterval('P1M'));
        }
        return $this->consultaHorarios($start, $end);
    }

    protected function consultaHorarios($start, $end)
    {
        $consulta = Interaction::join('users', 'interactions.user_id', '=', 'users.id')
            ->join('clients', 'interactions.client_id', '=', 'clients.id')
            ->where('interactions.data_contato', '>=', $start)
            ->where('interactions.data_contato', '<', $end)
            ->with('children')
            ->whereNotNull('interactions.data_contato')
            ->where('clients.status_id', '!=', 1)
            ->where('clients.status_id', '!=', 3);

        if (\Defender::hasPermission('calendario.geral') == false)
            $consulta = $consulta->where('users.id', '=', $this->userId);

        return $consulta
            ->select('interactions.id AS id',
                    'interactions.client_id AS client_id',
                    \DB::raw("REPLACE(data_contato, ' ', 'T') as start"),
                    \DB::raw("IFNULL(interactions.father_id, '') as color"),
                     'clients.nome AS title')
            ->get();
    }

    public function getPermissions()
    {
        return [
            [
                'id' => 'calendario.index',
                'name' => 'Ver calendário - Privativo',
                'help' => 'Exibe apenas as interações de contato pertinentes ao usuário'
            ],
            [
                'id' => 'calendario.geral',
                'name' => 'Ver calendário - Todos as interações',
                'help' => 'Exibe todas as interações de contato cadastradas'
            ]
        ];
    }
}
