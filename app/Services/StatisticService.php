<?php
/**
 * Created by PhpStorm.
 * User: ludio
 * Date: 25/07/16
 * Time: 21:04
 */

namespace App\Services;


use App\Models\Client;
use App\Models\Interaction;
use App\Models\InteractionType;
use App\Models\User;

class StatisticService extends Service
{

    public function __construct($userId=null)
    {
        $this->userId = $userId;
    }

    public function countUsers()
    {
        return User::count();
    }

    public function countClients($ativo = 1)
    {
        return Client::where('ativo', '=', $ativo)->count();
    }

    public function countInteractions()
    {
        $interactions = InteractionType::with('interactions')->get();

        return $interactions;
    }

}