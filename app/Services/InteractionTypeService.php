<?php

namespace App\Services;

use App\Services\Service;
use App\Models\InteractionType;

Class InteractionTypeService extends Service
{
    protected $rules = [
        'nome' => 'required|min:1',
        'exibe_carrinho' => 'required'
    ];

    protected $messages = [
        'type_not_found' => "Tipo não encontrado"
    ];

    protected $data = [
        'exibe_carrinho' => false
    ];

    protected $interactionTypeId = null;


    public function __construct($userId=null)
    {
        $this->userId = $userId;
    }

    public function getTypes($limit = 10)
    {
        return InteractionType::orderBy("ativo", "desc")->orderBy("id")->paginate($limit);
    }


    public function getTypesAtivos($limit = 10, $apenasContraparte = false)
    {
        $elements = InteractionType::where("ativo", '=', true)->where("father_id", '=', null);

        if($apenasContraparte == true){
            $elements = $elements->where('funcao', '=', false)->where('id', '<>', $this->interactionTypeId);
        }
        else
            $elements = $elements->where('funcao', '=', true);

        $elements = $elements->paginate($limit);

        foreach($elements as $element)
            $element->countChildrens();

        return $elements;
    }

    public function setInteractionTypeId($id)
    {
        $this->interactionTypeId = $id;
    }

    public function getType($id)
    {
        return InteractionType::find($id);
    }

    public function getFilhos($id)
    {
        if(!$type = InteractionType::find($id))
            return [];

        $childrens = $type->childrens()->getResults();
        foreach($childrens as $child)
            $child->countChildrens();

        return $childrens->toArray();
   }

    public function getPai($id)
    {
        if($type = InteractionType::find($id))
            return $type->father()->getResults()->toArray();

        return [];
    }

    public function save($data)
    {
        $this->data = array_merge($this->data, $data);

        if(!$this->validateType() || !$this->validateUser())
            return false;

        $this->data['ativo'] = true;
        $this->user->interaction_types()->create($this->data);

        return true;
    }

    /** Métodos públicos para uma interação **/
    public function apenasAtivos()
    {
        return InteractionType::where('ativo', '=', 1)->get()->toArray();
    }

    public function update($id, $change)
    {
        $this->data = array_merge($this->data, $change);
        $type = InteractionType::find($id);

        if($type == null) {
            $this->setError("tipo", $this->messages['type_not_found']);
            return false;
        }

        if($this->validateType()) {
            $type->update($change);
            return true;
        }

        return false;
    }

    public function delete($id)
    {
        $type = InteractionType::find($id);
        if($type) {
            $type->ativo = false;
            return true;
        }

        $this->setError('tipo', $this->messages['type_not_found']);
        return false;
    }

    public function typeExist($id)
    {
        if(InteractionType::find($id))
            return true;

        $this->setError('tipo', $this->messages['type_not_found']);
        return false;
    }

    protected function validateType()
    {
        return $this->validate($this->data, $this->rules, $this->messages);
    }

}
