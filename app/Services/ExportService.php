<?php

namespace App\Services;

use App\Services\XlsService;

class ExportService
{
    protected $xls;

    public function __construct(XlsService $xls)
    {
        $this->xls = $xls;
    }

    public function export($nome="Necessario nome", Array $dados)
    {
        $data['cabecalho'] = ['Sem registros'];
        $data['dados'] = $dados;
        $data['nome'] = $nome;

        if(!empty($dados)) {
            $data['cabecalho'] = [];
            foreach($dados[0] as $key=> $value){
                $data['cabecalho'][] = $key;
            }
        }
        return $this->xls->createReport('xlsx', $data);
    }

}
