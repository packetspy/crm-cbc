<?php

namespace App\Services;

use App\Models\Payment;
use App\Models\User;
use App\Services\Service;
use App\Services\PaymentTypeService;
use App\Services\ClientService;
use Carbon\Carbon;


Class PaymentService extends Service
{
    protected $clientService;
    protected $userService;
    protected $payment_type_id = null;
    protected $now;
    protected $data = [
      //nothing here
    ];

    protected $rules = [
        'payment_type_id' => 'required|integer'
    ];

    protected $messages = [
        'payment_type_id' => "Tipo de pagamento não definido"
    ];

    public function __construct($user_id = null)
    {
        if (\Auth::user() ? $this->user_id = \Auth::user()->id : $this->user_id = null);
        //if (\Auth::user() ? $this->user_id = \Auth::user()->id : $this->user_id = null);
        $this->clientService = new ClientService($user_id);
        $this->userService = new UserService($user_id);
        $this->now = $this->getNow();
    }

    public function getPermissions()
    {
        $permissions = [];

        $permissions[] = [
            'id' => 'payment.index',
            'name' => 'Ver valores de mensalidade',
            'help' => 'Usuário vê todos valores de mensalidades de clientes'
        ];

        $permissions[] = [
            'id' => 'payment.edit',
            'name' => 'Editar valor de mensalidade',
            'help' => 'Usuário edita todos valores de mensalidades de clientes'
        ];

        $permissions[] = [
            'id' => 'payment.create',
            'name' => 'Adicionar valor de mensalidade',
            'help' => 'Usuário adiciona novos valores de mensalidades ao cliente'
        ];

        $permissions[] = [
            'id' => 'payment.destroy',
            'name' => 'Excluir valor de mensalidade',
            'help' => 'Usuário remove valor de mensalidades do cliente'
        ];

        return $permissions;
    }


    

    /**
     * Salva um novo valor de pagamento para o cliente.
     *
     * @param      array   $data   The data
     *
     * @return     boolean
     */
    public function save($data, $client_id)
    {
        $this->data = $data;

        if(!$this->validatePayment())
        {
            return false;
        }else{
          $payment = new Payment;
          $payment->amount = $this->data['amount'];
          $payment->free = $this->data['free'];
          $payment->day = $this->data['day'];
          $payment->description = $this->data['description'];
          $payment->client_id = $client_id;
          $payment->user_id = $this->user_id;
          $payment->payment_type_id = $this->data['payment_type_id'];
          $payment->created_at = $this->now;
          $payment->save();
            if($payment){
              return true;
            }
        }
    }

    public function getPaymentsToExport()
    {
        $elementos = (new Payment)->exportFields;
        //codigo original
        /*$result = \DB::Table('payments')
           ->join('clients', 'payments.client_id', '=','clients.id')
           ->join('payment_types', 'payment_type_id', '=','payment_types.id')
           ->select($elementos)
           ->groupBy('payments.client_id')
           ->orderBy('payments.created_at', 'asc')
           ->get();
        return $result;*/

          //ajuste para retornar apenas ultima de cada cliente
          $result = Payment::select($elementos)
          ->join('clients', 'payments.client_id', '=','clients.id')
          ->join('payment_types', 'payment_type_id', '=','payment_types.id')
          ->where('payments.created_at', function($query){
              $query->selectRaw('max(created_at)')
              ->from('payments as p')
              ->where('p.client_id', \DB::raw('payments.client_id'));
          })->orderBy('client_id', 'asc')
          ->get()->ToArray();
          return $result;
    }

    /**
     * Validate if the function data match with the rules.
     *
     * @return     boolean  true when everything is ok. False when not.
     */
    protected function validatePayment()
    {
        return $this->validate($this->data, $this->rules, $this->messages);
    }

}
