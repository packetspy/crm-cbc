<?php

namespace App\Services;

use App\Services\Service;
use App\Services\ContactService;
use App\Helpers\SilverpopAPI;
use App\Models\UpdateIbm;

use Carbon\Carbon;

Class IbmService extends Service
{
  protected $user;
  protected $pass;
  protected $master_database; //Master-CBC
  protected $contact_list_leads_cbc; //Leads_CBC

    public function __construct()
    {
        if (\Auth::user() ? $this->user_id = \Auth::user()->id : $this->user_id = null);
        $this->contactService = new ContactService($this->user_id);

        $this->user = 'anderson@cbcnegocios.com.br' ;
        $this->pass = '@@@Packet#1984';
        $this->master_database = '134313';
        $this->contact_list_leads_cbc = '134388';

        //Base e contactlist de Teste
        //$this->master_database = '132893';
        //$this->contact_list_leads_cbc = '182912';
    }

    public function addLeadIBM($data)
    {
      //Leemos el cookie de silverpop para asignar la actividad web al usuario que estamos agregando
      $visitor_key = "";
      if(isset($_COOKIE["com_silverpop_iMAWebCookie"])){
      	$visitor_key = $_COOKIE["com_silverpop_iMAWebCookie"];
      }

      $api = new SilverpopAPI();
      $api->login($this->user, $this->pass);

      $contact_lists = array('Leads_CBC' => $this->contact_list_leads_cbc);
      $recipient = $api->add_recipient($this->master_database, $data, $contact_lists, $visitor_key);

      $api->logout();
    }



    public function updateContatoIBM($cliente_id=null, $contato_id=null)
    {
        $timeStart = Carbon::now();
        $visitor_key = "";
        $needsUpdate = UpdateIbm::whereNotNull('contact_id')->groupBy('contact_id')->get();
        foreach ($needsUpdate as $contatos) {
            $contato_to_update = $this->contactService->getContactsToIbm(null, $contatos->contact_id);

            foreach ($contato_to_update as $contato) {
                if (($contato->Email) && filter_var($contato->Email, FILTER_VALIDATE_EMAIL)) {

                    $data = [
                        'cliente_id' => $contato->cliente_id,
                        'empresa_plataforma_id' => $contato->empresa_plataforma_id,
                        'status_id' => $contato->status_id,
                        'escritorio' => $contato->escritorio,
                        'responsavel_id' => $contato->responsavel_id,
                        'responsavel_nome' => $contato->responsavel_nome,
                        'origem_id' => $contato->origem_id,
                        'origem' => $contato->origem,
                        'nomefantasia' => $contato->nomefantasia,
                        'razaosocial' => $contato->razaosocial,
                        'tipo_documento' => $contato->tipo_documento,
                        'documento' => $contato->documento,
                        'ie' => $contato->ie,
                        'tipoempresa_id' => $contato->tipoempresa_id,
                        'tipo_empresa' => $contato->tipo_empresa,
                        'cidade' => $contato->cidade,
                        'estado' => $contato->estado,

                        'contato_id' => $contato->contato_id,
                        'contato_plataforma_id' => $contato->contato_plataforma_id,
                        'nome' => $contato->nome,
                        'departamento' => $contato->departamento,
                        'Email' => $contato->Email,
                        'email2' => $contato->email2,
                        'email3' => $contato->email3,
                        'telefone1' => $contato->telefone1,
                        'telefone2' => $contato->telefone2,
                        'telefone3' => $contato->telefone3,
                        'data_ultimo_login' => $contato->data_ultimo_login,
                    ];
                    //Previne caracteres errados na master base
                    $data = array_map("utf8_decode", $data );

                    $api = new SilverpopAPI();
                    $api->login($this->user, $this->pass);
                    $contact_lists = array('Leads_CBC' => $this->contact_list_leads_cbc);
                    $recipient = $api->add_recipient($this->master_database, $data, $contact_lists, $visitor_key);
                    $api->logout();
                }
            }
            //Remove do indice registro que acabou de ser atualizado no IBM Watson
            \DB::table('update_ibm')->where('contact_id', '=', $contatos->contact_id)->delete();
        }
    }

    public function updateClienteIBM($cliente_id=null, $contato_id=null)
    {
        $visitor_key = "";
        $needsUpdate = UpdateIbm::whereNotNull('client_id')->groupBy('client_id')->get();
        foreach ($needsUpdate as $contatos) {
            $cliente_to_update = $this->contactService->getContactsToIbm($contatos->client_id, null);

            foreach ($cliente_to_update as $contato) {
                if (($contato->Email) && filter_var($contato->Email, FILTER_VALIDATE_EMAIL)) {

                    $data = [
                        'cliente_id' => $contato->cliente_id,
                        'empresa_plataforma_id' => $contato->empresa_plataforma_id,
                        'status_id' => $contato->status_id,
                        'escritorio' => $contato->escritorio,
                        'responsavel_id' => $contato->responsavel_id,
                        'responsavel_nome' => $contato->responsavel_nome,
                        'origem_id' => $contato->origem_id,
                        'origem' => $contato->origem,
                        'nomefantasia' => $contato->nomefantasia,
                        'razaosocial' => $contato->razaosocial,
                        'tipo_documento' => $contato->tipo_documento,
                        'documento' => $contato->documento,
                        'ie' => $contato->ie,
                        'tipoempresa_id' => $contato->tipoempresa_id,
                        'tipo_empresa' => $contato->tipo_empresa,
                        'cidade' => $contato->cidade,
                        'estado' => $contato->estado,

                        'contato_id' => $contato->contato_id,
                        'contato_plataforma_id' => $contato->contato_plataforma_id,
                        'nome' => $contato->nome,
                        'departamento' => $contato->departamento,
                        'Email' => $contato->Email,
                        'email2' => $contato->email2,
                        'email3' => $contato->email3,
                        'telefone1' => $contato->telefone1,
                        'telefone2' => $contato->telefone2,
                        'telefone3' => $contato->telefone3,
                        'data_ultimo_login' => $contato->data_ultimo_login,
                    ];
                    //Previne caracteres errados na master base
                    $data = array_map("utf8_decode", $data );

                    $api = new SilverpopAPI();
                    $api->login($this->user, $this->pass);
                    $contact_lists = array('Leads_CBC' => $this->contact_list_leads_cbc);
                    $recipient = $api->add_recipient($this->master_database, $data, $contact_lists, $visitor_key);
                    $api->logout();
                }
            }
            //Remove do indice registro que acabou de ser atualizado no IBM Watson
            \DB::table('update_ibm')->where('client_id', '=', $contatos->client_id)->delete();
        }
    }

    public function updateContatosIBM()
    {
        $contatos = $this->contactService->getContactsToExport();

        //Leemos el cookie de silverpop para asignar la actividad web al usuario que estamos agregando
        $visitor_key = "";
        if(isset($_COOKIE["com_silverpop_iMAWebCookie"])){
        	$visitor_key = $_COOKIE["com_silverpop_iMAWebCookie"];
        }

        /*$api = new SilverpopAPI();
        $api->login($this->user, $this->pass);*/

        foreach ($contatos as $contato) {
            if (($contato->Email) && filter_var($contato->Email, FILTER_VALIDATE_EMAIL)) {

                $api = new SilverpopAPI();
                $api->login($this->user, $this->pass);

                $data = [
                    'cliente_id' => $contato->cliente_id,
                    'empresa_plataforma_id' => $contato->empresa_plataforma_id,
                    'status_id' => $contato->status_id,
                    'escritorio' => $contato->escritorio,
                    'responsavel_id' => $contato->responsavel_id,
                    'responsavel_nome' => $contato->responsavel_nome,
                    'origem_id' => $contato->origem_id,
                    'origem' => $contato->origem,
                    'nomefantasia' => $contato->nomefantasia,
                    'razaosocial' => $contato->razaosocial,
                    'tipo_documento' => $contato->tipo_documento,
                    'documento' => $contato->documento,
                    'ie' => $contato->ie,
                    'tipoempresa_id' => $contato->tipoempresa_id,
                    'tipo_empresa' => $contato->tipo_empresa,
                    'cidade' => $contato->cidade,
                    'estado' => $contato->estado,

                    'contato_id' => $contato->contato_id,
                    'contato_plataforma_id' => $contato->contato_plataforma_id,
                    'nome' => $contato->nome,
                    'departamento' => $contato->nome,
                    'Email' => $contato->Email,
                    'email2' => $contato->email2,
                    'email3' => $contato->email3,
                    'telefone1' => $contato->telefone1,
                    'telefone2' => $contato->telefone2,
                    'telefone3' => $contato->telefone3,
                ];
                //Previne caracteres errados na master base
                $data = array_map("utf8_decode", $data );

                print_r($data);
                $contact_lists = array('Leads_CBC' => $this->contact_list_leads_cbc);
                $recipient = $api->add_recipient($this->master_database, $data, $contact_lists, $visitor_key);

                $api->logout();
            }

        }

        /*$contact_lists = array('Leads_CBC' => $this->contact_list_leads_cbc);
        $recipient = $api->add_recipient($this->master_database, $contatos, $contact_lists, $visitor_key);*/

        $total = Carbon::now()->diffInSeconds($timeStart);
        echo "Tempo de execução: ".gmdate('H:i:s', $total). "\n";
    }
}
