<?php

namespace App\Services;

use App\Services\Service;
use App\Services\ClientService;
use Carbon\Carbon;

use App\Models\HistoricoOperacao;
use App\Models\Relevance;
use App\Models\User;

Class RelevanceService extends Service
{
    protected $clientService;
    protected $userService;
    protected $now;
    protected $data = [
      //nothing here
    ];

    public function __construct($user_id = null)
    {
        if (\Auth::user() ? $this->user_id = \Auth::user()->id : $this->user_id = null);
        $this->clientService = new ClientService($user_id);
        $this->userService = new UserService($user_id);
        $this->now = $this->getNow();
    }

    /**
     * Gets the payments.
     *
     * @param      integer  $limit  The limit
     *
     * @return     Array   with relevance of client.
     */
    public function getRelevance($id_plataforma)
    {
        return Relevance::where('id_plataforma', '=', $id_plataforma)->orderBy('data_referencia', 'desc')->get();
    }


    public function getHistory($id_plataforma)
    {
      $now = Carbon::now('America/Sao_Paulo');
      $monthsago = Carbon::now('America/Sao_Paulo')->subMonths(12);
      return HistoricoOperacao::where('id_empresa_plataforma', '=', $id_plataforma)
        ->orderBy('data_inclusao', 'desc')
        ->whereBetween('data_inclusao', array($monthsago, $now))
        ->get();
    }



    public function delete($id)
    {
        $relevance = Relevance::where('id_plataforma', '=', $id);
        if($relevance) {
            $relevance->delete();
            return true;
        }
    }

    /**
     * Salva uma novainfo de relevâcia para o cliente.
     *
     * @param      array   $data   The data
     *
     * @return     boolean
     */
    public function save($data)
    {
        $this->data = $data;
        $relevance = new Relevance;
        $relevance->id_plataforma = $this->data['id_plataforma'];
        $relevance->indicacoes = $this->data['indicacoes'];
        $relevance->contraindicacoes = $this->data['contraindicacoes'];
        $relevance->negocios = $this->data['negocios'];
        $relevance->data_referencia = $this->data['data_referencia'];
        $relevance->created_at = $this->now;
        $relevance->updated_at = $this->now;
        $relevance->save();

          if($relevance){
            return true;
          }
    }

    public function ImportedData(Array $elements)
    {
        //\DB::table('relevance')->truncate();
        $resultado = [];

        foreach($elements as $element) {
            $result = $this->insertElement($element);

            if($result)
                $resultado[] = array_merge($element, ['status' => 'realizado com sucesso.']);
            else{
                if(!empty($this->getErrors())){
                    $msgErro = '';
                    foreach($this->getErrors() as $error)
                        $msgErro .= implode('|', $error). "|";

                    $resultado[] = array_merge($element, ['status' => $msgErro]);
                }
                else
                    $resultado[] = array_merge($element, ['status' => 'Ocorreu um erro.']);
            }
        }
        return $resultado;
    }

    protected function insertElement(Array $element)
    {
        $this->data = $element;
        //$this->delete($element['id_plataforma']);

        $element['data_referencia'] = $element['data_referencia'];

        //$element['created_at'] = $this->now;
        //$element['updated_at'] = $this->now;
        $result = $this->save($element);
        return true;
    }

}
