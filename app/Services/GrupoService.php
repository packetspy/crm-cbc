<?php

namespace App\Services;

use App\Services\Service;
use App\Models\Role;

Class GrupoService extends Service
{
    public function __construct()
    {
       //
    }

    public function getGrupos($limit = 50)
    {
      return Role::paginate($limit);
    }

    public function getListGrupos()
    {
      return Role::all();
    }
}
