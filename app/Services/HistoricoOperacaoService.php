<?php

namespace App\Services;

use App\Services\Service;
use Illuminate\Database\QueryException;
use App\Models\HistoricoOperacao;

Class HistoricoOperacaoService extends Service
{
    public function __construct()
    {
        //
    }

    public static function importOperacoesPlataforma($response = true, Array $elements)
    {
        $resultado = [];
        $success = false;

        foreach($elements as $element) {
          $operacao = HistoricoOperacao::where('id_operacao', '=', $element['id'])->where('id_oferta_indicacao', '=', $element['id_oferta_indicacao'])->first();

            if ($operacao) {
              if (($element['data_alteracao'] > $operacao['data_alteracao']) && ($element['data_alteracao'] == 'Andamento')) {
                $nova_operacao = new HistoricoOperacao();
                $nova_operacao->id_operacao = $element['id'];
                $nova_operacao->id_oferta_indicacao = $element['id_oferta_indicacao'];
                $nova_operacao->tipo = $element['tipo'];
                $nova_operacao->situacao = $element['situacao'];
                $nova_operacao->produto = $element['produto'];
                $nova_operacao->id_empresa_plataforma = $element['id_empresa'];
                $nova_operacao->razao_empresa = $element['razao_empresa'];
                $nova_operacao->id_usuario_plataforma = $element['id_usuario'];
                $nova_operacao->nome_usuario = $element['nome_usuario'];
                $nova_operacao->data_inclusao = $element['data_inclusao'];
                $nova_operacao->data_validade = $element['data_alteracao'];
                $nova_operacao->data_validade = $element['data_validade'];
                $success = $nova_operacao->save();
              }
              //verificada data de alteraçao para decidir se atualiza ou nao no DB
            }else{
              $nova_operacao = new HistoricoOperacao();
              $nova_operacao->id_operacao = $element['id'];
              $nova_operacao->id_oferta_indicacao = $element['id_oferta_indicacao'];
              $nova_operacao->tipo = $element['tipo'];
              $nova_operacao->situacao = $element['situacao'];
              $nova_operacao->produto = $element['produto'];
              $nova_operacao->id_empresa_plataforma = $element['id_empresa'];
              $nova_operacao->razao_empresa = $element['razao_empresa'];
              $nova_operacao->id_usuario_plataforma = $element['id_usuario'];
              $nova_operacao->nome_usuario = $element['nome_usuario'];
              $nova_operacao->data_inclusao = $element['data_inclusao'];
              $nova_operacao->data_validade = $element['data_alteracao'];
              $nova_operacao->data_validade = $element['data_validade'];
              $success = $nova_operacao->save();
            }

            if($success)
                $resultado[] = array_merge($element, ['status' => 'Atualizado com sucesso no DB.']);
            else
                $resultado[] = array_merge($element, ['status' => 'Data de acesso igual ou mais antiga que o DB.']);
        }

        if ($response) {
          return $resultado;
        }
        return true;
    }

}
