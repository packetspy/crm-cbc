<?php

namespace App\Services;

use App\Services\Service;

use App\Helpers\Estado;

use App\Models\ProductList;
use App\Models\ProductsInList;
use App\Models\ContactProduct;
use App\Models\Product;

Class ProductService extends Service
{
    protected $rules = [
        'nome'  => 'required|max:255',
        'descricao' => 'required|min:3'
    ];
    protected $messages = [
        'product_not_found' => "Produto não encontrado"
    ];

    public function __construct($userId=null)
    {
       // $parentUserId = parent::getUserId();
        $this->userId = $userId;
    }

    public function getClienteId()
    {
        return $this->clientId;
    }
    public function getContatoId()
    {
        return $this->contactId;
    }

    public function getProdutos($limit = 10, $paginate = true)
    {
        if ($paginate)
            return Product::orderBy("id")->paginate($limit);

        if (is_null($limit))
            $produtos = Product::orderBy('nome')->get();
            return $produtos;
    }

    public function getGruposProdutos($limit = 10, $paginate = true)
    {
        if ($paginate)
            return ProductList::orderBy("id")->paginate($limit);

        if (is_null($limit))
            return ProductList::orderBy('name')->get();
    }

    public function addProdutoContato($data)
    {
      $this->clientId = $data['cliente_id'];
      $this->contactId = $data['contato_id'];

      $produtos = $this->preparaProdutos($data);
      foreach ($produtos as $produto) {

        $exist = ContactProduct::where('product_id', '=', $produto['product_id'])
            ->where('contact_id', '=', $produto['contact_id'])
            ->where('estado', '=', $produto['estado'])
            ->where('transacao', '=', $produto['transacao'])
            ->exists();

        if(!$exist){
            ContactProduct::create($produto);
        }
      }
      return true;

    }

    public function preparaProdutos($data)
    {
      if(in_array('todos', $data['estados'])){
        $estados = Estado::getKeys();
      }else{
        $estados = $data['estados'];
      }
      foreach ($estados as $estado) {
        foreach ($data['produtos_selecionados'] as $linhaproduto) {
          if ($data['tipo_operacao'] == 'compra_venda') {
            $produtos[] = [
                'product_id' => $linhaproduto,
                'contact_id' => $data['contato_id'],
                'transacao' => 'venda',
                'estado' => $estado
            ];
            $produtos[] = [
                'product_id' => $linhaproduto,
                'contact_id' => $data['contato_id'],
                'transacao' => 'compra',
                'estado' => $estado
            ];
          }else{
            $produtos[] = [
                'product_id' => $linhaproduto,
                'contact_id' => $data['contato_id'],
                'transacao' => $data['tipo_operacao'],
                'estado' => $estado
            ];
          }
        }
      }
      return $produtos;
    }


    /*  **************** Rever daqui para baixo código duvidoso ******************** */

    public function getGruposProdutosAindaNaoSeiPraQue($id)
    {
        $list = ProductList::find($id);
        if ($list) {
            $list->loadProductsIdInArray();

            return $list;
        }

        $list = new ProductList();
        return $list;
    }

    public function getProductsToExport()
    {
        $elementos = (new Product)->exportFields;
        return Product::all($elementos)->toArray();
    }

    public function getPermissions()
    {
        $permissions = [];

        $permissions[] = [
            'id' => 'product.index',
            'name' => 'Ver produtos listados'
        ];

        $permissions[] = [
            'id' => 'product.create',
            'name' => 'Criar produto'
        ];

        $permissions[] = [
            'id' => 'product.destroy',
            'name' => 'Excluir produto'
        ];

        $permissions[] = [
            'id' => 'product.edit',
            'name' => 'Editar produto'
        ];

        return $permissions;
    }

    public function ImportedData(Array $elements)
    {
        $resultado = [];

        foreach($elements as $element) {
            $this->lastProductId = null;
            if($element['id'])
                $result = $this->updateElement($element);
            else
                $result = $this->insertElement($element);

            if($this->lastProductId)
                $element['id'] = $this->lastProductId;
            unset($element[0]);
            if($result)
                $resultado[] = array_merge($element, ['status' => 'realizado com sucesso.']);
            else{
                if(!empty($this->getErrors())){
                    $msgErro = '';
                    foreach($this->getErrors() as $error)
                        $msgErro .= implode('|', $error). "|";

                    $resultado[] = array_merge($element, ['status' => $msgErro]);
                }
                else
                    $resultado[] = array_merge($element, ['status' => 'Ocorreu um erro.']);
            }
        }
        return $resultado;
    }

    protected function updateElement(Array $element)
    {
        $product = Product::find($element["id"]);
        if($product)
            return $product->fill($element)->save();

        return $this->insertElement($element);
    }

    protected function insertElement(Array $element)
    {
        unset($element["id"]);

        $this->data = $element;
        if(!$this->validateUser() || !$this->validateProduct())
            return false;

        $element['ativo'] = true;
        $result = $this->user->products()->create($element);
        $this->lastProductId = $result->id;
        return true;
    }

    /**
     * Gets the product.
     *
     * @param      integer  $id     The identifier
     *
     * @return     App\Models\Product  The product.
     */
    public function getProduct($id)
    {
        return Product::find($id);
    }


    public function searchAutoComplete($query = null)
    {
        if (!$query)
        {
            return Product::select('id', 'nome')->limit(10)->get()->toArray();
        }
        $client = Product::select('id', 'nome');
        $tags = explode(" ", $query);
        foreach($tags as $tag)
        {
            $client->orWhere('nome', 'LIKE', '%' . $query . '%');
        }
        return $client->limit(10)->get()->toArray();
    }



    /**
     * Salva um produto de acordo com o id do usuario e os dados recebidos
     *
     * @param      integer   $userId  The user identifier
     * @param      array   $data    Elements to be saved
     *
     * @return     boolean  True when save false when have some error.
     */
    public function save($data)
    {
        $this->data = $data;

        if(!$this->validateProduct() || !$this->validateUser())
            return false;

        $this->data['ativo'] = true;

        $this->user->products()->create($this->data);

        return true;
    }

    /**
     * Update an product
     *
     * @param      integer   $id      The identifier
     * @param      array     $change  The change
     *
     * @return     boolean  true when success false when error.
     */
    public function update($id, $change)
    {
        $this->data = $change;
        $product = Product::find($id);

        if($product == null) {
            $this->setError("produto", $this->messages['product_not_found']);
            return false;
        }

        if($this->validateProduct()) {
            $product->update($change);
            return true;
        }

        return false;
    }

    /**
     * deactivate an product
     *
     * @param      integer   $id     The identifier
     *
     * @return     boolean
     */
    public function delete($id)
    {
        $product = Product::find($id);
        if($product) {
            $product->ativo = false;
            return true;
        }

        $this->setError('produto', $this->messages['product_not_found']);
        return false;
    }

    public function deleteProdutosContato($data)
    {
      $produto_contato = ContactProduct::where('product_id', '=', $data['product_id'])
          ->where('contact_id', '=', $data['contact_id'])
          ->where('transacao', '=', $data['transacao'])
          ->get();

          foreach ($produto_contato as $produtodelete) {
            ContactProduct::destroy($produtodelete->id);
          }

      if(!$produto_contato) {
          return false;
      }
      return true;
    }

    /**
     * Validate if the function data match with the rules.
     *
     * @return     boolean  true when everything is ok. False when not.
     */
    protected function validateProduct()
    {
        return $this->validate($this->data, $this->rules, $this->messages);
    }

    public function createList($data)
    {
        try {
            $productList = new ProductList();
            $productList->name = $data['nome'];
            $productList->save();
            foreach($data['products'] as $product) {
                $productInList = new ProductsInList();
                $productInList->product_id = $product;
                $productInList->list_id = $productList->id;
                $productInList->save();
            }

            return true;
        } catch (\Exception $e) {
            dd($e->getMessage());
            return false;
        }

    }

    public function updateList($data)
    {
        try {
            $productList = ProductList::find($data['id']);
            if (!$productList) {
                return false;
            }

            $productList->name = $data['nome'];
            $productList->save();
            $idsInRelation = [];
            foreach ($productList->products as $productRelation) {
                $idInRelation = $productRelation->product_id;
                $idsInRelation[] = $idInRelation;
                if (!in_array($idInRelation, $data['products'])) {
                    $productRelation->delete();
                }
            }

            foreach ($data['products'] as $product) {
                if(in_array($product, $idsInRelation)) {
                    continue;
                }

                $productInList = new ProductsInList();
                $productInList->product_id = $product;
                $productInList->list_id = $productList->id;
                $productInList->save();
            }

            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    public function getLists($limit = 10, $paginate = true)
    {

        if ($paginate) {
            $packages = ProductList::orderBy("id")->paginate($limit);
            foreach($packages as $package) {
                $package->loadProductsIdInArray();
            }
            return $packages;
        }


        if (is_null($limit)){
            $packages = ProductList::orderBy('name')->get();
            foreach($packages as $package) {
                $package->loadProductsIdInArray();
            }
            return $packages;
        }

    }

    public function searchListAutoComplete($query = null)
    {
        if (!$query) {
            return ProductList::select('id', 'name')->limit(10)->get()->toArray();
        }

        $productList = ProductList::select('id', 'name');

        $searchs = explode(" ", $query);

        foreach($searchs as $oneWord) {
            $productList->orWhere('name', 'LIKE', '%' . $oneWord . '%');
        }

        return $productList->limit(10)->get()->toArray();
    }


}
