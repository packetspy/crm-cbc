<?php

namespace App\Services;

use App\Services\Service;
use App\Models\Origem;

Class OrigemService extends Service
{
    public function __construct()
    {
       //
    }

    /**
     * Gets the status.
     * @param      string  $order_by  the column
     * @param      string  $order direction
     * @return     array   The status.
     */
    public function getOrigem($order_by = null, $order = null)
    {
      if(!$order_by){
        return $status = Origem::orderBy('origem', 'asc')->get();
      }else{
        return $status = Origem::orderBy($order_by, $order)->get();
      }
      return $status = Origem::all();
    }
}
