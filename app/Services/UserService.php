<?php
namespace App\Services;

use App\Models\Interaction;
use Carbon\Carbon;
use App\Services\Service;
use Artesaos\Defender\Facades\Defender;
use Artesaos\Defender\Permission;

use App\Models\User;
use App\Models\Client;
use App\Models\Role;

use Regulus\ActivityLog\Models\Activity as Activity;

class UserService extends Service
{

    protected $clientId = null;
    protected $rules = [
        'fname' => 'required|max:255',
        'lname' => 'required|max:255',
        'email' => 'required|email|max:255|unique:email',
        'password' => 'min:6',
    ];
    protected $messages = [
        "user_not_found" => 'Usuário não encontrado',
        "permission_not_set" => 'Permissão não foi setada'
    ];
    protected $data;
    protected $updatedPermissions = false;

    public function __construct($userId = null)
    {
        $this->userId = $userId;
    }

    public function getUsuarios($limit = 50){
      return $users = User::paginate($limit);
    }

    public function getUsuario($usuario_id){
      return $users = User::find($usuario_id);
    }

    /**
     * @param $data
     * @return bool
     */
    public function save($data)
    {
        $this->data = $data;

        if ( ! $this->validateData())
            return false;

        $usuario = new User();
        $data['password'] = bcrypt($data['password']);
        $data['remember_token'] = env('PASSWORD_HASH', '123');
        $data['name'] = $data['fname'].' '.$data['lname'];
        $usuario->fill($data);

        if ($usuario->save()) {
          $usuario->syncRoles($this->data['roles']);
            if ( isset($data['permissions']) )
                return $this->savePermissions($usuario->id, $data['permissions']);
            return true;
        }
        return false;
    }

    /**
     * @param $id_user
     * @param array $permissions
     * @return bool
     */
    public function savePermissions($id_user, array $permissions)
    {
        $usuario = User::find($id_user);
        if ( !$usuario ) {
            $this->setError("user_not_found", $this->messages['user_not_found']);
            return false;
        }

        if ( count ( $permissions ) <= 0 )
        {
            $this->setError("permission_not_set", $this->messages['permission_not_set']);
            return false;
        }

        foreach ($permissions as $permission)
        {
            $currentPermission = Defender::findPermission($permission);
            $usuario->attachPermission($currentPermission);
        }
        return true;
    }

    public function validateData($userId = null)
    {
        $userId = $userId ?: $this->userId;
        if ($userId !== null) {
            $this->rules['password'] = 'min:6|confirmed';
            $this->rules['email'] = 'required|email|max:255|unique:users,email,' . $userId;
        }

        return $this->validate($this->data, $this->rules, $this->messages);
    }


    public function updateUser($userId, array $input)
    {
        $this->data = $input;

        if  (!$this->validateData($userId))
            return false;

        $usuario = $this->getUserById($userId);
        if (!$usuario)
            return false;

        $usuario->email = $this->data['email'];
        $usuario->name  = $this->data['fname'].' '.$this->data['lname'];
        $usuario->fname  = $this->data['fname'];
        $usuario->lname  = $this->data['lname'];
        if (!empty($this->data['password']) && $this->data['password'] === $this->data['password_confirmation'])
            $usuario->password = bcrypt($this->data['password']);

        $usuario->save();
        $usuario->syncRoles($this->data['roles']);

        if ( !isset($this->data['permissions']) )
            $usuario->revokePermissions();
        else
            $this->updatePermissions($usuario->id, $this->data['permissions']);

        return true;
    }

    public function updatePermissions($id_user, array $permissions)
    {
        $usuario = $this->getUserById($id_user);

        if ( !$usuario ) {
            $this->setError("user_not_found", "Usuário não encontrado!");
            return false;
        }


        if (count($permissions) < 1) {
            $this->setError("permission_not_set", "Nenhuma permissão foi setada!");
            return false;
        }

        $usuario->revokePermissions();

        foreach ($permissions as $permission) {
            $currentPermission = Defender::findPermission($permission);
            if ($currentPermission)
                $usuario->attachPermission($currentPermission);
        }

        return true;
    }

    public function getUserById($id)
    {
        $usuario = User::find($id);
        return $usuario;
    }

    public function getConsultores()
    {
        $consultores = User::whereHas(
            'roles', function($q){
                $q->where('name', 'Consultores')
                ->orWhere('name', 'Hábito')
                ->orWhere('name', 'Liquidez')
                ->orWhere('name', 'Filial:Consultor');
            }
        )->get();
        return $consultores;
    }

    public function getAllRoles()
    {
      $roles = Role::all();
      return $roles;
    }

    public function getLogs($id)
    {
        $logs = Activity::where('user_id', '=', $id)->paginate(15);
        return $logs;
    }

    public function getStatistics($id = null, $start = '', $end = '', $dashboard=false)
    {

      /*  $where = [
            'user_id' => $id
        ];

        $empresasCadastradas = Client::where('user_id', '=', $id);
        $interacoes = Interaction::where('user_id', '=', $id);
        $negociosFechados = RelatorioService::indicadorNegocioFechado($where);
        $interacaoAtualizadas = RelatorioService::indicadorInteracaoAtualizacao($where);

        if($start!='') {
          $empresasCadastradas  = $empresasCadastradas->where('created_at', '>=', $start);
          $interacoes = $interacoes->where('created_at', '>=', $start);
        }

        if($end!='') {
          $empresasCadastradas  = $empresasCadastradas->where('created_at', '<=', $end);
          $interacoes = $interacoes->where('created_at', '<=', $end);
        }

        $empresasCadastradas = $empresasCadastradas->count();
        $empresasTotal = Client::count();
        $interacoes = $interacoes->count();

        $porcentagemNegocioFechado = ceil($negociosFechados / $interacoes * 100);

        return [
            'empresasCadastradas' => $empresasCadastradas,
            'interacoesCadastradas' => $interacoes,
            'empresasTotal' => $empresasTotal,
            'interacaoAtualizadas' => $interacaoAtualizadas,
            'negociosFechados' => $negociosFechados,
            'porcentagemNegocioFechado' => $porcentagemNegocioFechado
        ];
*/

        $where = [];
        if ($id != null) {
            $where['user_id'] = $id;
        }
        if ($start != '') {
            $where['start'] = $start;
        }
        if ($end != '') {
            $where['end'] = $end;
        }

        $indicadores = RelatorioService::indicadoresGeraisInteracoes($where)[0];

        $qtd_interacao = Interaction::count();

        if ($dashboard == true)
            $qtd_interacao = Interaction::where('user_id', '=', $where['user_id'])->count();

        $qtd_interacao = ($qtd_interacao < 1) ? 1 : $qtd_interacao;


        $interacoes = [
            'interacao_total_sistema' => $qtd_interacao,
            'interacao_negocio_nao_fechado_detalhado' => RelatorioService::indicadorNegocioNaoFechadoDetalhado(array_merge($where, ['group_by' => 'interactions.interaction_type_id'])),
            'pct_interacoes_iniciadas' => round($indicadores['interacoes_iniciadas_total'] / $qtd_interacao * 100, 2),
            'pct_interacoes_atualizadas' => round($indicadores['interacoes_iniciadas_atualizadas'] / $qtd_interacao * 100, 2),
            'pct_interacoes_tempestiva' => round($indicadores['interacoes_com_data_tempestivas'] / $qtd_interacao * 100, 2),
            'pct_interacoes_com_data_no_horario' => round($indicadores['interacoes_com_data_no_horario'] / $qtd_interacao * 100, 2),
            'pct_interacoes_negocios_gerados' => round($indicadores['negocios_gerados'] / $qtd_interacao * 100, 2),
            'pct_interacoes_negocios_nao_fechados' => round($indicadores['negocios_nao_fechados'] / $qtd_interacao * 100, 2),

        ];


        return array_merge($indicadores, $interacoes);


    }

    public function getUserWithPermission($permission)
    {
        if (!$permission)
            return [];

        $permission = Permission::where('name', '=', $permission)->first();

        if(!$permission)
            return [];

        return $permission->users()->getResults();
    }

    public function deleteUser($id)
    {
        $user = User::find($id);

        if($user){
          return $user->delete();
        }else{
          return false;
        }
    }



    public function getPermissions()
    {
        return [
            [
                'id' => 'user.index',
                'name' => 'Ver lista de usuários'
            ],
            [
                'id' => 'user.create.all',
                'name' => 'Criar usuário - Todos os níveis',
            ],
            /*[
                'id' => 'user.create.comercial',
                'name' => 'Criar usuário - Comercial'
            ],*/
            [
                'id' => 'user.edit',
                'name' => 'Editar usuário'
            ],
            [
                'id' => 'user.delete',
                'name' => 'Excluir usuário'
            ],
            [
                'id' => 'user.logs',
                'name' => 'Ver logs de usuário'
            ],
            [
                'id' => 'user.estatisticas.gerenciais',
                'name' => 'Estatísticas Gerenciais'
            ],
            [
                'id' => 'user.relatorios',
                'name' => 'Gerar relatórios'
            ],
        ];
   }
}
