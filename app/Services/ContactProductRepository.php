<?php

namespace App\Services;

use App\Models\ContactProduct;
use App\Models\Product;

class ContactProductRepository
{

    private $tiposWhere = [];

    public function getProductsFromContact($contactId, $transacao = '')
    {
        $products = [];
        $intermediate = ContactProduct::where('contact_id', '=', $contactId);
        $intermediate = $this->whereTransactionType($intermediate, $transacao);
        $result = $intermediate->get();
        foreach($result as $row) {
            $products[] = $row->product;
        }
        return $products;
    }

    public function searchContacts($transacao = 'venda', $products = null, $states = null)
    {

        $query = ContactProduct::where('transacao', '=', $transacao);

        if (!is_null($products)) {
            $query = $query->whereIn('product_id', $products);
        }

        if (!is_null($states)) {
            $query = $query->whereIn('estado', $states);

          }

        $query = $query->groupBy(['contact_products.product_id', 'contact_products.contact_id', 'contact_products.transacao']);
        $query = $query->select('contact_products.*', \DB::raw("(GROUP_CONCAT(DISTINCT estado SEPARATOR ', ')) as estados"));

        return $query;
    }

    public function getContactsWithProduct($productId, $transacao = '')
    {
        $contacts = [];
        $intermediate = ContactProduct::where('product_id', '=', $productId);
        $intermediate = $this->whereTransactionType($intermediate, $transacao);
        $result = $intermediate->get();

        foreach($result as $row) {
            $contacts[] = $row->contact;
        }

        return $contacts;
    }

    public function save($productId, $contactId, $data)
    {
        $data = array_merge($data, ['product_id'=> $productId, 'contact_id' => $contactId]);

        $exist = ContactProduct::where('product_id', '=', $productId)
            ->where('estado', '=', $data['estado'])
            ->where('transacao', '=', $data['transacao'])
            ->where('contact_id', '=', $contactId)
            ->exists();

        if($exist){
            return true;
        }

        if(ContactProduct::create($data)){
            return true;
        }

        return false;
    }

    public function deleteProduct($contactId, $productId, $estado, $transacao)
    {
        $products = ContactProduct::where('contact_id', '=', $contactId)
                            ->where('product_id', '=', $productId)
                            ->where('estado', '=', $estado)
                            ->where('transacao', '=', $transacao)
                            ->first();


        if ($products) {
            $products->delete();
            return true;
        }
        return false;
        /*foreach($products as $product)
            $product->delete();*/
    }

    protected function whereTransactionType($contactProduct, $transaction)
    {
        if($transaction == '')
            return $contactProduct;

        return $contactProduct->where('transacao', '=', $transaction);
    }
}
