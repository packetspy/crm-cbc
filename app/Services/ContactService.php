<?php

namespace App\Services;

use App\Jobs\UpdateIbm;
use App\Services\Service;
use App\Helpers\Estado;
use App\Services\ClientService;
use App\Services\ContactProductRepository;

use App\Models\ContactProduct;
use App\Models\Client;
use App\Models\Contact;
use App\Models\ContactInfo;
use App\Models\Origem;
use App\Models\Product;
use App\Models\Telefone;
use App\Models\Email;

Class ContactService extends Service
{
    protected $contactProduct = null;
    protected $cliente = null;
    public $clientId = null;
    public $contactId = null;

    protected $rules = [
        'nome' => 'required'
    ];
    protected $messages = [
      'not_found' => 'Nenhum contato encontrado.',
    ];

    public function __construct($userId = null)
    {
        $this->userId = $userId;
        $this->clientService = new ClientService($userId);
        $this->contactProduct = new ContactProductRepository();
    }

    public function getClienteId()
    {
        return $this->clientId;
    }
    public function getContatoId()
    {
        return $this->contactId;
    }

    public function Exist($contato_id)
    {
      $contato = Contact::find($contato_id);
      if ($contato) {
          return true;
      }else{
        return false;
      }
    }

    /**
     * Validate if the function data match with the rules.
     *
     * @return     boolean  true when everything is ok. False when not.
     */
    protected function validateContato()
    {
        return $this->validate($this->data, $this->rules, $this->messages);
    }

    public function sanitizeData($data = array())
    {
        foreach ($data as $key => $value)
        {
            if ($key != '_token'){
                $data[$key] = trim(preg_replace('/\t+/', '', $value));
              }
        }
        return $data;
    }

    //lista os contatos de um clientes especifico
    public function getContatoCliente($cliente_id)
    {
        $contatos = Contact::with(['intermediate_products' => function($query){
          $query->select("contact_id", "product_id", "transacao", \DB::raw("(GROUP_CONCAT(DISTINCT estado SEPARATOR ', ')) as estados"))
              ->groupBy(['product_id', 'contact_id', 'transacao']);
        }])
        ->where('client_id', '=', $cliente_id)
        ->get();

        return $contatos;
    }

    public function update($data, $contato_id)
    {
        $this->data = $data;
        $contato = Contact::find($contato_id);
        $cliente_id = $contato->client_id;

        //dump($contato->client_id);

        if(!$contato) {
            $this->setError("contato", $this->messages['not_found']);
            return false;
        }else{
            $cliente = $this->clientService->Exist($cliente_id);
            $this->clientId = $cliente_id;
        }

        if(!$cliente) {
            $this->setError("cliente", $this->clientService->messages['not_found']);
            return false;
        }

        if($this->validateContato()) {
            $data = $this->sanitizeData($data);

            $contato->nome 	= $data['nome'];
        	$contato->departamento		= $data['departamento'];
            $contato->origem_id		= $data['origem_id'];
        	$contato->updated_at 	= $this->getNow();
        	$success = $contato->save();
            $this->contactId = $contato->id;

            if($success){
                return true;
            }
        }
        return false;
    }

    public function salvarContatos($cliente_id, $dados)
    {
        $this->data = $dados;
        $this->idCliente = $cliente_id;
        $plataformaId = null;

        if(!$this->validationsForSave() || !$this->validate($dados, $this->rules))
            return false;

        if(isset($dados['id_plataforma']))
            $plataformaId = $dados['id_plataforma'];

        $dadosContato = [
            'user_id'      => $this->userId,
            'nome'         => $dados['nome'],
            'departamento' => $dados['departamento'],
            'id_plataforma' => $plataformaId
        ];

        $contact = $this->cliente->contacts()->create($dadosContato);

        foreach ($dados['contatos'] as $contatoInfo) {
            $this->insertContatoInfo($contact, $contatoInfo);
        }


        if (isset($dados['produtos'])){
            foreach($dados['produtos'] as $product) {
                $this->contactProduct->save($product['product_id'], $contact->id, $product);
            }
        }

        return true;
    }

    /**
     * Salva novo cliente no banco de dados
     *
     * @param      array   $data   The data
     *
     * @return     boolean
     */
    public function adicionaContato($data)
    {
        $this->data = $data;
        $this->clientId = $this->data['cliente_id'];

        //Cria novo contato para o cliente
        $contato = [
            'nome' => $this->data['nome'],
            'departamento' => $this->data['departamento'],
            'user_id' => \Auth::user()->id,
            'escritorio_id' => \Auth::user()->escritorio_id,
            'origem_id' => 2,
            'client_id' => $this->clientId
        ];
        $novoContato = Contact::create($contato);
        $this->contactId = $novoContato->id;

        //Cria EMAIL contato para o lead
        if ($this->data['email'] != '') {
          $email = [
              'contato' => $this->data['email'],
              'tipo' => 'email',
              'contact_id' => $novoContato->id,
              'created_at' => $this->now,
          ];
          Email::create($email);
        }

        //Cria TELEFONE contato para o lead
        if ($this->data['telefone'] != '') {
          $telefone = [
              'contato' => $this->data['telefone'],
              'tipo' => 'telefone',
              'contact_id' => $novoContato->id,
              'created_at' => $this->now,
          ];
          Telefone::create($telefone);
        }
        return true;
    }

    public function excluirContato($data)
    {
      $this->data = $data;
      $this->clientId = $this->data['cliente_id'];
      $this->contactId = $this->data['contato_id'];

      //Deleção recursiva via "model event deleting"
      $contato = Contact::destroy($this->contactId);

      if ($contato) {
        return true;
      }
      return false;
    }

    public function moverContato($request)
    {
      $contato_id = $request['contato_id'];
      $cliente_id = $request['cliente_id'];

      $contato = Contact::find($contato_id);
      if(!$contato) {
          return false;
      }else{
        $contato->client_id = $cliente_id;
        $contato->save();
        $this->clientId = $cliente_id;
        return true;
      }

    }

    public function getProducts($contactId)
    {
        if ( is_null ($contactId ) )
            return [];

        //,DB::raw("(GROUP_CONCAT(items_city.name SEPARATOR '@')) as `cities`"))
        return ContactProduct::select("contact_id", "product_id", "transacao", \DB::raw("(GROUP_CONCAT(DISTINCT estado SEPARATOR ', ')) as estados"))
            ->groupBy(['product_id', 'contact_id', 'transacao'])
            ->where('contact_id', '=', $contactId)
            ->get();
    }

    public function getContatos($idCliente)
    {
        $retorno = [];

        $cliente = $this->clientService->getClient($idCliente);
        if(!$cliente)
            return $retorno;

        foreach($cliente->contacts as $contact)
        {
            $dados = [];
            $dados["contact_id"] = $contact->id;
            $dados["id_plataforma"] = $contact->id_plataforma;
            $dados["client_id"] = $contact->client_id;
            $dados["nome"] = $contact->nome;
            $dados["departamento"] = $contact->departamento;
            $dados["created_at"] = $contact->datacriacao;
            $dados["origem"] = $contact->origem->origem;
            $dados["contatos"] = $contact->contact_infos->toArray();
            $dados["produtos"] = $this->getProducts($contact->id);
            $retorno[] = $dados;
        }

        return $retorno;
    }

    public function BuscaContatos($query, $limit, $status, $order_by, $order)
    {
      if ($query){
      $cliente = Contact::select('id', 'nome', 'departamento', 'client_id');
      $cliente = $cliente->where('nome', 'LIKE', '%'. $query .'%');
      $cliente = $cliente->orderBy($order_by, $order);
      $cliente = $cliente->with('client');
      $cliente = $cliente->with('contact_infos');
      $cliente = $cliente->with('intermediate_products');
      return $cliente->paginate($limit);
      }
    }

    public function deleteContato($idContato)
    {
        $contato = Contact::find($idContato);

        if(!$contato)
            return false;

        return $contato->delete();
    }

    protected function validationsForSave()
    {
        $this->cliente = $this->clientService->getClient($this->idCliente);

        if(!$this->cliente) {
            $this->setError('cliente', "Cliente não encontrado");
            return false;
        }
        return true;
    }

    public function getContato($client_id, $id)
    {
        return Contact::where(['client_id' => $client_id, 'id' => $id])->first();
    }

    public static function getContactsToExport()
    {
        $elementos = (new Contact)->exportFields;
        //remove o campo de data_ultimo_login da consulta
        array_pop($elementos);
        $result = \DB::Table('contacts')
           ->leftjoin('clients', 'contacts.client_id', '=','clients.id')
           ->leftjoin('users as user_responsavel', 'clients.responsavel_id', '=','user_responsavel.id')
           ->leftjoin('escritorios', 'clients.escritorio_id', '=','escritorios.id')
           ->leftjoin('status', 'clients.status_id', '=','status.id')
           ->leftjoin('origem', 'clients.origem_id', '=','origem.id')
           ->leftjoin('tipoempresa', 'clients.tipoempresa_id', '=','tipoempresa.id')
           ->leftjoin('contact_infos', 'contact_infos.contact_id', '=', 'contacts.id')
           ->leftjoin('contact_infos as e1', 'e1.id', '=', \DB::raw('(select ci.id FROM contact_infos as ci WHERE ci.contact_id = contacts.id AND tipo = "email" LIMIT 0,1)'))
           ->leftjoin('contact_infos as e2', 'e2.id', '=', \DB::raw('(select ci.id FROM contact_infos as ci WHERE ci.contact_id = contacts.id AND tipo = "email" LIMIT 1,1)'))
           ->leftjoin('contact_infos as e3', 'e3.id', '=', \DB::raw('(select ci.id FROM contact_infos as ci WHERE ci.contact_id = contacts.id AND tipo = "email" LIMIT 2,1)'))
           ->leftjoin('contact_infos as t1', 't1.id', '=', \DB::raw('(select ci.id FROM contact_infos as ci WHERE ci.contact_id = contacts.id AND tipo = "telefone" LIMIT 0,1)'))
           ->leftjoin('contact_infos as t2', 't2.id', '=', \DB::raw('(select ci.id FROM contact_infos as ci WHERE ci.contact_id = contacts.id AND tipo = "telefone" LIMIT 1,1)'))
           ->leftjoin('contact_infos as t3', 't3.id', '=', \DB::raw('(select ci.id FROM contact_infos as ci WHERE ci.contact_id = contacts.id AND tipo = "telefone" LIMIT 2,1)'))
           //Pau de timeout neste relatorio
           //->leftjoin('historico_login', 'historico_login.id_usuario_plataforma', '=', \DB::raw('(select hl.id FROM historico_login as hl WHERE hl.id_usuario_plataforma = contacts.id_plataforma)'))
           //->leftjoin('historico_login', 'historico_login.id_usuario_plataforma', '=','contacts.id_plataforma')
           //->join('historico_login', 'historico_login.id_empresa_plataforma', '=', 'empresa_plataforma_id')
           ->select($elementos)
           ->groupBy('contacts.id')
           ->whereNull('contacts.deleted_at')
           ->whereNull('contact_infos.deleted_at')
           ->get();

        return $result;
    }

    public static function getContactsToIbm($cliente_id=null, $contato_id=null)
    {
        $elementos = (new Contact)->exportFields;

        if ($cliente_id != null) {
            $result = \DB::Table('contacts')
               ->leftjoin('clients', 'contacts.client_id', '=','clients.id')
               ->leftjoin('users as user_responsavel', 'clients.responsavel_id', '=','user_responsavel.id')
               ->leftjoin('escritorios', 'clients.escritorio_id', '=','escritorios.id')
               ->leftjoin('status', 'clients.status_id', '=','status.id')
               ->leftjoin('origem', 'clients.origem_id', '=','origem.id')
               ->leftjoin('tipoempresa', 'clients.tipoempresa_id', '=','tipoempresa.id')
               ->leftjoin('contact_infos', 'contact_infos.contact_id', '=', 'contacts.id')
               ->leftjoin('contact_infos as e1', 'e1.id', '=', \DB::raw('(select ci.id FROM contact_infos as ci WHERE ci.contact_id = contacts.id AND tipo = "email" LIMIT 0,1)'))
               ->leftjoin('contact_infos as e2', 'e2.id', '=', \DB::raw('(select ci.id FROM contact_infos as ci WHERE ci.contact_id = contacts.id AND tipo = "email" LIMIT 1,1)'))
               ->leftjoin('contact_infos as e3', 'e3.id', '=', \DB::raw('(select ci.id FROM contact_infos as ci WHERE ci.contact_id = contacts.id AND tipo = "email" LIMIT 2,1)'))
               ->leftjoin('contact_infos as t1', 't1.id', '=', \DB::raw('(select ci.id FROM contact_infos as ci WHERE ci.contact_id = contacts.id AND tipo = "telefone" LIMIT 0,1)'))
               ->leftjoin('contact_infos as t2', 't2.id', '=', \DB::raw('(select ci.id FROM contact_infos as ci WHERE ci.contact_id = contacts.id AND tipo = "telefone" LIMIT 1,1)'))
               ->leftjoin('contact_infos as t3', 't3.id', '=', \DB::raw('(select ci.id FROM contact_infos as ci WHERE ci.contact_id = contacts.id AND tipo = "telefone" LIMIT 2,1)'))
               ->leftjoin('historico_login', 'historico_login.id_usuario_plataforma', '=','contacts.id_plataforma')
               ->select($elementos)
               ->groupBy('contacts.id')
               ->whereNull('contacts.deleted_at')
               ->whereNull('contact_infos.deleted_at')
               ->where('contacts.client_id', '=', $cliente_id)
               ->get();
            return $result;
        }elseif ($contato_id != null) {
            $result = \DB::Table('contacts')
               ->leftjoin('clients', 'contacts.client_id', '=','clients.id')
               ->leftjoin('users as user_responsavel', 'clients.responsavel_id', '=','user_responsavel.id')
               ->leftjoin('escritorios', 'clients.escritorio_id', '=','escritorios.id')
               ->leftjoin('status', 'clients.status_id', '=','status.id')
               ->leftjoin('origem', 'clients.origem_id', '=','origem.id')
               ->leftjoin('tipoempresa', 'clients.tipoempresa_id', '=','tipoempresa.id')
               ->leftjoin('contact_infos', 'contact_infos.contact_id', '=', 'contacts.id')
               ->leftjoin('contact_infos as e1', 'e1.id', '=', \DB::raw('(select ci.id FROM contact_infos as ci WHERE ci.contact_id = contacts.id AND tipo = "email" LIMIT 0,1)'))
               ->leftjoin('contact_infos as e2', 'e2.id', '=', \DB::raw('(select ci.id FROM contact_infos as ci WHERE ci.contact_id = contacts.id AND tipo = "email" LIMIT 1,1)'))
               ->leftjoin('contact_infos as e3', 'e3.id', '=', \DB::raw('(select ci.id FROM contact_infos as ci WHERE ci.contact_id = contacts.id AND tipo = "email" LIMIT 2,1)'))
               ->leftjoin('contact_infos as t1', 't1.id', '=', \DB::raw('(select ci.id FROM contact_infos as ci WHERE ci.contact_id = contacts.id AND tipo = "telefone" LIMIT 0,1)'))
               ->leftjoin('contact_infos as t2', 't2.id', '=', \DB::raw('(select ci.id FROM contact_infos as ci WHERE ci.contact_id = contacts.id AND tipo = "telefone" LIMIT 1,1)'))
               ->leftjoin('contact_infos as t3', 't3.id', '=', \DB::raw('(select ci.id FROM contact_infos as ci WHERE ci.contact_id = contacts.id AND tipo = "telefone" LIMIT 2,1)'))
               ->leftjoin('historico_login', 'historico_login.id_usuario_plataforma', '=','contacts.id_plataforma')
               ->select($elementos)
               ->groupBy('contacts.id')
               ->whereNull('contacts.deleted_at')
               ->whereNull('contact_infos.deleted_at')
               ->where('contacts.id', '=', $contato_id)
               ->get();
            return $result;
        }
        return false;
    }

    public function updateOrigemLeads(Array $data){
      $contatos = [];
      $naocontatos = [];
      $produtos_compra = [];
      $produtos_venda = [];
      $states = Estado::getKeys();
      $planilha = $data;
      $chars = array('[',']','(',')','{','}','/','|','-',' ');

      $contatos[] = self::AtualizaLeads($planilha, $states, $chars);

      return $contatos;
    }

    public function sincronizaInfoContato($response = true, Array $elements)
    {
        $resultado = [];

        foreach($elements as $element) {
          $contato_selecionado = Contact::whereHas('email', function ($query) use ($element) {
              $query->where('contato', '=', $element['email']);
          })->first();

          if ($contato_selecionado) {
            $contato_selecionado->id_plataforma = $element['id_usuario'];
            $contato_selecionado->nome_plataforma = $element['nome_usuario'];
            $contato_selecionado->cpf = $element['documento'];
            $contato_selecionado->perfil_plataforma = $element['perfil'];
            $contato_selecionado->datacadastro_plataforma = $element['data_inclusao'];
            $success = $contato_selecionado->save();

            if($success)
                $resultado[] = array_merge($element, ['status' => 'Atualizado com sucesso.']);
            else
                $resultado[] = array_merge($element, ['status' => 'Ocorreu um erro ao atualizar.']);
          }
        }

        if ($response) {
          return $resultado;
        }
        return true;
    }

    public function importedData(Array $data)
    {
        $contacts = [];
        $newContacts = [];
        foreach($data as $key => $row) {
            if($row['contato_id'] != '') {
                if(!isset($contacts[$row['contato_id']])) {
                    $contacts[$row['contato_id']] = $row;
                    $contacts[$row['contato_id']]['key_export'] = [];
                    $contacts[$row['contato_id']]['contatos'] = [];
                }
                $contacts[$row['contato_id']]['key_export'][] = $key;
                $contacts[$row['contato_id']]['contatos'][] = $this->organizaContatos($row);
            }else{
                if(!isset($newContacts[$row['cliente_id'].$row['nome']])){
                    $newContacts[$row['cliente_id'].$row['nome']] = $row;
                    $newContacts[$row['cliente_id'].$row['nome']]['key_export'] = [];
                    $newContacts[$row['cliente_id'].$row['nome']]['contatos'] = [];
                }

                $newContacts[$row['cliente_id'].$row['nome']]['key_export'][] = $key;
                $newContacts[$row['cliente_id'].$row['nome']]['contatos'][] = $this->organizaContatos($row);
            }
        }

        foreach($contacts as $contact){
            if($this->updateByImport($contact)) {
                foreach($contact['key_export'] as $key)
                    $data[$key]['status'] = "Realizado com sucesso";
            } else {
                foreach($contact['key_export'] as $key)
                    $data[$key]['status'] = "Ocorreu um erro durante a importação.";
            }
        }

        foreach($newContacts as $contact){
            $this->lastContactId = null;
            if($this->insertByImport($contact)) {
                foreach($contact['key_export'] as $key){
                    $data[$key]['contato_id'] = $this->lastContactId;
                    $data[$key]['status'] = "Realizado com sucesso";
                }
            }else{
                foreach($contact['key_export'] as $key)
                $data[$key]['status'] = "Ocorreu um erro durante a importação.";
            }
        }
        return $data;
    }

    protected function updateByImport(Array $raw)
    {
        $update = $this->organizaForImport($raw);
        $contact = Contact::find($raw['contato_id']);
        if($contact) {
            foreach($update['contatos'] as $contatoInfo) {
                $this->insertContatoInfo($contact, $contatoInfo);
            }

            unset($update['contatos']);
            return $contact->fill($update)->save();
        }

        return false;
    }

    protected function insertByImport(Array $raw)
    {
        $insert = $this->organizaForImport($raw);
        $contatoInfos = $insert['contatos'];
        unset($insert['contatos']);
        $contact = Contact::create($insert);
        if(!$contact) {
            return false;
        }

        foreach($contatoInfos as $contato) {
            $this->insertContatoInfo($contact, $contato);
        }

        $this->lastContactId = $contact->id;
        return true;
    }

    protected function organizaForImport($raw)
    {
        return [
            'client_id' => $raw['cliente_id'],
            'nome'      => $raw['nome'],
            'departamento' => $raw['departamento'],
            'contatos'  => $raw['contatos'],
            'user_id'   => $this->userId,
            "id_plataforma" => $raw["id_plataforma"]
        ];
    }

    protected function organizaContatos($row)
    {
        $contactInfoArray = [];
        $contactInfoArray['contact_info_id'] = $row['contact_info_id'];
        $contactInfoArray['tipo'] = $row['tipo'];
        $contactInfoArray['contato'] = $row['contato'];
        return $contactInfoArray;
    }

    protected function insertContatoInfo($contact, $contatoInfo)
    {
        if(!$contatoInfo['contact_info_id']) {
            unset($contatoInfo['contact_info_id']);
            if(strlen(trim($contatoInfo['contato'])) > 0 ) {
                $contact->contact_infos()->create($contatoInfo);
            }
            return true;
        }

        $contactInfoElement = ContactInfo::find($contatoInfo['contact_info_id']);
        if ($contactInfoElement) {
            $contactInfoElement->tipo = $contatoInfo['tipo'];
            $contactInfoElement->contato = $contatoInfo['contato'];
            $contactInfoElement->save();
            return true;
        }

        return false;
    }

    public function getPermissions()
    {
        return [
            [
                'id' => 'contact.index',
                'name' => 'Ver todos os contatos',
                'help' => 'Usuário vê todos as formas de contato do cliente'
            ],
            [
                'id' => 'contact.destroy',
                'name' => 'Excluir um contato',
                'help' => 'Usuário pode excluir um contato do cliente'
            ],
            [
                'id' => 'contact.move',
                'name' => 'Mover um contato',
                'help' => 'Usuário pode mover um contato do cliente'
            ]
        ];
    }
}
