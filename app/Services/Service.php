<?php

namespace App\Services;

use App\Models\User;
use Carbon\Carbon;

Class Service
{
    protected $now;
    protected $errors = [];
    protected $user = null;
    protected $userId = null;
    protected $escritorioId = null;
    protected $data = [];
    protected $messagesDefault = [
        'user'    => 'Usuário não encontrado',
        'required'  => 'Campo :attribute é obrigatório',
        'min'     => 'Campo :attribute deve possuir um tamanho mínimo de :min',
        'email'   => 'Campo :attribute preenchido de forma incorreta',
        'after'   => 'Campo :attribute não pode ser anterior a hoje',
        'boolean' => 'Campo :attribute deverá ser 0 ou 1',
        'url' => 'Campo :attribute deve ser um URL válido, no formato http:// ou https://',
    ];

    public function __construct()
    {
      //
    }

    public function getNow()
    {
      $this->now = Carbon::now('America/Sao_Paulo');
      return $this->now;
    }

    /**
     * Gets the errors.
     *
     * @return     <type>  The errors.
     */
    public function getErrors()
    {
        return $this->errors;
    }

    public function setError($indice, $error)
    {
        $this->errors = [$indice => [$error]];
    }

    public function setErrors($errors)
    {
        $this->errors = $errors;
    }

    /**
     * Sets the user identifier.
     *
     * @param      integer  $userId  The user identifier
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }
    public function setEscritorioId($userId)
    {
        $this->escritorioId = $escritorioId;
    }

    /**
     * ValidateUser
     *
     * Check if the id passed in construct or setUserId is a valid User.
     *
     * @return     boolean  true when user exist false when not.
     */
    protected function validateUser()
    {
        if(\Auth::user()->id)
            return true;

        $this->setError("usuario", $this->messagesDefault['user']);
        return false;
    }

    public function validate($accepted, $rules, $messages = [])
    {
        $validation = \Validator::make($accepted, $rules, array_merge($this->messagesDefault, $messages));

        if($validation->fails()) {
            $this->setErrors($validation->errors()->getMessages());
            return false;
        }
        return true;
    }
}
