<?php

namespace App\Services;

use App\Services\Service;
use App\Services\ClientService;

use App\Models\Client;
use App\Models\Cobranca;
use App\Models\HistoricoCobranca;

Class CobrancaService extends Service
{
    protected $clientService;
    protected $status_id;
    protected $now;
    protected $rules = [];
    public $messages = [];

    public function __construct()
    {
        $this->clientService = new ClientService();
        $this->status_id = null;
        $this->now = $this->getNow();
    }

    public function Exist($contato_id)
    {
      $contato = Contact::find($contato_id);
      if ($contato) {
          return true;
      }else{
        return false;
      }
    }

    public function setStatus($status_id)
    {
        $this->status_id = $status_id;
    }

    public function getStatus()
    {
      return $this->status_id;
    }

    public function updateStatusFinanceiro($cliente_id, $status_id)
    {
        if ($this->getStatus() != $status_id) {
            $update_cliente = Client::find($cliente_id);
            $update_cliente->status_id = $status_id;
            $update_cliente->save();
            return true;
        }
      return false;
    }

    public function getDadosCobranca($cliente_id)
    {
        $dados_cobranca = Cobranca::select('id','valor','vencimento','status_id','formapagamento_id','recorrencia_id')
        ->where('client_id', '=', $cliente_id)
        ->first();
        if (!$dados_cobranca) {
            $this->setStatus($this->clientService->getStatus($cliente_id));
            $dados_cobranca = new Cobranca;
            $dados_cobranca->valor = 0;
            $dados_cobranca->vencimento = 0;
            $dados_cobranca->status_id = $this->getStatus();
            $dados_cobranca->formapagamento_id = 1;
            $dados_cobranca->recorrencia_id = 1;

            $dados_cobranca->client_id = $cliente_id;
            $dados_cobranca->user_id = \Auth::user()->id;
            $dados_cobranca->save();
        }
        return $dados_cobranca;
    }

    public function getEnderecoCobranca($cliente_id)
    {
        $endereco_cobranca = Cobranca::select('id','endereco','complemento','bairro','cidade','estado','cep')
        ->where('client_id', '=', $cliente_id)
        ->first();
        return $endereco_cobranca;
    }

    public function getHistoricoCobranca($cliente_id, $limit = 10, $order_by = 'created_at', $order = 'DESC')
    {
        $historico_cobranca = HistoricoCobranca::where('client_id', '=', $cliente_id)->orderBy($order_by, $order)->paginate($limit);
        return $historico_cobranca;
    }

    public function updateDadosCobranca($data, $cliente_id)
    {
        $this->data = $data;

        //Regra: Se status Isento(12) valor definido como 0;
        if ($this->data['status_id'] == 12) {
            $this->data['valor'] = 0;
        }

        $dados_cobranca = Cobranca::find($this->data['dados_id']);
        if ($dados_cobranca) {
            $dados_cobranca->valor = $this->data['valor'];
            $dados_cobranca->vencimento = $this->data['vencimento'];
            $dados_cobranca->status_id = $this->data['status_id'];
            $dados_cobranca->formapagamento_id = $this->data['formapagamento_id'];
            $dados_cobranca->recorrencia_id = $this->data['recorrencia_id'];
            $success = $dados_cobranca->save();
            if ($success) {
                $this->updateStatusFinanceiro($cliente_id, $this->data['status_id']);
                return true;
            }
        }
        return false;
    }

    public function updateEnderecoCobranca($data, $cliente_id)
    {
        $this->data = $data;
        $endereco_cobranca = Cobranca::find($this->data['endereco_id']);
        if ($endereco_cobranca) {
            $endereco_cobranca->endereco = $this->data['endereco'];
            $endereco_cobranca->complemento = $this->data['complemento'];
            $endereco_cobranca->bairro = $this->data['bairro'];
            $endereco_cobranca->cidade = $this->data['cidade'];
            $endereco_cobranca->estado = $this->data['estado'];
            $endereco_cobranca->cep = $this->data['cep'];
            $success = $endereco_cobranca->save();
            if ($success) {
                return true;
            }
        }
        return false;
    }

    public function storeHistoricoCobranca($data, $cliente_id)
    {
        $this->data = $data;

        if(!$this->validatePayment())
        {
            return false;
        }else{
            $valor_atual = Cobranca::where('client_id', '=', $cliente_id)->select('valor')->first();
            $historico_cobranca = new HistoricoCobranca;
            $historico_cobranca->amount = $valor_atual['valor'];
            $historico_cobranca->description = $this->data['description'];
            $historico_cobranca->client_id = $cliente_id;
            $historico_cobranca->user_id = \Auth::user()->id;
            $historico_cobranca->created_at = $this->now;
            $historico_cobranca->save();
            if($historico_cobranca){
              return true;
            }
        }
    }

    public function getHistoricoToExport($data_inicial, $data_final)
    {
        //Previne 'erro' no between
        $data_final = $data_final.' 23:59:59';
        $elementos = (new HistoricoCobranca)->exportFields;

          //ajuste para retornar apenas ultima de cada cliente
          $result = HistoricoCobranca::select($elementos)
          ->join('clients', 'historico_cobranca.client_id', '=','clients.id')
          ->whereBetween('historico_cobranca.created_at', array($data_inicial, $data_final))
          ->orderBy('id_plataforma', 'asc')
          ->get()->ToArray();
          return $result;
    }

    /**
     * Validate if the function data match with the rules.
     *
     * @return     boolean  true when everything is ok. False when not.
     */
    protected function validatePayment()
    {
        return $this->validate($this->data, $this->rules, $this->messages);
    }



}
