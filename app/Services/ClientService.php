<?php

namespace App\Services;

use App\Services\Service;
use App\Services\HistoricoLoginService;

use Illuminate\Database\QueryException;
use App\Models\Client;
use App\Models\Contact;
use App\Models\ContactInfo;
use App\Models\Telefone;
use App\Models\Email;
use App\Models\HistoricoLead;

Class ClientService extends Service
{
    protected $now;
    protected $clienteId = null;
    protected $status_atual = null;
    protected $rules = [
        'nome'                  => 'min:1',
        'empresa'               => 'required',
    ];

    public $messages = [
        'not_found' => "Cliente não encontrado"
    ];

    public function __construct($userId=null, $escritorioId=null)
    {
        $this->userId = $userId;
        $this->escritorioId = $escritorioId;
        $this->clienteId = null;
        $this->now = $this->getNow();
    }

    public function getClienteId()
    {
        return $this->clienteId;
    }

    public function Exist($cliente_id)
    {
      $cliente = Client::find($cliente_id);
      //$cliente = Cliente::where('id', '=', $cliente_id)->exists();
      if ($cliente) {
          return true;
      }else{
        return false;
      }
    }

    /**
     * Validate if the function data match with the rules.
     *
     * @return     boolean  true when everything is ok. False when not.
     */
    protected function validateClient()
    {
        return $this->validate($this->data, $this->rules, $this->messages);
    }

    public function sanitizeData($data = array())
    {
        foreach ($data as $key => $value)
        {
            if ($key != '_token')
                $data[$key] = trim(preg_replace('/\t+/', '', $value));
        }
        return $data;
    }

    public function ClienteInfo($cliente_id)
    {
      if ($cliente_id == null){
        $info = [];
        $info['cliente_id'] = 0;
        $info['denominacaosocial'] = 'Novo Cliente/Novo Lead';
        $info['status_id'] = 10;
        $info['status'] = 'Lead';
        return $info;
      }else{
        if($this->Exist($cliente_id))
        {
          $info = [];
          $cliente = Client::find($cliente_id);
          $login = new HistoricoLoginService;

          $info['cliente_id'] = $cliente->id;
          $info['id_plataforma'] = $cliente->id_plataforma;
          $info['nome'] = $cliente->nome;
          $info['empresa'] = $cliente->empresa;
          $info['cnpj'] = $cliente->documento;
          $info['status_id'] = $cliente->status->id;
          $info['status'] = $cliente->status->status;

          $info['responsavel_id'] = $cliente->responsavel_id;
          if ($cliente->responsavel_id ? $info['responsavel'] = $cliente->responsavel->name : $info['responsavel'] = '');

          $info['escritorio_id'] = $cliente->escritorio_id;
          if ($cliente->escritorio_id ? $info['escritorio_nome'] = $cliente->escritorio->nome : $info['escritorio_nome'] = '');
          if ($cliente->escritorio_id ? $info['escritorio_sigla'] = $cliente->escritorio->sigla : $info['escritorio_sigla'] = '');

          if ($cliente->datacadastroplataforma ? $info['datacadastro_plataforma'] = $cliente->datacadastroplataforma : $info['datacadastro_plataforma'] = 'Não Cadastrado');
          $info['login'] = $login->getUltimoLogin($cliente->id_plataforma);

          $info['origem_id'] = $cliente->origem_id;
          if ($cliente->origem_id ? $info['origem'] = $cliente->origem->origem : $info['origem'] = '');

          return $info;
        }
      }
      return false;
    }


    public function getClientsPluck()
    {
        return Client::pluck('empresa', 'id');
    }

    /**
     * Gets the All clients.
     *
     * @return     Array   The clients.
     */
    public function getAllClients()
    {
      return Client::select('id', 'empresa')->orderBy('empresa', 'asc')->get();
    }

    /**
     * Retorna clientes paginados por status ou todos os registros.
     *
     * @return     Array de clientes.
     */
    public function getClientes($limit, $status, $order_by, $order, $responsavel_id)
    {
      if($status != null){
        return Client::orderBy($order_by, $order)->where('status_id','=', $status)->with('status')->paginate($limit);
      }else{
        if($responsavel_id != null){
          return Client::orderBy($order_by, $order)->with('status')->with('responsavel')->where('responsavel_id', $responsavel_id)->paginate($limit);
        }else{
        return Client::orderBy($order_by, $order)->with('status')->paginate($limit);
        }
      }
    }

    public function searchAutoComplete($query = null)
    {
        if (!$query)
        {
            //return Client::select('id', 'documento', 'nome',  'empresa', 'ativo')->limit(20)->get()->toArray();
            return Client::select('id', 'documento', 'nome',  'empresa', 'status_id')->with('status')->get()->toArray();
        }
        $client = Client::select('id', 'documento', 'nome', 'empresa', 'status_id')->with('status');
       // $client = $client->where('empresa', 'LIKE', '%'. $query . '%');
        //$client = $client->where('ativo', '=', true);
        /*$tags = explode(" ", $query);
        foreach($tags as $tag)
        {
            $client->orWhere('empresa', 'LIKE', '%' . $tag . '%');
            $client->orWhere('nome', 'LIKE', '%' . $tag . '%');
            $client->orWhere('documento', 'LIKE', '%' . $tag . '%');
        }*/

        //**** Melhorando a busca deixando mais "precisa" ****//
        $client = $client->where('empresa', 'LIKE', '%'. $query .'%');
        $client = $client->orWhere('nome', 'LIKE', '%'. $query .'%');
        $client = $client->orWhere('documento', 'LIKE', $query . '%');
        return $client->limit(300)->get()->toArray();
    }

    public function setStatus($status_id)
    {
        $this->status_atual = $status_id;
    }
    public function getStatus($cliente_id)
    {
      $status_cliente = Client::where('id','=',$cliente_id)->first(['status_id']);
      if ($status_cliente) {
        return $status_cliente['status_id'];
      }
      return false;
    }

    public function setStatusCliente($client_id, $status)
    {
      $cliente = Client::find($client_id);

      //REGRA: Ao transformar cadastro para ativo, checa se existe id_plataforma.
      //Se sim, pode ir para "Ativo/Cadastro", caso não tenha id_plataforma direciona para "Lead"
      if ($status == 8) {
        if (($cliente->id_plataforma == null) || ($cliente->id_plataforma == '')) {
          $status = 10;
        }
      }
      $cliente->status_id = $status;
      $cliente->save();
    }

    public function discoverStatus($status)
    {
      $status_id = null;

      switch ($status) {
        case 'ativos':
            $status_id = 1;
            break;
        case 'inativos':
            $status_id = 2;
            break;
        case 'pre-cadastro':
            $status_id = 3;
            break;
        case 'sem-perfil':
            $status_id = 4;
            break;
        default:
          $status_id = null;
      }
      return $status_id;
    }

    public function discoverTipoEmpresa($tipo)
    {
      $tipoempresa_id = null;

      switch ($tipo) {
        case 'Pessoa Jurídica no Brasil':
            $tipoempresa_id = 2;
            break;
        case 'Produtor Rural no Brasil':
            $tipoempresa_id = 3;
            break;
        case 'Corretor/Representante/Permissionário':
            $tipoempresa_id = 4;
            break;
        case 'Empresa Estrangeira':
            $tipoempresa_id = 5;
            break;
        default:
          $tipoempresa_id = null;
      }
      return $tipoempresa_id;
    }

    public function getStatisticsByClientId($client_id)
    {
        $interacoesNegocioFechado = [];
        $interacoes = [];
        $empresasCadastradas = [];
        $contatosCadastrados = [];
    }

    public function countAllClients()
    {
        return Client::count();
    }

    public function countClientsPlataforma()
    {
        return Client::where('id_plataforma','!=',NULL)->orWhere('id_plataforma','!=','0')->orWhere('id_plataforma', '!=', '')->count();
    }

    /**
     * Gets the client.
     *
     * @param      integer  $id     The identifier
     *
     * @return     App\Models\Client  The client.
     */
    public function getClient($cliente_id)
    {
        //$client = Client::where('id', '=', $cliente_id)->with('responsavel');
        $client = Client::where('id', '=', $cliente_id)
          ->with(array('status'=>function($query){
            $query->select('id','status');
            }))
            ->with(array('responsavel'=>function($query){
              $query->select('id','name');
              }))
          ->first();
        if ($client) {
          $this->clienteId = $client->id;
          return $client;
        }
        return false;
    }

    /**
     * Salva novo cliente no banco de dados
     *
     * @param      array   $data   The data
     *
     * @return     boolean
     */
    public function adicionaCliente($data)
    {
        $this->data = $data;
        if(!$this->validateClient() || !$this->validateUser()){
          return false;
        }
        $this->data['user_id'] = \Auth::user()->id;
        $this->data['responsavel_id'] = \Auth::user()->id;
        $this->data['escritorio_id'] = \Auth::user()->escritorio_id;
        $this->data['status_id'] = 10;
        $this->data['origem_id'] = 2;
        $novoCliente = Client::create($this->data);
        $this->clienteId = $novoCliente->id;
        return true;
    }

    /**
     * Salva novo cliente no banco de dados
     *
     * @param      array   $data   The data
     *
     * @return     boolean
     */
    public function adicionaLead($data)
    {
        $this->data = $data;
        //Cria novo Lead como Cliente
        $lead = [
            'nome'      => $this->data['nome'],
            'empresa'   => $this->data['nome'],
            'status_id' => 10,
            'origem_id' => 2,
            'user_id' => \Auth::user()->id,
            'responsavel_id' => \Auth::user()->id,
            'escritorio_id' => \Auth::user()->escritorio_id,
        ];

        //adiciona novo lead
        $novoLead = Client::create($lead);
        $this->clienteId = $novoLead->id;
        //grava historico de novo lead
        $lead['client_id'] = $this->clienteId;
        $gravaHistoricoLead = HistoricoLead::create($lead);

        //Cria novo contato para o cliente
        $contato = [
            'nome' => $this->data['nome'],
            'user_id' => \Auth::user()->id,
            'responsavel_id' => \Auth::user()->id,
            'escritorio_id' => \Auth::user()->escritorio_id,
            'origem_id' => 2,
            'client_id' => $this->clienteId
        ];
        $novoContato = Contact::create($contato);

        //Cria EMAIL contato para o lead
        if ($this->data['email'] != '') {
          $email = [
              'contato' => $this->data['email'],
              'tipo' => 'email',
              'contact_id' => $novoContato->id,
              'created_at' => $this->now,
          ];
          Email::create($email);
        }

        //Cria TELEFONE contato para o lead
        if ($this->data['telefone'] != '') {
          $telefone = [
              'contato' => $this->data['telefone'],
              'tipo' => 'telefone',
              'contact_id' => $novoContato->id,
              'created_at' => $this->now,
          ];
          Telefone::create($telefone);
        }
        return true;
    }

    /**
     * Update an client
     *
     * @param      integer   $id      The identifier
     * @param      array   $change  The change
     *
     * @return     boolean
     */
    public function update($id, $change)
    {
        $this->data = $change;
        $client = Client::find($id);

        if($client == null) {
            $this->setError("cliente", $this->messages['client_not_found']);
            return false;
        }

        if(($this->validateClient() || $this->validateUser())) {
            $change = $this->sanitizeData($change);
            if (($change['id_plataforma'] == 0) || ($change['id_plataforma'] == '')) {
                $change['id_plataforma'] = null;
            }elseif (ctype_digit($change['id_plataforma'])) {
                $change['status_id'] = 8;
            }
            $client->update($change);
            $this->clienteId = $client->id;
            return true;
        }

        // dd($client->toArray());

        return false;
    }

    public function excluirCliente($data)
    {
      $this->data = $data;
      $this->clienteId = $this->data['cliente_id'];

      //Deleção recursiva via "model event deleting"
      $cliente = Client::destroy($this->clienteId);

      if ($cliente) {
        return true;
      }
      return false;
    }

    public function getClientsToExport()
    {
        $elementos = (new Client)->exportFields;
        //return Client::all($elementos)->toArray();

        $result = \DB::Table('clients')
           ->leftjoin('users', 'clients.user_id', '=','users.id')
           ->leftjoin('users as user_responsavel', 'clients.responsavel_id', '=','user_responsavel.id')
           ->leftjoin('status', 'clients.status_id', '=','status.id')
           ->leftjoin('tipoempresa', 'clients.tipoempresa_id', '=','tipoempresa.id')
           ->leftjoin('origem', 'clients.origem_id', '=','origem.id')
           ->leftjoin('escritorios', 'clients.escritorio_id', '=','escritorios.id')
           ->select($elementos)
           ->whereNull('clients.deleted_at')
           ->get();
        return $result;
    }

    public function ImportedData(Array $elements)
    {
        $resultado = [];

        foreach($elements as $element) {
            $this->lastIdClient = null;
            if($element['id'])
                $result = $this->updateElement($element);
            else
                $result = $this->insertElement($element);

            if($this->lastIdClient)
                $element['id'] = $this->lastIdClient;
            unset($element[0]);
            if($result)
                $resultado[] = array_merge($element, ['status' => 'realizado com sucesso.']);
            else
                $resultado[] = array_merge($element, ['status' => 'Ocorreu um erro.']);
        }
        return $resultado;
    }

    public function sincronizaInfoCliente($response = true, Array $elements)
    {
        $resultado = [];

        foreach($elements as $element) {
          //dump($element);
          $cliente_selecionado = Client::where('id_plataforma', '=', $element['id_empresa'])->first();
          if ($cliente_selecionado['id_plataforma'] == $element['id_empresa']) {
            if ($element['cnpj'] ? $cliente_selecionado->documento = $element['cnpj'] : $cliente_selecionado->documento = $cliente_selecionado->documento);
            if ($element['cnpj'] ? $cliente_selecionado->tipo_documento = 2 : false);
            if ($element['cpf'] ? $cliente_selecionado->documento = $element['cpf'] : $cliente_selecionado->documento = $cliente_selecionado->documento);
            if ($element['cpf'] ? $cliente_selecionado->tipo_documento = 1 : false);
            if ($element['ie'] ? $cliente_selecionado->ie = $element['ie'] : $cliente_selecionado->ie = $cliente_selecionado->ie);
            $cliente_selecionado->tipoempresa_id = self::discoverTipoEmpresa($element['tipo_empresa']);
            if (($element['endereco'] != '') || ($element['endereco_numero'] != '')) {
              $cliente_selecionado->endereco = $element['endereco'].', '.$element['endereco_numero'];
            }
            $cliente_selecionado->complemento = $element['endereco_complemento'];
            $cliente_selecionado->bairro = $element['bairro'];
            $cliente_selecionado->cidade = $element['cidade'];
            $cliente_selecionado->estado = $element['estado'];
            $cliente_selecionado->cep = $element['cep'];
            $cliente_selecionado->datacadastro_plataforma = $element['data_inclusao'];
            $success = $cliente_selecionado->save();

            if($success)
                $resultado[] = array_merge($element, ['status' => 'realizado com sucesso.']);
            else
                $resultado[] = array_merge($element, ['status' => 'Ocorreu um erro.']);
          }
        }

        if ($response) {
          return $resultado;
        }
        return true;
    }

    protected function updateElement(Array $element)
    {
        $client = Client::find($element["id"]);
        if($client)
            return $client->fill($element)->save();

        return $this->insertElement($element);
    }

    protected function insertElement(Array $element)
    {
        unset($element["id"]);
        $this->validateUser();
        $result = $this->user->clients()->create($element);
        $this->lastIdClient = $result->id;
        return true;
    }

    public function BuscaClientes($query, $limit, $status, $order_by, $order, $responsavel_id)
    {
      if ($query){
      $cliente = Client::select('id', 'nome', 'empresa', 'documento', 'tipo_documento', 'estado','status_id','responsavel_id');
      $cliente = $cliente->where('nome', 'LIKE', '%'. $query .'%');
      $cliente = $cliente->orWhere('empresa', 'LIKE', '%'. $query .'%');
      $cliente = $cliente->orWhere('documento', 'LIKE', $query . '%');
      $cliente = $cliente->orderBy($order_by, $order);
      $cliente = $cliente->with('status');
      $cliente = $cliente->with('responsavel');
      return $cliente->paginate($limit);
      }
    }

    public function BuscaClienteId($query, $limit, $status, $order_by, $order, $responsavel_id)
    {
      if ($query){
      $cliente = Client::select('id', 'nome', 'empresa', 'documento', 'tipo_documento', 'estado','status_id','responsavel_id');
      $cliente = $cliente->where('id_plataforma', '=', $query);
      $cliente = $cliente->orderBy($order_by, $order);
      $cliente = $cliente->with('status');
      $cliente = $cliente->with('responsavel');
      return $cliente->paginate($limit);
      }
    }

    public function getPermissions()
    {

        return [
            [
                'id' => 'client.destroy',
                'name' => 'Excluir cliente',
                'help' => 'Usuário pode excluir um cliente'
            ],
            [
                'id' => 'client.edit',
                'name' => 'Editar cliente',
                'help' => 'Usuário pode editar um cliente'
            ],
            [
                'id' => 'client.create',
                'name' => 'Criar cliente',
                'help' => 'Usuário pode criar um cliente'
            ],
            [
                'id' => 'client.negocios-notify',
                'name' => 'Noticação de Negócios',
                'help' => 'O usuário receberá notificação de negócios fechados',
            ],

        ];
    }
}
