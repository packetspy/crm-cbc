<?php

namespace App\Services;

use App\Models\Interaction;
use App\Models\Cart;
use App\Models\Client;
use App\Models\User;
use Fenos\Notifynder\Notifynder;
use Fenos\Notifynder\Exceptions\EntityNotIterableException;
use Fenos\Notifynder\Exceptions\IterableIsEmptyException;
use Fenos\Notifynder\Builder\NotifynderBuilder;
use App\Helpers\InteractionType as EnumInteractionType;
use App\Services\Service;
use App\Services\InteractionTypeService;
use App\Services\ClientService;


Class InteractionService extends Service
{
    protected $clientService;
    protected $typeService;
    protected $userService;
    protected $lastInteractionId = null;
    protected $notifynder;
    protected $interactionTypeId = null;
    protected $bloqueados = null;

    protected $rulesContato = [
        'data_contato' => 'required|date|after:yesterday',
        'interaction_type_id' => 'required|integer'
    ];

    protected $messagesContato = [
    ];

    //public function __construct($userId = null, Notifynder $notifynder)
    public function __construct($userId = null)
    {
        $this->userId = $userId;
        $this->typeService = new InteractionTypeService($userId);
        $this->clientService = new ClientService($userId);
        //$this->notifynder = $notifynder;
        $this->userService = new UserService($userId);
    }

    public function buscarDadosParaNovaInteracao($idCliente)
    {
        $cliente =  $this->clientService->getClient($idCliente);
        if($cliente == null)
            return ['cliente' => [], 'tipos' => []];

        return [
            'cliente' => $cliente->toArray(),
            'tipos'   =>  $this->typeService->apenasAtivos()
        ];
    }

    public function getLastInteractionId()
    {
        return $this->lastInteractionId;
    }

    public function getInteraction($wheres = [])
    {

        if(is_int($wheres))
            return Interaction::find((int) $wheres);

        return Interaction::where($wheres)->first();
    }

    public function getInteractionInfo($client_id, $interaction_id)
    {
        return Interaction::select('id','observacao','data_contato','client_id')->where('id', '=', $interaction_id)->first();
    }


    public function salvarInteracao($idCliente, $dados)
    {
        $this->data = $dados;
        $this->idCliente = $idCliente;

        if(!$this->validationsForSave())
            return false;

        $interaction = Interaction::create(array_merge($dados, ['user_id'=> $this->userId, 'client_id'=>$idCliente]));

        $this->setLastInteractionId($interaction->id);

        if ( strtolower($interaction->interaction_type->nome) == 'negócio fechado')
            $this->attachNotify();

        return true;
    }

    public function userInteractions($limit=100)
    {
        return Interaction::where('user_id', '=', $this->userId)->paginate($limit);
    }

    public function clientInteractions($clientId, $limit=100)
    {
        return Interaction::where('client_id', '=', $clientId)->paginate($limit);
    }

    public function interactionPurchaseItems($idInteraction)
    {
        $cart = Cart::where("interaction_id", "=", $idInteraction)->first();
        if(!$cart)
            return [];

        return $cart->cart_products()->get();
    }

    protected function setLastInteractionId($id)
    {
        $this->lastInteractionId = $id;
    }

    protected function validationsForSave()
    {
        $cliente = $this->clientService->getClient($this->idCliente);

        if(!$cliente) {
            $this->setError('cliente', "cliente não encontrado");
            return false;
        }


        if(!$this->typeService->typeExist($this->data['interaction_type_id'])) {
            $this->setError('tipo_de_interacao', 'Tipo de interação não encontrada');
            return false;
        }

        return true;
    }

    protected function createCartAndSave($interaction)
    {
        $cart = $interaction->cart()->create([
            "interaction_id" => $interaction->id
        ]);

        foreach($this->data['produtos'] as $produto){
            if($produto["quantidade"] > 0)
                $cart->cart_products()->create($produto);
        }
        return true;
    }

    public function getPermissions()
    {
        $permissions = [];

        $permissions[] = [
            'id' => 'interaction.index',
            'name' => 'Ver interações',
            'help' => 'Usuário vê todas os tipos de interações'
        ];

        $permissions[] = [
            'id' => 'interaction.edit',
            'name' => 'Editar interação',
            'help' => 'Usuário edita um tipo de interação'
        ];

        $permissions[] = [
            'id' => 'interaction.destroy',
            'name' => 'Excluir interação',
            'help' => 'Usuário exclui uma interação'
        ];

        $permissions[] = [
            'id' => 'interaction.move',
            'name' => 'Mover interação',
            'help' => 'Usuário pode mover uma interação'
        ];

        return $permissions;
    }

    public function exibeApenasTipoFuncao($id)
    {
        $interaction = Interaction::find($id);

        if(!$interaction || $interaction->interaction_type->funcao == true)
            return false;

        return true;
    }

    public function exibeApenasContraParte($id)
    {
        $interaction = Interaction::find($id);

        if(!$interaction)
            return false;
        $this->interactionTypeId = $interaction->interaction_type->id;
        if($interaction->interaction_type->exibe_apenas_contraparte == true)
            return true;

        return false;
    }

    public function getInteractionTypeId()
    {
        return $this->interactionTypeId;
    }

    public function obrigatorioContraparte($userId, $atualTentativaDeResposta)
    {
        $interacaoExtraObrigatoria = EnumInteractionType::contraparte();

        $interacoes = Interaction::whereIn("interaction_type_id", $interacaoExtraObrigatoria)
            ->where("user_id", "=", $userId)
            ->get(['id'])->toArray();

        $interacoesId = array_map(function($id){
            return $id['id'];
        }, $interacoes);

        if(in_array($atualTentativaDeResposta, $interacoesId)){
            return false;
        }

        $naoBloqueadas = Interaction::whereIn('father_id', $interacoesId)
                            ->get(['father_id'])->toArray();
        $possivelBloqueio = [];
        foreach($interacoesId as $InteracaoId){
            $possivelBloqueio[$InteracaoId] = [];
            foreach($naoBloqueadas as $interacao){
                if(in_array($interacao['father_id'], $interacoesId)){
                    $possivelBloqueio[$interacao['father_id']][] = 1;
                }
            }
        }

        $bloquear = array_filter($possivelBloqueio, function($array){
            return empty($array);
        });

        if(empty($bloquear))
            return false;

        $this->bloqueados = Interaction::whereIn('id', array_keys($bloquear))->get();
        return true;
    }

    public function getBloqueadas()
    {
        return $this->bloqueados;
    }

    public function moveInteraction($request)
    {
      $interaction_id = $request['interaction_id'];
      $client_id = $request['client_id'];

      $interaction = Interaction::find($request['interaction_id']);
      if(!$interaction) {
          return false;
      }else{
        $interaction->client_id = $client_id;
        $interaction->save();
        return true;
      }

    }

    public function getLastInteractionsToExport()
    {
        $elementos = [
            'clients.id as id_crm',
            'id_plataforma as id_plataforma',
            'clients.nome as cliente_nomefantasia',
            'clients.empresa as cliente_razaosocial',
            'interactions.observacao as ultima_interacao',
        ];
        //codigo original
        /*$result = \DB::Table('payments')
           ->join('clients', 'payments.client_id', '=','clients.id')
           ->join('payment_types', 'payment_type_id', '=','payment_types.id')
           ->select($elementos)
           ->groupBy('payments.client_id')
           ->orderBy('payments.created_at', 'asc')
           ->get();
        return $result;*/

          /*//ajuste para retornar apenas ultima de cada cliente
          $result = Payment::select($elementos)
          ->join('clients', 'payments.client_id', '=','clients.id')
          ->join('payment_types', 'payment_type_id', '=','payment_types.id')
          ->where('payments.created_at', function($query){
              $query->selectRaw('max(created_at)')
              ->from('payments as p')
              ->where('p.client_id', \DB::raw('payments.client_id'));
          })->orderBy('client_id', 'asc')
          ->get()->ToArray();
          return $result;*/

          /*//ajuste para retornar apenas ultima de cada cliente
          $result = Client::select($elementos)
          //->join('interactions', 'interactions.client_id', '=','clients.id')->limit(1);
          //->join(\DB::raw('(SELECT * FROM interactions LIMIT 1) interactions'), function($join)
          ->join('interactions', function($join){
               $join->on('clients.id', '=', 'interactions.client_id');
           })
          ->orderBy('nome', 'asc')
          ->get()
          ->ToArray();*/

          /*$result = \DB::table('clients')
          ->select($elementos)
          ->leftJoin('interactions', function ($leftJoin) {
              $leftJoin->on('clients.id', '=', 'interactions.client_id');
              //->on('limit 2');
                   //->where('recahrge.create_date', '=', DB::raw("(select max(`create_date`) from recahrge)"));
                   //->where(\DB::raw("limit 2"));
          })
          ->get();*/

          /*$clientes = Client::all();
          $dados = [];
          foreach ($clientes as $cliente) {
            $dados['id'] = $cliente->id;
            $dados['nome'] = $cliente->nome;
            $dados['empresa'] = $cliente->empresa;
          }
          $result = $dados;*/

          /*$result = \DB::table('clients')
            ->select('clients.id','nome','empresa','observacao','interactions.id','interactions.client_id')
            //->leftJoin('users', 'clients.responsavel_id', '=', 'users.id')
            ->leftjoin('interactions', function ($leftJoin) {
              $leftJoin->on('clients.id', '=', 'interactions.client_id')
                   ->where('interactions.id', '=',\DB::raw("(select id from interactions)"));
                   //->where(\DB::raw("(select id from interactions)"));
                   //->where('client.id', '=', 'interactions.client_id');
          })
          ->get();*/

          $result = \DB::select('
                SELECT
                clients.id, clients.id_plataforma, clients.nome, clients.empresa,
                consultor_responsavel.name as consultor_responsavel,
                users.name as autor_interacao,
                DATE_FORMAT(interactions.data_contato, "%d/%m/%Y") as data_contato, interactions.id as id_interacao, interaction_types.nome as acao, interactions.observacao as interacao
                FROM clients
                JOIN interactions on interactions.id = (
                    select interactions.id from interactions
                    where interactions.client_id = clients.id
                    order by created_at desc
                    limit 1
                )
                JOIN interaction_types on interaction_types.id = interaction_type_id
                JOIN users as consultor_responsavel on clients.responsavel_id = consultor_responsavel.id
                JOIN users on users.id = (
                    select users.id from users
                    where interactions.user_id = users.id
                    order by created_at desc
                    limit 1
                )'
              );

          return $result;
    }

    /**
     *
     * @param $interaction_id - id da interação
     * @param $author_id - id de quem criou a interação
     */
    protected function attachNotify()
    {

        $authorId = $this->userId;
        $interaction = Interaction::find($this->getLastInteractionId());
        if (!$interaction) {
            $this->setError("interaction_not_found", "Interação não existe");
            return false;
        }

        $clientId = $interaction->client_id;

        $users = $this->userService->getUserWithPermission('client.negocios-notify');

        $withoutLogged = [];
        foreach($users as $user){
            if($user->id != $authorId)
                $withoutLogged[] = $user;
        }
        $users = $withoutLogged;
        $empresa = $interaction->client->empresa;
    }

    public function getPending($user_id){
        $interacaoExtraObrigatoria = EnumInteractionType::contraparte();


        $test = Interaction::where("user_id", $user_id)
        ->whereIn("interaction_type_id", $interacaoExtraObrigatoria)
        ->lists('id')->all();

        return Interaction::where('user_id',$user_id)
        ->where('data_contato','<',(new \Carbon\Carbon())->now())
        ->whereIn('father_id', $test)
        ->get();
    }

    public function isPending($user_id){

        if(count($this->getPending($user_id)->toArray()) > 0){
            return true;
        }
        return false;
    }
}
