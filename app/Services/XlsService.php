<?php

namespace App\Services;

use Excel;

class XlsService
{
    private $excel;

    public function __construct()
    {
    }

    public function createReport($format='xlsx', $data)
    {
        $excel = Excel::create($data["nome"], function($excel) use ($data){
            $excel->setTitle($data["nome"])
                  ->setCreator("TI")
                  ->setCompany("CBC Negócios");
        });

        $excel->sheet("Aba", function($sheet) use($data){
            $i = 2;
            $sheet->row(1, $data["cabecalho"]);
            foreach($data["dados"] as $row) {
                if(is_object($row))
                    $sheet->row($i, get_object_vars($row));
                else
                    $sheet->row($i, $row);
                $i++;
            }
        });

        return $excel->download($format);
    }

    public function readFile($file, $sheet = null)
    {
      if ($sheet != null) {
        //return Excel::load($file)->selectSheets($sheet)->get()->toArray();
        return Excel::selectSheets($sheet)->load($file)->toArray();
      }else{
        return Excel::load($file)->get()->toArray();
      }
    }
}
