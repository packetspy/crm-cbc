<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;

class CrmController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('needsPermission');

    }
}
