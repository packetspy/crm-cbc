<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function reset2(Request $request)
    {
        $email = \Auth::user()->email;
        $old_password = $request->get('old_password');
        $current_user_password = \Auth::user()->getAuthPassword();

        if ( \Hash::check ( $old_password, $current_user_password ) )
        {
            $rules = [
                'password' => 'required|min:6|confirmed',
            ];

            $validator = \Validator::make( $request->all(), $rules );

            if ( $validator->fails() )
            {
                return redirect()->back()->with('alert-danger', 'Senhas não coincidem.');
            }

           $usuario = \Auth::user();
           $usuario->password = bcrypt($request->get('password'));
           $usuario->save();
           return redirect()->back()->with('alert-success', 'Senha atualizada com sucesso!');

        }
        else {
            return redirect()->back()->with('alert-danger', 'Senha atual não coincide.');
        }
    }
}
