<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

use App\Models\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/contabilidade';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    public function login(Request $request)
    {
      //return bcrypt('Rb4#8$1v');
      $email = $request['email'];
      $password = $request['password'];
      //$defaultpass = '$2y$10$BA4nexkKheWf4FHWw6v5ZuGC.xZXVc0B/xJE3ErGHNFCJqNx9inAK';
      $user = User::where('email', '=', $email)->first();
      if($user){
        if (Auth::attempt(['email' => $email, 'password' => $password, 'deleted_at' => null])){
          $request->session()->put('escritorio', $user->escritorio_id);
          return redirect()->intended();
        }else{
          return redirect()->back()->withInput()->withErrors(['message' => 'Email ou senha incorretos']);
        }
      }
    }

    public function reset(Request $request)
    {
      if($request['password'] == $request['password_confirmation']){
        $user = User::where('email', '=', $request['email'])->first();
        $user->password = bcrypt($request['password']);
        $user->save();
        return redirect()->intended();
      }
    }

    public function showLoginForm()
    {
        return view('auth.login');
    }
}
