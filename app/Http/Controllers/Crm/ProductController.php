<?php

/**
 * Created by PhpStorm.
 * User: Lúdio
 * Date: 07/07/2016
 * Time: 13:07
 */

namespace App\Http\Controllers\Crm;

use App\Http\Controllers\CrmController;
use App\Models\ProductList;
use App\Services\ProductService as ProductService;
use Illuminate\Http\Request;


class ProductController extends CrmController
{

    protected $productService;

    public function __construct(ProductService $service)
    {
        if (\Auth::user() ? $user_id = \Auth::user()->id : $user_id = null);
        $this->productService = $service;
        $this->productService->setUserId($user_id);
    }

    public function getIndex()
    {
        $products = $this->productService->getProdutos(30);
        return view('gestao.produtos.index', compact('products'));
    }

    /**
     * Formulário para criar um produto
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCreate(Request $request)
    {
        if (!$request->ajax())
            abort(404);

        return view('gestao.produtos.create');
    }

    public function getEdit(Request $request, $id)
    {
        if (!$request->ajax())
            abort(404);

        $produto = $this->productService->getProduct($id);
        if ($produto)
            return view('gestao.produtos.create', compact('produto'));

        return response()->make('Produto não encontrado.');

    }

    public function postSaveProduct(Request $request)
    {
        $data = $request->all();
        $product = $this->productService->save($data);
        if ($product) {
            return redirect()->action('Crm\ProductController@getIndex')->with('alert-success', 'Produto salvo!');
        }

        return redirect()->action('Crm\ProductController@getIndex')->withErrors( $this->productService->getErrors() );
    }

    public function postUpdateProduct(Request $request, $id)
    {
        $produto = $this->productService->getProduct($id);
        if (!$produto)
            return redirect()->action('Crm\ProductController@getIndex')->with('alert-danger', 'Produto não encontrado.');

        $data = $request->all();
        $productUpdate = $this->productService->update($id, $data);
        if ($productUpdate) {
            return redirect()->action('Crm\ProductController@getIndex')->with('alert-success', 'Produto atualizado com sucesso');
        }

        return redirect()->action('Crm\ProductController@getIndex')->with('alert-danger', 'Erro')->withErrors($this->productService->getErrors());
    }

    public function getExclude(Request $request, $id)
    {
        $produto = $this->productService->getProduct($id);
        if ($produto && $request->ajax())
        {
            return view('gestao.produtos.delete', compact('produto'));
        }
        return response()->make('Produto não encontrado');
    }

    public function getSearch(Request $request)
    {
        if ($request->ajax()) {
            $q = $request->get('name');

            $products = ($request->has('name')) ?
                $this->productService->searchAutoComplete($q)
                : $this->productService->searchAutoComplete();

            return response()->json($products);
        }
    }

    public function getCreateList()
    {
        $list = $this->productService->getList(0);
        $products = $this->productService->getProducts(null, false);
        return view('gestao.produtos.list.create', compact('products', 'list'));
    }

    public function getViewLists()
    {
        $lists = $this->productService->getlists(10);
        return view('gestao.produtos.list.index', compact('lists'));
    }

    public function getListSearch(Request $request)
    {
        if ($request->ajax()) {
            $q = $request->get('name');

            $products = ($request->has('name')) ?
                $this->productService->searchListAutoComplete($q)
                : $this->productService->searchListAutoComplete();

            return response()->json($products);
        }
    }

    public function getListEdit($id)
    {
        $list = $this->productService->getList($id);
        $products = $this->productService->getProducts(null, false);
        return view('gestao.produtos.list.create', compact('products', 'list'));
    }

    public function getListDelete($id)
    {
        $list = ProductList::find($id);
        if ($list) {
            foreach ($list->products as $productAssociation) {
                $productAssociation->delete();
            }
            $list->delete();
            return 1;
        }

        return 0;
    }

    public function postCreateList(Request $request)
    {
        $redirect = redirect()->action('Crm\ProductController@getViewLists');
        if($request->get('id')) {
            if ($this->productService->updateList($request->all())) {
                return $redirect->with('alert-success', 'Atualizado com sucesso!');
            }
            return $redirect->with('alert-danger', 'Ocorreu um erro durante atualização da lista');
        }

        $this->productService->createList($request->all());
        return $redirect->with('alert-success', 'Lista criada!');
    }
}
