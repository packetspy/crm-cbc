<?php

namespace App\Http\Controllers\Crm\Cliente;

use Illuminate\Http\Request;
use App\Http\Requests;

use App\Http\Controllers\CrmController;

use App\Services\ClientService;
use App\Services\StatusService;
use App\Services\UserService;
use App\Services\TipoEmpresaService;
use App\Services\OrigemService;


class DadosController extends CrmController
{
  protected $clientService;
  protected $statusService;
  protected $tipoEmpresaService;
  protected $origemService;
  protected $userService;

  public function __construct(ClientService $clienteService, StatusService $statusService, UserService $userService, TipoEmpresaService $tipoEmpresaService, OrigemService $origemService)
  {
      if (\Auth::user() ? $this->user_id = \Auth::user()->id : $this->user_id = null);
      $this->section = 'Dados Cadastrais';
      $this->clientService = new ClientService($this->user_id);
      $this->statusService = $statusService;
      $this->tipoEmpresaService = $tipoEmpresaService;
      $this->origemService = $origemService;
      $this->userService = $userService;
  }
  public function index($cliente_id = null)
  {
      return $this->show($cliente_id);
  }

  public function show($cliente_id)
  {
    $section = $this->section;
    $cliente = $this->clientService->getClient($cliente_id);

    if($cliente){
      $clienteInfo = $this->clientService->ClienteInfo($cliente_id);
      $status = $this->statusService->getStatus();
      $consultores = $this->userService->getConsultores();
      $estados = \App\Helpers\Estado::getAll();
      $tipodocumento = \App\Helpers\TipoDocumento::getAll();
      $tipoempresa = $this->tipoEmpresaService->getTipoEmpresa();
      $origens = $this->origemService->getOrigem();
      return view('crm.cliente.dados', compact('section','clienteInfo','cliente','status','consultores','estados','tipodocumento','tipoempresa','origens'));
    }else{
      return redirect('clientes')
        ->with('type', 'warning')
        ->with('title', 'Cliente não encontrado')
        ->with('message', 'Infelizmente, não localizamos o cliente solicitado');
    }
  }

  public function update(Request $request, $cliente_id)
  {
    $data = $request->all();
    $client = $this->clientService->update($cliente_id, $data);
    if ($client) {
        return redirect()->action('Crm\Cliente\DadosController@show', $this->clientService->getClienteId())->with(['alert-success' => 'Cliente atualizado com sucesso!']);
    }
    return redirect()->action('Crm\Cliente\DadosController@show', $cliente_id)->with('alert-danger', 'Falha ao atualizar')->withErrors($this->clientService->getErrors());
  }

}
