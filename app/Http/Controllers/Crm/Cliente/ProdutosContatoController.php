<?php

namespace App\Http\Controllers\Crm\Cliente;

use Illuminate\Http\Request;
use App\Http\Requests;

use App\Http\Controllers\CrmController;

use App\Services\ClientService;
use App\Services\ContactService;
use App\Services\ContactInfoService;
use App\Services\ProductService;


class ProdutosContatoController extends CrmController
{
  protected $productService;

  public function __construct(ProductService $productService)
  {
      if (\Auth::user() ? $this->user_id = \Auth::user()->id : $this->user_id = null);
      $this->section = 'Produtos';
      $this->productService = new ProductService($this->user_id);
  }

  public function store(Request $request)
  {
    $data = $request->all();
    $produto = $this->productService->addProdutoContato($data);

      if ($produto) {
          return redirect()->action('Crm\Cliente\ContatosController@show', $this->productService->getClienteId())
          ->with('type', 'success')
          ->with('title', 'Adicionado!')
          ->with('message', 'Produto adicionado com sucesso!')
          ->with('contato_id', $this->productService->getContatoId());
      }
      return redirect()->action('Crm\Cliente\ContatosController@show', $this->productService->getClienteId())
        ->with('type', 'error')
        ->with('title', 'Erro ao atualizar!')
        ->with('message', 'Ocorreu um erro ao inserir a informação.')
        ->with('contato_id', $this->productService->getContatoId())
        ->withErrors($this->productService->getErrors());

  }

  public function destroy(Request $request)
    {
      $data = $request->all();
      $produto = $this->productService->deleteProdutosContato($data);

      if ($produto) {
          $response = \Response::json(array(
  			      'type' => 'info',
              'title' => 'Produto excluído!',
  			      'message' => 'Produto selecionado foi excluído.'), 200
  			  	);
  				return $response;
      }
        return false;
    }
}
