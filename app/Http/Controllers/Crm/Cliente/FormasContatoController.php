<?php

namespace App\Http\Controllers\Crm\Cliente;

use Illuminate\Http\Request;
use App\Http\Requests;

use App\Http\Controllers\CrmController;

use App\Services\ClientService;
use App\Services\ContactService;
use App\Services\ContactInfoService;
use App\Services\ProductService;


class FormasContatoController extends CrmController
{
  protected $contactInfoService;

  public function __construct(ContactInfoService $contactInfoService)
  {
      if (\Auth::user() ? $this->user_id = \Auth::user()->id : $this->user_id = null);
      $this->section = 'Formas de Contatos';
      $this->contactInfoService = new ContactInfoService($this->user_id);
  }


  public function store(Request $request)
  {
    $data = $request->all();
    $formadecontato = $this->contactInfoService->insert($data);

      if ($formadecontato) {
          return redirect()->action('Crm\Cliente\ContatosController@show', $this->contactInfoService->getClienteId())
          ->with('type', 'success')
          ->with('title', 'Adicionado!')
          ->with('message', 'Forma de Contato adicionada com sucesso!')
          ->with('contato_id', $this->contactInfoService->getContatoId());
      }
      return redirect()->action('Crm\Cliente\ContatosController@show', $this->contactInfoService->getClienteId())
        ->with('type', 'error')
        ->with('title', 'Erro ao atualizar!')
        ->with('message', 'Ocorreu um erro ao inserir a informação.')
        ->with('contato_id', $this->contactInfoService->getContatoId())
        ->withErrors($this->contactInfoService->getErrors());
  }

  public function destroy(Request $request)
    {
      $formadecontato_id = $request->id;
      $formadecontato = $this->contactInfoService->delete($formadecontato_id);

      if ($formadecontato) {
          $response = \Response::json(array(
  			      'type' => 'info',
              'title' => 'Forma de contato removida!',
  			      'message' => 'A forma de contato selecionada foi removida.'), 200
  			  	);
  				return $response;
      }
        return false;
    }
}
