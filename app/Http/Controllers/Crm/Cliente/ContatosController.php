<?php

namespace App\Http\Controllers\Crm\Cliente;

use Illuminate\Http\Request;
use App\Http\Requests;

use App\Http\Controllers\CrmController;

use App\Services\ClientService;
use App\Services\ContactService;
use App\Services\ProductService;
use App\Services\OrigemService;


class ContatosController extends CrmController
{
  protected $clientService;
  protected $statusService;
  protected $userService;

  public function __construct(ClientService $clienteService, ContactService $contactService, ProductService $productService, OrigemService $origemService)
  {
      if (\Auth::user() ? $this->user_id = \Auth::user()->id : $this->user_id = null);
      $this->section = 'Contatos';
      $this->clientService = new ClientService($this->user_id);
      $this->contactService = new ContactService($this->user_id);
      $this->productService = new ProductService($this->user_id);
      $this->origemService = $origemService;
  }
  public function index($cliente_id = null)
  {
      return $this->show($cliente_id);
  }

  public function show($cliente_id)
  {
    $section = $this->section;
    $cliente = $this->clientService->Exist($cliente_id);

    if($cliente){
      $clienteInfo = $this->clientService->ClienteInfo($cliente_id);
      $contatos = $this->contactService->getContatoCliente($cliente_id);
      $estados = \App\Helpers\Estado::getAll();
      $produtos = $this->productService->getProdutos(null, false);
      $grupoprodutos = $this->productService->getGruposProdutos(null, false);
      $origens = $this->origemService->getOrigem();
      return view('crm.cliente.contatos', compact('section','clienteInfo','contatos','estados','produtos','grupoprodutos','origens'));
    }else{
      return redirect('clientes')
        ->with('type', 'warning')
        ->with('title', 'Cliente não encontrado')
        ->with('message', 'Infelizmente, não localizamos o cliente solicitado');
    }
  }

  public function store(Request $request)
  {
    $data = $request->all();
    $contato = $this->contactService->adicionaContato($data);
    if ($contato){
        return redirect('cliente/contatos/'.$this->contactService->getClienteId())
        ->with('alert-success', 'Contato adicionado com sucesso!')
        ->with('contato_id', $this->contactService->getContatoId());
    }else{
      return redirect('cliente/'.$this->contactService->getClienteId().'/novo-cliente')
        ->withInput($data)
        ->withErrors($this->clientService->getErrors());
    }
  }

  public function update(Request $request, $contato_id)
  {
      $data = $request->all();
      $contato = $this->contactService->update($data, $contato_id);
      if ($contato) {
          return redirect('cliente/contatos/'.$this->contactService->getClienteId())
          ->with('type', 'success')
          ->with('title', 'Contato atualizado!')
          ->with('message', 'Informações do Contato atualizadas com sucesso.')
          ->with('contato_id', $this->contactService->getContatoId());
      }
      return redirect('cliente/contatos/'.$this->contactService->getClienteId())
        ->with('type', 'error')
        ->with('title', 'Erro ao atualizar!')
        ->with('message', 'Ocorreu um erro ao atualizar o Contato.')
        ->withErrors($this->contactService->getErrors());
  }

  public function destroy(Request $request)
  {
      $data = $request->all();
      $contato = $this->contactService->excluirContato($data);
      if ($contato) {
          return redirect('cliente/contatos/'.$this->contactService->getClienteId())
          ->with('type', 'success')
          ->with('title', 'Contato excluído!')
          ->with('message', 'Contato excluído com sucesso.')
          ->with('contato_id', $this->contactService->getContatoId());
      }
      return redirect('cliente/contatos/'.$this->contactService->getClienteId())
        ->with('type', 'error')
        ->with('title', 'Erro ao atualizar!')
        ->with('message', 'Ocorreu um erro ao excluir o Contato.')
        ->withErrors($this->contactService->getErrors());
  }

  public function getListaSimples($cliente_id)
  {
    $clienteInfo = $this->clientService->ClienteInfo($cliente_id);
    $contatos = $this->contactService->getContatoCliente($cliente_id);
    return view('gestao.interacao.lista-contatos', compact('clienteInfo','contatos'));
  }

  /**
   * Exibe formulário para criar um novo contato.
   * @param Request $request
   * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
   */
  public function getNovoContato($cliente_id)
  {
    $clienteInfo = $this->clientService->ClienteInfo($cliente_id);
    $origens = $this->origemService->getOrigem();
    return view('crm.cliente.novo-contato', compact('clienteInfo','origens'));
  }

  public function getMoverContato($cliente_id, $contato_id)
  {
      $this->middleware('needsPermission:contact.move');

      $clientes = $this->clientService->getAllClients();
      $contato = $this->contactService->getContato($cliente_id, $contato_id);

      if($contato){
          return view('crm.cliente.mover-contato', compact('clientes', 'contato'));
      }
  }

  public function putMoverContato(Request $request)
  {
      $this->middleware('needsPermission:contact.move');

      if($this->contactService->moverContato($request->all())){
          return redirect('cliente/contatos/'.$this->contactService->getClienteId())
          ->with('alert-success', 'Contato movido com sucesso!')
          ->with('nav', 'contatos');
      }
  }

}
