<?php

namespace App\Http\Controllers\Crm\Cliente;

use Illuminate\Http\Request;
use App\Http\Requests;

use App\Http\Controllers\CrmController;

use App\Services\ClientService;
use App\Services\InteractionTypeService;
use App\Services\InteractionService;

class InteracoesController extends CrmController
{
  protected $clientService;
  protected $interactionTypeService;
  protected $interactionService;
  protected $user_id;

  public function __construct(clientService $clientService, InteractionService $interactionService, InteractionTypeService $intTypeService)
  {
    if (\Auth::user() ? $this->user_id = \Auth::user()->id : $this->user_id = null);
    $this->section = 'Histórico de interações';
    $this->clientService = new ClientService($this->user_id);

    $this->interactionTypeService = $intTypeService;
    $this->interactionTypeService->setUserId($this->user_id);

    $this->interactionService = $interactionService;
    $this->interactionService->setUserId($this->user_id);
  }

  public function index($cliente_id = null)
  {
      return $this->show($cliente_id);
  }

  public function show($cliente_id)
  {
    $section = $this->section;
    $cliente = $this->clientService->getClient($cliente_id);

    if($cliente){
      $clienteInfo = $this->clientService->ClienteInfo($cliente_id);
      $interactions = $cliente->interactions()->orderBy('id', 'DESC')->paginate(20);
      $pending = $this->interactionService->isPending($this->user_id);
      return view('crm.cliente.interacoes', compact('section','clienteInfo','interactions','pending'));
    }else{
      return redirect('clientes')
        ->with('type', 'warning')
        ->with('title', 'Cliente não encontrado')
        ->with('message', 'Infelizmente, não localizamos o cliente solicitado');
    }
  }

}
