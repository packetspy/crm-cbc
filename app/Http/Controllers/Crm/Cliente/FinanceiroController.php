<?php

namespace App\Http\Controllers\Crm\Cliente;

use Illuminate\Http\Request;
use App\Http\Requests;

use App\Http\Controllers\CrmController;

use App\Services\ClientService;
use App\Services\CobrancaService;
use App\Services\StatusService;

class FinanceiroController extends CrmController
{
  protected $clientService;
  protected $cobrancaService;
  protected $statusService;
  protected $user_id;

  public function __construct(clientService $clientService, CobrancaService $cobrancaService, StatusService $statusService)
  {
    if (\Auth::user() ? $this->user_id = \Auth::user()->id : $this->user_id = null);
    $this->section = 'Financeiro/Cobrança';
    $this->clientService = new ClientService($this->user_id);
    $this->cobrancaService = $cobrancaService;
    $this->statusService = $statusService;
  }

  public function index($cliente_id = null)
  {
      return $this->show($cliente_id);
  }

  public function show($cliente_id)
  {
    $section = $this->section;
    $cliente = $this->clientService->getClient($cliente_id);

    if($cliente){
      $clienteInfo = $this->clientService->ClienteInfo($cliente_id);
      $dados_cobranca = $this->cobrancaService->getDadosCobranca($this->clientService->getClienteId());
      $endereco_cobranca = $this->cobrancaService->getEnderecoCobranca($this->clientService->getClienteId());
      $historico_cobranca = $this->cobrancaService->getHistoricoCobranca($this->clientService->getClienteId());
      $status = $this->statusService->getStatusFinanceiro();
      $estados = \App\Helpers\Estado::getAll();
      $formaspagamentos = \App\Helpers\FormaPagamento::getAll();
      $recorrencia = \App\Helpers\Recorrencia::getAll();
      return view('crm.cliente.financeiro', compact('section','clienteInfo','dados_cobranca','endereco_cobranca','historico_cobranca','estados','formaspagamentos','recorrencia','status'));
    }else{
      return redirect('clientes')
        ->with('type', 'warning')
        ->with('title', 'Cliente não encontrado')
        ->with('message', 'Infelizmente, não localizamos o cliente solicitado');
    }
  }

  public function putDadosCobranca(Request $request)
  {
      $data = $request->all();
      $client_id = $data['cliente_id'];
      $payment = $this->cobrancaService->updateDadosCobranca($data, $client_id);
      if ($payment) {
          return redirect()->action('Crm\Cliente\FinanceiroController@show', $client_id)->with(['alert-success' => 'Dados de Cobrança atualizado!']);
      }
      return redirect()->action('Crm\Cliente\FinanceiroController@show', $client_id)->with('alert-danger', 'Falha ao salvar dados!')->withErrors($this->cobrancaService->getErrors());
  }

  public function putEnderecoCobranca(Request $request)
  {
      $data = $request->all();
      $client_id = $data['cliente_id'];
      $payment = $this->cobrancaService->updateEnderecoCobranca($data, $client_id);
      if ($payment) {
          return redirect()->action('Crm\Cliente\FinanceiroController@show', $client_id)->with(['alert-success' => 'Endereço de Cobrança atualizado!']);
      }
      return redirect()->action('Crm\Cliente\FinanceiroController@show', $client_id)->with('alert-danger', 'Falha ao salvar dados!')->withErrors($this->cobrancaService->getErrors());
  }

  public function postHistoricoCobranca(Request $request)
  {
      $data = $request->all();
      $client_id = $data['cliente_id'];
      $payment = $this->cobrancaService->storeHistoricoCobranca($data, $client_id);
      if ($payment) {
          return redirect()->action('Crm\Cliente\FinanceiroController@show', $client_id)->with(['alert-success' => 'Histórico de cobrança adicionado!']);
      }
      return redirect()->action('Crm\Cliente\FinanceiroController@show', $client_id)->with('alert-danger', 'Falha ao salvar dados!')->withErrors($this->cobrancaService->getErrors());
  }

}
