<?php

namespace App\Http\Controllers\Crm\Cliente;

use Illuminate\Http\Request;
use App\Http\Requests;

use App\Http\Controllers\CrmController;

use App\Services\ClientService;
use App\Services\ContactService;
use App\Services\UserService;


class ResumoController extends CrmController
{
  protected $clientService;
  protected $statusService;
  protected $userService;

  public function __construct(ClientService $clienteService, ContactService $contactService, UserService $userService)
  {
      if (\Auth::user() ? $this->user_id = \Auth::user()->id : $this->user_id = null);
      $this->section = 'Resumo';
      $this->clientService = new ClientService($this->user_id);
      $this->contactService = $contactService;
      $this->userService = $userService;
  }
  public function index($cliente_id = null)
  {
      return $this->show($cliente_id);
  }

  public function show($cliente_id)
    {
      $section = $this->section;
      $cliente = $this->clientService->getClient($cliente_id);

      if($cliente){
        $clienteInfo = $this->clientService->ClienteInfo($cliente_id);
        $contatos = $this->contactService->getContatoCliente($this->clientService->getClienteId());
        return view('crm.cliente.resumo', compact('section','clienteInfo','cliente','contatos'));
      }else{
        return redirect('clientes')
          ->with('type', 'warning')
          ->with('title', 'Cliente não encontrado')
          ->with('message', 'Infelizmente, não localizamos o cliente solicitado');
      }
    }
}
