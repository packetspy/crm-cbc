<?php

namespace App\Http\Controllers\Crm\Cliente;

use Illuminate\Http\Request;
use App\Http\Requests;

use App\Http\Controllers\CrmController;

use App\Services\ClientService;
use App\Services\RelevanceService;

class RelevanciaController extends CrmController
{
  protected $clientService;
  protected $relevanceService;
  protected $user_id;

  public function __construct(clientService $clientService, RelevanceService $relevanceService)
  {
    if (\Auth::user() ? $this->user_id = \Auth::user()->id : $this->user_id = null);
    $this->section = 'Relevancia';
    $this->clientService = new ClientService($this->user_id);

    $this->relevanceService = $relevanceService;
    $this->relevanceService->setUserId($this->user_id);
  }

  public function index($cliente_id = null)
  {
      return $this->show($cliente_id);
  }

  public function show($cliente_id)
  {
    $section = $this->section;
    $cliente = $this->clientService->getClient($cliente_id);

    if($cliente){
      $clienteInfo = $this->clientService->ClienteInfo($cliente_id);
      $relevancia = $this->relevanceService->getRelevance($clienteInfo['id_plataforma']);
      $historico = $this->relevanceService->getHistory($clienteInfo['id_plataforma']);
      return view('crm.cliente.relevancia', compact('section','clienteInfo','relevancia','historico'));
    }else{
      return redirect('clientes')
        ->with('type', 'warning')
        ->with('title', 'Cliente não encontrado')
        ->with('message', 'Infelizmente, não localizamos o cliente solicitado');
    }
  }

}
