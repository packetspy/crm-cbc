<?php

namespace App\Http\Controllers\Crm\Cliente;

use Illuminate\Http\Request;
use App\Http\Requests;

use App\Http\Controllers\CrmController;

use App\Services\ClientService;
use App\Services\StatusService;
use App\Services\OrigemService;
use App\Services\TipoEmpresaService;
use App\Services\UserService;

class ClienteController extends CrmController
{
  protected $clientService;
  protected $user_id;
  protected $escritorio_id;

  public function __construct(clientService $clientService, StatusService $statusService, TipoEmpresaService $tipoEmpresaService, OrigemService $origemService, UserService $userService)
  {
    if (\Auth::user() ? $this->user_id = \Auth::user()->id : $this->user_id = null);
    if (\Auth::user() ? $this->escritorio_id = \Auth::user()->escritorio_id : $this->escritorio_id = null);
    $this->section = 'Financeiro/Pagamentos';
    $this->clientService = new ClientService($this->user_id, $this->escritorio_id);
    $this->statusService = new StatusService();
    $this->origemService = new OrigemService();
    $this->tipoEmpresaService = new TipoEmpresaService();
    $this->userService = $userService;
  }

  public function store(Request $request)
  {
      $data = $request->all();
      if ($data['tipo'] == 'cliente') {
        $cliente = $this->clientService->adicionaCliente($data);
        if ($cliente){
            return redirect('cliente/resumo/'.$this->clientService->getClienteId() )
            ->with('alert-success', 'Cliente cadastrado com sucesso!');
        }else{
          return redirect('cliente/novo-cliente')
            ->withInput($data)
            ->withErrors($this->clientService->getErrors());
        }

      }elseif($data['tipo'] == 'lead'){
        $lead = $this->clientService->adicionaLead($data);
        if ($lead){
            return redirect('cliente/resumo/'.$this->clientService->getClienteId())
            ->with('alert-success', 'Cliente cadastrado com sucesso!');
        }else{
          return redirect('cliente/novo-lead')
            ->withInput($data)
            ->withErrors($this->clientService->getErrors());
      }
  }
}

  public function destroy(Request $request)
  {
    $data = $request->all();
    $cliente = $this->clientService->excluirCliente($data);
    if ($cliente) {
        return redirect('clientes')
        ->with('type', 'success')
        ->with('title', 'Cliente excluído!')
        ->with('message', 'Cliente excluído com sucesso.');
    }
    return redirect('cliente/contatos/'.$this->clientService->getClienteId())
      ->with('type', 'error')
      ->with('title', 'Erro ao atualizar!')
      ->with('message', 'Ocorreu um erro ao excluir o Cliente.')
      ->withErrors($this->clientService->getErrors());
  }

  /**
   * Exibe formulário para criar um novo cliente.
   * @param Request $request
   * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
   */
  public function getNovoCliente(Request $request)
  {
    $consultores = $this->userService->getConsultores();
    $status = $this->statusService->getStatus(null, null);
    $origens = $this->origemService->getOrigem();
    $tipoempresa = $this->tipoEmpresaService->getTipoEmpresa();
    $tipodocumento = \App\Helpers\TipoDocumento::getAll();
      return view('crm.cliente.novo-cliente', compact('consultores','status','origens','tipoempresa','tipodocumento'));
  }

  /**
   * Exibe formulário para criar um novo lead.
   * @param Request $request
   * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
   */
  public function getNovoLead(Request $request)
  {
    $consultores = $this->userService->getConsultores();
    $status = $this->statusService->getStatus(null, null);
    $origens = $this->origemService->getOrigem();
      return view('crm.cliente.novo-lead', compact('consultores','status','origens'));
  }

}
