<?php
namespace App\Http\Controllers\Crm;

use App\Http\Controllers\CrmController;
use App\Services\InteractionTypeService;
use Illuminate\Http\Request;

class InteractionTypeController extends CrmController
{

    protected $interactionService;

    public function __construct(interactionTypeService $service)
    {
      if (\Auth::user() ? $user_id = \Auth::user()->id : $user_id = null);
      $this->interactionTypeService = $service;
      $this->interactionTypeService->setUserId($user_id);
    }

    public function getIndex()
    {
        $interactionTypes = $this->interactionTypeService->getTypes(10);
        return view('gestao.tipo_interacao.index', compact('interactionTypes'));
    }

    /**
     * Formulário para criar um produto
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCreate(Request $request)
    {
        if (!$request->ajax())
            abort(404);

        return view('gestao.tipo_interacao.create');
    }

    public function getEdit(Request $request, $id)
    {
        if (!$request->ajax())
            abort(404);

        $interactionType = $this->interactionTypeService->getType($id);
        if ($interactionType)
            return view('gestao.tipo_interacao.create', compact('interactionType'));

        return response()->make('Tipo de interação não encontrada.');
    }

    public function getCreateSub(Request $request, $id)
    {
        if (!$request->ajax())
            abort(404);

        return view('gestao.tipo_interacao.create')->with('father_id', $id);
    }

    public function getInteractionSub(Request $request, $id)
    {
        return response()->json($this->interactionTypeService->getFilhos($id));
    }

    public function postSaveInteractionType(Request $request)
    {
        $data = $request->all();
        $wasCreated = $this->interactionTypeService->save($data);

        if ($wasCreated)
        {
            return redirect()->action('Crm\InteractionTypeController@getIndex')->with('alert-success', 'Tipo de interação salva!');
        }
        return redirect()->action('Crm\InteractionTypeController@getIndex')->withErrors( $this->interactionTypeService->getErrors() );
    }

    public function postUpdateInteractionType(Request $request, $id)
    {
        $interactionType = $this->interactionTypeService->getType($id);
        if (!$interactionType){
            return redirect()->action('Crm\InteractionTypeController@getIndex')->with('alert-danger', 'Interação não encontrada');
        }

        $data = $request->all();
        $wasUpdated = $this->interactionTypeService->update($id, $data);
        if ($wasUpdated) {
            return redirect()->action('Crm\InteractionTypeController@getIndex')->with('alert-success', 'Tipo de interação atualizada com sucesso');
        }

        return redirect()->action('Crm\InteractionTypeController@getIndex')->with('alert-danger', 'Erro')->withErrors($this->interactionTypeService->getErrors());
    }
}
