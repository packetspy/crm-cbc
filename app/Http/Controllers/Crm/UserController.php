<?php
/**
 * Created by PhpStorm.
 * User: Lúdio
 * Date: 07/07/2016
 * Time: 16:13
 */

namespace App\Http\Controllers\Crm;


use App\Http\Controllers\CrmController;
use App\Services\ClientService;
use App\Services\ExportService;
use App\Services\PermissionService;
use App\Services\ProductService;
use App\Services\ContactService;
use App\Services\EscritorioService;
use App\Services\UserService;
use App\Services\CalendarService;
use App\Services\InteractionService;
use App\Services\PaymentService;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use App\Models\User;
use App\Models\Role;
use Illuminate\Support\Facades\Auth;

class UserController extends CrmController
{
    protected $productService;
    protected $userService;
    protected $contactService;
    protected $escritorioService;
    protected $interactionService;
    protected $paymentService;
    protected $clientService;
    protected $permissionService;
    protected $calendarService;
    protected $permissoesDisponiveis = array();

    public function __construct(PermissionService $permissionService, ProductService $productService, ContactService $contactService, EscritorioService $escritorioService, InteractionService $interactionService, PaymentService $paymentService, UserService $userService, ClientService $clientService, CalendarService $calendarService)
    {
        //$user_id = Auth::user()->id;
        if (\Auth::user() ? $user_id = \Auth::user()->id : $user_id = null);

        $this->productService = $productService;
        $this->productService->setUserId($user_id);

        $this->userService = $userService;
        $this->userService->setUserId($user_id);

        $this->contactService = $contactService;
        $this->contactService->setUserId($user_id);

        $this->escritorioService = $escritorioService;

        $this->interactionService = $interactionService;
        $this->interactionService->setUserId($user_id);

        $this->paymentService = $paymentService;
        $this->paymentService->setUserId($user_id);

        $this->clientService = $clientService;
        $this->clientService->setUserId($user_id);

        $this->permissionService = $permissionService;
        $this->calendarService = $calendarService;

        $this->permissoesDisponiveis = array (
            'product' => $this->productService->getPermissions(),
            'user' => $this->userService->getPermissions(),
            'client' => $this->clientService->getPermissions(),
            'contact' => $this->contactService->getPermissions(),
            'calendar' => $this->calendarService->getPermissions(),
            'interaction' => $this->interactionService->getPermissions(),
            'payment' => $this->paymentService->getPermissions(),
            'export_import' => ''
        );

    }

    public function getIndex()
    {
        $this->middleware('needsPermission:user.index');

        $users = User::orderBy('name', 'asc')->paginate(20);
        return view('gestao.usuarios.index', compact('users'));
    }

    public function getCreate()
    {
        $roles = $this->userService->getAllRoles();
        $escritorios = $this->escritorioService->getEscritorios();
        $permissoes = $this->permissoesDisponiveis;
        return view('gestao.usuarios.create', compact('permissoes', 'roles','escritorios'));
    }

    public function getEdit($id)
    {
        $this->middleware('needsPermission:user.edit');

        $user = User::find($id);
        $rolesofuser = $user->roles;

        //$user = User::find($id)->with('roles');

        /*$user = User::find($id)->whereHas('roles', function($query)
      {
        $query->where('name',true);
      })->get();*/

        //$roles = Role::pluck('id', 'name');
        //$roles = Role::all();
        $roles = $this->userService->getAllRoles();
        $escritorios = $this->escritorioService->getEscritorios();
        $user_permissions = $user->getArrayPermissions();
        $permissoes = $this->permissoesDisponiveis;
        return view('gestao.usuarios.create', compact('user','permissoes','user_permissions','roles','rolesofuser','escritorios'));
    }

    public function postStore(UserRequest $request)
    {
        $data = array();
        $data['permissions'] = [];
        $data = $request->all();

        if ( $this->userService->save($data) ) {
            return redirect()->action('Crm\UserController@getIndex')->with('success', 'Usuário cadastrado com sucesso!');
        }
        return redirect()->back()->with('danger', 'Falha ao salvar usuário!');
    }

    public function postUpdate(Request $request, $id)
    {
        $data = array();
        $data['permissions'] = [];
        $data = $request->all();

        $update = $this->userService->updateUser($user_id = $id, $data);

        if (!$update)
            return redirect()->action('Crm\UserController@getEdit', $id)->withErrors(  $this->userService->getErrors() );

        return redirect()->action('Crm\UserController@getEdit', $id)->with('success', 'Usuário atualizado com sucesso!');

    }

    public function getLogs($id)
    {
        $user = $this->userService->getUserById($id);
        if (!$user)
            return redirect()->action('Crm\UserController@getIndex')->with('danger', 'Usuário não existe');

        $logs = $this->userService->getLogs($id);

        return view('gestao.usuarios.logs', compact('user', 'logs'));
    }

    public function getStatistics(Request $request, $id)
    {
        $user = $this->userService->getUserById($id);
        if (!$user)
            return redirect()->action('Crm\UserController@getIndex')->with('danger', 'Usuário não existe');

        $start = $request->has('from') ? $request->get('from') : '';
        $end = $request->has('to') ? $request->get('to') : '';

        $estatisticas = $this->userService->getStatistics($id, $start, $end);

        return view('gestao.usuarios.statistics', compact('user', 'estatisticas', 'start', 'end'));
    }

    public function getDeletar(Request $request, $id)
    {

      $user_delete = $this->userService->deleteUser($id);

      if ($user_delete)
      {
          //return redirect()->action('Crm\UserController@getIndex')->with('success', 'Usuário foi excluido!');
          return redirect()->action('Crm\UserController@getIndex')
            ->with('type', 'success')
            ->with('title', 'Usuário foi excluido!')
            ->with('message', '');
      }
      return redirect()->action('Crm\UserController@getIndex')
            ->with('type', 'danger')
            ->with('title', 'Usuário não foi excluído!')
            ->with('message', '');
    }

    public function getPerfil()
    {
      $user = User::find(\Auth::user()->id);
      return view('auth/perfil', compact('user'));
    }

    public function postAlterarSenha(Request $request)
    {
        $email = \Auth::user()->email;
        $old_password = $request->get('old_password');
        $current_user_password = \Auth::user()->getAuthPassword();

        if (\Hash::check ( $old_password, $current_user_password ) )
        {
            $rules = [
                'password' => 'required|min:6|confirmed',
            ];

            $validator = \Validator::make( $request->all(), $rules );

            if ( $validator->fails() )
            {
                return redirect()->back()
                ->with('type', 'danger')
                ->with('title', 'Senhas não coincidem.')
                ->with('message', '');
            }



           $usuario = \Auth::user();
           $usuario->password = bcrypt($request->get('password'));
           $usuario->save();
           //return redirect()->back()->with('success', 'Senha atualizada com sucesso!');
           //return redirect('/perfil')->with(['alert-success' => 'Senha atualizada com sucesso!']);

           //return 'AQUIIIIIII';
           return redirect('perfil')
             ->with('type', 'success')
             ->with('title', 'Senha Atualizada!')
             ->with('message', '');

        }
        else {
            return redirect()->back()
            ->with('type', 'danger')
            ->with('title', 'Senha atual não coincide.')
            ->with('message', '');
        }
        return redirect()->back()
        ->with('type', 'danger')
        ->with('title', 'Ocorreu um erro.')
        ->with('message', '');
    }
}
