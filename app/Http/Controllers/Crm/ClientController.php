<?php

namespace App\Http\Controllers\Crm;

use App\Services\ContactProductRepository;
use App\Services\ProductService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

use App\Http\Controllers\CrmController;
use App\Services\ClientService;
use App\Services\InteractionTypeService;
use App\Services\InteractionService;
use App\Services\PaymentService;
use App\Services\ContactService;
use App\Services\RelevanceService;
use App\Services\StatusService;
use App\Services\UserService;

class ClientController extends CrmController
{
    protected $clientService;
    protected $interactionTypeService;
    protected $interactionService;
    protected $paymentService;
    protected $contactService;
    protected $relevanceService;
    protected $productService;
    protected $statusService;
    protected $message = null;
    protected $contactRepository;

    protected $interacoes_contraparte = null;

    public function __construct(InteractionService $interactionService, PaymentService $paymentService, InteractionTypeService $intTypeService, ContactService $contactService, RelevanceService $relevanceService, StatusService $statusService, UserService $userService)
    {
        if (\Auth::user() ? $this->user_id = \Auth::user()->id : $this->user_id = null);
        $this->clientService = new ClientService($this->user_id);

        $this->interactionTypeService = $intTypeService;
        $this->interactionTypeService->setUserId($this->user_id);

        $this->interactionService = $interactionService;
        $this->interactionService->setUserId($this->user_id);

        $this->paymentService = $paymentService;
        $this->paymentService->setUserId($this->user_id);

        $this->relevanceService = $relevanceService;
        $this->relevanceService->setUserId($this->user_id);

        $this->contactService = $contactService;
        $this->contactService->setUserId($this->user_id);

        $this->productService = new ProductService($this->user_id);
        $this->statusService = new StatusService();
        $this->contactRepository = new ContactProductRepository();

        $this->userService = $userService;

        $this->interacoes_contraparte = ['Negócio fechado', 'Não', 'Produto ruím', 'Negócio direto', 'Negócio cancelado'];
    }

    /**
     * @return clientes
     */
    public function getIndex(Request $request)
    {
      $titulo = "Todos os clientes";
      $subtitulo = "Todos os clientes relacionados no CRM";

      if ($request->has('order_by') ? $order_by = $request->get('order_by') : $order_by = 'empresa');
      if ($request->has('order') ? $order = $request->get('order') : $order = 'asc');
      //if ($request->has('status') ? $status = $this->clientService->discoverStatus($request->get('status')) : $status = null);
      if ($request->has('status') ? $status = $request->get('status') : $status = null);
      if ($request->has('limit') ? $limit = $request->get('limit') : $limit = 50);

      $clients = $this->clientService->getClientes($limit, $status, $order_by, $order, null);

      return view('crm.cliente.index', compact('titulo', 'subtitulo', 'clients'));
    }

    public function getMeusClientes(Request $request)
    {
      $titulo = "Meus Clientes";
      $subtitulo = "Clientes que sou responsável";

      if ($request->has('order_by') ? $order_by = $request->get('order_by') : $order_by = 'empresa');
      if ($request->has('order') ? $order = $request->get('order') : $order = 'asc');
      if ($request->has('status') ? $status = $this->clientService->discoverStatus($request->get('status')) : $status = null);
      if ($request->has('limit') ? $limit = $request->get('limit') : $limit = 50);

      $clients = $this->clientService->getClientes($limit, $status, $order_by, $order, \Auth::user()->id);
      //$clients_in_trash = $this->clientService->countClientsInTrash();
      //$products = $this->productService->getProducts(null, false);

      if ($request->has('orderBy'))
      {
          $order_by = $request->get('orderBy');
          $order = $request->get('order');
          $clients = $this->clientService->getClients($qtd = 50, $ativo = 1, $order_by, $order);
      }

      return view('crm.clientes', compact('titulo', 'subtitulo', 'clients'));
    }

    /**
     * Função para Buscar Contatos por Interesse
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getContactsSearch(Request $request)
    {
        if (!$request->has('search'))
            abort(404);

        $products = $request->has('products') ? $request->get('products') : null;
        $estados = $request->has('estados') ? $request->get('estados') : null;
        $transacao = $request->has('tipo') ? $request->get('tipo') : null;
        $contacts = $this->contactRepository->searchContacts($transacao, $products, $estados);
        $contacts = $contacts->paginate(10)->appends(['search' => true, 'products[]' => $products, 'estados[]' => $estados, 'tipo' => $transacao]);
        $products = $this->productService->getProducts(null, false);

        return view('gestao.clientes.contatos', compact('contacts', 'products'));
    }

    /**
     * Exibe dados do cliente
     * e seus respectivos contatos / histórico de interações.
     * @param Request $request
     * @param null $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getEdit____remover(Request $request, $id = null)
    {

        if( $request->has('nav') )
            \Session::set('nav', $request->get('nav'));

        if ($id == null) abort(404);
        $client = $this->clientService->getClient($id);

        if ($client)
        {
          //return $this->clientService->getClienteId();
          //return $client->id;

            $contatos = $this->contactService->getContatos($this->clientService->getClienteId());
            $interactions = $client->interactions()->orderBy('id', 'DESC')->paginate(10);
            $payment_types = $this->paymentTypeService->getPaymentsTypes();
            $payments = $this->paymentService->getPayment($this->clientService->getClienteId());
            $relevance = $this->relevanceService->getRelevance($client->id_plataforma);
            $consultores = $this->userService->getConsultores();
            $packages = $this->productService->getLists(null, false);
            $products = $this->productService->getProducts(null, false);
            $status = $this->statusService->getStatus(null, null);

            $pending = $this->interactionService->isPending($this->user_id);

            // $contraparte_bloqueada = null;
            // if($this->interactionService->obrigatorioContraparte(\Auth::user()->id, null)){
            //     $contraparte_bloqueada = $this->interactionService->getBloqueadas();
            // }

            return view('gestao.clientes.edit', compact(
                  'client',
                  'contatos',
                  'interactions',
                  'payments',
                  'payment_types',
                  'relevance',
                  'products',
                  'packages',
                  'status',
                  'consultores',
                  'pending'
                ));
        }

        return redirect()->action('Crm\ClientController@getIndex')->with('alert-warning', 'Cliente não encontrado.');
    }

    /*
     * Inserir novo cliente
     */
    public function postStoreClient(Request $request)
    {
        $data = $request->all();
        $client = $this->clientService->save($data);
        if ($client)
        {
            return redirect()->action('Crm\Cliente\ResumoController@show', $this->clientService->getClienteId())->with('alert-success', 'Cliente cadastrado com sucesso!');
        }

        return redirect()->action('Crm\ClientController@getCreate')->withInput($data)->withErrors($this->clientService->getErrors());
    }

    public function changeClientResponsible(Request $request)
    {
      $data = $request->all();
      $id = $data['id'];
      unset($data['_token']);
      unset($data['id']);

      // dd($data);

      $client = $this->clientService->update($id,$data);

      // dd($client);

      if ($client) {

        $data_tosave = [
            'observacao' => 'Cliente direcionado para um consultor',
            'interaction_type_id' => 17,
            "father_id" => null,
            'data_contato' => date('Y-m-d H:i:s')
        ];

        $interacao = $this->interactionService->salvarInteracao($id, $data_tosave);

        return redirect()->action('Crm\ClientController@getIndex')->with('alert-success', 'Consultor adicionado com sucesso.');
        //return true;
      }
      else {
        return redirect()->action('Crm\ClientController@getIndex')->with('alert-danger', 'Falha ao adicionar Consultor.');
        //return false;
      }
    }


    public function getEscolheInteracao(Request $request, $client_id, $father_id=null, $show_form = true)
    {
        $referrerUrl = $request->headers->get('referer');

        $contraparte = $this->interactionService->exibeApenasContraParte($father_id);
        $this->interactionTypeService->setInteractionTypeId($this->interactionService->getInteractionTypeId());
        $interacoes = $this->interactionTypeService->getTypesAtivos(10, $contraparte);
        $message = $this->message;

        $exibir_fechar = true;
        if($father_id) {
             $interacao_father = $this->interactionService->getInteraction(['id' => $father_id])->interaction_type->nome;
             if (in_array($interacao_father, $this->interacoes_contraparte))
             {
                 $exibir_fechar = false;
             }
        }


        //Excluir contraparte bloqueada
        /*if($this->interactionService->obrigatorioContraparte(\Auth::user()->id, $father_id)){
            return view('gestao.interacao.bloqueada', [
                'contraparte_bloqueada' => $this->interactionService->getBloqueadas()
            ]);
        }*/

        $contraparte_bloqueada = null;
        if($this->interactionService->obrigatorioContraparte(\Auth::user()->id, $father_id)){
            $contraparte_bloqueada = $this->interactionService->getBloqueadas();
        }
        return view('gestao.interacao.escolha', compact('interacoes', 'client_id', 'father_id', "message", "show_form", "contraparte", "contraparte_bloqueada", "exibir_fechar","referrerUrl"));
    }


    /**
     * Cadastrar interação
     * @param Request $request
     * @param $id
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function postInteracaoSimples(Request $request, $cliente_id)
    {
        $data = $request->all();
        $idInteraction = null;
        $client_id = $cliente_id;

        for($i = 1; $i < count($data); $i++) {
            if(isset($data["interacao_".$i]))
                $data["interaction_type_id"] = $data["interacao_".$i];
        }

        $interactionType = $this->interactionTypeService->getType($data["interaction_type_id"]);
        $data_tosave = [
            'observacao' => $data['observacao'],
            'interaction_type_id' => $data['interaction_type_id'],
            "father_id" => $data['father_id'] == "" ? null : $data['father_id'],
            'data_contato' => null
        ];

        if($interactionType->nome != "Negócio fechado" && $data['dia_contato'] != '' && $data['hora_contato'] != ''){
            $data_tosave = array_merge($data_tosave, ['data_contato' => $data['dia_contato'] . ' ' . $data['hora_contato']]);
        }

        $interacao = $this->interactionService->salvarInteracao($client_id, $data_tosave);
        if ($interacao) {

          //Inativa cliente a partir de uma interaçao
          switch ($data['interacao_1']) {
            case 16:
                $alteraStatus = $this->clientService->setStatusCliente($client_id, 3);
                $this->message = "Interaçao registrada, Cliente Inativado";
                break;
            case 17:
                $alteraStatus = $this->clientService->setStatusCliente($client_id, 1);
                $this->message = "Interaçao registrada, Cliente Sem perfil";
                break;
            case 18:
                $alteraStatus = $this->clientService->setStatusCliente($client_id, 8);
                $this->message = "Interaçao registrada, Cliente atualizado";
                break;
            default:
              $this->message = "Cadastro de interação realizado com sucesso";
          }

            $interacoes_de_contraparte = $this->interacoes_contraparte;

           // if($interactionType->nome == "Negócio fechado")
             if (in_array($interactionType->nome, $interacoes_de_contraparte))
                $idInteraction = $this->interactionService->getLastInteractionId();

            if($interactionType->exibir_formulario == false){
                return $this->getEscolheInteracao($request,$client_id, $idInteraction, null, (isset($show_form)? $show_form : false));
            }

            return $this->getEscolheInteracao($request, $client_id, $idInteraction);
        }

        return $this->interactionService->getErrors();
    }

    /**
     * Busca do autocomplete
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSearch(Request $request)
    {
        $q = $request->get('name');

        $clients = ($request->has('name')) ?
                $this->clientService->searchAutoComplete($q)
                : $this->clientService->searchAutoComplete();

        return response()->json($clients);
    }

    /*
     * Visualizar interação
     */
    public function getViewInteracao($client_id = null, $id = null)
    {
        if (is_null($client_id) || is_null($id))
            abort(404);

        $interacao = $this->interactionService
                         ->getInteraction(['client_id' => $client_id, 'id' => $id]);

        if ($interacao)
            return view('gestao.interacao.view', compact('interacao'));

        return response()->make('Interação não existe!');
    }

    public function getViewSelectResponsible($client_id = null,$responsavel_id = null)
    {
      if (is_null($client_id))
          abort(404);

          $client = $this->clientService->getClient($client_id);

          $consultores = $this->userService->getConsultores();

      return view('partials.responsaveis', compact('consultores','client','responsavel_id'));
    }


    public function getStatistics($id)
    {
        $clientStatistics = $this->clientService->getStatisticsByClientId($id);
    }


    public function getMoveInteraction($client_id, $interaction_id)
    {
        $this->middleware('needsPermission:interaction.move');

        $clients = $this->clientService->getAllClients();
        $interaction = $this->interactionService->getInteractionInfo($client_id, $interaction_id);

        if($interaction){
            return view('crm.cliente.mover-interacao', compact('clients', 'interaction'));
        }
    }

    public function putMoveInteraction(Request $request)
    {
        $this->middleware('needsPermission:interaction.move');

        if($this->interactionService->moveInteraction($request->all())){
            return redirect()->action('Crm\Cliente\InteracoesController@show', $request->actualclient_id)
                ->with('alert-success', 'Interação movida com sucesso!')
                ->with('nav', 'historico');
        }
    }


}
