<?php

namespace App\Http\Controllers\Crm\Database;

use Illuminate\Http\Request;
use App\Http\Requests;

use App\Http\Controllers\CrmController;
use App\Services\ClientService;
use App\Services\ExportService;
use App\Services\ContactService;
use App\Services\HistoricoLeadService;
use App\Services\ProductService;
use App\Services\CobrancaService;
use App\Services\InteractionService;


class ExportController extends CrmController
{
    protected $clientService;
    protected $exportService;

    public function __construct(ExportService $export)
    {
        if (\Auth::user() ? $this->user_id = \Auth::user()->id : $this->user_id = null);
        $this->exportService = $export;
        $this->clientService = new ClientService($this->user_id);
        $this->contactService = new ContactService($this->user_id);
        $this->historicoLeadService = new HistoricoLeadService($this->user_id);
        $this->productService = new ProductService($this->user_id);
        $this->cobrancaService = new CobrancaService($this->user_id);
        $this->interactionService = new InteractionService($this->user_id, null);

        //Habilita muita memoria para exportacao de relatorios grandes
        ini_set('memory_limit',env('MEMORY'));
    }

    public function index()
    {
        return view('crm.database.export');
    }

    public function getClientes()
    {
        return $this->exportService->export('Clientes', $this->clientService->getClientsToExport());
    }

    public function getContatos()
    {

        return $this->exportService->export("Contatos", $this->contactService->getContactsToExport(null,null));
    }

    public function postLeads(Request $request)
    {
        $data = $request->all();
        return $this->exportService->export("Leads", $this->historicoLeadService->getLeadsToExport($data['data_inicial'], $data['data_final']));
    }

    public function getProdutos()
    {
        return $this->exportService->export("Produtos", $this->productService->getProductsToExport());
    }

    public function postFinanceiro(Request $request)
    {
        $data = $request->all();
        return $this->exportService->export("Financeiro", $this->cobrancaService->getHistoricoToExport($data['data_inicial'], $data['data_final']));
    }

    public function getUltimasInteracoes()
    {
      return $this->exportService->export("LastInteractions", $this->interactionService->getLastInteractionsToExport());
    }
}
