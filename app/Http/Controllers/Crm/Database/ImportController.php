<?php

namespace App\Http\Controllers\Crm\Database;

use Illuminate\Http\Request;

use App\Http\Controllers\CrmController;
use App\Services\ClientService;
use App\Services\ContactService;
use App\Services\ContactInfoService;
use App\Services\HistoricoLoginService;
use App\Services\ProductService;
use App\Services\RelevanceService;
use App\Services\ImportService;
use App\Services\ExportService;


class ImportController extends CrmController
{
    protected $clientService;
    protected $importService;
    protected $user_id;

    public function __construct(ImportService $import, ExportService $export)
    {
        if (\Auth::user() ? $this->user_id = \Auth::user()->id : $this->user_id = null);
        $this->importService = $import;
        $this->exportService = $export;
        $this->clientService = new ClientService($this->user_id);
        $this->contactService = new ContactService($this->user_id);
        $this->historicoLoginService = new HistoricoLoginService();
        $this->productService = new ProductService($this->user_id);
        $this->relevanceService = new RelevanceService();
    }

    public function index()
    {
        return view('crm.database.import');
    }

    public function postClientes(Request $request)
    {
        $file = $request->file('arquivo');
        $resultado = $this->clientService->sincronizaInfoCliente($this->importService->sync($file, 'Empresas'));
        return $this->exportService->export("Resultado da importacao dos clientes", $resultado);
    }

    public function postContatos(Request $request)
    {
        $file = $request->file('arquivo');
        $resultado = $this->contactService->importedData($this->importService->import($file));
        return $this->exportService->export("Resultado da importacao dos contatos", $resultado);
    }

    public function postProdutos(Request $request)
    {
        $file = $request->file('arquivo');
        $resultado = $this->productService->importedData($this->importService->import($file));
        return $this->exportService->export("Resultado da importacao dos Produtos", $resultado);
    }

    public function postRelevancia(Request $request)
    {
        $file = $request->file('arquivo');
        $resultado = $this->relevanceService->importedData($this->importService->import($file));
        return $this->exportService->export("Resultado da importacao de relevância dos clientes", $resultado);
    }

    public function postOrigemLead(Request $request)
    {
        $file = $request->file('arquivo');
        $resultado = $this->contactService->updateOrigemLeads($this->importService->import($file));
        return $this->exportService->export("Resultado da importacao de relevância dos clientes", $resultado);
    }

    public function postLogin(Request $request)
    {
        $file = $request->file('arquivo');
        $resultado = $this->historicoLoginService->importLoginsPlataforma($this->importService->sync($file, 'Usuarios'));
        return $this->exportService->export("Resultado da importacao logins na plataforma", $resultado);
    }
}
