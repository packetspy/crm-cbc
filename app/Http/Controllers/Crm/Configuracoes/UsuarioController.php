<?php

namespace App\Http\Controllers\Crm\Configuracoes;

use Illuminate\Http\Request;
use App\Http\Requests;

use App\Http\Controllers\CrmController;

use App\Services\UserService;
use App\Services\GrupoService;
use App\Services\EscritorioService;

class UsuarioController extends CrmController
{

  public function __construct(UserService $userService, GrupoService $grupoService, EscritorioService $escritorioService)
  {
      $this->userService = $userService;
      $this->grupoService = $grupoService;
      $this->escritorioService = $escritorioService;
  }

  public function index()
  {
    $titulo = 'Usuários';
    $subtitulo = 'Relação de usuários do CRM';
    $usuarios = $this->userService->getUsuarios();
    return view('crm.configuracoes.usuarios.index', compact('titulo','subtitulo','usuarios'));
  }

  public function edit($usuario_id)
  {
    $titulo = 'Editar Usuário';
    $usuario = $this->userService->getUsuario($usuario_id);
    $grupos = $this->grupoService->getListGrupos();
    $escritorios = $this->escritorioService->getListEscritorios();
    return view('crm.configuracoes.usuarios.edit', compact('titulo','usuario','grupos','escritorios'));
  }
}
