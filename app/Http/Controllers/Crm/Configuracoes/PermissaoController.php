<?php

namespace App\Http\Controllers\Crm\Configuracoes;

use Illuminate\Http\Request;
use App\Http\Requests;

use App\Http\Controllers\CrmController;

use App\Services\PermissionService;

class PermissaoController extends CrmController
{
  public function __construct(PermissionService $permissionService)
  {
      $this->permissionService = $permissionService;
  }

  public function index()
  {
    $titulo = 'Permissões';
    $subtitulo = 'Permissões(Permissions) aplicadas aos grupos';
    $permissoes = $this->permissionService->getPermissoes();
    return view('crm.configuracoes.permissoes.index', compact('titulo','subtitulo','permissoes'));
  }
}
