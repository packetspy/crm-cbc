<?php

namespace App\Http\Controllers\Crm\Configuracoes;

use Illuminate\Http\Request;
use App\Http\Requests;

use App\Http\Controllers\CrmController;

use App\Services\GrupoService;

class GrupoController extends CrmController
{
  protected $grupoService;

  public function __construct(GrupoService $grupoService)
  {
      $this->grupoService = $grupoService;
  }

  public function index()
  {
    $titulo = 'Grupos';
    $subtitulo = 'Grupos(Roles) aplicadas aos usuários';
    $grupos = $this->grupoService->getGrupos();
    return view('crm.configuracoes.grupos.index', compact('titulo','subtitulo','grupos'));
  }
}
