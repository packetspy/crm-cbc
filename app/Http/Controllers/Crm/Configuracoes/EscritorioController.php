<?php

namespace App\Http\Controllers\Crm\Configuracoes;

use Illuminate\Http\Request;
use App\Http\Requests;

use App\Http\Controllers\CrmController;

use App\Services\EscritorioService;

class EscritorioController extends CrmController
{
  protected $escritorioService;

  public function __construct(EscritorioService $escritorioService)
  {
      $this->escritorioService = $escritorioService;
  }

  public function index()
  {
    $titulo = 'Escritórios';
    $subtitulo = 'Relação de filiais e parceiros com acesso ao CRM';
    $escritorios = $this->escritorioService->getEscritorios();
    return view('crm.configuracoes.escritorios.index', compact('titulo','subtitulo','escritorios'));
  }
}
