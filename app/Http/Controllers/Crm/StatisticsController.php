<?php
/**
 * Created by PhpStorm.
 * User: ludio
 * Date: 25/07/16
 * Time: 20:58
 */

namespace App\Http\Controllers\Crm;


use App\Http\Controllers\CrmController;
use App\Services\StatisticService;

class StatisticsController extends CrmController
{

    protected $statisticService ;
    public function __construct(StatisticService $statisticService)
    {
        parent::__construct();

        if (\Auth::user() ? $user_id = \Auth::user()->id : $user_id = null);
        $this->statisticService = $statisticService;
        $this->statisticService->setUserId($user_id);
    }

    public function getIndex()
    {
        $usersCount = $this->statisticService->countUsers();
        $interactions = $this->statisticService->countInteractions();
        $clientsAtivos = $this->statisticService->countClients(1);
        $clientsInativos = $this->statisticService->countClients(0);
        $clientsCount = $clientsAtivos + $clientsInativos;

        return view('gestao.estatisticas.index', compact('usersCount', 'clientsAtivos', 'interactions', 'clientsInativos', 'clientsCount'));
    }


}