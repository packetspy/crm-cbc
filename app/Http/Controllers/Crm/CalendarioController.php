<?php
/**
 * Created by PhpStorm.
 * User: Lúdio
 * Date: 07/07/2016
 * Time: 16:13
 */

namespace App\Http\Controllers\Crm;


use App\Http\Controllers\CrmController;
use Illuminate\Http\Request;
use App\Models\User;
use App\Services\CalendarService;
use App\Services\InteractionService;

class CalendarioController extends CrmController
{

    protected $calendarService;
    protected $interactionService;

    public function __construct(CalendarService $calendar, InteractionService $interactionService)
    {
      if (\Auth::user() ? $user_id = \Auth::user()->id : $user_id = null);
      $this->calendarService = $calendar;
      $this->calendarService->setUserId($user_id);
      $this->interactionService = $interactionService;
    }

    public function getIndex()
    {
        $getContatos = action('Crm\CalendarioController@getContatos');
        if(empty($getContatos)){
            $getContatos = [];
        }
        return view('gestao.calendario.index', compact('calendario','getContatos'));
    }

    public function getContatos(Request $request)
    {
        $start = $request->get('start');
        $end = $request->get('end');
        $dataCalendario = $this->calendarService->getInfoCalendarioUsuario($start, $end);

        return response()->json($dataCalendario);
    }

}
