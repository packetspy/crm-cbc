<?php

namespace App\Http\Controllers\Crm;

use App\Http\Controllers\CrmController;
use App\Services\UserService;
use App\Services\ClientService as ClientService;
use Illuminate\Http\Request;
use App\Services\ProductService;
use App\Services\ContactProductRepository;
use App\Services\ContactService as ContactService;

class DashboardController extends CrmController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $userService;
    protected $productService;
    protected $contactService;

    public function __construct(UserService $userService, ContactService $contactService)
    {
        $this->middleware('auth');
        $this->userService = new UserService();

        if (\Auth::user() ? $user_id = \Auth::user()->id : $user_id = null);

        $this->clientService = new ClientService($user_id);

        $this->contactService = $contactService;
        $this->contactService->setUserId($user_id);

        $this->productService = new ProductService($user_id);
        $this->contactRepository = new ContactProductRepository();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $id = \Auth::user()->id;

        // *** Estatisticas removidas, pois estavam causando lentidão após o login (high TTFB) *** //
        //$estatisticas = $this->userService->getStatistics($id, '', '', $dashboard = true);
        //$estatisticas_gerenciais = $this->userService->getStatistics();
        // $stats = Array();
        // $stats['clientes_total'] = $this->clientService->countAllClients();
        // $stats['clientes_plataforma'] = $this->clientService->countClientsPlataforma();
        // $stats['clientes_inativos'] = $this->clientService->countClientsInTrash();
        // return view('home', compact('estatisticas', 'estatisticas_gerenciais', 'stats'));

        $responsavel = null;

        if(\Defender::is(['Atendimento', 'Gestores']))
        {
          $responsavel = 'não';
        }
        else if(\Defender::is('Consultores'))
        {
          $responsavel = 'sim';
        }

        $getContatos = action('Crm\CalendarioController@getContatos');
        if(empty($getContatos)){
            $getContatos = [];
        }

        $clients = $this->clientService->getClientes(10, null,'empresa','ASC',$responsavel);
        //$clients_in_trash = $this->clientService->countClientsInTrash();

        if ($request->has('orderBy'))
        {
            $order_by = $request->get('orderBy');
            $order = $request->get('order');
            $clients = $this->clientService->getClientes($qtd = 10, null,$order_by, $order);
        }

        return view('crm.dashboard.index', compact('clients', 'getContatos'));
    }

}
