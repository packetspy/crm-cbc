<?php

namespace App\Http\Controllers\Crm;

use App\Models\ContactInfo;
use Illuminate\Http\Request;
use App\Http\Controllers\CrmController;
use App\Helpers\Estado;
use App\Services\ClientService as ClientService;
use App\Services\ContactProductRepository;
use App\Services\ContactService as ContactService;
use App\Services\ProductService;

class ContactClientController extends CrmController
{

    protected $clientService;
    protected $productService;
    protected $contactProductRepository;
    protected $contactService;

    public function __construct(ClientService $service, ContactService $contactService, ProductService $productService)
    {
        if (\Auth::user() ? $user_id = \Auth::user()->id : $user_id = null);

        $this->clientService = $service;
        $this->clientService->setUserId($user_id);

        $this->contactService = $contactService;
        $this->contactService->setUserId($user_id);

        $this->productService = $productService;
        $this->productService->setUserId($user_id);

        $this->contactProductRepository = new ContactProductRepository();
    }

    /*
     * Used for save a contact
     */
    public function postCreateContact(Request $request, $client_id)
    {
        $client = $this->clientService->getClient($client_id);

        if ( !$client ) {
            abort(404);
            return false;
        }
        $contact_info_id= $request->get('contact_info_id');
        $tipos = $request->get('tipo');
        $valores = $request->get('valor');
        $produtos_types = $request->get('productType');
        $produtos_id = $request->get('productID');
        $produtos_states = $request->get('productStates');
        $produtos = [];
        $contatos = [];

        /* Produtos do contato */
        foreach ($produtos_id as $key => $value)
        {
            $states = explode(",", $produtos_states[$key]);
            if(in_array('todos', $states)){
                $states = Estado::getKeys();
            }

            foreach ($states as $state) {
                $produtos[] = [
                    'product_id' => $value,
                    'estado' => $state,
                    'transacao' => $produtos_types[$key]
                ];
            }
        }
        /* E-mails */
        for ($i = 0; $i < count($valores); $i++) {
            if (!empty($valores[$i])) {
                $contatos[] = [
                    'contact_info_id' => $contact_info_id[$i],
                    'tipo' => $tipos[$i],
                    'contato' => $valores[$i]
                ];
            }
        }

        $data = [
            'nome' => $request->get('nome'),
            'departamento' => $request->get('departamento'),
            'id_plataforma' => $request->get('id_plataforma'),
            'contatos' => $contatos,
            'produtos' => $produtos
        ];

        $cadastro = $this->contactService->salvarContatos($client_id, $data);
        if (!$cadastro)
            return redirect()->action('Crm\ClientController@getEdit', $client_id)->withErrors( $this->contactService->getErrors() )->with('nav', 'contatos');

        return redirect()->action('Crm\ClientController@getEdit', $client_id)->with('alert-success', 'Contato adicionado com sucesso!')->with('nav', 'contatos');
    }

    public function getEditContact($clientId, $id)
    {
        $contact = $this->contactService->getContato($clientId, $id);

        if($contact){
            $elementos = $contact->contact_infos->toArray();
            $products = $this->productService->getProducts(null, false);
            $packages = $this->productService->getLists(null, false);
            return view('gestao.clientes.contato.edit', compact('contact', 'elementos', 'products', 'packages'));
        }
    }

    public function putUpdateContact(Request $request)
    {
        if($this->contactService->atualizarContato($request->all())){
            return redirect()->action('Crm\ClientController@getEdit', $this->contactService->clientId)
                ->with('alert-success', 'Contato atualizado com sucesso!')
                ->with('nav', 'contatos');
        }
    }

    public function deleteContactInfo($id) {
        $contact = ContactInfo::find($id);

        if ($contact) {
           $contact->delete();
        }

        return true;
    }

    public function postExcludeContact($contact)
    {
       $contact_delete = $this->contactService->deleteContato($contact);

       if ($contact_delete)
       {
             return redirect()->back()->with('alert-success', 'Contato excluído com sucesso!')->with('nav', 'contatos');
       }
       return redirect()->back()->with('alert-danger', 'Contato não foi excluído!')->with('nav', 'contatos');

    }

    public function getExcludeContact($client_id, $contact_id)
    {
        $contact = $this->contactService->getContato($client_id, $contact_id);
        if ( !$contact )
            return response()->make('Contato não existe!');

        return view('gestao.clientes.contato.excluir', compact('contact'));
    }

    public function postRemoveProductFromContact(Request $request)
    {
        $contact_id = $request->get('contact_id');
        $product_id = $request->get('product_id');
        $transacao = $request->get('transacao');
        $estado = $request->get('estado');

        if (is_null($contact_id) || is_null($product_id) || is_null($estado) || is_null($transacao))
            return [];

        $product = $this->contactProductRepository->deleteProduct($contact_id, $product_id, $estado, $transacao);

        return response()->json($product);

       // if ($product)
        //    return response()->json(['status' => 'sucesso', 'msg' => 'Excluído com sucesso!']);

        //return response()->json(['status' => 'erro', 'msg' => 'Falha ao excluir']);
    }

    public function getMoveContact($client_id, $contact_id)
    {
        $this->middleware('needsPermission:contact.move');

        $clients = $this->clientService->getAllClients();
        $contact = $this->contactService->getContato($client_id, $contact_id);

        if($contact){
            return view('gestao.clientes.contato.move', compact('clients', 'contact'));
        }
    }

    public function putMoveContact(Request $request)
    {
        $this->middleware('needsPermission:contact.move');

        if($this->contactService->moveContact($request->all())){
            return redirect()->action('Crm\ClientController@getEdit', $request->actualclient_id)
                ->with('alert-success', 'Contato movido com sucesso!')
                ->with('nav', 'contatos');
        }
    }

    public function getList($client_id)
    {
        $contatos = $this->contactService->getContatos( $client_id );
        $cliente = $this->clientService->getClient($client_id);

        if (! $cliente )
            return response()->make('Nenhum cliente encontrado');

        return view('gestao.clientes.contato.list', compact('cliente', 'contatos'));
    }



}
