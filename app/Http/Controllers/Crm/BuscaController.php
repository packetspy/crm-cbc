<?php

namespace App\Http\Controllers\Crm;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Session;

use App\Http\Controllers\CrmController;
use App\Services\ClientService;
use App\Services\ContactService;
use App\Services\ContactInfoService;

class BuscaController extends CrmController
{
    protected $clientService;
    protected $contactService;
    protected $contactinfoService;

    public function __construct(ClientService $clientService, ContactService $contactService, ContactInfoService $contactinfoService)
    {
        if (\Auth::user() ? $this->user_id = \Auth::user()->id : $this->user_id = null);
        $this->clientService = $clientService;
        $this->contactService = $contactService;
        $this->contactinfoService = $contactinfoService;
    }

    /**
     * Busca do autocomplete
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getClientes(Request $request)
    {

      //return $request->all();
        $q = trim($request->get('query'));

        $clients = ($request->has('query')) ?
                $this->clientService->searchAutoComplete($q)
                : $this->clientService->searchAutoComplete();

        return response()->json($clients);
    }

    public function getAvancada(Request $request)
    {

      if ((!$request->has('query')) && (!$request->has('type'))) {
        return view('gestao.busca.avancada');
      }

      $type = $request->get('type');
      $query = $request->get('query');

      switch ($type) {
        case 'cliente':
          $titulo = 'Resultado da busca para Clientes';
            if ($request->has('order_by') ? $order_by = $request->get('order_by') : $order_by = 'empresa');
            if ($request->has('order') ? $order = $request->get('order') : $order = 'asc');
            if ($request->has('status') ? $status = $this->clientService->discoverStatus($request->get('status')) : $status = null);
            if ($request->has('limit') ? $limit = $request->get('limit') : $limit = 50);
          $resultados = $this->clientService->BuscaClientes($query, $limit, $status, $order_by, $order, null);
          return view('gestao.busca.cliente', compact('resultados','titulo'));
          break;
        case 'contato':
          $titulo = 'Resultado da busca para Contato';
            if ($request->has('order_by') ? $order_by = $request->get('order_by') : $order_by = 'nome');
            if ($request->has('order') ? $order = $request->get('order') : $order = 'asc');
            if ($request->has('status') ? $status = $request->get('status') : $status = null);
            if ($request->has('limit') ? $limit = $request->get('limit') : $limit = 50);
          $resultados = $this->contactService->BuscaContatos($query, $limit, $status, $order_by, $order);
          return view('gestao.busca.contato', compact('resultados','titulo'));
          break;
        case 'email':
          $titulo = 'Resultado da busca para E-mail';
            if ($request->has('order_by') ? $order_by = $request->get('order_by') : $order_by = 'contato');
            if ($request->has('order') ? $order = $request->get('order') : $order = 'asc');
            if ($request->has('status') ? $status = $request->get('status') : $status = null);
            if ($request->has('limit') ? $limit = $request->get('limit') : $limit = 50);
          $resultados = $this->contactinfoService->BuscaFormasDeContato($query, $limit, $status, $order_by, $order, $type);
          return view('gestao.busca.email', compact('resultados','titulo'));
          break;
        case 'telefone':
          $titulo = 'Resultado da busca para Telefone';
            if ($request->has('order_by') ? $order_by = $request->get('order_by') : $order_by = 'contato');
            if ($request->has('order') ? $order = $request->get('order') : $order = 'asc');
            if ($request->has('status') ? $status = $request->get('status') : $status = null);
            if ($request->has('limit') ? $limit = $request->get('limit') : $limit = 50);
          $resultados = $this->contactinfoService->BuscaFormasDeContato($query, $limit, $status, $order_by, $order, $type);
          return view('gestao.busca.telefone', compact('resultados','titulo'));
          break;
          case 'id_plataforma':
            $titulo = 'Resultado da busca para Clientes';
              if ($request->has('order_by') ? $order_by = $request->get('order_by') : $order_by = 'empresa');
              if ($request->has('order') ? $order = $request->get('order') : $order = 'asc');
              if ($request->has('status') ? $status = $this->clientService->discoverStatus($request->get('status')) : $status = null);
              if ($request->has('limit') ? $limit = $request->get('limit') : $limit = 50);
            $resultados = $this->clientService->BuscaClienteId($query, $limit, $status, $order_by, $order, null);
            return view('gestao.busca.cliente', compact('resultados','titulo'));
            break;
        default:
          # code...
          break;
      }
      //nothing here
    }

}
