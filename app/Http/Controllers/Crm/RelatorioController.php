<?php

namespace App\Http\Controllers\Crm;

use Illuminate\Http\Request;
use App\Http\Controllers\CrmController;
use App\Services\ClientService;
use App\Services\XlsService;
use App\Services\RelatorioService;

use Carbon\Carbon;

class RelatorioController extends CrmController
{

    protected $clientService = false;
    protected $tipos = [
        [
            'nome'      => "Contatos realizados",
            'relatorio' => "contatosVendedor",
            'filtros'   => [
                'start' => 'dt_inicio',
                'end'   => 'dt_fim'
            ]
        ],
        [
            'nome'      => "Indicadores Usuários - Mais interações",
            'relatorio' => "indicadoresVendedoresMaisInteracoes",
            'filtros'   => [
                'start' => 'dt_inicio',
                'end'   => 'dt_fim'
            ]
        ],
        [
            'nome'      => "Indicadores Usuários - Mais negócio fechado",
            'relatorio' => "indicadoresMelhoresVendedores",
            'filtros'   => [
                'start' => 'dt_inicio',
                'end'   => 'dt_fim'
            ]
        ],
        [
            'nome'      => "Indicadores Gerais",
            'relatorio' => "indicadoresGeraisInteracoes",
            'filtros'   => [
                'start' => 'dt_inicio',
                'end'   => 'dt_fim'
            ]
        ]
    ];

    protected $tipos_individuais =  [
        [
            'nome'      => "Contatos realizados",
            'relatorio' => "contatosVendedor",
            'filtros'   => [
                'start' => 'dt_inicio',
                'end'   => 'dt_fim'
            ]
        ],
        [
            'nome'      => "Indicadores Gerais",
            'relatorio' => "indicadoresGeraisInteracoes",
            'filtros'   => [
                'start' => 'dt_inicio',
                'end'   => 'dt_fim'
            ]
        ]
    ];


    public function __construct(ClientService $clientService, XlsService $xls)
    {
        $this->clientService = $clientService;
        $this->xls = $xls;
    }

    public function getIndex()
    {
        $tipos = $this->tipos;
        return view('gestao.relatorios.index', compact('tipos'));
    }


    public function getIndexIndividual()
    {
        $tipos = $this->tipos_individuais;
        return view('gestao.relatorios.individual', compact('tipos'));
    }

    /*
     * Gerar relatório individual
     */
    public function postGerarIndividual(Request $request)
    {
        $formData = $request->all();
        foreach($this->tipos_individuais AS $rel) {
            if($rel['relatorio'] == $formData['relatorio']){
                foreach($rel["filtros"] as $key => $value){
                    $where[$key] = $formData[$value];
                }
                break;
            }
        }

        $where['user_id'] = \Auth::user()->id;

        $where = $this->verifyDate($where);
        $tipo = $formData["tipo"];
        $relatorio = $formData["relatorio"];
        switch($relatorio) {
            case 'contatosVendedor':
            case 'indicadoresGeraisInteracoes':
                return RelatorioService::create($relatorio, $where, $tipo);
                break;
            default:
                break;
        }
    }


    /*
     * Gerar relatório total
     */
    public function postGerar(Request $request)
    {
        $formData = $request->all();
        foreach($this->tipos AS $rel) {
            if($rel['relatorio'] == $formData['relatorio']){
                foreach($rel["filtros"] as $key => $value){
                    $where[$key] = $formData[$value];
                }
                break;
            }
        }
        $where = $this->verifyDate($where);
        $tipo = $formData["tipo"];
        $relatorio = $formData["relatorio"];
        switch($relatorio) {
            case 'contatosVendedor':
            case 'indicadoresVendedoresMaisInteracoes':
            case 'indicadoresMelhoresVendedores':
            case 'indicadoresGeraisInteracoes':
                return RelatorioService::create($relatorio, $where, $tipo);
            break;
            default:
            break;
        }
    }

    private function verifyDate($where)
    {
        foreach($where as $key => $data){
            if(in_array($key, ['start', 'end']) && $data != ""){
                $brDate = explode('/', $data);
                $where[$key] = Carbon::create($brDate[2], $brDate[1], $brDate[0])->format('Y-m-d');
            }
        }
        return $where;
    }
}
