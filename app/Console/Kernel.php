<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
         Commands\Inspire::class,
         Commands\ReportCmaHourly::class,
         Commands\ReportCmaFull::class,
         Commands\UpdateIbmHourly::class,
         Commands\GetApiCma::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
      $schedule->command('report-cma-hourly')->hourly()->timezone('America/Sao_Paulo');
      $schedule->command('get-api-cma')->hourly()->timezone('America/Sao_Paulo');
      $schedule->command('report-cma-full')->dailyAt('02:00')->timezone('America/Sao_Paulo');
      $schedule->command('report-cma-full')->dailyAt('18:30')->timezone('America/Sao_Paulo');

      //Sincornização IBM
      $schedule->command('update-ibm-hourly')->dailyAt('07:00')->timezone('America/Sao_Paulo');
      $schedule->command('update-ibm-hourly')->dailyAt('10:00')->timezone('America/Sao_Paulo');
      $schedule->command('update-ibm-hourly')->dailyAt('13:00')->timezone('America/Sao_Paulo');
      $schedule->command('update-ibm-hourly')->dailyAt('18:00')->timezone('America/Sao_Paulo');
      $schedule->command('update-ibm-hourly')->dailyAt('22:00')->timezone('America/Sao_Paulo');
    }
}
