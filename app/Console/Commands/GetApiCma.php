<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;

use App\Services\DownloadService;

class GetApiCma extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get-api-cma';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Retorna JSON com ultimos cadastros com base na data inicial informada.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      if (env('APP_ENV') == 'production') {
        $report = new DownloadService();
        $report->getCadastrosFromApiCma();
      }
    }
}
