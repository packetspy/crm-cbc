<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;

use App\Services\DownloadService;

class ReportCmaFull extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'report-cma-full';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Faz download, importação na base e envio de e-mail';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      if (env('APP_ENV') == 'production') {
        $report = new DownloadService();
        $report->getReportCmaFull();
      }
    }
}
