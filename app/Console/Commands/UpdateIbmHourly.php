<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;

use App\Services\IbmService;

class UpdateIbmHourly extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update-ibm-hourly';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Executa update de contatos para IBM Watson a cada 1 hora';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      if (env('APP_ENV') == 'production') {
        $update = new IbmService();
        $update->updateContatoIBM();
        $update->updateClienteIBM();
      }
    }
}
