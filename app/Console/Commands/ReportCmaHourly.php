<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;

use App\Services\DownloadService;

class ReportCmaHourly extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'report-cma-hourly';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Faz download de importação de logins a cada 1 hora';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      if (env('APP_ENV') == 'production') {
        $report = new DownloadService();
        $report->getReportCmaHourly();
      }
    }
}
