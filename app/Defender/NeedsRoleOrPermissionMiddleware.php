<?php

namespace App\Defender;

use Closure;
use Artesaos\Defender\Middlewares\AbstractDefenderMiddleware;

/**
 * Class DefenderHasPermissionMiddleware.
 */
class NeedsRoleOrPermissionMiddleware extends AbstractDefenderMiddleware
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param callable                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next, $roles = null, $any = false)
    {
        // echo '<pre>';
        // print_r($roles);
        // echo '</pre>';

        if (is_null($roles)) {
            $roles = $this->getRoles($request);
            $anyRole = $this->getAny($request);
        } else {
            $roles = explode('|', $roles); // Laravel 5.1 - Using parameters
            $anyRole = $any;
        }

        // echo '<pre>';
        // print_r($roles);
        // echo '</pre>';


        if (is_null($this->user)) {
            return $this->forbiddenResponse();
        }

        if (is_array($roles) and count($roles) > 0) {
            $hasResult = true;

            foreach ($roles as $role) {
                // echo 'role -> '.$role.'<br>';
                $hasRole = $this->user->hasRole($role);

                // echo 'hasRole => '.$hasRole.'<br>';

                // Check if any role is enough
                if ($anyRole and $hasRole) {
                    return $next($request);
                }

                $hasResult = $hasResult & $hasRole;
            }

            // print_r($hasResult);


        }

        $permissions = null;

        if (is_null($permissions)) {
            $permissions = $this->getPermissions($request);
            $anyPermission = $this->getAny($request);
        } else {
            $permissions = explode('|', $permissions); // Laravel 5.1 - Using parameters
            $anyPermission = $any;
        }



         if (is_array($permissions) and count($permissions) > 0) {
            $canResult = true;

            foreach ($permissions as $permission) {
                $canPermission = $this->user->hasPermission($permission);

                // Check if any permission is enough
                if ($anyPermission and $canPermission) {
                    return $next($request);
                }

                $canResult = $canResult & $canPermission;
            }

            if (! $canResult) {
                return $this->forbiddenResponse();
            }
            else if (! isset($hasResult)) {
                return $this->forbiddenResponse();
            }
        }


        return $next($request);
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    private function getRoles($request)
    {
        $routeActions = $this->getActions($request);

        // echo '<pre>';
        // print_r($routeActions);
        // echo '</pre>';

        $roles = array_get($routeActions, 'is', []);

        // echo '<pre>';
        // print_r($roles);
        // echo '</pre>';

        return is_array($roles) ? $roles : (array) $roles;
    }

    private function getPermissions($request)
    {
        $routeActions = $this->getActions($request);

        $permissions = array_get($routeActions, 'shield', []);

        return is_array($permissions) ? $permissions : (array) $permissions;
    }
}
