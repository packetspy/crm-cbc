<?php

namespace App\Defender;

use Artesaos\Defender\Handlers\ForbiddenHandler;

class CustomizationForbidden extends ForbiddenHandler
{
    public function handle()
    {
        // throw new ForbiddenException;
        abort(403);
    }
}

?>