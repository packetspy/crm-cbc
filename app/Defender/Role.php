<?php

namespace App\Defender;
use Illuminate\Database\Eloquent\Model;
use Artesaos\Defender\Traits\Permissions\RoleHasPermissions;

/**
 * Class Role.
 *
 * @property string $id
 * @property integer $domain_id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $users
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Defender\Permission[] $permissions
 * @method static \Illuminate\Database\Query\Builder|\App\Defender\Role whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Defender\Role whereDomainId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Defender\Role whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Defender\Role whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Defender\Role whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Role extends Model
{
    use RoleHasPermissions;

    /**
     * Table name.
     *
     * @var string
     */
    protected $table;

    /**
     * Mass-assignment whitelist.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->table = config('defender.role_table', 'roles');
    }

    /**
     * Many-to-many role-user relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(
            config('defender.user_model'),
            config('defender.role_user_table'),
            config('defender.role_key'),
            'user_id'
        );
    }
}
