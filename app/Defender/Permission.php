<?php

namespace App\Defender;
use Illuminate\Database\Eloquent\Model;
use Artesaos\Defender\Pivots\PermissionRolePivot;
use Artesaos\Defender\Pivots\PermissionUserPivot;

/**
 * Class Permission.
 *
 * @property string $id
 * @property string $name
 * @property string $readable_name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Defender\Role[] $roles
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $users
 * @method static \Illuminate\Database\Query\Builder|\App\Defender\Permission whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Defender\Permission whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Defender\Permission whereReadableName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Defender\Permission whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Defender\Permission whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Permission extends Model
{

    protected $table;

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'readable_name',
    ];

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->table = config('defender.permission_table', 'permissions');
    }

    /**
     * Many-to-many permission-role relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany(
            config('defender.role_model'),
            config('defender.permission_role_table'),
            config('defender.permission_key'),
            config('defender.role_key')
        )->withPivot('value', 'expires','domain_id')->where('permission_role.domain_id',\Auth::user()->domain_id);
    }

    /**
     * Many-to-many permission-user relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(
            config('defender.user_model'),
            config('defender.permission_user_table'),
            config('defender.permission_key'),
            'user_id'
        )->withPivot('value', 'expires','domain_id')->where('permission_user.domain_id',\Auth::user()->domain_id);;
    }

    /**
     * @param Model  $parent
     * @param array  $attributes
     * @param string $table
     * @param bool   $exists
     *
     * @return PermissionUserPivot|\Illuminate\Database\Eloquent\Relations\Pivot
     */
    public function newPivot(Model $parent, array $attributes, $table, $exists)
    {
        $userModel = app()['config']->get('defender.user_model');
        $roleModel = app()['config']->get('defender.role_model');

        if ($parent instanceof $userModel) {
            return new PermissionUserPivot($parent, $attributes, $table, $exists);
        }

        if ($parent instanceof $roleModel) {
            return new PermissionRolePivot($parent, $attributes, $table, $exists);
        }

        return parent::newPivot($parent, $attributes, $table, $exists);
    }
}
