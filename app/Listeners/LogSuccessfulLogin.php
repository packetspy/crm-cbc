<?php

namespace App\Listeners;

use Illuminate\Auth\Events\Login;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Regulus\ActivityLog\Models\Activity as Activity;

class LogSuccessfulLogin
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SomeEvent  $event
     * @return void
     */
    public function handle(Login $event)
    {
        Activity::log([
           'contentId' => $event->user->id,
           'contentType' => 'User',
           'action' => 'Login',
            'description' => 'Efetuou login',
            'details' => 'Username: ' . $event->user->name
        ]);
    }
}
