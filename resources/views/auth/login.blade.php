@extends('layouts.app')

@section('content')
<div class="container">

    <div class="card card-container">

        <img src="{{ asset('theme/assets/img/logo_cbc_login.png') }}" class="img-responsive" />

        <form class="form-signin" role="form" method="POST" action="{{ url('/login') }}">
            {{ csrf_field() }}

            <h3 class="text-center">Acesso ao CRM</h3>

            <span id="reauth-email" class="reauth-email"></span>
            <input id="inputEmail" type="email" class="form-control" name="email" placeholder="E-mail" required autofocus value="{{ old('email') }}">

            @if ($errors->has('email'))
                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
            @endif
            <input id="inputPassword" type="password" class="form-control" name="password" placeholder="Senha">

            @if ($errors->has('password'))
                <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
            @endif
            <div id="remember" class="checkbox">
                <label>
                    <input type="checkbox" value="remember"> Lembrar-me
                </label>
            </div>
            <button class="btn btn-fill btn-lg btn-primary btn-block btn-signin" type="submit">Login</button>
        </form><!-- /form -->

        <!-- <a href="{{ url('/password/reset') }}" class="forgot-password">
           Esqueceu a senha?
        </a>
        -->

    </div><!-- /card-container -->

</div>
@endsection
