@extends('layouts.base')
@section('content')

    <div class="row">
        <div class="col-md-8 col-sm-8 col-xs-12">
            <div class="card">
                <div class="header">
                    <h4 class="title">Minha conta</h4>
                    <p class="category">Alterar senha</p>
                </div>
                <div class="panel-body">
                    {{$user->fname}}
                    {{$user->lname}}
                    {{$user->escritorio->nome}}
                    {{$user->escritorio_id}}
                    {{\Auth::user()->id}}
                    {{\Auth::user()->escritorio_id}}

                    <form role="form" id="form_dados" method="POST" action="{{ url ('password/reset/') }}" accept-charset="UTF-8">
                        <input type="hidden" name="_method" value="POST">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group {{ $errors->has('old_password') ? ' has-error' : '' }}">
                                {!! Form::label('old_password', 'Senha anterior') !!}
                                {!! Form::password('old_password', ['class' => 'form-control']) !!}
                                @if ($errors->has('old_password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('old_password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                {!! Form::label('password', 'Nova senha') !!}
                                {!! Form::password('password', ['class' => 'form-control']) !!}
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                {!! Form::label('password_confirmation', 'Confirmar senha') !!}
                                {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group text-right">
                                <button type="submit" class="btn btn-fill btn-primary">
                                    <i class="fa fa-send"></i> Enviar
                                </button>
                            </div>
                        </div>

                    </div>
                  </form>
                </div>
            </div>
        </div>
    </div>

@endsection
