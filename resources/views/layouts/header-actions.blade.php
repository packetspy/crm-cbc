<div class="collapse navbar-collapse navbar-right">
  <ul class="nav navbar-nav">
    <li class="dropdown">
      <a href="#" class="dropdown-toggle btn btn-fill btn-info" data-toggle="dropdown">
        <i class="fa fa-plus"></i>
          Novo
        <b class="caret"></b>
      </a>
      <ul class="dropdown-menu">
        <li>
          <a href="{{ url('cliente/novo-cliente') }}">
            <i class="fa fa-briefcase"></i> Cliente
          </a>
        </li>
        @is(['Marketing','Gestores','Hábito','Liquidez'])
        <li>
          <a href="{{ url('cliente/novo-lead') }}">
            <i class="fa fa-id-badge"></i> Lead
          </a>
        </li>
        @endis
        @if(isOnCliente())
        <li>
          <a href="{{ url('cliente/contatos') }}/{{ Request::segment(3) }}/novo-contato">
            <i class="fa fa-male"></i> Contato
          </a>
        </li>
        @endif
        @if(isOnCliente())
        <li>
          <a href="javascript:void(0)" onClick="abreModal('{{ action('Crm\ClientController@getEscolheInteracao', Request::segment(3)) }}', 'Nova interação', 'modal_escolha');">
            <i class="fa fa-list"></i> Interação
          </a>
        </li>
        @endif
        @shield('user.create.all')
        <li class="divider"></li>
        <li>
          <a href="{{ url('configuracoes/usuario/novo-usuario') }}" >
            <i class="fa fa-user"></i> Usuário
          </a>
        </li>
        @endif
        @shield('user.create.all')
        <li>
          <a href="{{ url('configuracoes/grupo/novo-grupo') }}" >
            <i class="fa fa-group"></i> Grupo
          </a>
        </li>
        @endif
        @shield('user.create.all')
        <li>
          <a href="{{ url('configuracoes/permissao/novo-permissao') }}" >
            <i class="fa fa-shield"></i> Permissão
          </a>
        </li>
        @endif
        @shield('user.create.all')
        <li>
          <a href="{{ url('configuracoes/escritorio/novo-escritorio') }}" >
            <i class="fa fa-building-o"></i> Escritório
          </a>
        </li>
        @endif
      </ul>
    </li>
    <li class="dropdown">
      <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        {{ \Auth::user()->fname }}
        <i class="fa fa-user"></i>
        <b class="caret"></b>
      </a>
      <ul class="dropdown-menu">
        <li>
          <a href="{{ url('/perfil') }}">
            <i class="fa fa-key"></i> Alterar senha
          </a>
        </li>
        <li>
          <a href="{{ url('/logout') }}">
            <i class="fa fa-sign-out"></i> Sair
          </a>
        </li>
      </ul>
    </li>
  </ul>
</div>
