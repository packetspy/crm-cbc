<div class="sidebar" data-color="" data-image="{{ asset('theme/assets/img/sidebar-5.jpg') }}">

  <div class="logo">
    <a href="{{ url('/') }}" class="simple-text">
      <img src="{{ asset('theme/assets/img/logo_cbc.svg') }}" class="img-responsive" />
    </a>
  </div>

  <div class="sidebar-wrapper">
    <ul class="nav">
      <li>
        <a href="{{ url('/') }}">
          <i class="fa fa-tachometer"></i>
          <p>Dashboard</p>
        </a>
      </li>

      <li>
          <a data-toggle="collapse" href="#sidebarClientes" class="collapsed" aria-expanded="false">
              <i class="fa fa-briefcase fa-3"></i>
              <p>Clientes
                 <b class="caret"></b>
              </p>
          </a>
          <div class="collapse" id="sidebarClientes" aria-expanded="false">
              <ul class="nav">
                  <li><a href="{{ url('clientes') }}"><i class="fa fa-chevron-right fa-1"></i>Todos os Clientes</a></li>
                  @shield('menu.meusclientes')
                  <li><a href="{{ url('clientes/meus-clientes') }}"><i class="fa fa-chevron-right"></i> Meus Clientes</a></li>
                  @endshield
                  <li><a href="{{ url('clientes?status=2') }}"><i class="fa fa-chevron-right"></i> Em prospecção</a></li>
                  <li><a href="{{ url('clientes?status=8') }}"><i class="fa fa-chevron-right"></i> Ativo/Cadastrado</a></li>
              </ul>
          </div>
      </li>

      <li>
        <a href="{{ action('Crm\CalendarioController@getIndex') }}">
          <i class="fa fa-calendar"></i>
          <p>Calendário</p>
        </a>
      </li>

      <li>
        <a href="{{ action('Crm\RelatorioController@getIndexIndividual') }}">
          <i class="fa fa-line-chart"></i>
          <p>Meus Relatórios</p>
        </a>
      </li>

      <li>
        <a href="{{ action('Crm\RelatorioController@getIndex') }}">
          <i class="fa fa-file-text"></i>
          <p>Relatórios</p>
        </a>
      </li>
      <li>
        <a href="{{ action('Crm\ProductController@getIndex') }}">
          <i class="fa fa-cubes"></i>
          <p>Produtos</p>
        </a>
      </li>
      @shield('menu.database')
      <li>
          <a data-toggle="collapse" href="#sidebarDatabase" class="collapsed" aria-expanded="false">
              <i class="fa fa-database"></i>
              <p>Database
                 <b class="caret"></b>
              </p>
          </a>
          <div class="collapse" id="sidebarDatabase" aria-expanded="false">
              <ul class="nav">
                <li><a href="{{ url('database/importar') }}"><i class="fa fa-upload"></i><p>Importar</p></a></li>
                <li><a href="{{ url('database/exportar') }}"><i class="fa fa-download"></i><p>Exportar</p></a></li>
              </ul>
          </div>
      </li>
      @endshield
      @shield('menu.configuracoes')
      <li>
          <a data-toggle="collapse" href="#sidebarConfiguracoes" class="collapsed" aria-expanded="false">
              <i class="fa fa-gear"></i>
              <p>Configuraçoes
                 <b class="caret"></b>
              </p>
          </a>
          <div class="collapse" id="sidebarConfiguracoes" aria-expanded="false">
              <ul class="nav">
                <li><a href="{{ url('configuracoes/usuarios') }}"><i class="fa fa-user"></i><p>Usuários</p></a></li>
                <li><a href="{{ url('configuracoes/grupos') }}"><i class="fa fa-group"></i><p>Grupos</p></a></li>
                <li><a href="{{ url('configuracoes/permissoes') }}"><i class="fa fa-shield"></i><p>Permissões</p></a></li>
                <li><a href="{{ url('configuracoes/escritorios') }}"><i class="fa fa-building-o"></i><p>Escritórios</p></a></li>
              </ul>
          </div>
      </li>
      @endshield
    </ul>
  </div>

  <div class="sidebar-background" style="background-image: url({{ asset('theme/assets/img/sidebar-5.jpg') }}) "></div>
</div>
