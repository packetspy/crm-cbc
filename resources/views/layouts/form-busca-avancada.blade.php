<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Buscar: Clientes / Contatos / E-mails / Telefones</h3>
        </div>
        <div class="panel-body">
            <form method="get" action="{{ url('busca/avancada') }}" accept-charset="UTF-8">
              <input type="hidden" name="_method" value="get">
          <div class="col-md-7">
            <div class="form-group">
                  <div class="controls" id="remote">
                      <input class="form-control" autocomplete="off" name="query" type="text" value="{{ app('request')->input('query')}}" required>
                  </div>
            </div>
          </div>
          <div class="col-md-3">
            <select class="select show-tick form-control" id="type" name="type" title="Selecione ..." data-live-search="false" required>
               <option value="cliente" {{ app('request')->input('type') == 'cliente' ? 'selected="selected"' : '' }}>Cliente</option>
               <option value="contato" {{ app('request')->input('type') == 'contato' ? 'selected="selected"' : '' }}>Contato</option>
               <option value="email" {{ app('request')->input('type') == 'email' ? 'selected="selected"' : '' }}>E-mail</option>
               <option value="telefone" {{ app('request')->input('type') == 'telefone' ? 'selected="selected"' : '' }}>Telefone</option>
               <option value="id_plataforma" {{ app('request')->input('type') == 'id_plataforma' ? 'selected="selected"' : '' }}>ID Plataforma</option>
            </select>
          </div>
          <div class="col-md-2">
            <button type="submit" class="btn btn-fill btn-primary">
                <i class="fa fa-search"></i> Buscar
            </button>
          </div>
          </form>
        </div>
    </div>
  </div>
</div>
