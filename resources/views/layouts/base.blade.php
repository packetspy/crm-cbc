<!DOCTYPE html>
<html lang="pt-br">
<head>
  <meta charset="utf-8">
  <title>CRM | CBC Negócios</title>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}" />

  <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" />
  <link href='//fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
  <link href="{{ asset('theme/assets/css/bootstrap.min.css') }}" rel="stylesheet" />
  <link href="{{ asset('theme/assets/css/animate.min.css') }}" rel="stylesheet"/>
  <link href="{{ asset('theme/assets/css/light-bootstrap-dashboard.css') }}?<?php echo time(); ?>" rel="stylesheet"/>
  <link href="{{ asset('theme/assets/css/pe-icon-7-stroke.css') }}" rel="stylesheet" />
  <link href="{{ asset('theme/assets/css/font-awesome.min.css') }}" rel="stylesheet" />
  <link href="{{ asset('lib/js/bootstrap3-dialog/bootstrap-dialog.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('lib/css/jquery.typeahead.css') }}" rel="stylesheet" />
  <link href="{{ asset('lib/css/sweetalert.css') }}" rel="stylesheet" />
  <link href="{{ asset ('theme/assets/css/custom.css') }}?<?php echo time(); ?>" rel="stylesheet" type="text/css" />
  <link href="{{ asset('lib/css/custom.css') }}?<?php echo time(); ?>" rel="stylesheet" />
  <link href="{{ asset('lib/js/fullcalendar/fullcalendar.min.css') }}" type="text/css" rel="stylesheet" />
  <link href="{{ asset('lib/css/daterangepicker.css') }}" type="text/css" rel="stylesheet" />
  <link href="{{ asset('lib/css/bootstrap-select.min.css') }}" type="text/css" rel="stylesheet" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/css/bootstrap-select.min.css">

  <script type='text/javascript' src="{{ asset('lib/js/sweetalert.js') }}"></script>

  @yield('scripts_head')

  <!--[if lt IE 9]>
  <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
</head>

<body id="app-layout">

  <!-- Loader usado para todo sistema -->
  <div id="main-loader" class="hide">
    <div class="loader">Carregando ...</div>
  </div>

  <div class="wrapper">
    @include('layouts.sidebar')
    <div class="main-panel">
      <nav class="navbar navbar-default navbar-fixed">
        <div class="container-fluid">
          <div class="row">
            <div class="col-lg-8">
              @if(Request::is('busca/avancada') != 'busca/avancada')
                @include('layouts.form-busca-global')
              @endif
            </div>
            <div class="col-lg-4">
              @include('layouts.header-actions')
            </div>
          </div>
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
          </div>
        </div>
      </nav>

      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              @if(count($errors) > 0)
                @foreach ($errors->all() as $error)
                  <div class="alert alert-danger">{{ $error }}</div>
                @endforeach
              @endif

              @include('layouts.flash_messages')

            </div>
          </div>
          @yield('content')
        </div>
      </div>


      <footer class="footer">
        <div class="container-fluid">
          <p class="copyright pull-right">
            &copy; 2016 CBC Negócios
          </p>
        </div>
      </footer>

      </div>
      </div>

    <!-- Javascripts -->
    <script type='text/javascript' src="{{ asset('lib/js/jquery.min.js') }}"></script>
    <script type='text/javascript' src="{{ asset('lib/js/jquery-ui.min.js') }}"></script>
    <script type='text/javascript' src="{{ asset('lib/js/bootstrap.min.js') }}"></script>
    <script type='text/javascript' src="{{ asset('lib/js/bootstrap-select.min.js') }}"></script>
    <script type='text/javascript' src="{{ asset('lib/js/perfect-scrollbar.jquery.min.js') }}"></script>
    <script type='text/javascript' src="{{ asset('theme/assets/js/bootstrap-checkbox-radio-switch.js') }}"></script>
    <script type='text/javascript' src="{{ asset('theme/assets/js/bootstrap-notify.js') }}"></script>
    <script type='text/javascript' src="{{ asset('lib/js/jquery.mask.min.js') }}"></script>
    <script type='text/javascript' src="{{ asset('lib/js/bootstrap3-dialog/bootstrap-dialog.min.js') }}"></script>
    <script type='text/javascript' src="{{ asset('lib/js/jquery.typeahead.min.js') }}"></script>
    <script type='text/javascript' src="{{ asset('lib/js/clipboard.min.js') }}"></script>
    <script type='text/javascript' src="{{ asset('lib/js/sweetalert.js') }}"></script>
    <script type='text/javascript' src="{{ asset('lib/js/moment/moment.min.js') }}"></script>
    <script type='text/javascript' src="{{ asset('lib/js/fullcalendar/fullcalendar.min.js') }}"></script>
    <script type='text/javascript' src="{{ asset('lib/js/daterangepicker.js') }}"></script>
    <script type='text/javascript' src="{{ asset('theme/assets/js/light-bootstrap-dashboard.js') }}"></script>

    <script type='text/javascript' src="{{ asset('lib/js/utils.js') }}?<?php echo time(); ?>"></script>
    <script type='text/javascript' src="{{ asset('lib/js/search.js') }}?<?php echo time(); ?>"></script>
    <script type='text/javascript' src="{{ asset('lib/js/contato.js') }}?<?php echo time(); ?>"></script>
    <script type='text/javascript' src="{{ asset('lib/js/contact-product.js') }}?<?php echo time(); ?>"></script>
    <script type='text/javascript' src="{{ asset('lib/js/global.js') }}?<?php echo time(); ?>"></script>

    @yield('scripts')

</body>
</html>
