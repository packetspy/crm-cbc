<div class="flash-message">
  @foreach (['danger', 'warning', 'success', 'info'] as $msg)
    @if(\Session::has('alert-' . $msg))
      <p class="alert alert-{{ $msg }}">{{ \Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
    @endif
  @endforeach
</div> <!-- end .flash-message -->

<script type="text/javascript">
    @if(session('type'))
      swal({
        title: "{{session('title')}}",
        text: "{{session('message')}}",
        type: "{{session('type')}}",
        showCancelButton: false,
        confirmButtonClass: "btn-{{session('type')}} btn-fill",
        confirmButtonText: "Ok",
        closeOnConfirm: false,
        closeOnCancel: false,
        html: true
      });
    @endif
</script>
