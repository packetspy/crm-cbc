  <form id="form-busca" name="form-busca">
      <div class="typeahead__container">
          <div class="typeahead__field">
              <span class="typeahead__query">
                  <input class="js-typeahead-busca" name="query" type="search" placeholder="Buscar" autocomplete="off">
              </span>
              <span class="typeahead__button">
                  <button type="submit">
                      <i class="typeahead__search-icon"></i>
                  </button>
              </span>

          </div>
      </div>
  </form>
  <a href="{{ url('busca/avancada') }}" class="pull-right"
     rel="tooltip"
     data-toggle="tooltip"
     data-delay='{"show":"1", "hide":"100"}'
     data-html="true"
     data-placement="bottom"
     title="<p>Experimente a busca avaçada clicando aqui!</p>">
     Busca avancada! <i class="fa fa-question-circle"></i>
   </a>
