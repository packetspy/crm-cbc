<footer class="footer">
  <div class="container-fluid">
    <nav class="pull-left">
      <ul>
        <li>
          <a href="#">
            Home
          </a>
        </li>

      </ul>
    </nav>
    <p class="copyright pull-right">
      &copy; 2016 CBC Negócios
    </p>
  </div>
</footer>

</div>
</div>


<!-- Javascripts -->
<script type='text/javascript' src="{{ asset('lib/js/bootstrap.min.js') }}"></script>
<script type='text/javascript' src="{{ asset('theme/assets/js/bootstrap-checkbox-radio-switch.js') }}"></script>
<script type='text/javascript' src="{{ asset('theme/assets/js/bootstrap-notify.js') }}"></script>
<script type='text/javascript' src="{{ asset('lib/js/jquery.mask.min.js') }}"></script>
<script type='text/javascript' src="{{ asset('lib/js/bootstrap3-dialog/bootstrap-dialog.min.js') }}"></script>
<script type='text/javascript' src="{{ asset('lib/js/utils.js') }}"></script>
<script type='text/javascript' src="{{ asset('lib/js/jquery.typeahead.min.js') }}"></script>
<script type='text/javascript' src="{{ asset('lib/js/search.js') }}"></script>
<script type='text/javascript' src="{{ asset('theme/assets/js/light-bootstrap-dashboard.js') }}"></script>
<script type='text/javascript' src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/js/bootstrap-select.min.js"></script>

@yield('scripts')

</body>
</html>
