@extends('layouts.base')

@section('content')
    <div class="row">
       <div class="col-md-12">
           <div class="alert alert-danger">
               Você não tem privilégios para acessar este módulo. Contacte o seu gerente/administrador.
           </div>
       </div>
    </div>
@endsection
