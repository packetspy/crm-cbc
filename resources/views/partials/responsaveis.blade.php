<div class="row">
  <div class="col-md-12">
    {!! Form::open(['action' => ['Crm\ClientController@changeClientResponsible'], 'method' => 'post', 'id' => 'changeClientResponsible'])  !!}
    {!! Form::label('responsavel_id', 'Consultor Responsavel') !!}
    <select class="select show-tick form-control" id="responsavel_id" name="responsavel_id" title="Selecione Consultor..." data-live-search="true">
        @foreach($consultores as $consultor)
            <option value="{{ $consultor->id }}" {{ $consultor->id == $responsavel_id ? 'selected="selected"' : '' }}>{{ $consultor->name }}</option>
        @endforeach
    </select>
    {!! Form::hidden('id', $client->id) !!}
    {!! Form::close() !!}
  </div>
</div>
