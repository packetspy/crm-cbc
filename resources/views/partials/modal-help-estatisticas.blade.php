<!-- Modal -->
<div class="modal fade" id="helpEstatisticas" tabindex="-1" role="dialog" aria-labelledby="helpEstatisticasLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="helpEstatisticasLabel">Estatísticas (KPIs)</h4>
            </div>
            <div class="modal-body">
                <strong>Estatísticas Gerenciais</strong>
                <p>As estatísticas gerenciais são calculadas com base nas interações que aconteceram no sistema como um todo.</p>
                <strong>Estatísticas Individuais</strong>
                <p>As estatísticas individuais (quando se vê pela página inicial do CRM) é apresentada apenas com relação ao usuário logado. Usuários com privilégios para visualizar estatísticas de outros usuários vêem os dados com base em interações do sistema todo, e não do particular.</p>

                <strong>Forma de cálculo</strong>
                <ul>
                    <li><strong>Interações iniciadas:</strong> são as interações que o usuário iniciou. Não são consideradas interações de resposta (aquelas que são geradas a partir de uma interação previamente cadastrada).</li>
                    <li><strong>Interações que tiveram atualização (continuadas):</strong> interações que obtiveram resposta.</li>
                    <li><strong>Interações com data tempestiva: </strong> interações que obtiveram atualização em até 12h antes do horário agendado.</li>
                    <li><strong>Interações respondidas com atraso:</strong> o contrário da data tempestiva, interação que obteve atualização, mas depois no horário e/ou depois do horário agendado.</li>
                    <li><strong>Interações de neǵocio fechado:</strong> interações que o usuário fechou negócio.</li>
                    <li><strong>Interações com negócios não fechados:</strong> interações que o usuário não fechou negócio e suas justificativas.</li>

                </ul>
                <strong>Exemplo de cálculo de porcentagem:</strong>
                <ul>
                    <li>
                        <strong>Interações Iniciadas (KPI individual):</strong>
                        (Interações Iniciadas / Total de Interações do Usuário) <strong>* 100</strong> = Resultado em %.
                    </li>
                    <li>
                        <strong>Interações Iniciadas (KPI gerencial):</strong>
                        (Interações Iniciadas / Total de Interações do Sistema) <strong>* 100</strong> = Resultado em %.
                    </li>
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-fill btn-default" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>