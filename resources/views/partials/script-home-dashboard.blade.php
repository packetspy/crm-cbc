<script>



    function detalhaItemCalendario(item)
    {
        var action_url = '{!! action('Crm\ClientController@getViewInteracao') !!}';
        var url = action_url + '/' + item.client_id + '/' + item.id;

        return BootstrapDialog.show({
            title: 'Visualizando item',
            message: $('<div></div>').load(url),
            buttons: [{
                label: 'Ver cliente',
                action: function(dialogRef){
                    window.location.href = item.url;
                    return false;
                    dialogRef.close();
                }
            }, {
                label: 'Fechar',
                action: function(dialogRef){
                    dialogRef.close();
                }
            }]
        });
    }

    $(document).ready(function() {
        $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,basicWeek,basicDay'
            },
            defaultDate: '{!! date('Y-m-d') !!}',
            defaultView: 'basicWeek',
            validRange: {
                start: '2017-05-08'
            },
            editable: false,
            eventLimit: false, // allow "more" link when too many events
            loading: function(bool) {
              if (bool){
                $('#main-loader').show();
                document.getElementById("main-loader").className = "";
              }else{
                $('#main-loader').hide();
                document.getElementById("main-loader").className = "hide";
              }
            },
            events: '{!! action('Crm\CalendarioController@getContatos') !!}',
            eventClick:  function(event, jsEvent, view) {
                //set the values and open the modal
                jsEvent.preventDefault();
                return detalhaItemCalendario(event);
            }
        });

          $(function(){
            var searchDashboard = new Bloodhound({
              datumTokenizer: function (datum) {
                  return Bloodhound.tokenizers.whitespace(datum);
              },
              queryTokenizer: Bloodhound.tokenizers.whitespace,
              remote: {
                  url: '{{ action('Crm\ClientController@getSearch') }}',
                  replace: function(url, query) {
                      return url + "?name=" + query;
                  },
                  filter: function(searchDashboard) {
                      return $.map(searchDashboard, function(data) {
                          return {
                              id: data.id,
                              nome: data.nome,
                              empresa: data.empresa,
                              documento: data.documento,
                              ativo: data.ativo
                          }
                      });
                  }
              }
            });
            searchDashboard.initialize();
            $('.typeahead-autocomplete').typeahead(null, {
                  name: 'searchDashboard',
                  displayKey: ['id','nome', 'empresa','documento','ativo'],
                  minLength: 1, // send AJAX request only after user type in at least X characters
                  hint: true,
                  highlight: true,
                  limit: 50,
                  source: searchDashboard.ttAdapter(),
                  templates: {
                        empty: [
                            '<div class="row"><div class="col-sm-10"><div class="empty-message">Não encontramos nenhuma ocorrência</div></div></div>'
                        ],
                        suggestion: function (clientes) {

                            if(clientes.ativo == "1")
                            {
                              var $texto = '<div class="row">'+
                                  '<div class="col-sm-9"><a href="{{ url('cliente/resumo') }}/'+clientes.id+'"><b>Razão Social: </b> '+ clientes.empresa +' | <b>Nome Fantasia:</b> '+ clientes.nome +'</a></div>' +
                                  '<div class="col-sm-3"><a class="btn btn-fill btn-xs btn-info" href="{{ url('cliente/resumo') }}/'+clientes.id+'">Acessar</a>' +
                                  '<a class="btn btn-fill btn-xs btn-default" onClick="abreModal(\'{{ action('Crm\ClientController@getEscolheInteracao') }}/'+clientes.id+'\', \'Nova interação\', \'modal_escolha_autocomplete\');">+ Interação</a>' +
                                  '</div></div>';
                            }
                            else {
                              var $texto = '<div class="row">'+
                                  '<div class="col-sm-9"><b>Razão Social: </b> '+ clientes.empresa +' | <b>Nome Fantasia:</b> '+ clientes.nome +'</div>' +
                                  '<div class="col-sm-3"><a class="btn btn-fill btn-xs btn-info" href="{{ url('cliente/resumo') }}/'+clientes.id+'">Acessar</a>' +
                                  '<label class="label label-warning">Inativo</label></div></div>';
                                  '</div></div>';
                            }
                            return $texto;
                        }
                    }
              });
            });
});
</script>
