<p>Cliente: <br/>
<a href="{{ url('cliente/resumo', $interacao->client->id) }}" >
    {{$interacao->client->nome}}
</a>
    <a class="btn btn-fill btn-xs btn-primary" href="{{ url('cliente/resumo', $interacao->client->id) }}" >
        <i class="fa fa-users"></i> Ver cliente
    </a>
    <a href="#" onClick="return abreModal('{{ url('cliente/contatos/lista-simples', [$interacao->client_id]) }}', 'Lista de contatos do cliente')" class="btn btn-fill btn-xs btn-primary">
        <i class="fa fa-book"></i> Ver contatos
    </a>
</p>
<strong>Interação #{{ $interacao->id }}</strong>

<p><i class="fa fa-clock-o"></i> {{ $interacao->data_criacao }}</p>
<p><strong>Descrição: </strong><br />{{ $interacao->titulo }}</p>

<p><strong>Autor:</strong><br />{{$interacao->author->name}}</p>
<p><span class="label label-default">{!! $interacao->tipo_interacao !!}</span></p>

@if ($interacao->father_id)
<p>
    <strong>Interação de Referência</strong><br />
    <a href="#" onClick="return abreModal('{{ url('interacao/nova-interacao', [$interacao->client_id, $interacao->father_id]) }}', 'Visualizando interação')" class="btn btn-fill btn-sm">
        <i class="fa fa-eye"></i> #{!! $interacao->father_id !!} - Ver detalhes
    </a>
</p>
@endif

<!--
<p>Observação: {{ $interacao->observacao }}</p>-->

<button type="button" class="btn btn-fill btn-primary btn-xs" onClick="abreModal('{{ action('Crm\ClientController@getEscolheInteracao', [$interacao->client_id, $interacao->id]) }}', 'Nova interação', 'modal_escolha');">
    <i class="fa fa-reply"></i> Responder à esta interação.
</button>
