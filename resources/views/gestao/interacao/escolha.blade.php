@include('gestao.interacao.elementos.mensagem')

{{-- @include('gestao.interacao.bloqueada') --}}

@if(!$exibir_fechar)
    <h4>Respondendo interação de contraparte</h4>
@endif

@if ($show_form == true)
    <div class="show_form">
        {!! Form::open(['action' => ['Crm\ClientController@postInteracaoSimples', $client_id], 'method' => 'post', "id"=>"form_interacao"]) !!}
        <input type="hidden" name="father_id" value="{{$father_id}}">
        @include('gestao.interacao.elementos.interacao_nivel_1')
        @include('gestao.interacao.elementos.interacao_nivel_2')
        @include('gestao.interacao.elementos.interacao_nivel_3')
        @include('gestao.interacao.elementos.interacao_nivel_4')
        @include('gestao.interacao.elementos.data_contato')
        @include('gestao.interacao.elementos.observacao')
        {!! Form::submit('Enviar', ['class' => 'btn btn-primary hidden', "id"=>"submit_form"]) !!}
        {!! Form::close() !!}
    </div>
@endif

<script>
    $(document).ready(function () {
        var elementsBind = function(e) {
            e.preventDefault();
            var value = e.target.value;
            var next = $(this).data("to");
            var genericElementsId = "#interacao_"
            var genericElementsLabelId = "#label_interacao_"
            var optionSelected = genericElementsId + (next-1) +" option:selected";

            if($(optionSelected).data("obsobrigatoria") == true) {
                $('.label-observacao').html("Justificativa")
                $('#observacao').attr("required", true);
            }else {
                $('.label-observacao').html("Observação");
                $('#observacao').removeAttr("required");
            }

            if($(optionSelected).data("dtobrigatoria") == true){
                $('#data_contato').attr('required', true);
                $('#hora_contato').attr('required', true);
            }else {
                $('#data_contato').removeAttr('required');
                $('#hora_contato').removeAttr('required');
            }

            if($(optionSelected).data("childrens") == 0) {
                if($(optionSelected).text() != 'Negócio fechado' && next != 4)
                    $("#interacao_contato").removeClass("hidden");

                $("#interacao_observacao").removeClass("hidden");
                $("#submit_form").removeClass("hidden");
            } else {
                $("#interacao_contato").addClass("hidden");
                $("#interacao_observacao").addClass("hidden");
                $("#submit_form").addClass("hidden");
            }

            /*Esconde os elementos seguintes*/
            for(var i=next; i < 5; i++) {
                $(genericElementsId + i).addClass("hidden");
                $(genericElementsId + i + " option").remove();
                $(genericElementsLabelId + i).addClass("hidden");
            }

            $.get("{{action('Crm\InteractionTypeController@getInteractionSub', 0)}}"+value, function(data){
                if(data.length > 0) {
                    $(genericElementsId + next).removeClass("hidden");
                    $(genericElementsLabelId + next).removeClass("hidden");
                    $(genericElementsId + next + " option").remove();
                    $(genericElementsId + next).append(
                            $("<option>").val("0").data("childrens", 0)
                    )

                    for(var i = 0; i < data.length; i++){
                        $(genericElementsId + next).append(
                                $("<option>").val(data[i].id)
                                        .data("childrens", data[i].childrens_number)
                                        .data("dtobrigatoria", data[i].data_obrigatoria)
                                        .data("obsobrigatoria", data[i].observacao_obrigatoria)
                                        .append(data[i].nome)

                        )
                    }
                }
            }, "json");
        };

        $('#form_interacao').submit(function(e){
            e.preventDefault();
            var url = $(this).attr("action");
            $.post(url, $("#form_interacao" ).serialize(), function(data){

                var bsDialogmsg =  $(".modal_escolha .bootstrap-dialog-message");
                bsDialogmsg.html(data);
                bsDialogmsg.show();
                setTimeout(1200, function() {
                    bsDialogmsg.fadeOut(1200);
                });

            });
        });

        $('.bootstrap-dialog-footer .bootstrap-dialog-footer-buttons .btn')
                .on("click", function(e){


                    e.preventDefault();
                    window.location.href = "{{$referrerUrl}}";

        });

        $('.ajax_interacao').on('change', elementsBind);
        $(".dateTime").mask('00/00/0000 00:00:00');


        @if(!$exibir_fechar)
            $('.modal_escolha .bootstrap-dialog-footer .bootstrap-dialog-footer-buttons .btn').first().remove();
            $('.modal_escolha .bootstrap-dialog-close-button').first().remove();
        @endif
    });
</script>


@if ($contraparte == true)
    <script>
        $(document).ready(function () {
            $('#interacao_1').trigger('change');
        });
    </script>
@endif
