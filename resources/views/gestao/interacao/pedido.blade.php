@extends('layouts.base')
@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">
            <div class="card">
                <div class="header">
                    <h4 class="title">Cliente</h4>
                    <p class="category">Interação #{{ $interacao->id }}</p>
                </div>
                <div class="content">

                    <h4>Cliente</h4>
                    {{ $interacao->client->nome }}

                    <h4>Observação</h4>
                    <p>{{ $interacao->observacao }}</p>
                    <hr />
                    <div class="margin-top"></div>
                    <table class="table table-bordered">
                        <thead>
                            <th>#</th>
                            <th>Produto</th>
                            <th>Quantidade</th>
                            <th>Valor</th>
                        </thead>
                        <tbody>
                        @foreach( $interacao->cart->cart_products  as $item)
                            <tr>
                                <td>{{ $item->product_id }}</td>
                                <td>{{ $item->product->nome }}</td>
                                <td>{{ $item->quantidade }}</td>
                                <td>R$ {{ $item->valor_total }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
