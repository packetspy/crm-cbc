<h4>Cliente: {!! $clienteInfo['nome'] !!}</h4>

<div class="panel-group" id="accordion_contact_list" role="tablist" aria-multiselectable="true" style="max-height: 500px; overflow-y:scroll">

    @foreach ($contatos as $contato)
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="heading_collapse_contact_list{!! $contato["contact_id"] !!}">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse_contact_list{!! $contato->id !!}" aria-expanded="false" aria-controls="collapse_contact_list{!! $contato["contact_id"] !!}">
                        {{ $contato["nome"] }}
                    </a>
                </h4>
            </div>
            <div id="collapse_contact_list{!! $contato->id !!}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_collapse_contact_list{!! $contato["contact_id"] !!}">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            <p>
                                <label>Nome</label>
                                {!! $contato["nome"] !!}
                            </p>
                            <p>
                                <label>Departamento</label>
                                {!! $contato['departamento'] !!}
                            </p>
                        </div>
                        <div class="col-md-6">
                            <p>
                                <label>Contatos</label>
                            </p>
                            <ul class="list-inline list-unstyled">
                              @if($contato->email->count() != 0)
                                <ul class="list-unstyled">
                                @foreach($contato->email as $email)
                                <li id="li{{$email->id}}"><button type="button" class="btn btn-danger btn-xs btn-fill delete" data-id="{{$email->id}}" data-token="{{csrf_token()}}" data-type="email" data-titledelete="{!! trans('email.titledelete') !!}" data-textdelete="{!! trans('email.textdelete') !!}" data-titlesuccess="{!! trans('email.titlesuccess') !!}" data-url="cliente/formas-de-contato"><i class="fa fa-trash-o"></i></button> <a href="mailto:{{$email->contato}}" target="_blank">{{$email->contato}}</a></li>
                                @endforeach
                              </ul>
                              @else
                                <spam class="label label-warning">Nenhum e-mail cadastrado.</spam>
                              @endif
                              @if($contato->telefone->count() != 0)
                                <ul class="list-unstyled">
                                @foreach($contato->telefone as $telefone)
                                <li id="li{{$telefone->id}}"><button type="button" class="btn btn-danger btn-xs btn-fill delete" data-id="{{$telefone->id}}" data-token="{{csrf_token()}}" data-type="telefone" data-titledelete="{!! trans('telefone.titledelete') !!}" data-textdelete="{!! trans('telefone.textdelete') !!}" data-titlesuccess="{!! trans('telefone.titlesuccess') !!}" data-url="cliente/formas-de-contato"><i class="fa fa-trash-o"></i></button> {{$telefone->contato}}</li>
                                @endforeach
                              </ul>
                              @else
                                <spam class="label label-warning">Nenhum telefone cadastrado.</spam>
                              @endif
                              @if($contato->skype->count() != 0)
                                <ul class="list-unstyled">
                                @foreach($contato->skype as $skype)
                                <li id="li{{$skype->id}}"><button type="button" class="btn btn-danger btn-xs btn-fill delete" data-id="{{$skype->id}}" data-token="{{csrf_token()}}" data-type="skype" data-titledelete="{!! trans('skype.titledelete') !!}" data-textdelete="{!! trans('skype.textdelete') !!}" data-titlesuccess="{!! trans('skype.titlesuccess') !!}" data-url="cliente/formas-de-contato"><i class="fa fa-trash-o"></i></button> {{$skype->contato}}</li>
                                @endforeach
                                </ul>
                              @else
                                <spam class="label label-warning">Nenhum telefone cadastrado.</spam>
                              @endif
                            </ul>
                        </div>
                    </div>


                    @if($contato->intermediate_products->count() != 0)
                        <div class="row">
                            <div class="col-md-12">
                                <h4>Produtos de Interesse</h4>
                            </div>
                            <div class="col-md-12">

                                <table class="table table-striped table-responsive table-bordered">
                                    <thead>
                                    <th>Produto</th>
                                    <th>Tipo</th>
                                    <th>Estados</th>
                                    </thead>
                                    <tbody>
                                    @foreach ($contato->intermediate_products as $product)
                                        <tr>
                                            <td>{!! $product->product->nome !!}</td>
                                            <td>{!! $product->tipo_transacao !!}</td>
                                            <td>{!! $product->estados !!}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    @else
                    <spam class="label label-warning">Contato sem produto!</spam>
                    @endif
                </div>
            </div>
        </div>
    @endforeach
</div>
