
{{-- @if($contraparte_bloqueada)--}}
<!--<div class="alert alert-danger">
    Você possui interações que devem ser respondidas.
</div>-->
{{-- @endif --}}

@if(count($contraparte_bloqueada)> 0)
    @foreach($contraparte_bloqueada as $one)
        <a href="{{ url('cliente/interacoes', $one->client_id) }}" class="btn btn-fill btn-primary btn-xs">
            <i class="fa fa-reply"></i> Interação #{!! $one->id !!}
        </a>
    @endforeach
@endif
