@if ($message != null)
   <div class="alert alert-success" id="msg-ajax-response">
      {{$message}}
   </div>
@endif