<div id="interacao_contato" class="hidden">
   {!! Form::label('contato', 'Retornar contato em', ['class'=>"label-contato"]) !!}
   {!! Form::date('dia_contato', '',
     ['id'=>'data_contato', 'class' => 'form-control', 'placeholder' => 'dd/mm/   YYY'])
   !!}
    {!! Form::time('hora_contato', '', ['class' => 'form-control', 'id'=>"hora_contato"]) !!}
</div>
