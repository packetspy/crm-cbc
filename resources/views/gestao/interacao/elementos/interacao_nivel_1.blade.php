<div {!! ($contraparte == true) ? "style='display:none'" : "" !!}>
    {!! Form::label('escolha', 'Tipo') !!}
    <select class="form-control ajax_interacao" data-to="2" name="interacao_1" id="interacao_1">
        <option value="" />Selecione</option>
        <?php $i = 0; ?>
        @foreach ($interacoes as $interacao)
           <option value="{{ $interacao->id }}"
           data-childrens="{{ $interacao->childrens_number}}"
           data-dtobrigatoria="{{$interacao->data_obrigatoria ? 1 : 0}}"
           data-obsobrigatoria="{{$interacao->observacao_obrigatoria ? 1 : 0}}"
           <?php /* Solucao de contorno == Gambiarra */ ?>
           {!! $i == 0 && $contraparte == true ? ' selected ' : $i++ !!}
           <?php /* Fim da gambiarra */ ?>
           >{{ $interacao->nome }}</option>
        @endforeach
    </select>
</div>
