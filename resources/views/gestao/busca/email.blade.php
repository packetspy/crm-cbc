@extends('layouts.base')

@section('content')
@include('layouts.form-busca-avancada')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                  <div class="row">
                    <div class="col-lg-12">
                      <h4 class="title">{{$titulo}} ({{ $resultados->total() }})</h4>
                    </div>
                  </div>
                </div>
                <div class="panel-body">

                  <div class="alert alert-info {{ $resultados->total() == 0 ? '' : 'hide' }}">
                    <i class="fa fa-info"></i>Sua busca não retornou nenhum resultado, tente novamente.
                  </div>

                    <!-- Row -->
                    <table class="table table-responsive {{ $resultados->total() == 0 ? 'hide' : '' }}" style="margin-top: 1em;">
                        <thead>
                            <th>E-mail</th>
                            <th>Nome</th>
                            <th>Empresa</th>
                            <th>Ações</th>
                        </thead>
                        <tbody>
                        @foreach ($resultados as $resultado)
                            <tr>
                              <td>{{ $resultado->contato }}</td>
                              <td>{{ $resultado->contact['id'] == null ? 'Contato Excluído' : $resultado->contact['nome'] }}</td>
                              <td>{{ $resultado->contact['client']['id'] == null ? 'Empresa Excluída' : '' }} <a href="{{ url('cliente/resumo', $resultado->contact['client']['id']) }}">{{ $resultado->contact['client']['empresa']}}</a></td>
                              <td>
                                <div style="padding:12px 0;">
                                  <a href="{{ url('cliente/resumo', $resultado->contact['client']['id']) }}" class="btn btn-fill btn-primary btn-xs pull-left {{ $resultado->contact['client']['id'] == null ? 'hide' : '' }}">
                                      <i class="fa fa-edit"></i> Ver
                                  </a>
                                </div>
                              </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                    <div class="row">
											<div class="col-md-12 text-center">
												Total de registros: <b>{{ $resultados->total() }}</b>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12 text-center">
													{!! $resultados->appends(\Request::except('page'))->render() !!}
											</div>
										</div>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

@endsection
