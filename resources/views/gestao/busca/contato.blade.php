@extends('layouts.base')

@section('content')
@include('layouts.form-busca-avancada')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                  <div class="row">
                    <div class="col-lg-12">
                      <h4 class="title">{{$titulo}} ({{ $resultados->total() }})</h4>
                    </div>
                  </div>
                </div>
                <div class="panel-body">

                  <div class="alert alert-info {{ $resultados->total() == 0 ? '' : 'hide' }}">
                    <i class="fa fa-info"></i>Sua busca não retornou nenhum resultado, tente novamente.
                  </div>

                    <!-- Row -->
                    <table class="table table-responsive {{ $resultados->total() == 0 ? 'hide' : '' }}" style="margin-top: 1em;">
                        <thead>
                            <th>#</th>
                            <th>Nome</th>
                            <th>Departamento</th>
                            <th>Empresa</th>
                            <th>Contatos</th>
                            <th>Ações</th>
                        </thead>
                        <tbody>
                        @foreach ($resultados as $resultado)
                            <tr>
                                <td>#{{ $resultado->id }}</td>
                                <td>{{ $resultado->nome }}</td>
                                <td>{{ $resultado->departamento }}</td>
                                <td><a href="{{ url('cliente/resumo', $resultado->client_id) }}">{{ $resultado->client->empresa }}</a></td>
                                <td>
                                  <a tabindex="0" class="btn btn-xs btn-info btn-fill"
                                     rel="popover"
                                     role="button"
                                     data-toggle="popover"
                                     data-trigger="focus"
                                     data-placement="top"
                                     data-html="true"
                                     data-delay='{"show":"1", "hide":"100"}'
                                     title="Contatos"
                                     data-content="
                                     @foreach ($resultado->contact_infos as $contato)
                                       @if($contato->tipo == 'email')
                                         <a href='mailto:{{$contato->contato}}'><i class='fa fa-envelope-o'></i> {{$contato->contato}}</a> <br>
                                       @endif
                                     @endforeach
                                      ">
                                     Ver contatos</i></a>
                                </td>
                                <td>
                                  <div style="padding:12px 0;">
                                    <a href="{{ url('cliente/resumo', $resultado->client_id) }}" class="btn btn-fill btn-primary btn-xs" style="float:left;">
                                        <i class="fa fa-edit"></i> Ver
                                    </a>
                                  </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                    <div class="row">
											<div class="col-md-12 text-center">
												Total de registros: <b>{{ $resultados->total() }}</b>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12 text-center">
													{!! $resultados->appends(\Request::except('page'))->render() !!}
											</div>
										</div>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

@endsection
