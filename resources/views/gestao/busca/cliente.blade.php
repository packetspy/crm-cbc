@extends('layouts.base')

@section('content')
@include('layouts.form-busca-avancada')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                  <div class="row">
                    <div class="col-lg-12">
                      <h4 class="title">{{$titulo}} ({{ $resultados->total() }})</h4>
                    </div>
                  </div>
                </div>
                <div class="panel-body">

                  <div class="alert alert-info {{ $resultados->total() == 0 ? '' : 'hide' }}">
                    <i class="fa fa-info"></i>Sua busca não retornou nenhum resultado, tente novamente.
                  </div>

                    <!-- Row -->
                    <table class="table table-responsive {{ $resultados->total() == 0 ? 'hide' : '' }}" style="margin-top: 1em;">
                        <thead>
                            <th>#</th>
                            <th>Tipo</th>
                            <th>Documento</th>
                            <th>Empresa</th>
                            <th>Estado</th>
                            <th>Status</th>
                            <th>Responsável</th>
                            <th>Ações</th>
                        </thead>
                        <tbody>
                        @foreach ($resultados as $resultado)
                            <tr>
                                <td>#{{ $resultado->id }}</td>
                                <td>{{ $resultado->tipo_pessoa_short }}</td>
                                <td>{{ $resultado->documento }}</td>
                                <td><a href="{{ url('cliente/resumo', $resultado->id) }}">{{ $resultado->empresa }}</a></td>
                                <td>{{ $resultado->estado }}</td>
                                <td><span class="label label-info">@if(isset($resultado->status)) {{ $resultado->status->status }} @endif</span></td>
                                <td>@if(isset($resultado->responsavel)) {{ $resultado->responsavel->name }} @endif</td>
                                <td>
                                  <div style="padding:12px 0;">
                                    <a href="{{ url('cliente/resumo', $resultado->id) }}" class="btn btn-fill btn-primary btn-xs" style="float:left;">
                                        <i class="fa fa-edit"></i> Ver
                                    </a>

                                    @is(['Atendimento','Gestores'])
                                        @if(isset($resultado->responsavel->name))
                                        <button style="float:left;" type="button" onclick="selecionaConsultor({{ $resultado->id }},{{ $resultado->responsavel->id }});" class="btn btn-fill btn-xs btn-success">
                                            <i class="fa fa-refresh"></i> Alterar Consultor
                                        </button>

                                        @else
                                          <button style="float:left;" type="button" onclick="selecionaConsultor({{ $resultado->id }});" class="btn btn-fill btn-xs btn-success">
                                              <i class="fa fa-user-plus"></i> Selecionar Consultor
                                          </button>
                                        @endif
                                    @endis
                                  </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                    <div class="row">
											<div class="col-md-12 text-center">
												Total de registros: <b>{{ $resultados->total() }}</b>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12 text-center">
													{!! $resultados->appends(\Request::except('page'))->render() !!}
											</div>
										</div>

                </div>
            </div>
        </div>
    </div>
@endsection

<script>
  function selecionaConsultor(client_id, responsavel_id)
  {
    if(responsavel_id === undefined)
    {
      responsavel_id = '';
    }

    var action_url = '{!! url('clientes/selecionar-responsavel/') !!}';
    var url = action_url + '/' + client_id + '/' + responsavel_id;

      return BootstrapDialog.show({
          title: 'Seleciona Consultor',
          message: $('<div></div>').load(url),
          buttons: [{
              label: 'Salvar',
              action: function(dialogRef){
                  $('#changeClientResponsible').submit();
                  //window.location.href = '{!! url()->full() !!}';
                  return false;
                  dialogRef.close();
              }
          }, {
              label: 'Fechar',
              action: function(dialogRef){
                  dialogRef.close();
              }
          }]
      });
  }
</script>

@section('scripts')

@endsection
