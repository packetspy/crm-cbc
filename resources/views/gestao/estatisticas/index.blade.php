@extends('layouts.base')

@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h4 class="title">Estatísticas</h4>
                    <p class="category">Indicadores e demais informações</p>
                </div>
                <div class="panel-body">

                </div>
            </div>
        </div>
    </div>

    <!-- Publico -->
    <div class="row">
        <div class="col-md-3 col-sm-3 col-xs-12">
           <div class="card">
               <div class="header">
                    <h2 class="text-center"><i class="fa pe-7s-user fa-3x" style="width: 100%"></i> </h2>
                   <h6 class="text-center margin-top">{!! $clientsCount !!} Clientes cadastrados</h6>
               </div>
               <div class="panel-body">
                  <h6 class="text-success">{!! $clientsAtivos !!} ativos</h6><br />
                  <h6 class="text-danger">{!! $clientsInativos !!} inativos</h6>
               </div>
           </div>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2 class="text-center"><i class="fa pe-7s-users fa-3x" style="width: 100%"></i> </h2>
                    <h6 class="text-center margin-top">{!! $usersCount !!} Usuários cadastrados</h6>
                </div>
                <div class="panel-body">
                    <p></p>
                </div>
            </div>
        </div>

        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2 class="text-center"><i class="fa pe-7s-users fa-3x" style="width: 100%"></i> </h2>
                    <h6 class="text-center margin-top">{!! $interactions->count() !!} Tipos de Interações</h6>
                </div>
                <div class="panel-body">
                    <p>
                        <table class="table table-bordered">
                            <?php $countInteractions = 0; ?>
                            <thead>
                                <th>Nome</th>
                                <th>Tipo</th>
                            </thead>
                            <tbody>

                                @foreach ($interactions as $interaction)
                                    <?php  $countInteractions += $interaction->interactions()->count() ; ?>
                                    <tr>
                                        <td>{{ $interaction->nome() }}</td>
                                        <td class="text-right">{!! $interaction->interactions()->count() !!}</td>
                                    </tr>
                                @endforeach

                            <tr>
                                <td>TOTAL</td>
                                <td class="text-right"><strong>{!! $countInteractions !!}</strong></td>
                            </tr>

                            </tbody>
                        </table>


                    </p>
                </div>
            </div>
        </div>


    </div>

    <!-- ## Publico -->

@endsection
