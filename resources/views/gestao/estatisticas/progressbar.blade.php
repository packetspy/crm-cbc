@if ($estatisticas['interacao_total_sistema'] > 0)

    <?php $time = substr(hash('ripemd160', rand(0, 4)), 0,4); ?>
    <div class="panel-group" id="accordion{!! $time !!}" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="heading{!! $time !!}">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion{!! $time !!}" href="#collapse{!! $time !!}" aria-expanded="false" aria-controls="collapse{!! $time !!}">
                       {!! isset($title_tab) ? $title_tab : 'Ver dados' !!}
                    </a>
                </h4>
            </div>
            <div id="collapse{!! $time !!}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading{!! $time !!}">
                <div class="panel-body">



                    <p>
                        <label>Interações iniciadas</label>
                        {!! $estatisticas['interacoes_iniciadas_total'] !!}
                    </p>


                    <div class="progress">
                        <div class="progress-bar" role="progressbar" aria-valuenow="{!! $estatisticas['pct_interacoes_iniciadas'] !!}"
                             aria-valuemin="0" aria-valuemax="100" style="width:{!! $estatisticas['pct_interacoes_iniciadas'] !!}%">
                            <span class="text-warning">{!! $estatisticas['pct_interacoes_iniciadas'] !!}%</span>
                        </div>
                    </div>



                    <p>
                        <label>Interações que tiveram atualização (continuadas)</label>
                        {!! $estatisticas['interacoes_iniciadas_atualizadas'] !!}
                    </p>

                    <div class="progress">
                        <div class="progress-bar" role="progressbar" aria-valuenow="{!! $estatisticas['pct_interacoes_atualizadas'] !!}"
                             aria-valuemin="0" aria-valuemax="100" style="width:{!! $estatisticas['pct_interacoes_atualizadas'] !!}%">
                            <span class="text-warning">{!! $estatisticas['pct_interacoes_atualizadas'] !!}%</span>
                        </div>
                    </div>


                    <p>
                        <label>Interações com data tempestiva</label>
                        {!! $estatisticas['interacoes_com_data_tempestivas'] !!}
                    </p>

                    <div class="progress">
                        <div class="progress-bar" role="progressbar" aria-valuenow="{!! $estatisticas['pct_interacoes_tempestiva'] !!}"
                             aria-valuemin="0" aria-valuemax="100" style="width:{!! $estatisticas['pct_interacoes_tempestiva'] !!}%">
                            <span class="text-warning">{!! $estatisticas['pct_interacoes_tempestiva'] !!}%</span>
                        </div>
                    </div>


                    <p>
                        <label>Interações respondidas com atraso</label>
                        {!! $estatisticas['interacoes_com_data_no_horario'] !!}
                    </p>


                    <div class="progress">
                        <div class="progress-bar" role="progressbar" aria-valuenow="{!! $estatisticas['pct_interacoes_com_data_no_horario'] !!}"
                             aria-valuemin="0" aria-valuemax="100" style="width:{!! $estatisticas['pct_interacoes_com_data_no_horario'] !!}%">
                            <span class="text-warning">{!! $estatisticas['pct_interacoes_com_data_no_horario'] !!}%</span>
                        </div>
                    </div>

                    <p>
                        <label>Interações de Negócios Fechados</label>
                        {!! $estatisticas['negocios_gerados'] !!}
                    </p>

                    <div class="progress">
                        <div class="progress-bar" role="progressbar" aria-valuenow="{!! $estatisticas['pct_interacoes_negocios_gerados'] !!}"
                             aria-valuemin="0" aria-valuemax="100" style="width:{!! $estatisticas['pct_interacoes_negocios_gerados'] !!}%">
                            <span class="text-warning">{!! $estatisticas['pct_interacoes_negocios_gerados'] !!}%</span>
                        </div>
                    </div>

                    <p>
                        <label>Interações com Negócios Não Fechados</label>
                        {!! $estatisticas['negocios_nao_fechados'] !!}
                    </p>

                    <div class="progress">
                        <div class="progress-bar" role="progressbar" aria-valuenow="{!! $estatisticas['pct_interacoes_negocios_nao_fechados'] !!}"
                             aria-valuemin="0" aria-valuemax="100" style="width:{!! $estatisticas['pct_interacoes_negocios_nao_fechados'] !!}%">
                            <span class="text-warning">{!! $estatisticas['pct_interacoes_negocios_nao_fechados'] !!}%</span>
                        </div>
                    </div>

                    @if(($nao_fechado_detalhado = $estatisticas['interacao_negocio_nao_fechado_detalhado']) != null)
                        <?php $total = $estatisticas['negocios_nao_fechados']; ?>
                        @foreach($nao_fechado_detalhado as $negocio)
                            <p>
                                <label>
                                    {!! $negocio->interaction_type->nome !!}
                                    <strong class="text-warning">{!! $negocio->total !!} ({!! round ( $negocio->total / $total * 100, 2 ) !!}%)</strong>
                                </label>
                            </p>
                        @endforeach
                    @endif

                </div>
            </div>
        </div>
    </div>

@else

    <p class="text-warning">Não há dados suficientes.</p>
@endif

