@extends('layouts.base')
@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="card">
                <div class="header">
                    <h4 class="title">Usuários</h4>
                    <p class="category">Gestão de Usuários</p>
                </div>
                <div class="content">

                    <a href="{{ action('Crm\UserController@getCreate') }}" class="btn btn-fill btn-md btn-info"><i class="fa fa-user"></i> Cadastrar usuário</a>

                    <div class="clearfix margin-top"></div>

                    <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <th>#</th>
                        <th>Nome</th>
                        <th>E-mail</th>
                        <th>Ação</th>
                        </thead>
                        <tbody>
                        @foreach ($users as $user)
                            <tr>
                                <td>{!! $user->id !!}</td>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>
                                    <a href="{{ action('Crm\UserController@getEdit', $user->id) }}" class="btn btn-fill btn-warning btn-sm">
                                        <i class="fa fa-edit"></i> Editar
                                    </a>
                                    <a href="{{ action('Crm\UserController@getDeletar', $user->id) }}" class="btn btn-fill btn-sm btn-danger">
                                        <i class="fa fa-trash-o"></i> Excluir
                                    </a>

                                    @shield('user.logs')
                                        <a href="{{ action('Crm\UserController@getLogs', $user->id) }}" class="btn btn-fill btn-sm btn-info">
                                            <i class="fa fa-info"></i> Histórico</a>

                                        <a href="{{ action('Crm\UserController@getStatistics', $user->id) }}" class="btn btn-fill btn-sm btn-default">
                                            <i class="fa fa-area-chart"></i> Estatísticas </a>
                                    @endshield

                                   <!-- <a href="#" class="btn btn-fill btn-danger btn-sm">
                                        <i class="fa fa-trash"></i> Excluir
                                    </a>-->
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    </div>

                    {!! $users->render() !!}

                </div>
            </div>
        </div>

    </div>

@endsection
