
<div class="row">
    <div class="col-md-12">
        <h4 class="title">Permissões</h4>
        <button type="button" class="btn btn-fill btn-xs btn-info" onClick="marcar_todos(false);">
            Marcar todos
        </button>
        <button type="button" class="btn btn-fill btn-xs btn-primary" onClick="marcar_todos(true);">
            Excluir todos
        </button>
    </div>
</div>


<div class="row"> <!-- start row -->
  <div class="col-md-4">
    <div class="panel panel-default">
      <div class="panel-heading">Usuários</div>
      <div class="panel-body">
        @foreach ($permissoes['user'] as $permissao)
            <p>
            {!! Form::checkbox('permissions[]', $permissao['id'], isset($user_permissions[ $permissao['id']] ) && $user_permissions[ $permissao['id']] == '1' ? $user_permissions[ $permissao['id']] : null, ['id' => $permissao['id']]) !!}
            {!! Form::label($permissao['id'], $permissao['name']) !!}
            </p>
        @endforeach
      </div>
    </div>
  </div>

  <div class="col-md-4">
    <div class="panel panel-default">
      <div class="panel-heading">Clientes</div>
      <div class="panel-body">
        @foreach ($permissoes['client'] as $permissao)
            <p>
                {!! Form::checkbox('permissions[]', $permissao['id'],  isset($user_permissions[ $permissao['id']])  && $user_permissions[ $permissao['id']] == '1'  ? $user_permissions[ $permissao['id']] : null, ['id' => $permissao['id']]) !!}
                {!! Form::label($permissao['id'], $permissao['name']) !!}
            </p>
        @endforeach
      </div>
    </div>
  </div>

  <div class="col-md-4">
    <div class="panel panel-default">
      <div class="panel-heading">Contatos</div>
      <div class="panel-body">
        @foreach ($permissoes['contact'] as $permissao)
            <p>
                {!! Form::checkbox('permissions[]', $permissao['id'],  isset($user_permissions[ $permissao['id']])  && $user_permissions[ $permissao['id']] == '1'  ? $user_permissions[ $permissao['id']] : null, ['id' => $permissao['id']]) !!}
                {!! Form::label($permissao['id'], $permissao['name']) !!}
            </p>
        @endforeach
      </div>
    </div>
  </div>
</div><!-- end row -->

<div class="row"> <!-- start row -->
  <div class="col-md-4">
    <div class="panel panel-default">
      <div class="panel-heading">Produtos</div>
      <div class="panel-body">
        @foreach ($permissoes['product'] as $permissao)
            <p>
                {!! Form::checkbox('permissions[]', $permissao['id'], isset($user_permissions[ $permissao['id']] ) && $user_permissions[ $permissao['id']] == '1'  ? $user_permissions[ $permissao['id']] : null, ['id' => $permissao['id']]) !!}
                {!! Form::label($permissao['id'], $permissao['name']) !!}
            </p>
        @endforeach
      </div>
    </div>
  </div>

  <div class="col-md-4">
    <div class="panel panel-default">
      <div class="panel-heading">Interações</div>
      <div class="panel-body">
        @foreach ($permissoes['interaction'] as $permissao)
            <p>
                {!! Form::checkbox('permissions[]', $permissao['id'],  isset($user_permissions[ $permissao['id']])  && $user_permissions[ $permissao['id']] == '1'  ? $user_permissions[ $permissao['id']] : null, ['id' => $permissao['id']]) !!}
                {!! Form::label($permissao['id'], $permissao['name']) !!}
            </p>
        @endforeach
      </div>
    </div>
  </div>

  <div class="col-md-4">
    <div class="panel panel-default">
      <div class="panel-heading">Calendários</div>
      <div class="panel-body">
        @foreach ($permissoes['calendar'] as $permissao)
            <p>
                {!! Form::checkbox('permissions[]', $permissao['id'],  isset($user_permissions[ $permissao['id']])  && $user_permissions[ $permissao['id']] == '1'  ? $user_permissions[ $permissao['id']] : null, ['id' => $permissao['id']]) !!}
                {!! Form::label($permissao['id'], $permissao['name']) !!}
            </p>
        @endforeach
      </div>
    </div>
  </div>
</div><!-- end row -->

<div class="row"> <!-- start row -->
  <div class="col-md-4">
    <div class="panel panel-default">
      <div class="panel-heading">Exportação e Importação</div>
      <div class="panel-body">
        @foreach ($permissoes['export_import'] as $permissao)
            <p>
                {!! Form::checkbox('permissions[]', $permissao['id'],  isset($user_permissions[ $permissao['id']])  && $user_permissions[ $permissao['id']] == '1'  ? $user_permissions[ $permissao['id']] : null, ['id' => $permissao['id']]) !!}
                {!! Form::label($permissao['id'], $permissao['name']) !!}
            </p>
        @endforeach
      </div>
    </div>
  </div>

  <div class="col-md-4">
    <div class="panel panel-default">
      <div class="panel-heading">Financeiro</div>
      <div class="panel-body">
        @foreach ($permissoes['payment'] as $permissao)
            <p>
                {!! Form::checkbox('permissions[]', $permissao['id'],  isset($user_permissions[ $permissao['id']])  && $user_permissions[ $permissao['id']] == '1'  ? $user_permissions[ $permissao['id']] : null, ['id' => $permissao['id']]) !!}
                {!! Form::label($permissao['id'], $permissao['name']) !!}
            </p>
        @endforeach
      </div>
    </div>
  </div>

  <div class="col-md-4">
    <!--
    <div class="panel panel-default">
      <div class="panel-heading">title</div>
      <div class="panel-body">
        code here
      </div>
    </div> -->
  </div>
</div><!-- end row -->

<div class="row"><!-- start row -->
  <div class="col-md-12">
      <button type="submit" class="btn btn-fill btn-primary">
          @if(isset($user))
              <i class="fa fa-btn fa-refresh"></i>
              Atualizar os dados do usuário
          @else
              <i class="fa fa-btn fa-user"></i>
              Cadastrar novo usuário
           @endif
      </button>
  </div>
</div><!-- end row -->
