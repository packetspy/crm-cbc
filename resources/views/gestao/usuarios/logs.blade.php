@extends('layouts.base')
@section('content')
   <div class="row">
       <div class="card">
           <div class="header">
               <h4 class="title">Usuários</h4>
               <p class="category">Gestão de Usuários</p>
           </div>

           <div class="content">

               <p><i class="fa fa-user"></i> {!! $user->name !!}</p>
               <p><i class="fa fa-envelope"></i> {!! $user->email !!}</p>

               <table class="table table-bordered"
                    <thead>
                        <th></th>
                        <th>Usuário</th>
                        <th>Data / Hora</th>
                        <th>Ação</th>
                        <th>Detalhes</th>
                        <th>IP</th>
                        <th>Navegador</th>
                    </thead>
                    <tbody>
                        @foreach ($logs as $log)
                            <tr>
                                <td>
                                    {!! $log->getIconMarkup() !!}
                                </td>
                                <td>
                                    {!! $log->user->name !!}
                                </td>
                                <td>
                                    {!! $log->created_at !!}
                                </td>
                                <td>
                                    {!! $log->action !!}
                                </td>
                                <td>
                                    {!! $log->getDescription() !!}
                                </td>
                                <td>
                                    {!! $log->ip_address !!}
                                </td>
                                <td>
                                    {!! $log->user_agent !!}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
               </table>

               {!! $logs->render() !!}

           </div>

       </div>
   </div>

@endsection