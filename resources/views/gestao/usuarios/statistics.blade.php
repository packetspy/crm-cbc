@extends('layouts.base')
@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="header">
                    <h4 class="title">Usuários</h4>
                    <p class="category">Estatísticas</p>
                </div>

                <div class="content">

                    <label><i class="fa fa-user"></i> USUÁRIO</label>
                    <p> {!! $user->name !!}</p>
                    <label><i class="fa fa-envelope"></i> EMAIL</label>
                    <p> {!! $user->email !!}</p>

                    @include('gestao.estatisticas.progressbar', ['estatisticas' => $estatisticas])


                </div>

            </div>
        </div>

        <div class="col-md-6">
            <div class="card">
                <div class="content">


                    <strong>Período</strong>

                    {!! Form::open(['method' => 'get']) !!}
                    <div class="row">
                        <div class="col-md-6">
                           {!! Form::label('de') !!}
                            {!! Form::date('from', $start, ['class'=> 'form-control', 'placeholder' => 'dd/mm/YYYY']) !!}
                        </div>
                        <div class="col-md-6">

                            {!! Form::label('Até') !!}
                            <div class="input-group">
                                {!! Form::date('to', $end, ['class'=> 'form-control', 'placeholder' => 'dd/mm/YYYY']) !!}
                                <div class="input-group-btn">
                                    <button type="submit" class="btn btn-fill btn-info btn-sm">
                                        <i class="fa fa-search"></i> Filtrar
                                    </button>
                                </div>
                            </div>
                        </div>


                    </div>
                    {!! Form::close() !!}




                </div>
            </div>
        </div>


        </div>
<?php /*
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="content">


                </div>
            </div>

        </div>

        <div class="col-md-6">
            <div class="card">
                <div class="content">
                    <div id="chartInteracoes" class="ct-chart ct-perfect-fourth"></div>
                </div>
                <div class="footer">
                    <h6>Interações</h6>
                    <i class="fa fa-circle text-success"></i> Geraram negócio
                    <i class="fa fa-circle text-danger"></i> Não geraram negócio
                    <i class="fa fa-circle text-info"></i> Outros
                </div>
            </div>
        </div>



    </div> */?>

@endsection


@section('scripts')
    <script src="{{ asset('theme/assets/js/chartist.min.js') }}"></script>
    <script>
        /*  **************** Public Preferences - Pie Chart ******************** */

        var dataPreferences = {
            series: [
                [25, 30, 20, 25]
            ]
        };

        var optionsPreferences = {
            donut: true,
            donutWidth: 40,
            startAngle: 0,
            height: "245px",
            total: 100,
            showLabel: false,
            axisX: {
                showGrid: false
            }
        };

        Chartist.Pie('#chartInteracoes', dataPreferences, optionsPreferences);

        Chartist.Pie('#chartInteracoes', {
            labels: ['62%','32%'],
            series: [62, 32]
        });

    </script>

@endsection