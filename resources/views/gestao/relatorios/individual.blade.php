@extends('layouts.base')
@section('content')

    <div class="row">
        <div class="col-md-8 col-sm-8 col-xs-12">
            <div class="card">
                <div class="header">
                    <h4 class="title">Meus Relatórios</h4>
                    <p class="category">Gerar relatórios</p>
                </div>
                <div class="content">
                  {!! Form::open(['url' => action('Crm\RelatorioController@postGerarIndividual'), 'method' => 'post', 'class' => 'form-horizontal', "id" => "form_relatorio"]) !!}
                    {!! Form::label('tipo', 'Tipo de Relatório') !!}
                    <select id="relatorios" class="form-control">
                        <option selected disabled>Selecione</option>
                        @foreach($tipos as $tipo)
                            <?php 
                                $data = "";
                                foreach($tipo["filtros"] as $filtro){
                                    $data .= "data-".$filtro.'="true" '; 
                                }
                            ?>
                            <option {{$data}} data-relatorio="{{$tipo['relatorio']}}">{{$tipo['nome']}}</option>
                        @endforeach
                    </select>
                    <input type="hidden" id="relatorio" name="relatorio">
                    <div class="dt_inicio hidden">
                        {!! Form::label('dt_inicio', 'Inicio') !!}
                        {!! Form::text('dt_inicio', null, ['class' => 'form-control date', 'placeholder' => 'dd/mm/YYYY'] ) !!}
                    </div>
                    <div class="dt_fim hidden">
                        {!! Form::label('dt_fim', 'Fim') !!}
                        {!! Form::text('dt_fim', null, ['class' => 'form-control date', 'placeholder' => 'dd/mm/YYYY'] ) !!}
                    </div>
                    <div class="formato">
                      {!! Form::label('tipo', 'Excel') !!}
                      <input class="" type="radio" name="tipo" value="xlsx" checked="true" />
                      <!--{!! Form::label('tipo', 'Pdf') !!}
                      <input class="" type="radio" name="tipo" value="pdf" required="true"/>
                      -->
                    </div>
                  {!! Form::submit("Gerar", ["class" => "btn btn-submit"]) !!}
                  {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>


@endsection
@section('scripts')
<script>
$(document).ready(function () {
    $("#relatorios").on("change", function(){
        var option = $('#relatorios option:selected');
        
        $('#relatorio').val($(option).data("relatorio"));

        if(option.data("dt_inicio")){
          $('.dt_inicio').removeClass("hidden");
        }else{
          $('.dt_inicio').addClass("hidden");
        }

        if(option.data("dt_fim")){
          $('.dt_fim').removeClass("hidden");
        }else{
          $('.dt_fim').addClass("hidden");
        }
    });

    $(".date").mask('00/00/0000');
});
</script>

@endsection