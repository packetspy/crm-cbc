@extends('layouts.base')

@section('content')
    <div class="row">
        <div class="col-md-8 col-sm-8 col-xs-12">
            <div class="card">
                <div class="header">
                    <h4 class="title">Gestão de Tipo de Interações</h4>
                    <p class="category">Tipo de Interações cadastradas</p>
                </div>
                <div class="panel-body">
                    <ul class="nav navbar-nav">
                        <li>
                            <a href="#" onClick="abreModal('{{ action('Crm\InteractionTypeController@getCreate') }}', 'Cadastrar Interação');" class="btn btn-fill btn-default btn-sm">
                                <i class="fa fa-plus-square"></i> Cadastrar Tipo de interação
                            </a>
                        </li>
                    </ul>

                    <!-- Row -->
                    <table class="table table-bordered" style="margin-top: 6em;">
                        <thead>
                            <th>#</th>
                            <th>Visível</th>
                            <th>Nome</th>
                            <th>Ações</th>
                        </thead>
                        <tbody>
                            @foreach ($interactionTypes as $oneType)
                            <tr>
                                <td>#{{ $oneType->id }}</td>
                                <td>
                                    @if (!$oneType->ativo)
                                    <a href="#" class="btn btn-fill btn-danger btn-xs">
                                        <i class="fa fa-eye-dash"></i> inativo
                                    </a>
                                    @else
                                    <a href="#" class="btn btn-fill btn-primary btn-xs">
                                        <i class="fa fa-eye"></i> ativo
                                    </a>
                                    @endif
                                </td>
                                <td>
                                {!! empty($oneType->father()->first()) ? "" : ($oneType->nomePai() . "/") !!} <b>{{ $oneType->nome }}</b>
                                </td>
                                <td>
                                    <a href="#" onClick="abreModal('{{ action('Crm\InteractionTypeController@getEdit', $oneType->id) }}', 'Editar Interação');" class="btn btn-fill btn-default btn-sm">
                                        <i class="fa fa-edit"></i> Editar Interação
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                    {!! $interactionTypes->render() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
