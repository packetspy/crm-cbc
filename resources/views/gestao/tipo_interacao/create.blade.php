    <div class="row">
        <div class="col-xs-12">

                    @if(isset($interactionType))
                        {!! Form::model($interactionType, ['action' => ['Crm\InteractionTypeController@postUpdateInteractionType', $interactionType->id], 'method' => 'post', 'class' => 'form-horizontal']) !!}
                    @else
                        {!! Form::open(['action' => 'Crm\InteractionTypeController@postSaveInteractionType', 'method' => 'post', 'class' => 'form-horizontal']) !!}
                    @endif

                    @if(isset($father_id))
                        {!! Form::input("hidden", "father_id",$father_id,["class" => "hidden"]) !!}
                    @endif

                    @if(isset($interactionType))
                    <div class="form-group{{ $errors->has('ativo') ? ' has-error' : '' }}">
                         {!! Form::label('ativo', 'Status', ['class' => 'control-label col-md-4 col-sm-4 col-xs-12']) !!}
                         <div class="col-md-8 col-sm-8 col-xs-12">
                             {!! Form::select('ativo', ['0' => 'Inativo', '1' => 'Ativo'], null, ['class' => 'form-control']) !!}
                             @if ($errors->has('ativo'))
                             <span class="help-block">
                               <strong>{{ $errors->first('ativo') }}</strong>
                             </span>
                             @endif
                         </div>
                    </div>
                    @endif


                    <div class="form-group{{ $errors->has('nome') ? ' has-error' : '' }}">
                        {!! Form::label('nome', 'Nome do Tipo de interação', ['class' => 'control-label col-md-4 col-sm-4 col-xs-12']) !!}
                        <div class="col-md-8 col-sm-8 col-xs-12">
                            {!! Form::text('nome', null, ['class' => 'form-control']) !!}
                            @if ($errors->has('nome'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('nome') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group text-right">
                        <div class="col-md-6 col-xs-6">
                            @if (isset($interactionType) && !$interactionType->father()->first())
                                <a href="#" onClick="abreModal('{{ action('Crm\InteractionTypeController@getCreateSub', $interactionType->id) }}', 'Criar Sub Interação');" class="btn btn-fill btn-default btn-sm"><i class="fa fa-send"></i> Cadastrar sub interação
                                </a>
                            @endif
                        </div>
                        <div class="col-md-6 col-xs-6">
                            <button type="submit" class="btn btn-fill btn-primary">
                                @if (isset($interactionType))
                                    <i class="fa fa-edit"></i> Editar Tipo de interação
                                @else
                                    <i class="fa fa-send"></i> Cadastrar Tipo de interação
                                @endif
                            </button>
                        </div>
                    </div>


                    {!! Form::close() !!}
            </div>
    </div>
    <script>
        $(document).ready(function() {
            $('.money-input').mask('000.000.000.000.000,00', {reverse: true});
        });
    </script>
