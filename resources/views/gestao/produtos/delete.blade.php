
    <p>Você está prestes a excluir o produto <strong>#{{ $produto->id }}</strong>.</p>

    <p>Deseja continuar?</p>

    <div class="form-group">

        <button type="submit" class="btn btn-fill btn-primary btn-sm">
            <i class="fa fa-thumbs-up"></i> Sim, excluir o produto
        </button>
        <button type="reset" class="btn btn-fill btn-default btn-sm">
            <i class="fa fa-close"></i> Não, desisti da exclusão.
        </button>

    </div>