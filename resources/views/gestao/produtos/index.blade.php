@extends('layouts.base')

@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h4 class="title">Gestão de Produtos</h4>
                    <p class="category">Produtos cadastrados</p>
                </div>
                <div class="panel-body">
                    <ul class="nav navbar-nav">
                        <li>
                            <a href="#" onClick="abreModal('{{ action('Crm\ProductController@getCreate') }}', 'Cadastrar produto');" class="btn btn-fill btn-info btn-sm">
                                <i class="fa fa-plus-square"></i> Cadastrar produto
                            </a>
                        </li>
                        <li>
                            <a href="{{ action('Crm\ProductController@getViewLists') }}" class="btn btn-fill btn-success btn-sm">
                                <i class="fa fa-plus-square"></i> Pacotes de produtos
                            </a>
                        </li>
                    </ul>

                    <!-- Row -->
                    <table class="table table-bordered" style="margin-top: 6em;">
                        <thead>
                            <th>#</th>
                            <th>Visível</th>
                            <th>Nome</th>
                            <th>Ações</th>
                        </thead>
                        <tbody>
                            @foreach ($products as $product)

                            <tr>
                                <td>#{{ $product->id }}</td>
                                <td>
                                    @if (!$product->ativo)
                                    <a href="#" class="btn btn-fill btn-danger btn-xs">
                                        <i class="fa fa-eye-dash"></i> inativo
                                    </a>
                                    @else
                                    <a href="#" class="btn btn-fill btn-primary btn-xs">
                                        <i class="fa fa-eye"></i> ativo
                                    </a>
                                    @endif
                                </td>
                                <td>{{ $product->nome }}</td>
                                <td>
                                    <a href="#" onClick="abreModal('{{ action('Crm\ProductController@getEdit', $product->id) }}', 'Editar produto');" class="btn btn-fill btn-default btn-sm">
                                        <i class="fa fa-edit"></i> Editar produto
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {!! $products->render() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
