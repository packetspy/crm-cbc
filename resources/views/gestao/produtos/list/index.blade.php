@extends('layouts.base')

@section('content')
    <div class="row">
        <div class="col-md-8 col-sm-8 col-xs-12">
            <div class="card">
                <div class="header">
                    <h4 class="title">Gestão de pacotes de Produtos</h4>
                    <p class="category">Pacotes cadastrados</p>
                </div>
                <div class="panel-body">
                    <ul class="nav navbar-nav">
                        <li>
                            <a href="#" onClick="abreModal('{{ action('Crm\ProductController@getCreateList') }}', 'Criar pacote de produtos');" class="btn btn-fill btn-info btn-sm">
                                <i class="fa fa-plus-square"></i> Cadastrar pacote de produtos
                            </a>
                        </li>

                        <li>
                            <a href="{{ action('Crm\ProductController@getIndex') }}" class="btn btn-fill btn-success btn-sm">
                                <i class="fa fa-plus-square"></i> Voltar para listagem de produtos
                            </a>
                        </li>
                    </ul>

                    <!-- Row -->
                    <table class="table table-bordered" style="margin-top: 6em;">
                        <thead>
                            <th>#</th>
                            <th>Nome</th>
                            <th>Ações</th>
                        </thead>
                        <tbody>
                            @foreach ($lists as $list)

                            <tr>
                                <td>#{{ $list->id }}</td>
                                <td>{{ $list->name }}</td>
                                <td class="text-center">
                                    <a href="#" onClick="abreModal('{{ action('Crm\ProductController@getListEdit', $list->id) }}', 'Editar produto');" class="btn btn-fill btn-default btn-sm">
                                        <i class="fa fa-edit"></i> Editar lista
                                    </a>
                                    <a href="#" onClick="excluirPacote({{$list->id}})" class="btn btn-fill btn-danger btn-sm">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {!! $lists->render() !!}
                </div>
            </div>
        </div>

        <div class="col-md-4 col-sm-4 col-xs-12">

            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-filter"></i> Filtrar</h3>

                </div>
                <div class="panel-body">
                    <hr />

                    <div class="form-group">
                        {!! Form::label('nome', 'por Nome', ['class' => 'control-label']) !!}
                        <div class="controls" id="remote">
                            {!! Form::text('nome', null, ['class' => 'form-control typeahead', 'autocomplete' => 'off']) !!}
                            <small>Digite o nome </small>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
@endsection

@section('scripts')

    <script src="{{ asset('lib/js/typeahead.bundle.min.js') }}"></script>
    <script>
        jQuery(document).ready(function($) {
            var engine = new Bloodhound({
                remote: {
                    url: '{{ action('Crm\ProductController@getListSearch') }}?name=%QUERY',
                    wildcard: '%QUERY'
                },
                // '...' = displayKey: '...'
                datumTokenizer: Bloodhound.tokenizers.whitespace('nome'),
                queryTokenizer: Bloodhound.tokenizers.whitespace
            });

            engine.initialize();
            $("#remote .typeahead").typeahead({
                hint: true,
                highlight: true,
                minLength: 2
            }, {
                source: engine.ttAdapter(),
                // This will be appended to "tt-dataset-" to form the class name of the suggestion menu.
                name: 'Product_list',
                // the key from the array we want to display (name,id,email,etc...)
                displayKey: 'name',
                templates: {
                    empty: [
                        '<div class="empty-message">Não encontramos nenhuma ocorrência</div>'
                    ],
                    suggestion: function (data) {
                        return '<div>'+ data.name +'<br /> <a class="btn btn-fill btn-xs btn-info" onClick="editaLista('+ data.id +')">Ver lista</a></div>';
                    }
                }
            });
        });

        var editaLista = function(product_id)
        {
            return abreModal('/produtos/lista/editar/' + product_id, 'Editar produto');
        }

        var excluirPacote = function(id)
        {
            console.log(id)
            if(confirm("Tem certeza que deseja excluir esse pacote?")) {
                $.get('/produtos/lista/excluir/' + id, function(data){
                    window.location.href = "/produtos/lista";
                });
            } else {
                console.log("Nao exclui")
            }
        }
    </script>

@endsection