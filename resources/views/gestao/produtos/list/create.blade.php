{!! Form::open(['action' => 'Crm\ProductController@postCreateList', 'method' => 'post', 'class' => 'form-horizontal']) !!}
<div class="row">
    <div class="col-xs-12">
        <div class="form-group{{ $errors->has('nome') ? ' has-error' : '' }}">
            {!! Form::label('nome', 'Nome do pacote', ['class' => 'control-label col-md-4 col-sm-4 col-xs-12']) !!}
            <div class="col-md-8 col-sm-8 col-xs-12">
                {!! Form::hidden('id', $list->id, ['class' => 'form-control']) !!}
                {!! Form::text('nome', $list->name, ['class' => 'form-control']) !!}
                @if ($errors->has('nome'))
                    <span class="help-block">
                        <strong>{{ $errors->first('nome') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('descricao') ? ' has-error' : '' }}">
            {!! Form::label('descricao', 'Pacote de produtos', ['class' => 'control-label col-md-4 col-sm-4 col-xs-12']) !!}
            <div class="col-md-8 col-sm-8 col-xs-12">
                <select class="select form-control show-tick" data-live-search="true" name="products[]" id="productsSelectList" multiple="multiple" title="Selecione os produtos" data-live-search="true">
                    @foreach ($products as $product)
                    <option value="{!! $product->id !!}" {{in_array($product->id, $list->products_id) ? "selected='selected' " : ""}}>{!! $product->nome !!}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group">
            <table class="table table-striped" id="info-list-product">
            </table>
        </div>

        <div class="form-group text-right">
            <div class="col-md-12 col-xs-12">
                <button type="submit" class="btn btn-fill btn-primary">
                    <i class="fa fa-edit"></i> {{$list->id ? "Editar pacote" : "Criar pacote"}}
                </button>
            </div>
        </div>

    </div>
</div>
{!! Form::close() !!}

<script type="application/javascript">
    var products = [];
    @foreach ($products as $product)
        products[{{$product->id}}] = "{!! $product->nome !!}";
    @endforeach
    var removeProductFromList = function(id) {
        id = id.toString()
        var selectElements = $("#productsSelectList").val()
        $('.tr-' + id).remove()
        selectElements.splice(selectElements.indexOf(id), 1)
        $("#productsSelectList").val(selectElements)
        $("#productsSelectList").selectpicker("refresh")
    }

    var loadProduct = function(productAdd) {
        $("#info-list-product").append(
            $('<tr>').append(
                $("<td>").text(products[productAdd])
            ).append(
                $("<td>").append(
                    '<a class="removeProductFromList btn btn-xs btn-danger btn-fill" onclick=removeProductFromList(' + productAdd + ')><i class="fa fa-trash"></i></a></td>')
            ).addClass("tr-"+productAdd)
        );
    }

    var checkIfProductsHasChanged = function(){
        var arrayProducts = $("#productsSelectList").val()
        $("#info-list-product").text("")
        for(var product in arrayProducts) {
            loadProduct(arrayProducts[product])
        }
    }

    $("#productsSelectList").selectpicker();
    $("#productsSelectList").on("change", checkIfProductsHasChanged);
    checkIfProductsHasChanged();
</script>