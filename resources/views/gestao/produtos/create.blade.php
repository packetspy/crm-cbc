    <div class="row">
        <div class="col-xs-12">

                    @if(isset($produto))
                        {!! Form::model($produto, ['action' => ['Crm\ProductController@postUpdateProduct', $produto->id], 'method' => 'post', 'class' => 'form-horizontal']) !!}
                    @else
                        {!! Form::open(['action' => 'Crm\ProductController@postSaveProduct', 'method' => 'post', 'class' => 'form-horizontal']) !!}
                    @endif

                    @if(isset($produto))
                    <div class="form-group{{ $errors->has('ativo') ? ' has-error' : '' }}">
                         {!! Form::label('ativo', 'Status', ['class' => 'control-label col-md-4 col-sm-4 col-xs-12']) !!}
                         <div class="col-md-8 col-sm-8 col-xs-12">
                             {!! Form::select('ativo', ['0' => 'Inativo', '1' => 'Ativo'], null, ['class' => 'form-control']) !!}
                             @if ($errors->has('ativo'))
                             <span class="help-block">
                               <strong>{{ $errors->first('ativo') }}</strong>
                             </span>
                             @endif
                         </div>
                    </div>
                    @endif


                    <div class="form-group{{ $errors->has('nome') ? ' has-error' : '' }}">
                        {!! Form::label('nome', 'Nome do Produto', ['class' => 'control-label col-md-4 col-sm-4 col-xs-12']) !!}
                        <div class="col-md-8 col-sm-8 col-xs-12">
                            {!! Form::text('nome', null, ['class' => 'form-control']) !!}
                            @if ($errors->has('nome'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('nome') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>


                    <div class="form-group{{ $errors->has('descricao') ? ' has-error' : '' }}">
                        {!! Form::label('descricao', 'Descrição do Produto', ['class' => 'control-label col-md-4 col-sm-4 col-xs-12']) !!}
                        <div class="col-md-8 col-sm-8 col-xs-12">
                            {!! Form::textarea('descricao', null, ['class' => 'form-control']) !!}
                            @if ($errors->has('descricao'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('descricao') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group text-right">
                        <div class="col-md-12 col-xs-12">
                            <button type="submit" class="btn btn-fill btn-primary">
                                @if (isset($produto))
                                    <i class="fa fa-edit"></i> Editar produto
                                @else
                                    <i class="fa fa-send"></i> Cadastrar produto
                                @endif
                            </button>
                        </div>
                    </div>


                    {!! Form::close() !!}
            </div>
    </div>
    <script>
        $(document).ready(function() {
            $('.money-input').mask('000.000.000.000.000,00', {reverse: true});
        });
    </script>
