<!-- Histórico de Cliente -->
<div role="tabpanel" class="tab-pane {!! (!isset($nav) || $nav == 'historico') ? 'active' : ''  !!}" id="historico_cliente">

        {{-- @if(!$pending) --}}
        <button type="button" class="btn btn-fill btn-info btn-sm" onClick="abreModal('{{ action('Crm\ClientController@getEscolheInteracao', $client->id) }}', 'Nova interação', 'modal_escolha');">
            <i class="fa fa-plus-square"></i> Nova interação
        </button>
        {{-- @else --}}
        <!--<div class="alert alert-danger">
            Você possui interações que devem ser respondidas.
        </div>-->
        {{-- @endif --}}


    <div class="clearfix margin-top"></div>

    @if(!$interactions->isEmpty())
    <div class="table-responsive">
        <table class="table table-bordered table-striped">
            <thead>
            <th class="text-center">#</th>
            <th class="col-md-2">Data</th>
            <th class="col-md-2">Autor</th>
            <th class="col-md-1">Interação anterior</th>
            <th class="col-md-3">Tipo de Interação</th>
            <th class="col-md-2">Título</th>
            <th class="col-md-2">Detalhes</th>
            </thead>
            <tbody>
            @foreach($interactions as $interaction)
                <tr>
                    <td>#{{ $interaction->id }}</td>
                    <td>{{ $interaction->data_criacao }}</td>
                    <td>{{ $interaction->author->name }}</td>
                    <td class="text-center">
                        @if($interaction->father_id)
                            <a href="#" onClick="return abreModal('{{ action('Crm\ClientController@getViewInteracao', [$client->id, $interaction->father_id]) }}', 'Visualizando interação')" class="text-info">
                                <i class="fa fa-eye"></i> #{!! $interaction->father_id !!}
                            </a>
                        @else
                            <small class="text-warning">Nenhuma</small>
                        @endif
                    </td>
                    <td>
                        <span class="label {!! $interaction->tipo_interacao_class !!}"> {!! $interaction->tipo_interacao !!}</span>
                        @if(!is_null($interaction->children))
                            <a href="#" class="label label-success" onClick="return abreModal('{{ action('Crm\ClientController@getViewInteracao', [$client->id, $interaction->children->id]) }}', 'Visualizando interação')">
                                <i class="fa fa-check-circle"></i> Ver resposta
                            </a>
                        @endif
                    </td>
                    <td>{{ substr($interaction->titulo, 0, 255) }}...</td>
                    <td>
                        <a href="#" onClick="return abreModal('{{ action('Crm\ClientController@getViewInteracao', [$client->id, $interaction->id]) }}', 'Visualizando interação')" class="btn btn-fill btn-primary btn-xs">
                            <i class="fa fa-eye"></i> Detalhes
                        </a>

                        @shield('interaction.move')
                        <a href="#" onClick="return abreModal('{!! action('Crm\ClientController@getMoveInteraction', [$client->id, $interaction->id]) !!}', 'Mover interação');" class="btn btn-fill btn-xs btn-warning">
                            <i class="fa fa-arrows-alt"></i> Mover
                        </a>
                        @endshield
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
          {!! $interactions->appends(array_merge(\Request::except(['page', 'nav']), ['nav' => 'historico']))->render() !!}
      </div>
        @else
        <div class="row">
          <div class="col-md-12">
            <div class="alert alert-warning">
              <i class="fa fa-info"></i>Nenhuma interação cadastrada.
            </div>
          </div>
        </div>
        <div class="clearfix margin-top"></div>
        @endif


</div>
