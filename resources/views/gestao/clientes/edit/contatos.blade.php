<!-- contatos -->
<div role="tabpanel" class="tab-pane {!! (!isset($nav) || $nav == 'contatos') ? 'active' : ''  !!}" id="contatos">

    <button id="contact-new" type="button" onclick="show_product()" class="btn btn-fill btn-info btn-sm">
        <i class="fa fa-plus-square"></i> Novo contato
    </button>

    <div class="clearfix margin-top"></div>

    <div id="contact-form">
        @include('gestao.clientes.contato.create')
    </div>

    @if(!$contatos == null)
    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        @foreach ($contatos as $contato)
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="heading{!! $contato["contact_id"] !!}">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{!! $contato["contact_id"] !!}" aria-expanded="false" aria-controls="collapse{!! $contato["contact_id"] !!}">
                        {{ $contato["nome"] }}
                    </a>
                </h4>
            </div>
            <div id="collapse{!! $contato["contact_id"] !!}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading{!! $contato["contact_id"] !!}">
                <div class="panel-body">

                    <div class="row">

                        <div class="col-md-12 text-right">
                          @is(['Consultores'])
                          <a href="#" onClick="loadContact('{!! action('Crm\ContactClientController@getEditContact', [$client->id, $contato["contact_id"]]) !!}', 'Editar contato');" class="btn btn-fill btn-xs btn-success {{ isResponsavel($client->responsavel_id) ? '' : 'hide' }}">
                              <i class="fa fa-close"></i> Editar
                          </a>
                          @endis

                          @is(['Atendimento', 'Gestores'])
                            <a href="#" onClick="return abreModal('{!! action('Crm\ContactClientController@getMoveContact', [$client->id, $contato["contact_id"]]) !!}', 'Mover contato');" class="btn btn-fill btn-xs btn-warning">
                                <i class="fa fa-arrows-alt"></i> Mover
                            </a>

                            <a href="#" onClick="loadContact('{!! action('Crm\ContactClientController@getEditContact', [$client->id, $contato["contact_id"]]) !!}', 'Editar contato');" class="btn btn-fill btn-xs btn-success">
                                <i class="fa fa-close"></i> Editar
                            </a>
                            <a href="#" onClick="return abreModal('{!! action('Crm\ContactClientController@getExcludeContact', [$client->id, $contato["contact_id"]]) !!}', 'Excluir contato');" class="btn btn-fill btn-xs btn-danger">
                                <i class="fa fa-close"></i> Excluir
                            </a>
                            @endis
                        </div>

                        <div class="col-md-3">
                            <p>
                                <label>ID (CRM):</label>
                                {!! $contato["contact_id"] !!}
                            </p>
                            <p>
                                <label>ID (Plataforma):</label>
                                {!! $contato["id_plataforma"] !!}
                            </p>
                        </div>

                        <div class="col-md-3">
                            <p>
                                <label>Nome</label>
                                {!! $contato["nome"] !!}
                            </p>
                            <p>
                                <label>Departamento</label>
                                {!! $contato['departamento'] !!}
                            </p>
                        </div>
                        <div class="col-md-3">
                           <p>
                               <label>Contatos</label>
                           </p>
                            <ul class="list-inline list-unstyled">
                              @if( ! empty($contato["contatos"]))
                                @foreach($contato["contatos"] as $item)
                                    <li>
                                        <strong>{!! ucfirst($item['tipo']) !!}: </strong>
                                        <span>{!! $item["contato"] !!}</span>
                                    </li>
                                @endforeach
                              @endif
                            </ul>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <p>
                                <label>Data de criação: </label>
                                {!! $contato["created_at"] !!}
                            </p>
                            <p>
                                <label>Origem: </label>
                                {!! $contato['origem'] !!}
                            </p>
                        </div>
                        <div class="col-md-4">

                        </div>
                    </div>
                    @if(count($contato['produtos']) > 0)
                    <div class="row">
                        <div class="col-md-12">
                            <h4>Produtos de Interesse</h4>
                        </div>
                        <div class="col-md-12">

                              <table class="table table-striped table-responsive table-bordered">
                                  <thead>
                                    <th>Produto</th>
                                    <th>Tipo</th>
                                    <th>Estados</th>
                                  </thead>
                                  <tbody>
                                    @foreach ($contato['produtos'] as $product)
                                       <tr>
                                           <td>{!! $product->product->nome !!}</td>
                                           <td>{!! $product->tipo_transacao !!}</td>
                                           <td>{!! $product->estados !!}</td>
                                       </tr>
                                    @endforeach
                                  </tbody>
                                </table>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
        @endforeach
    </div>
    @else
    <div class="row">
      <div class="col-md-12">
        <div class="alert alert-warning">
          <i class="fa fa-info"></i>Nenhum contato cadastrado.
        </div>
      </div>
    </div>
    <div class="clearfix margin-top"></div>
    @endif

</div>
