
<!-- Dados Pessoais -->
<div role="tabpanel" class="tab-pane  {!! (!isset($nav) || $nav == 'dados_pessoais') ? 'active' : ''  !!}" id="dados_pessoais">

    {!! Form::model($client, ['action' => ['Crm\ClientController@postUpdateClient', $client->id], 'method' => 'post', 'id' => 'form_dados']) !!}

      @is(['Atendimento', 'Gestores'])
        <input type="hidden" id="isResponsavel" name="isResponsavel" value="true">
      @else
        <input type="hidden" id="isResponsavel" name="isResponsavel" value="{{ isResponsavel($client->responsavel_id) ? 'true' : 'false' }}">
      @endis

        <div class="row">
          @shield('responsavel.edit')
            <div class="col-md-3">
              {!! Form::label('responsavel_id', 'Consultor Responsavel') !!}
              <select class="select show-tick form-control" id="responsavel_id" name="responsavel_id" title="Selecione Consultor..." data-live-search="true">
      						@foreach($consultores as $consultor)
      								<option value="{{ $consultor->id }}" {{ $client->responsavel_id == $consultor->id ? 'selected="selected"' : '' }}>{{ $consultor->name }}</option>
      						@endforeach
      				</select>
            </div>
          @else
            <div class="col-md-3">
              {!! Form::label('responsavel_id', 'Consultor Responsavel:') !!}<br>
              {{$client->responsavel ? $client->responsavel->name : 'Sem responsável atribuído' }}
            </div>
          @endshield

          @shield('status.edit')
            <div class="col-md-3">
              {!! Form::label('status_id', 'Situação/Status') !!}
              <select class="select show-tick form-control" id="status_id" name="status_id" title="Selecione ..." data-live-search="true" required>
      						@foreach($status as $status)
      								<option value="{{ $status->id }}" {{ $client->status_id == $status->id ? 'selected="selected"' : '' }}>{{ $status->status }}</option>
      						@endforeach
      				</select>
            </div>
          @else
          <div class="col-md-3">
            {!! Form::label('status_id', 'Situação/Status:') !!}<br>
            {{$client->status->status}}
          </div>
          @endshield

          @shield('idplataforma.edit')
            <div class="col-md-3">
              <div class="form-group {{ $errors->has('id_plataforma') ? ' has-error' : '' }}">
                  {!! Form::label('id_plataforma', 'ID na Plataforma') !!}
                  {!! Form::text('id_plataforma', null, ['class' => 'form-control'])  !!}
                  @if ($errors->has('id_plataforma'))
                      <span class="help-block">
                          <strong>{{ $errors->first('id_plataforma') }}</strong>
                      </span>
                  @endif
              </div>
            </div>
          @else
          <div class="col-md-3">
            {!! Form::label('status_id', 'ID Plataforma:') !!}<br>
            {{!$client->id_plataforma ? 'Não cadastrado' : $client->id_plataforma }}
          </div>
          @endshield

        </div>

        <div class="row">
            <div class="col-md-2">
                <div class="form-group {{ $errors->has('tipo_documento') ? ' has-error' : '' }}">
                    {!! Form::label('tipo_documento', 'Tipo de Documento') !!}
                    {!! Form::select('tipo_documento', ['0' => 'Selecione...', '1' => 'CPF', '2' => 'CNPJ'], null, ['class' => 'select show-tick form-control']) !!}
                    @if ($errors->has('tipo_documento'))
                        <span class="help-block">
                    <strong>{{ $errors->first('tipo_documento') }}</strong>
                </span>
                    @endif
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group {{ $errors->has('documento') ? ' has-error' : '' }}">
                    {!! Form::label('documento', 'CPF', ['id' => 'label-documento']) !!}
                    {!! Form::text('documento', null, ['class' => 'form-control inpt-documento']) !!}
                    @if ($errors->has('documento'))
                        <span class="help-block">
                    <strong>{{ $errors->first('documento') }}</strong>
                </span>
                    @endif
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group {{ $errors->has('empresa') ? ' has-error' : '' }}">
                    {!! Form::label('empresa', 'Empresa / Razão Social') !!}<span class="required">*</span>
                    {!! Form::text('empresa', null, ['class' => 'form-control', 'required' => 'required']) !!}
                    @if ($errors->has('empresa'))
                        <span class="help-block">
                            <strong>{{ $errors->first('empresa') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group {{ $errors->has('nome') ? ' has-error' : '' }}">
                    {!! Form::label('nome', 'Nome Fantasia') !!}
                    {!! Form::text('nome', null, ['class' => 'form-control']) !!}
                    @if ($errors->has('nome'))
                        <span class="help-block">
                            <strong>{{ $errors->first('nome') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
        </div>

        <div class="row">
          <div class="col-md-4">
            <div class="form-group {{ $errors->has('endereco') ? ' has-error' : '' }}">
                {!! Form::label('endereco', 'Endereço') !!} <a href="https://www.google.com.br/maps/place/{{$client->endereco}}+{{$client->cidade}}+{{$client->estado}}+{{$client->cep}}" target="_blank">(<i class="fa fa-auto fa-map-marker"></i> Ver no Google Maps)</a>
                {!! Form::text('endereco', null, ['class' => 'form-control']) !!}
                @if ($errors->has('endereco'))
                    <span class="help-block"><strong>{{ $errors->first('endereco') }}</strong></span>
                @endif
            </div>
          </div>
          <div class="col-md-2">
            <div class="form-group {{ $errors->has('complemento') ? ' has-error' : '' }}">
                {!! Form::label('complemento', 'Complemento') !!}
                {!! Form::text('complemento', null, ['class' => 'form-control']) !!}
                @if ($errors->has('complemento'))
                    <span class="help-block"><strong>{{ $errors->first('complemento') }}</strong></span>
                @endif
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group {{ $errors->has('cidade') ? ' has-error' : '' }}">
                {!! Form::label('cidade', 'Cidade') !!}
                {!! Form::text('cidade', null, ['class' => 'form-control']) !!}
                @if ($errors->has('cidade'))
                    <span class="help-block"><strong>{{ $errors->first('cidade') }}</strong></span>
                @endif
            </div>
          </div>
          <div class="col-md-1">
            <div class="form-group {{ $errors->has('estado') ? ' has-error' : '' }}">
                {!! Form::label('estado', 'Estado') !!}
                <?php $estados = App\Helpers\Estado::getAll(); ?>
                <select class="select form-control show-tick" name="estado" id="estado" title="Selecione o estado" data-live-search="true">
                    @foreach ($estados as $key => $value)
                      @if($key == $client->estado)
                        <option selected value="{!! $key !!}">{!! $key !!}</option>
                      @else
                       <option value="{!! $key !!}">{!! $key !!}</option>
                      @endif
                    @endforeach
                    @if ($errors->has('estado'))
                        <span class="help-block"><strong>{{ $errors->first('estado') }}</strong></span>
                    @endif
                </select>
            </div>
          </div>
          <div class="col-md-2">
            <div class="form-group {{ $errors->has('cep') ? ' has-error' : '' }}">
                {!! Form::label('cep', 'CEP') !!}
                {!! Form::text('cep', null, ['class' => 'form-control']) !!}
                @if ($errors->has('cep'))
                    <span class="help-block"><strong>{{ $errors->first('cep') }}</strong></span>
                @endif
            </div>
          </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group {{ $errors->has('segmento') ? ' has-error' : '' }}">
                    {!! Form::label('segmento', 'Segmento') !!}
                    {!! Form::text('segmento', null, ['class' => 'form-control']) !!}
                    @if ($errors->has('segmento'))
                        <span class="help-block">
                    <strong>{{ $errors->first('segmento') }}</strong>
                </span>
                    @endif
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group {{ $errors->has('site') ? ' has-error' : '' }}">
                    {!! Form::label('site', 'Site') !!}
                    {!! Form::text('site', null, ['class' => 'form-control']) !!}
                    @if ($errors->has('site'))
                        <span class="help-block">
                    <strong>{{ $errors->first('site') }}</strong>
                </span>
                    @endif
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 text-right">
                <button type="submit" class="btn btn-fill btn-primary">
                    <i class="fa fa-send"></i> Atualizar dados
                </button>
            </div>
        </div>

    {!! Form::close() !!}

</div>
