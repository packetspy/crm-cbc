
<!-- relevancia -->
<div role="tabpanel" class="tab-pane  {!! (!isset($nav) || $nav == 'relevancia_cliente') ? 'active' : ''  !!}" id="relevancia_cliente">

    @if(!count($relevance) == null)

    <p>* Informações <b>acumuladas</b> a cada última atualização.</p>

        <table class="table table-responsive" style="margin-top: 1em;">
            <thead>
                <th>Indicações</th>
                <th>Contraindicações</th>
                <th>Negócios</th>
                <th>Data de atualização</th>
            </thead>
            <tbody>
            @foreach ($relevance as $rl)
                <tr>
                    <td>{{$rl->indicacoes}}</td>
                    <td>{{$rl->contraindicacoes}}</td>
                    <td>{{$rl->negocios}}</td>
                    <td>{{$rl->datacriacao}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>

        <div class="clearfix margin-top"></div>
        @else
        <div class="row">
          <div class="col-md-12">
            <div class="alert alert-warning">
              <i class="fa fa-info"></i>Nenhuma informação sobre relevância deste cliente.
            </div>
          </div>
        </div>
        <div class="clearfix margin-top"></div>
        @endif
</div>
