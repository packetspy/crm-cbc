@extends('layouts.base')

@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h4 class="title">Gestão de Clientes</h4>
                    <p class="category">Clientes cadastrados</p>
                </div>
                <div class="panel-body">
                    <ul class="nav navbar-nav" style="width: 100%">
                        <li>
                            <a href="{{ action('Crm\ClientController@getCreate') }}" class="btn btn-fill btn-info btn-sm">
                                <i class="fa fa-user-plus"></i> Cadastrar cliente
                            </a>
                        </li>

                        @if (!isset($is_trash_page) || $is_trash_page == false)
                        <li class="pull-right" style="margin-left: 20px">
                            <a href="{{ action('Crm\ClientController@getClientsInTrash') }}" class="btn btn-fill btn-warning btn-sm">
                                <i class="fa fa-trash"></i> Empresas inativas {!! isset($clients_in_trash) ? '(' . $clients_in_trash .')' : '' !!}
                            </a>
                        </li>
                        @endif

                        <li class="pull-right">
                            <a href="{{ action('Crm\ExportController@getClients') }}" class="btn btn-fill btn-default btn-sm">
                                <i class="fa fa-download"></i> Exportar clientes
                            </a>
                        </li>
                    </ul>

                    <!-- Row -->
                    <table class="table table-bordered table-responsive" style="margin-top: 6em;">
                        <thead>
                            <th># <a href="{!! action('Crm\ClientController@getIndex',  array_merge(\Request::except(['orderBy', 'order']), ['orderBy' => 'id'])) !!}" class="pull-right"><i class="fa fa-sort-numeric-asc"></i></a></th>
                            <th>Tipo</th>
                            <th>Documento</th>
                            <th>
                                <a href="{!! action('Crm\ClientController@getIndex', array_merge(\Request::except(['orderBy', 'order']), ['orderBy' => 'empresa', 'order' => 'DESC'])) !!}" class="pull-right"><i class="fa fa-sort-alpha-desc"></i></a>
                                Empresa
                                <a href="{!! action('Crm\ClientController@getIndex', array_merge(\Request::except(['orderBy', 'order']), ['orderBy' => 'empresa'])) !!}" class="pull-right"><i class="fa fa-sort-alpha-asc"></i></a>
                            </th>
                            <th>Responsável</th>
                            <th>Ações</th>
                        </thead>
                        <tbody>
                        @foreach ($clients as $client)
                            <tr>
                                <td>#{{ $client->id }}</td>
                                <td>{{ $client->tipo_pessoa_short }}</td>
                                <td>{{ $client->documento }}</td>
                                <td><a href="{{ url('cliente/resumo', $client->id) }}">{{ $client->empresa }}</a></td>
                                <td>@if(isset($client->responsavel)) {{ $client->responsavel->name }} @endif</td>
                                <td>
                                  <div style="padding:12px 0;">
                                    <a href="{{ url('cliente/resumo', $client->id) }}" class="btn btn-fill btn-primary btn-xs" style="float:left;">
                                        <i class="fa fa-edit"></i> Ver
                                    </a>

                                    @is(['Atendimento','Gestores'])

                                        @if(isset($client->responsavel->name))

                                        <button style="float:left;" type="button" onclick="selecionaConsultor({{ $client->id }},{{ $client->responsavel->id }});" class="btn btn-fill btn-xs btn-success">
                                            <i class="fa fa-refresh"></i> Alterar Consultor
                                        </button>

                                        @else

                                          <button style="float:left;" type="button" onclick="selecionaConsultor({{ $client->id }});" class="btn btn-fill btn-xs btn-success">
                                              <i class="fa fa-user-plus"></i> Selecionar Consultor
                                          </button>

                                        @endif
                                    @endis
                                  </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                    {!! $clients->appends(\Request::except('page'))->render() !!}

                </div>
            </div>
        </div>
    </div>
@endsection

<script>
  function selecionaConsultor(client_id, responsavel_id)
  {
    if(responsavel_id === undefined)
    {
      responsavel_id = '';
    }

    var action_url = '{!! url('clientes/selecionar-responsavel/') !!}';
    var url = action_url + '/' + client_id + '/' + responsavel_id;

      return BootstrapDialog.show({
          title: 'Seleciona Consultor',
          message: $('<div></div>').load(url),
          buttons: [{
              label: 'Salvar',
              action: function(dialogRef){
                  $('#changeClientResponsible').submit();
                  // window.location.href = '{!! url()->full() !!}';
                  return false;
                  dialogRef.close();
              }
          }, {
              label: 'Fechar',
              action: function(dialogRef){
                  dialogRef.close();
              }
          }]
      });
  }
</script>

@section('scripts')

@endsection
