@extends('layouts.base')
@section('content')
    <input type="hidden" name="location_intern"/>
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">

            <div class="card">
                <div class="content">
                    <label>RAZÃO SOCIAL</label>
                    <p>{!! $client->empresa !!}</p>

                    <label>NOME FANTASIA</label>
                    <p>{!! $client->nome !!}</p>
                </div>
            </div>
            <div class="card">
                <div class="content content-full-width">

                    <?php
                    $nav = \Session::has('nav') ? \Session::get('nav') : 'dados_pessoais';
                    ?>
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" {!! (isset($nav) && $nav == 'dados_pessoais') ? 'class="active"' : ''  !!}><a href="#dados_pessoais" aria-controls="dados_pessoais" role="tab" data-toggle="tab"><i class="fa fa-pencil"></i> Dados Gerais</a></li>
                        <li role="presentation" {!! (isset($nav) && $nav == 'contatos') ? 'class="active"' : ''  !!}><a href="#contatos" aria-controls="contatos" role="tab" data-toggle="tab"><i class="fa fa-users"></i>Contatos</a></li>
                        <li role="presentation" {!! (isset($nav) && $nav == 'historico') ? 'class="active"' : ''  !!}><a href="#historico_cliente" aria-controls="historico_cliente" role="tab" data-toggle="tab"><i class="fa fa-history"></i>Histórico</a></li>
                        @shield('payment.index')
                        <li role="presentation" {!! (isset($nav) && $nav == 'financeiro') ? 'class="active"' : ''  !!}><a href="#financeiro_cliente" aria-controls="financeiro_cliente" role="tab" data-toggle="tab"><i class="fa fa-dollar"></i>Financeiro</a></li>
                        @endshield
                        <li role="presentation" {!! (isset($nav) && $nav == 'relevancia') ? 'class="active"' : ''  !!}><a href="#relevancia_cliente" aria-controls="relevancia_cliente" role="tab" data-toggle="tab"><i class="fa fa-flash"></i>Relevância</a></li>
                    </ul>

                    <div class="content">
                        <!-- Tab panes -->
                        <div class="tab-content">
                            @include('gestao.clientes.edit.dados')
                            @include('gestao.clientes.edit.contatos')
                            @include('gestao.clientes.edit.historico')
                            @shield('payment.index')
                              @include('gestao.clientes.edit.financeiro')
                            @endshield
                            @include('gestao.clientes.edit.relevancia')
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $.fn.modal.Constructor.prototype.enforceFocus = function() {};
    </script>
    <script src="{{ asset('lib/js/client.form.js') }}"></script>
    <script>
        var showProductForm = function()
        {

            $("#productInput").show();
        }
    </script>
@endsection
