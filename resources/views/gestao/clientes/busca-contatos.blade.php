@extends('layouts.base')

@section('content')
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="card">
        <div class="header">
          <h4 class="title">Contatos</h4>
          <p class="category">Buscar contatos</p>
        </div>
        <div class="panel-body" style="overflow-x: scroll;
        overflow-y: auto;">
        {{-- <ul class="nav navbar-nav" style="width: 100%">
          <li>
            <a href="{{ action('Crm\ClientController@getIndex') }}" class="btn btn-fill btn-info btn-sm">
              <i class="fa fa-chevron-left"></i> Voltar para lista de clientes
            </a>
          </li>
        </ul> --}}

        <div class="table-responsive">
          <!-- Row -->
          <table class="table table-bordered">
            <thead>
              <th>#</th>
              <th>Tipo</th>
              <th style="min-width:145px;">Documento</th>
              <th>Nome</th>
              <th>E-mail</th>
              <th style="min-width:130px;">Telefone</th>
              <th style="min-width:130px;">Celular</th>
            </thead>
            <tbody>

              {{-- {{ dd($contacts) }} --}}

              @forelse ($contacts as $contact)

                <tr>
                  <td>{!! $contact['contact_id'] !!}</td>
                  <td>
                    @if($contact['client']['tipo_documento'] == 1)
                      PF
                    @else
                      PJ
                    @endif
                  </td>
                  <td>{!! $contact['client']['documento'] !!}</td>
                  <td>{!! $contact['client']['nome'] !!}</td>
                  <td>{!! $contact['client']['email'] !!}</td>
                  <td>{!! $contact['client']['telefone'] !!}</td>
                  <td>{!! $contact['client']['celular'] !!}</td>
                </tr>
              @empty

                <tr>
                  <td colspan="7">
                    <p class="text-danger">
                      Nenhum resultado para a sua busca. Tente novamente.
                    </p>
                  </td>
                </tr>
              @endforelse
            </tbody>
          </table>

        </div>

      </div>
    </div>
  </div>
</div>
@endsection

@section('scripts')

@endsection
