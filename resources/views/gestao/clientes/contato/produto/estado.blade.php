
<div class="col-md-3">
    {!! Form::label('estados') !!}
    <?php $estados = App\Helpers\Estado::get(); ?>
    <select class="select form-control show-tick" name="estados{{isset($tipo) ? $tipo : ''}}[]" id="estado{{isset($tipo) ? $tipo : ''}}Select" multiple="multiple" title="Selecione um ou mais estados" data-live-search="true">
        @foreach ($estados as $key => $value)
            <option value="{!! $key !!}">{!! $value !!}</option>
        @endforeach
    </select>
</div>
