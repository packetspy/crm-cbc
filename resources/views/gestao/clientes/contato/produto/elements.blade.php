<div class="row">
    <div class="col-md-12 text-left">
        <h4>Adicionar produtos individuais ou um grupo de produtos</h4>
          {!! Form::label('Produto ou Grupo:') !!}
          <label class="btn btn-fill btn-md btn-default" onclick="show_product()">Produto Individual</label>
          <label class="btn btn-fill btn-md btn-default" onclick="show_package()">Grupo de Produtos</label>
    </div>
</div>

@if($packages->count() != 0)
    <div class="row package-show">
        <div class="col-md-2">
            {!! Form::label('Tipo de transação') !!}
            {!! Form::select('tipo_pacote', ['' => 'Selecione ...', 'venda' => 'Venda', 'compra' => 'Compra', 'venda_compra' => 'Venda e Compra'], null, ['id' => 'packageType', 'class' => 'select show-tick form-control']) !!}
        </div>

        <div class="col-md-3">
            {!! Form::label('Grupo') !!}
            <select class="select show-tick form-control" id="packageSelect" name="package" title="Selecione um grupo" data-live-search="true">
                @foreach($packages as $package)
                <option value="{!! $package->id !!}" data-products-id="{{implode(',', $package->products_id)}}">{!! $package->name !!}</option>
                @endforeach
            </select>
        </div>
        @include('gestao.clientes.contato.produto.estado', ['tipo' => 'pacote'])

        <div class="col-md-2 text-right">
          {!! Form::label('.') !!}
            <button type="button" class="btn btn-fill btn-default btn-success" id="adicionarPacote">
                <i class="fa fa-plus-square"></i> Adicionar produtos do Grupo
            </button>
        </div>
    </div>
@endif

<div class="row product-show">
    <div class="col-md-2">
        {!! Form::label('Tipo de transação') !!}
        {!! Form::select('tipo_produto', ['' => 'Selecione ...', 'venda' => 'Venda', 'compra' => 'Compra', 'venda_compra' => 'Venda e Compra'], null, ['id' => 'productType', 'class' => 'select show-tick form-control']) !!}
    </div>
    <div class="col-md-3">
        {!! Form::label('Produto') !!}
        <select class="select show-tick form-control" id="productSelect" name="produto_id" title="Selecione um produto" data-live-search="true">
            @foreach($products as $product)
                <option value="{!! $product->id !!}">{!! $product->nome !!}</option>
            @endforeach
        </select>
    </div>
    @include('gestao.clientes.contato.produto.estado', ['tipo' =>''])

    <div class="col-md-2 text-right">
      {!! Form::label('.') !!}
        <button type="button" class="btn btn-fill btn-default btn-info" id="adicionarProduto">
            <i class="fa fa-plus-square"></i> Adicionar produto
        </button>
    </div>
</div>
