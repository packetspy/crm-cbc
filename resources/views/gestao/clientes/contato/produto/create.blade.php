
<div id="productInput">
    @include('gestao.clientes.contato.produto.elements')
</div>
<div class="row">
  <div class="col-md-12">
    <div class="content table-responsive table-full-width">
        <table id="productList" class="table table-hover table-striped">
            <th>Tipo</th>
            <th>Produto</th>
            <th>Estados</th>
            <th>Ação</th>
            <tbody>
                @if(isset($contact) && $contact->intermediate_products->count() > 0)
                    @foreach($contact->intermediate_products as $product)
                        <tr id="{!! $product->id !!}">
                            <td>
                                {!! strtoupper($product->transacao) !!}
                            </td>
                            <td>
                                {!! $product->product->nome !!}
                            </td>
                            <td>
                                {!! $product->estado !!}
                            </td>
                            <td>
                               <a href="#" class="btn btn-fill btn-xs btn-danger removeProduct" data-id="{!! $product->id !!}" data-product-id="{!! $product->product_id !!}" data-db="true" data-contact-id="{!! $product->contact_id !!}" data-transaction="{!! $product->transacao !!}" data-state="{!! $product->estado !!}">
                                   <i class="fa fa-trash"></i>
                               </a>
                            </td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    </div>
</div>
</div>

<script>
    var url_remove = '{!! action('Crm\ContactClientController@postRemoveProductFromContact') !!}';
    var products = []
    @foreach ($products as $product)
         products[{{$product->id}}] = "{!! $product->nome !!}";
    @endforeach
</script>
