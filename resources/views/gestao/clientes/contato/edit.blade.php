{!! Form::model($contact, ['action' => ['Crm\ContactClientController@putUpdateContact', $contact->id], 'method' => 'put', 'id' => 'contactForm', 'class' => 'form-horizontal']) !!}
{!! Form::text('id', $contact->id, ['class' => 'hidden'])!!}
@include('gestao.clientes.contato.elements.form')
{!! Form::close() !!}
@include('gestao.clientes.contato.elements.script')