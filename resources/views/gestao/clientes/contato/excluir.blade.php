{!! Form::open(['action' => ['Crm\ContactClientController@postExcludeContact', $contact->id], 'method' => 'post']) !!}

    <div class="form-group col-sm-12">
        <p>Você está prestes a excluir este contato. Clique no botão abaixo para continuar</p>
        <button type="submit" class="btn btn-fill btn-primary">
            <i class="fa fa-trash"></i> Sim, desejo excluir o contato.
        </button>
    </div>

{!! Form::close() !!}