
{!! Form::open(['action' => ['Crm\ContactClientController@postCreateContact', $client->id], 'method' => 'post', 'class' => 'form-horizontal', 'id' => 'contactForm']) !!}
@include('gestao.clientes.contato.elements.form')
{!! Form::close() !!}
@include('gestao.clientes.contato.elements.script')
