<script>
    var formSelect = '{!! Form::select('tipo[]', ['email' => 'E-mail', 'telefone' => 'Telefone', 'skype' => 'Skype', 'site' => 'Site'], null, ['class' => 'form-control slct-tipo']) !!}';
    var formInput = '{!! Form::text('valor[]', null, ['class' => 'form-control input-contact']) !!}';
</script>
<script type='text/javascript' src="{{ asset('lib/js/jquery.min.js') }}"></script>
<script type='text/javascript' src="{{ asset('lib/js/bootstrap-select.min.js') }}"></script>
<script type='text/javascript' src="{{ asset('theme/assets/js/bootstrap-notify.js') }}"></script>
<script type='text/javascript' src="{{ asset('lib/js/utils.js') }}"></script>
<script type='text/javascript' src="{{ asset('lib/js/contato.js') }}"></script>
<script type='text/javascript' src="{{ asset('lib/js/contact-product.js') }}"></script>
