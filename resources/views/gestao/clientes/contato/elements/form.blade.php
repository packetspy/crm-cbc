<div class="alerta">
    <div class="alert alert-danger" style="display: none" id="erro_input">Preencha ao menos um contato!</div>
</div>

<fieldset>
  <legend>Informações do contato</legend>
  <div class="row">
      <div class="col-md-6">
          {!! Form::label('nome', '') !!}*
          {!! Form::text('nome', null, ['class' => 'form-control', 'required' => 'required']) !!}
      </div>
      <div class="col-md-6">
          {!! Form::label('Departamento') !!}
          {!! Form::text('departamento', null, ['class' => 'form-control']) !!}
      </div>
  </div>
</fieldset>

<div class="clearfix"></div>

<fieldset>
  <legend>Formas de contato | <a href="#" id="more-contact-forms" class="linkonlegend"><i class="fa fa-plus-square"></i> Adicionar mais formas de contatos</a></legend>

  <div class="row contact-form-append">
    <div class="col-sm-12"><!-- Input new fields here --></div>
  </div>

  <div class="contact-form-elements-hide" style="display:none;">
    <div class="row" >
        <div class="col-sm-4">
            {{ Form::select('tipo[]', ['email' => 'E-mail', 'telefone' => 'Telefone', 'skype' => 'Skype', 'site' => 'Site'], '', ['class' => 'form-control slct-tipo']) }}
        </div>
        <div class="col-sm-6">
            {{ Form::hidden('contact_info_id[]', 0, ['class' => 'contact-info-id'])}}
            {{ Form::text('valor[]', '', ['class' => 'form-control input-contact']) }}
        </div>
        <div class="col-sm-2">
            <a href="#" class="remove-contact remove_field btn btn-xs btn-fill btn-danger" onclick="removerContatoHtml(this);"><i class="fa fa-close"></i></a>
        </div>
    </div>
  </div>
  @include('gestao.clientes.contato.elements.type_contact')
</fieldset>

<div class="clearfix"></div>
<fieldset>
  <legend>Produtos de interesse do contato</legend>
  @include('gestao.clientes.contato.produto.create')
</fieldset>

<div class="form-group col-sm-12 col-md-12 text-right">
  <button type="button" class="btn btn-fill btn-warning" onClick="neverMind()">
      <i class="fa fa-close"></i> Agora não =/
   </button>
    <button type="submit" class="btn btn-fill btn-primary" id="checaContato">
        <i class="fa fa-send"></i> Salvar contato
     </button>
</div>

<div class="clearfix"></div>
