<div class="fields_wrap">
    @if(isset($elementos))
        @foreach($elementos as $elemento)
            <div class="form-repeat" data-type="contact_repeat">
                <div class="row">
                  <div class="col-sm-4">
                      {{ Form::select('tipo[]', ['email' => 'E-mail', 'telefone' => 'Telefone', 'skype' => 'Skype', 'site' => 'Site'], $elemento['tipo'], ['class' => 'form-control slct-tipo']) }}
                  </div>
                  <div class="col-sm-6">
                      {{ Form::hidden('contact_info_id[]', $elemento['id'], ['class' => 'contact-info-id'])}}
                      {{ Form::text('valor[]', $elemento['contato'], ['class' => 'form-control input-contact']) }}
                  </div>
                  <div class="col-sm-2">
                      <a href="#" class="remove_field btn btn-xs btn-fill btn-danger"><i class="fa fa-close"></i></a>
                  </div>
                </div>
            </div>
        @endforeach
    @endif
</div>
