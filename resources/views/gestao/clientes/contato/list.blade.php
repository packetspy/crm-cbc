<h4>Cliente: {!! $cliente->nome !!}</h4>


<div class="panel-group" id="accordion_contact_list" role="tablist" aria-multiselectable="true" style="max-height: 500px; overflow-y:scroll">

    @foreach ($contatos as $contato)
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="heading_collapse_contact_list{!! $contato["contact_id"] !!}">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse_contact_list{!! $contato["contact_id"] !!}" aria-expanded="false" aria-controls="collapse_contact_list{!! $contato["contact_id"] !!}">
                        {{ $contato["nome"] }}
                    </a>
                </h4>
            </div>
            <div id="collapse_contact_list{!! $contato["contact_id"] !!}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_collapse_contact_list{!! $contato["contact_id"] !!}">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            <p>
                                <label>Nome</label>
                                {!! $contato["nome"] !!}
                            </p>
                            <p>
                                <label>Departamento</label>
                                {!! $contato['departamento'] !!}
                            </p>
                        </div>
                        <div class="col-md-6">
                            <p>
                                <label>Contatos</label>
                            </p>
                            <ul class="list-inline list-unstyled">
                                @foreach($contato["contatos"] as $item)
                                    <li>
                                        <strong>{!! ucfirst($item['tipo']) !!}: </strong>
                                        <span>{!! $item["contato"] !!}</span>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>

                    @if(count($contato['produtos']) > 0)
                        <div class="row">
                            <div class="col-md-12">
                                <h4>Produtos de Interesse</h4>
                            </div>
                            <div class="col-md-12">

                                <table class="table table-striped table-responsive table-bordered">
                                    <thead>
                                    <th>Produto</th>
                                    <th>Tipo</th>
                                    <th>Estados</th>
                                    </thead>
                                    <tbody>
                                    @foreach ($contato['produtos'] as $product)
                                        <tr>
                                            <td>{!! $product->product->nome !!}</td>
                                            <td>{!! $product->tipo_transacao !!}</td>
                                            <td>{!! $product->estados !!}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    @endforeach
</div>
