@extends('emails.layout')
@section('content')
<center style="width: 100%; background: #ffffff;">
    <!-- Visually Hidden Preheader Text : BEGIN -->
    <div style="display:none;font-size:1px;line-height:1px;max-height:0px;max-width:0px;opacity:0;overflow:hidden;mso-hide:all;font-family: sans-serif;">
        Base CBC Negócios gerada automaticamente
    </div>
    <!-- Visually Hidden Preheader Text : END -->
    <!--
        Set the email width. Defined in two places:
        1. max-width for all clients except Desktop Windows Outlook, allowing the email to squish on narrow but never go wider than 680px.
        2. MSO tags for Desktop Windows Outlook enforce a 680px width.
    -->
    <div style="max-width: 600px; margin: auto;">
        <!--[if (gte mso 9)|(IE)]>
        <table cellspacing="0" cellpadding="0" border="0" width="580" align="center">
            <tr>
                <td>
        <![endif]-->
        <!-- Email Header : BEGIN -->
        <table cellspacing="0" cellpadding="0" border="0" align="left" width="100%" style="max-width: 600px;">
            <tr>
                <td style="padding: 20px 0; text-align: left; background-color:#797979">
                    <a href="https://www.cbcnegocios.com.br" target="_blank"><img src="https://cdn.cbcnegocios.com.br/emailstransacionais/v3/images/logo-cbcnegocios-xs.png" width="200" height="50" alt="CBC Negócios - Central Brasileira de Comercialização" border="0" /></a>
                </td>
            </tr>
        </table>
        <!-- Email Header : END -->
        <!-- Email Body : BEGIN -->
        <table cellspacing="0" cellpadding="0" border="0" align="center" bgcolor="#ffffff" width="100%" style="max-width: 600px;">
            <!-- 1 Column Text + Button : BEGIN -->
            <tr>
                <td>
                    <table cellspacing="0" cellpadding="0" border="0" width="100%">
                        <tr>
                            <td style="padding:20px; text-align: left; font-family: sans-serif; font-size: 25px; mso-height-rule: exactly; line-height: 25px; color: #555555;">{{$data['subject']}}
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 10px; text-align: left; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555;">
                                <p>
                                  {{$data['to-name']}}, <br>
                                  Em anexo, arquivo referente a {{$data['subject']}} <br>
                                </p>
                                <p>
                                  <b>**Este e-mail foi gerado automaticamente atráves do CRM - CBC Negócios</b><br>
                                </p>

                                Atenciosamente,<br>
                                Equipe de TI

                                <br />
                                <!-- Button : END -->
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <!-- 2 Even Columns : END -->
        </table>
        <!-- Email Body : END -->
        <!-- Email Footer : BEGIN -->
        <table cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="max-width: 600px;">
            <tr>
                <td style="padding: 40px 10px;width: 100%;font-size: 12px; font-family: sans-serif; mso-height-rule: exactly; line-height:18px; text-align: center; color: #888888;">
                    <p>
                        <a target="_blank" href="http://goo.gl/81R2AZ"><img src="https://cdn.cbcnegocios.com.br/emailstransacionais/v3/images/icon_link.png" alt="Linkedin" /></a>
                        <a target="_blank" href="http://goo.gl/bimZL1"><img src="https://cdn.cbcnegocios.com.br/emailstransacionais/v3/images/icon_fb.png" alt="Facebook" /></a>
                        <a target="_blank" href="http://goo.gl/NdcD8I"><img src="https://cdn.cbcnegocios.com.br/emailstransacionais/v3/images/icon_yt.png" alt="youtube" /></a>
                        <a target="_blank" href="http://goo.gl/jMBym1"><img src="https://cdn.cbcnegocios.com.br/emailstransacionais/v3/images/icon_in.png" alt="Instagram" /></a>
                        <a target="_blank" href="http://goo.gl/xmvxlM"><img src="https://cdn.cbcnegocios.com.br/emailstransacionais/v3/images/icon_tw.png" alt="Twitter" /></a>
                        <br />
                        <a target="_blank" href="http://www.cbcnegocios.com.br" style="color:#76787a;text-decoration:none;">www.cbcnegocios.com.br</a>
                    </p>

                    <img src="https://cdn.cbcnegocios.com.br/emailstransacionais/v3/images/linha.png" width="600" alt="" />
                    <center><span style="color:#dbdbdb; font-size:11px;">E-mail gerado por: http://crm.cbcnegocios.com.br</span></center>

                </td>
            </tr>
        </table>
        <!-- Email Footer : END -->
        <!--[if (gte mso 9)|(IE)]>
        </td>
        </tr>
        </table>
        <![endif]-->
    </div>
</center>
@endsection
