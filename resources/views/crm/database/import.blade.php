@extends('layouts.base')
@section('content')

<style>
    .box-buttons{
        display: flex;
        justify-content: space-between;
        padding-bottom: 10px;
    }
</style>

<div class="row box-card-contatos">
  <div class="col-md-4 col-sm-6 col-xs-12 ">
    <div class="card card-contatos">
      <div class="header"><h4 class="title">Sincronizar Clientes</h4></div>
      <div class="content">
        <p>Executa a leitura do arquivo indicado, busca pela aba <b>"Empresas"</b> e atualiza os clientes do CRM que tenha o número de ID PLATAFORMA.</p>
        <form action="{{url('database/importar/clientes')}}" method="post" enctype="multipart/form-data">
          <input type="hidden" name="_method" value="POST">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <div class="form-group">
              <input type="file" name="arquivo" required="true"/>
          </div>
          <div class="form-group">
              <button type="submit" class="btn btn-sm btn-fill btn-info ">Enviar</button>
          </div>
        </form>
      </div>
    </div>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12">
    <div class="card card-contatos">
      <div class="header"><h4 class="title">Importar Relevância</h4></div>
      <div class="content">
        <p>Adiciona no banco ações do cliente na plataforma, indicações, contraindicações e negócios. Nenhuma dado é sobrescrito ou apagado, apenas adicionado</p>
        <form action="{{url('database/importar/relevancia')}}" method="post" enctype="multipart/form-data">
          <input type="hidden" name="_method" value="POST">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <div class="form-group">
              <input type="file" name="arquivo" required="true"/>
          </div>
          <div class="form-group">
              <button type="submit" class="btn btn-sm btn-fill btn-info ">Enviar</button>
          </div>
        </form>
      </div>
    </div>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12">
    <div class="card card-contatos">
      <div class="header"><h4 class="title">Importar logins</h4></div>
      <div class="content">
        <p>Importa e acumula ultimo login do cliente na plataforma.</p>
        <form action="{{url('database/importar/login')}}" method="post" enctype="multipart/form-data">
          <input type="hidden" name="_method" value="POST">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <div class="form-group">
              <input type="file" name="arquivo" required="true"/>
          </div>
          <div class="form-group">
              <button type="submit" class="btn btn-sm btn-fill btn-info ">Enviar</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h4 class="title">Gestão de Importações</h4>
            </div>
            <div class="panel-body">
                <div class="row box-buttons">
                    <div class="col-md-3 importar">
                        <a data-href="{{url('database/importar/clientes')}}" class="btn btn-fill btn-info btn-sm" data-name="Importação de clientes">
                            <i class="fa fa-download"></i> Importar clientes
                        </a>
                    </div>
                    <div class="col-md-3 importar">
                        <a data-href="{{url('database/importar/contatos')}}" class="btn btn-fill btn-info btn-sm" data-name="Importação de contatos">
                            <i class="fa fa-user-plus"></i> Importar contatos
                        </a>
                    </div>
                    <div class="col-md-3 importar">
                        <a data-href="{{url('database/importar/produtos')}}" class="btn btn-fill btn-info btn-sm" data-name="Importação de produtos">
                            <i class="fa fa-download"></i> Importar produtos
                        </a>
                    </div>
                    <div class="col-md-3 importar">
                      <a data-href="{{url('database/importar/relevancia')}}" class="btn btn-fill btn-info btn-sm" data-name="Importação de relevância">
                          <i class="fa fa-flash"></i> Importar relevância
                      </a>
                    </div>
                    <div class="col-md-3 importar">
                      <a data-href="{{url('database/importar/origem-lead')}}" class="btn btn-fill btn-info btn-sm" data-name="Importar Origem MKT">
                          <i class="fa fa-flash"></i> Importar Origem MKT
                      </a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')

@endsection
