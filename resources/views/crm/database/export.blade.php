@extends('layouts.base')
@section('content')

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h4 class="title">Gestão de Exportações</h4>
            </div>
            <div class="content">
                @shield('export.marketing')
                  @include('crm.database.export-marketing')
                @endshield
                @shield('export.financeiro')
                  @include('crm.database.export-financeiro')
                @endshield
                @shield('export.gestao')
                  @include('crm.database.export-gestao')
                @endshield
              </div>
            </div>
        </div>
    </div>
</div>
@endsection
