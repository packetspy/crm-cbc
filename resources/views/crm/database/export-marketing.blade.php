
<div class="row">
  <div class="col-md-4 col-sm-6 col-xs-12 ">
    <div class="card card-contatos">
      <div class="header"><h4 class="title">Exportar Clientes</h4></div>
      <div class="content">
        <p>Exportar todos os Clientes da plataforma.</p>
        <a href="{{ url('database/exportar/clientes') }}" class="btn btn-fill btn-info btn-sm"><i class="fa fa-download"></i> Exportar clientes</a>
      </div>
    </div>
  </div> <!-- .col -->

  <div class="col-md-4 col-sm-6 col-xs-12 ">
    <div class="card card-contatos">
      <div class="header"><h4 class="title">Exportar Contatos (Padrão IBM)</h4></div>
      <div class="content">
        <p>Exportar todos os Contatos da plataforma no padrão de importação do IBM Watson.</p>
        <a href="{{ url('database/exportar/contatos') }}" class="btn btn-fill btn-info btn-sm"><i class="fa fa-download"></i> Exportar contatos</a>
      </div>
    </div>
  </div><!-- .col -->

  <div class="col-md-4 col-sm-6 col-xs-12 ">
    <div class="card card-contatos">
      <div class="header"><h4 class="title">Exportar Leads</h4></div>
      <div class="content">
        <p>Exportar Leads por período.</p>
          <form class="form-inline" action="{{url('database/exportar/leads')}}" method="post" enctype="multipart/form-data">
            <input type="hidden" name="_method" value="POST">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-group">
              <input class="daterange" type="text" name="data_inicial" id="data_inicial" required="true" placeholder="Data inicial"/>
              <input class="daterange" type="text" name="data_final" id="data_final" required="true" placeholder="Data final"/>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-sm btn-fill btn-info">Exportar</button>
            </div>
          </form>
      </div>
    </div>
  </div><!-- .col -->

</div><!-- .row -->
