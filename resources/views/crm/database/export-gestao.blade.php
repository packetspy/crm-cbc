
<div class="row">
  <div class="col-md-4 col-sm-6 col-xs-12 ">
    <div class="card card-contatos">
      <div class="header"><h4 class="title">Clientes + Ultima Interação</h4></div>
      <div class="content">
        <p>Exporta todos os clientes e a ultima interação realizada pelo consultor.</p>
        <a href="{{ url('database/exportar/ultimas-interacoes') }}" class="btn btn-fill btn-info btn-sm"><i class="fa fa-download"></i> Exportar</a>
      </div>
    </div>
  </div><!-- .col -->
</div> <!-- .row -->
