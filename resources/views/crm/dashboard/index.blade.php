@extends('layouts.base')

@section('content')

@is(['Hábito', 'Liquidez', 'Gestores', 'Financeiro'])
<div class="row">
  <div class="col-sm-12">
    <div class="card">
      <div class="header">
        <h4 class="title">Calendário</h4>
        <p class="category">Gestão de contatos</p>
      </div>
      <div class="content">
        <div id="calendar"></div>
      </div>
    </div>
  </div>
</div>
@endis
@endsection

@section('scripts')
    <script>
        function detalhaItemCalendario(item)
        {
            var action_url = '{!! url("interacao/visualizar") !!}';
            var url = action_url + '/' + item.client_id + '/' + item.id;

            return BootstrapDialog.show({
                title: 'Visualizando item',
                message: $('<div></div>').load(url),
                buttons: [{
                    label: 'Fechar',
                    cssClass: 'btn btn-fill pull-right',
                    action: function(dialogRef){
                        dialogRef.close();
                    }
                }]
            });
        }

        $(document).ready(function() {
            $('#calendar').fullCalendar({
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,basicWeek,basicDay'
                },
                defaultDate: '{!! date('Y-m-d') !!}',
                defaultView: 'basicWeek',
                validRange: {
                    start: '2017-05-08'
                },
                editable: false,
                eventLimit: false, // allow "more" link when too many events
                loading: function(bool) {
                  if (bool){
                    $('#main-loader').show();
                    document.getElementById("main-loader").className = "";
                  }else{
                    $('#main-loader').hide();
                    document.getElementById("main-loader").className = "hide";
                  }
                },
                events:  '{!! $getContatos  !!}',
                eventClick:  function(event, jsEvent, view) {
                    //set the values and open the modal
                    jsEvent.preventDefault();
                    return detalhaItemCalendario(event);
                }
            });
        });
    </script>
@endsection
