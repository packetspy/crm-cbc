<form role="form" method="POST" action="{{ url ('cliente/produtos') }}" accept-charset="UTF-8">
  <input type="hidden" name="_method" value="POST">
  <input type="hidden" name="_token" value="{{ csrf_token() }}">
  <input type="hidden" name="cliente_id" value="{{$clienteInfo['cliente_id']}}">
  <input type="hidden" name="contato_id" value="{{$contato->id}}">

    <div class="row">
      <div class="col-md-4">
          <label for="tipo_operacao">Tipo de transação</label>
          <select class="form-control select show-tick " id="tipo_operacao" name="tipo_operacao" title="Selecione ..." data-live-search="false" required="true">
            <option value="compra">Compra</option>
            <option value="venda">Venda</option>
            <option value="compra_venda">Compra e Venda</option>
          </select>
      </div>

      <div class="col-md-4">
          <label for="produto_id">Produto</label>
          <select class="form-control select show-tick " id="produtos_selecionados[]" name="produtos_selecionados[]" multiple="multiple" title="Selecione um produto" data-live-search="true" required="true">
              @foreach($produtos as $produto)
                  <option value="{!! $produto->id !!}">{!! $produto->nome !!}</option>
              @endforeach
          </select>
      </div>

      <div class="col-md-4">
        <label for="estados">Estados</label>
        <select class="form-control select show-tick" name="estados[]" id="estados[]" multiple="multiple" title="Selecione um ou mais estados" data-live-search="true" required="true">
            <option value="todos">[Todos estados]</option>
            @foreach ($estados as $key => $value)
                <option value="{!! $key !!}">{!! $value !!}</option>
            @endforeach
        </select>
      </div>

    </div>
    <div class="row">
      <div class="col-md-12">
          <button type="submit" class="btn btn-fill btn-default btn-info pull-right">
              <i class="fa fa-plus-square"></i> Adicionar grupo de produtos
          </button>
      </div>
    </div>
</form>
