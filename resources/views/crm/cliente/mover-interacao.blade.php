{!! Form::model($interaction, ['action' => ['Crm\ClientController@putMoveInteraction'], 'method' => 'put', 'id' => 'notifyMsg', 'class' => 'form-horizontal moveInteractionForm']) !!}
{!! Form::text('interaction_id', $interaction->id, ['class' => 'hidden'])!!}
{!! Form::text('actualclient_id', $interaction->client_id, ['class' => 'hidden'])!!}

<form role="form" id="formdelete" class="disableonclick form-horizontal" method="POST" action="{{ url ('cliente/interacao/mover/'.$interaction->id) }}" accept-charset="UTF-8">
  <input type="hidden" name="_method" value="PUT">
  <input type="hidden" name="_token" value="{{ csrf_token() }}">
  <input type="hidden" name="interaction_id" value="{{$interaction->id}}">
  <input type="hidden" name="actualclient_id" value="{{$interaction->client_id}}">

<div class="alerta">
    <div class="alert alert-danger" style="display: none" id="erro_input">Preencha ao menos um cliente!</div>
</div>

<div class="row">
  <div class="col-xs-12">
    <div class="form-group">
        <b>ID:</b> {{$interaction->id}} <br>
        <b>Observação:</b> {{ substr($interaction->observacao, 0, 100) }} ...<br>
        <b>Data:</b> {{$interaction->data_contato}}
    </div>
  </div>

<div class="col-xs-12">
<div class="form-group">
    {!! Form::label('Mover para:') !!}
    <select class="form-control select" id="client_id" name="client_id" title="Escolha uma empresa para mover o contato" data-live-search="true" data-size="10" data-width="95%">
        @foreach($clients as $client)
            <option value="{!! $client->id !!}">{!! $client->id !!} | {{ str_limit($client->empresa, 55) }} </option>
        @endforeach
    </select>
</div>
</div>
</div>

<div class="clearfix"></div>

<div class="form-group col-sm-12 col-md-12 text-right">
    <button type="submit" class="btn btn-fill btn-primary">
        <i class="fa fa-send"></i> Mover
     </button>
</div>

<div class="clearfix"></div>

</form>

<script>
$(document).ready(function() {
    $('.select').selectpicker({
      //nothing todo here
    });
});
</script>
