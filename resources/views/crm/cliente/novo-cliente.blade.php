@extends('layouts.base')
@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">
            <div class="card">
                <div class="header">
                    <h4 class="title">Cadastrar cliente</h4>
                    <p class="category">Preencha os campos</p>
                </div>
                <div class="content">
                    <form role="form" method="POST" action="{{ url ('cliente') }}" accept-charset="UTF-8">
                    <input type="hidden" name="_method" value="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="tipo" value="cliente">
                    <input type="hidden" name="responsavel_id" id="responsavel_id" value="{{Auth::user()->id}}">
                    <input type="hidden" name="status_id" id="status_id" value="10">
                    <input type="hidden" name="id_plataforma" id="id_plataforma" value="">

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group {{ $errors->has('empresa') ? ' has-error' : '' }}">
                                <label for="empresa">Empresa / Razão Social</label><span class="required">*</span>
                                <input type="text" class="form-control" id="empresa" name="empresa" value="">
                                @if ($errors->has('empresa'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('empresa') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group {{ $errors->has('nome') ? ' has-error' : '' }}">
                                <label for="nome">Nome Fantasia</label>
                                <input type="text" class="form-control" id="nome" name="nome" value="">
                                @if ($errors->has('nome'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('nome') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="row">
                      <div class="col-md-3">
                          <div class="form-group {{ $errors->has('tipo_documento') ? ' has-error' : '' }}">
                              <label for="">Tipo de Empresa</label>
                              <select class="form-control select show-tick" name="tipoempresa_id" id="tipoempresa_id" title="Selecione o tipo de empresa">
                                  @foreach ($tipoempresa as $tipo)
                                     <option value="{!! $tipo->id !!}">{!! $tipo->tipo !!}</option>
                                  @endforeach
                                  @if ($errors->has('tipoempresa_id'))
                                      <span class="help-block"><strong>{{ $errors->first('tipoempresa_id') }}</strong></span>
                                  @endif
                              </select>
                          </div>
                      </div>
                      <div class="col-md-3">
                          <div class="form-group {{ $errors->has('tipo_documento') ? ' has-error' : '' }}">
                              <label for="">Tipo de Documento</label>
                              <select class="form-control select show-tick" name="tipo_documento" id="tipo_documento" title="Selecione o tipo de documento">
                                  @foreach ($tipodocumento as $key => $value)
                                     <option value="{!! $key !!}">{!! $value !!}</option>
                                  @endforeach
                                  @if ($errors->has('tipo_documento'))
                                      <span class="help-block"><strong>{{ $errors->first('tipo_documento') }}</strong></span>
                                  @endif
                              </select>
                          </div>
                      </div>
                        <div class="col-md-3">
                            <div class="form-group {{ $errors->has('documento') ? ' has-error' : '' }}">
                                <label for="documento">CPF/CNPJ:</label>
                                <input type="text" class="form-control" id="documento" name="documento" value="">
                                @if ($errors->has('documento'))
                                    <span class="help-block">
                                <strong>{{ $errors->first('documento') }}</strong>
                            </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group {{ $errors->has('ie') ? ' has-error' : '' }}">
                                <label for="empresa">Inscrição Estadual</label>
                                <input type="text" class="form-control" id="ie" name="ie" value="">
                                @if ($errors->has('ie'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('ie') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="row">
                      <div class="col-md-4">
                        <div class="form-group {{ $errors->has('endereco') ? ' has-error' : '' }}">
                            {!! Form::label('endereco', 'Endereço') !!}
                            {!! Form::text('endereco', null, ['class' => 'form-control']) !!}
                            @if ($errors->has('endereco'))
                                <span class="help-block"><strong>{{ $errors->first('endereco') }}</strong></span>
                            @endif
                        </div>
                      </div>
                      <div class="col-md-2">
                        <div class="form-group {{ $errors->has('complemento') ? ' has-error' : '' }}">
                            {!! Form::label('complemento', 'Complemento') !!}
                            {!! Form::text('complemento', null, ['class' => 'form-control']) !!}
                            @if ($errors->has('complemento'))
                                <span class="help-block"><strong>{{ $errors->first('complemento') }}</strong></span>
                            @endif
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group {{ $errors->has('cidade') ? ' has-error' : '' }}">
                            {!! Form::label('cidade', 'Cidade') !!}
                            {!! Form::text('cidade', null, ['class' => 'form-control']) !!}
                            @if ($errors->has('cidade'))
                                <span class="help-block"><strong>{{ $errors->first('cidade') }}</strong></span>
                            @endif
                        </div>
                      </div>
                      <div class="col-md-1">
                        <div class="form-group {{ $errors->has('estado') ? ' has-error' : '' }}">
                            {!! Form::label('estado', 'Estado') !!}
                            <?php $estados = App\Helpers\Estado::getAll(); ?>
                            <select class="select form-control show-tick" name="estado" id="estado" title="Selecione o estado" data-live-search="true">
                                @foreach ($estados as $key => $value)
                                   <option value="{!! $key !!}">{!! $key !!}</option>
                                @endforeach
                                @if ($errors->has('estado'))
                                    <span class="help-block"><strong>{{ $errors->first('estado') }}</strong></span>
                                @endif
                            </select>
                        </div>
                      </div>
                      <div class="col-md-2">
                        <div class="form-group {{ $errors->has('cep') ? ' has-error' : '' }}">
                            {!! Form::label('cep', 'CEP') !!}
                            {!! Form::text('cep', null, ['class' => 'form-control']) !!}
                            @if ($errors->has('cep'))
                                <span class="help-block"><strong>{{ $errors->first('cep') }}</strong></span>
                            @endif
                        </div>
                      </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group {{ $errors->has('segmento') ? ' has-error' : '' }}">
                                {!! Form::label('segmento', 'Segmento') !!}
                                {!! Form::text('segmento', null, ['class' => 'form-control']) !!}
                                @if ($errors->has('segmento'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('segmento') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group {{ $errors->has('site') ? ' has-error' : '' }}">
                                {!! Form::label('site', 'Site') !!}
                                {!! Form::text('site', null, ['class' => 'form-control']) !!}
                                @if ($errors->has('site'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('site') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 text-right">
                            <button type="submit" class="btn btn-fill btn-success">
                                <i class="fa fa-plus"></i> Adicionar
                            </button>
                        </div>
                    </div>
                  </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('lib/js/client.form.js') }}"></script>
@endsection
