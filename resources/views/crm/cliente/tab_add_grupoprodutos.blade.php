<form role="form" method="POST" action="{{ url ('cliente/produtos') }}" accept-charset="UTF-8">

    <div class="row">
      <div class="col-md-4">
          <label for="tipo_produto">Tipo de transação</label>
          <select class="form-control select show-tick " id="tipo_produto" name="tipo_produto" data-live-search="false">
            <option value="1">Compra</option>
            <option value="2">Venda</option>
            <option value="3">Compra e Venda</option>
          </select>
      </div>

      <div class="col-md-4">
          <label for="grupo_id">Grupo de produtos</label>
          <select class="select show-tick form-control" id="grupo_id" name="grupo_id" title="Selecione um grupo" data-live-search="true">
              @foreach($grupoprodutos as $grupo)
                <option value="{!! $grupo->id !!}" data-products-id="{{implode(',', $grupo->products_id)}}">{!! $grupo->name !!}</option>
              @endforeach
          </select>
      </div>

      <div class="col-md-4">
        <label for="estados">Estados</label>
        <select class="form-control select show-tick" name="estados[]" id="estados" multiple="multiple" title="Selecione um ou mais estados" data-live-search="true">
            @foreach ($estados as $key => $value)
                <option value="{!! $key !!}">{!! $value !!}</option>
            @endforeach
        </select>
      </div>

    </div>
    <div class="row">
      <div class="col-md-12">
          <button type="button" class="btn btn-fill btn-default btn-success pull-right">
              <i class="fa fa-plus-square"></i> Adicionar produto
          </button>
      </div>
    </div>
</form>
