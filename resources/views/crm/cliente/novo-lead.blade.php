@extends('layouts.base')
@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">
            <div class="card">
                <div class="header">
                    <h4 class="title">Cadastrar Lead</h4>
                    <p class="category">Preencha os campos para cadastrar o lead</p>
                </div>
                <div class="content">
                  <form role="form" method="POST" class="form-horizontal" action="{{ url ('cliente') }}" accept-charset="UTF-8">
                  <input type="hidden" name="_method" value="POST">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <input type="hidden" name="tipo" value="lead">

                  <div class="form-group">
                    <label for="nome" class="col-sm-2 control-label">Nome</label>
                    <div class="col-sm-6">
                      <input type="text" class="form-control" name="nome" id="nome" placeholder="Nome" required="true">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="email" class="col-sm-2 control-label">E-mail</label>
                    <div class="col-sm-6">
                      <input type="email" class="form-control" name="email" id="email" placeholder="E-mail" required="true">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="telefone" class="col-sm-2 control-label">Telefone</label>
                    <div class="col-sm-6">
                      <input type="text" class="form-control" name="telefone" id="telefone" placeholder="Telefone">
                    </div>
                  </div>
                    <div class="row">
                        <div class="col-md-6 text-right">
                            <button type="submit" class="btn btn-fill btn-success">
                                <i class="fa fa-plus"></i> Adicionar
                            </button>
                        </div>
                    </div>

                  </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('lib/js/client.form.js') }}"></script>
@endsection
