@extends('layouts.base')
@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">

            <div class="card">
                <div class="content content-full-width">
                    @include('crm.cliente.tabs')
                    <div class="content">
                        <!-- Tab panes -->
                        <div class="tab-content">
                          <div class="tab-panel active" role="tabpanel" >

                            <div class="row">
                              <div class="col-md-12 col-xs-12 col-sm-12">
                                @include('crm.cliente.info-general')
                              </div>
                            </div>

                              @if(!$relevancia->isEmpty())
                              <p>*Informações <b>acumuladas</b> a cada última atualização</p>
                              <div class="table-responsive">
                                  <table class="table table-striped">
                                      <thead>
                                      <th class="col-md-2">Indicações</th>
                                      <th class="col-md-1">Contraindicações</th>
                                      <th class="col-md-1">Negócios</th>
                                      <th class="col-md-1">Data de atualização</th>
                                      </thead>
                                      <tbody>
                                      @foreach($relevancia as $linharelevancia)
                                          <tr>
                                              <td>{{ $linharelevancia->indicacoes }}</td>
                                              <td>{{ $linharelevancia->contraindicacoes }}</td>
                                              <td>{{ $linharelevancia->negocios }}</td>
                                              <td>{{ $linharelevancia->datacriacao }}</td>
                                          </tr>
                                      @endforeach
                                      </tbody>
                                  </table>
                                  </div>
                                  <div class="clearfix margin-top"></div>
                                  @else
                                  <div class="row">
                                    <div class="col-md-12">
                                      <div class="alert alert-warning">
                                        <i class="fa fa-info"></i>Nenhuma atividade deste cliente.
                                      </div>
                                    </div>
                                  </div>
                                  <div class="clearfix margin-top"></div>
                                  @endif



                          </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $.fn.modal.Constructor.prototype.enforceFocus = function() {};
    </script>
    <script src="{{ asset('lib/js/client.form.js') }}"></script>
    <script>
        var showProductForm = function()
        {

            $("#productInput").show();
        }
    </script>
@endsection
