@extends('layouts.base')
@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">

            <div class="card">
                <div class="content content-full-width">
                    @include('crm.cliente.tabs')
                    <div class="content">
                        <!-- Tab panes -->
                        <div class="tab-content">
                          <div class="tab-panel active" role="tabpanel" >

                            <div class="row">
                              <div class="col-md-12 col-xs-12 col-sm-12">
                                @include('crm.cliente.info-general')
                              </div>
                            </div>

                            <div class="row">
                              <div class="col-md-6">
                                <div class="card">
                                    <div class="header">
                                        <h4 class="title">Endereço</h4>
                                    </div>
                                    <div class="content">
                                      <label>Endereço:</label> {{$cliente->endereco}}</br>
                                      <label>Complemento:</label> {{$cliente->complemento}}</br>
                                      <label>Bairro:</label> {{$cliente->bairro}}</br>
                                      <label>Cidade:</label> {{$cliente->cidade}}</br>
                                      <label>Estado:</label> {{$cliente->estado}}</br>
                                      <label>CEP:</label> {{$cliente->cep}}</br>
                                    </div>
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="card">
                                    <div class="header">
                                        <h4 class="title">Outras informações</h4>
                                    </div>
                                    <div class="content">
                                      <label>CNPJ/CPF:</label> {{$cliente->documento}}</br>
                                      <label>Inscrição Estadual:</label> {{$cliente->ie}}</br>
                                      <label>ID Plataforma:</label> {{$cliente->id_plataforma}}</br>
                                      <label>Tipo de Documento:</label> {{$cliente->tipo_pessoa_short}}</br>
                                      <label>Segmento:</label> {{$cliente->segmento}}</br>
                                      <label>Site:</label> {{$cliente->site ? $cliente->site : ''}}
                                    </div>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-12">
                                <div class="card">
                                    <div class="header">
                                        <h4 class="title">Contatos</h4>
                                    </div>
                                    <div class="content">
                                        <div class="panel-group" id="accordion">
                                          @if($contatos->count() != 0)
                                            @foreach ($contatos as $contato)
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-target="#panel{{$contato->id}}" href="javascript:void(0)" data-toggle="collapse" class="{{ $contato->id == session('contato_id') ? '' : 'collapsed' }}" aria-expanded="{{ $contato->id == session('contato_id') ? 'true' : 'false' }}">
                                                            {{ $contato->nome }} {{$contato->departamento ? "($contato->departamento)" : ''}}
                                                            <b class="caret"></b>
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="panel{{$contato->id}}" class="panel-collapse {{ $contato->id == session('contato_id') ? 'collapse in' : 'collapse' }}" aria-expanded="{{ $contato->id == session('contato_id') ? 'true' : 'false' }}" {{ $contato->id == session('contato_id') ? '' : 'style="height: 0px;"' }}>
                                                    <div class="panel-body">
                                                      <div class="row">
                                                        <div class="col-md-4">
                                                          <label>Nome:</label> {{ $contato['nome'] }} </br>
                                                          <label>Departamento:</label> {{ $contato["departamento"] }}
                                                        </div>
                                                        <div class="col-md-4">
                                                          <ul class="list-unstyled">
                                                            @if( ! empty($contato->email))
                                                              @foreach($contato->email as $email)
                                                                  <li>
                                                                      <a href="mailto:{{$email->contato}}" target="_blank">{!! $email->contato !!}</a> <button type="button" class="btn btn-default btn-fill btn-xs clipboard {{ $email->contato == '' ? 'hide' : '' }}" data-clipboard-text="{{$email->contato}}" data-toggle="tooltip" data-placement="top" title="Copiar!" data-delay='{"show":"5", "hide":"5"}'><i class="fa fa-clone"></i></button>
                                                                  </li>
                                                              @endforeach
                                                            @endif
                                                          </ul>
                                                        </div>
                                                        <div class="col-md-4">
                                                          <ul class="list-inline list-unstyled">
                                                            @if( ! empty($contato->telefone))
                                                              @foreach($contato->telefone as $telefone)
                                                                  <li>{!! $telefone->contato !!}</li>
                                                              @endforeach
                                                            @endif
                                                          </ul>
                                                        </div>
                                                      </div>
                                                      <div class="row">
                                                        <div class="col-lg-12">
                                                          <h4>Produtos de interesse</h4>
                                                          @if($contato->intermediate_products->count() != 0)
                                                          <table class="table table-responsive" style="margin-top: 1em;">
                                                              <thead>
                                                                  <th>#</th>
                                                                  <th>Tipo de Operação</th>
                                                                  <th>Produto</th>
                                                                  <th>Estados</th>
                                                              </thead>
                                                              <tbody>
                                                              @foreach ($contato->intermediate_products as $produto)
                                                                  <tr id="tr_p{{$produto->contact_id}}-{{$produto->product_id}}-{{$produto->transacao}}">
                                                                      <td>#</td>
                                                                      <td>{{ $produto->transacao }}</td>
                                                                      <td>{{ $produto->product->nome }}</td>
                                                                      <td>{{ $produto->estados }}</td>
                                                                  </tr>
                                                              @endforeach
                                                              </tbody>
                                                          </table>
                                                          @else
                                                            <spam class="label label-warning">Nenhum produto de interesse cadastrado.</spam>
                                                          @endif
                                                        </div>
                                                      </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @endforeach
                                          @else
                                          <p>Nenhum contato cadastrado!</p>
                                          @endif
                                        </div>
                                    </div>

                                </div>
                              </div>
                            </div>


                          </div> <!-- .tab-pane -->
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $.fn.modal.Constructor.prototype.enforceFocus = function() {};
    </script>
    <script src="{{ asset('lib/js/client.form.js') }}"></script>
    <script>
        var showProductForm = function()
        {

            $("#productInput").show();
        }
    </script>
@endsection
