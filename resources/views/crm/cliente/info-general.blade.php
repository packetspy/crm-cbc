<div class="card">
  <div class="row">
    <div class="col-md-8">
      <div class="header">
        <h4 class="title">Informações do cliente</h4>
      </div>
      <div class="content">
        <label>ID CRM:</label> {!! $clienteInfo['cliente_id'] !!} | <label>ID PLATAFORMA:</label> {{ $clienteInfo['id_plataforma'] ? $clienteInfo['id_plataforma'] : "" }} </br>
        <label>RAZÃO SOCIAL:</label> {!! $clienteInfo['empresa'] !!}</br>
        <label>NOME FANTASIA:</label> {!! $clienteInfo['nome'] !!}</br>
        <label>RESPONSAVEL:</label> {!! $clienteInfo['responsavel'] !!} | {!! $clienteInfo['escritorio_nome'] !!}</br>
        <label>STATUS:</label> <span class="label label-default">{!! $clienteInfo['status'] !!}</span></br>
        <label>ORIGEM:</label> <span class="label label-default">{!! $clienteInfo['origem'] !!}</span></br>
        <label>CADASTRO PLATAFORMA:</label> <span class="label label-default">{!! $clienteInfo['datacadastro_plataforma'] !!}</span>
      </div>
    </div>
    <div class="col-md-4">
      <div class="row">
        <div class="col-md-12">
          <div class="header">
            <h4 class="title">Último login do cliente</h4>
          </div>
          <div class="content">
            {!! checkLoginTime($clienteInfo['login']['ultimo_login']) !!}
          </div>
        </div>
      </div>
      @shield('client.destroy')
      <div class="row">
        <div class="col-md-12">
          <div class="header">
            <h4 class="title">Excluir!?! Tem certeza?</h4>
          </div>
          <div class="content">
            <form role="form" id="formdeletecliente{{$clienteInfo['cliente_id']}}" class="disableonclick form-delete" method="POST" action="{{ url ('cliente/'.$clienteInfo['cliente_id']) }}" accept-charset="UTF-8">
              <input type="hidden" name="_method" value="DELETE">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <input type="hidden" name="cliente_id" value="{{$clienteInfo['cliente_id']}}">
              <button type="submit" class="btn btn-fill btn-xs btn-danger disableonclick sure" data-type="cliente"  data-form="#formdeletecliente{{$clienteInfo['cliente_id']}}" data-titledelete="{!! trans('cliente.titledelete') !!}" data-textdelete="{!! trans('cliente.textdelete') !!}" data-titlesuccess="{!! trans('cliente.titledelete_success') !!}" >
                  <i class="fa fa-trash-o"></i> Excluir Cliente
              </button>
            </form>
          </div>
        </div>
      </div>
      @endshield
    </div>
  </div>
</div>
