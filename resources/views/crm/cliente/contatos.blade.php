@extends('layouts.base')
@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">

            <div class="card">
                <div class="content content-full-width">
                    @include('crm.cliente.tabs')
                    <div class="content">
                        <!-- Tab panes -->
                        <div class="tab-content">
                          <div class="tab-panel active" role="tabpanel" >

                            <div class="row">
                              <div class="col-md-12 col-xs-12 col-sm-12">
                                @include('crm.cliente.info-general')
                              </div>
                            </div>

                            <div class="row">
                              <div class="col-md-12">
                                <div class="card">
                                    <div class="header">
                                        <h4 class="title">Contatos</h4>
                                    </div>
                                    <div class="content">
                                        <div class="panel-group" id="accordion">
                                          @if($contatos->count() != 0)
                                            @foreach ($contatos as $contato)
                                                <div class="panel panel-default">
                                                  <div class="panel-heading">
                                                      <h4 class="panel-title">
                                                          <a data-target="#panel{{$contato->id}}" href="javascript:void(0)" data-toggle="collapse" class="{{ $contato->id == session('contato_id') ? '' : 'collapsed' }}" aria-expanded="{{ $contato->id == session('contato_id') ? 'true' : 'false' }}">
                                                              {{ $contato->nome }} {{$contato->departamento ? "($contato->departamento)" : ''}}
                                                              <b class="caret"></b>
                                                          </a>
                                                      </h4>
                                                  </div>
                                                <div id="panel{{$contato->id}}" class="panel-collapse {{ $contato->id == session('contato_id') ? 'collapse in' : 'collapse' }}" aria-expanded="{{ $contato->id == session('contato_id') ? 'true' : 'false' }}" {{ $contato->id == session('contato_id') ? '' : 'style="height: 0px;"' }}>
                                                    <div class="panel-body">
                                                      <div class="row">
                                                        <div class="col-lg-4">
                                                          <div class="card">
                                                            <div class="header"><h4 class="title">Dados do contato</h4></div>
                                                            <div class="content">
                                                            <form role="form" method="POST" action="{{ url ('cliente/contatos/'.$contato->id) }}" accept-charset="UTF-8">
  																														<input type="hidden" name="_method" value="PUT">
  																														<input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                              <input type="hidden" name="cliente_id" value="{{$clienteInfo['cliente_id']}}">

                                                              <div class="row">
                                                                <div class="col-lg-6"><b>ID:</b> {{$contato->id}}</div>
                                                                <div class="col-lg-6"><b>ID Plataforma:</b> {{$contato->id_plataforma}}</div>
                                                              </div>
                                                              <div class="row">
                                                                <div class="col-lg-12"><b>Nome Plataforma:</b> {{$contato->nome_plataforma}}</div>
                                                              </div>
                                                              <div class="row">
                                                                <div class="col-lg-12"><b>Perfil Plataforma:</b> {{$contato->perfil_plataforma}}</div>
                                                              </div>
                                                              <div class="row">
                                                                <div class="col-lg-12"><b>Origem:</b> {{$contato->origem->origem}}</div>
                                                              </div>
                                                              <div class="row">
                                                                <div class="col-lg-12"><b>Criado em:</b> {{$contato->datacriacao}}</div>
                                                              </div>

                                                              <div class="form-group">
                                                                  <label>Nome</label>
                                                                  <input type="text" class="form-control" id="nome" name="nome" value="{{$contato->nome}}" required="true">
                                                              </div>
                                                              <div class="form-group">
                                                                  <label>Departamento</label>
                                                                  <input type="text" class="form-control" id="departamento" name="departamento" value="{{$contato->departamento}}">
                                                              </div>

                                                              @shield('origem.edit')
                                                                <div class="form-group">
                                                                  <label for="origem_id">Origem:</label>
                                                                  <select class="select show-tick form-control" id="origem_id" name="origem_id" title="Selecione ..." data-live-search="true">
                                                                      @foreach($origens as $origem)
                                                                          <option value="{{ $origem->id }}" {{ $contato->origem_id == $origem->id ? 'selected="selected"' : '' }}>{{ $origem->origem }}</option>
                                                                      @endforeach
                                                                  </select>
                                                                </div>
                                                              @else
                                                                <div class="col-md-3">
                                                                  <label>Origem:</label><br>
                                                                  <input type="hidden" id="origem_id" name="origem_id" value="{{$contato->origem_id}}">
                                                                  {{$contato->origem->origem}}
                                                                </div>
                                                              @endshield

                                                              <div class="row">
                                                                  <div class="col-md-12 text-center">
                                                                      <button type="submit" class="btn btn-fill btn-primary">
                                                                          <i class="fa fa-send"></i> Atualizar contato
                                                                      </button>
                                                                  </div>
                                                              </div>
                                                            </form>
                                                            </div>
                                                          </div>
                                                        </div>

                                                        <div class="col-lg-4">
                                                          <div class="card card-contatos">
                                                            <div class="header"><h4 class="title">Meios de contato</h4></div>
                                                            <div class="content">
                                                              @if($contato->email->count() != 0)
                                                                <ul class="list-unstyled">
                                                                @foreach($contato->email as $email)
                                                                <li id="li{{$email->id}}"><button type="button" class="btn btn-danger btn-xs btn-fill delete" data-id="{{$email->id}}" data-token="{{csrf_token()}}" data-type="email" data-titledelete="{!! trans('email.titledelete') !!}" data-textdelete="{!! trans('email.textdelete') !!}" data-titlesuccess="{!! trans('email.titlesuccess') !!}" data-url="cliente/formas-de-contato"><i class="fa fa-trash-o"></i></button> <a href="mailto:{{$email->contato}}" target="_blank">{{$email->contato}}</a></li>
                                                                @endforeach
                                                              </ul>
                                                              @else
                                                                <spam class="label label-warning">Nenhum E-mail cadastrado.</spam>
                                                              @endif
                                                              @if($contato->telefone->count() != 0)
                                                                <ul class="list-unstyled">
                                                                @foreach($contato->telefone as $telefone)
                                                                <li id="li{{$telefone->id}}"><button type="button" class="btn btn-danger btn-xs btn-fill delete" data-id="{{$telefone->id}}" data-token="{{csrf_token()}}" data-type="telefone" data-titledelete="{!! trans('telefone.titledelete') !!}" data-textdelete="{!! trans('telefone.textdelete') !!}" data-titlesuccess="{!! trans('telefone.titlesuccess') !!}" data-url="cliente/formas-de-contato"><i class="fa fa-trash-o"></i></button> {{$telefone->contato}}</li>
                                                                @endforeach
                                                              </ul>
                                                              @else
                                                                <spam class="label label-warning">Nenhum Telefone cadastrado.</spam>
                                                              @endif
                                                              @if($contato->skype->count() != 0)
                                                                <ul class="list-unstyled">
                                                                @foreach($contato->skype as $skype)
                                                                <li id="li{{$skype->id}}"><button type="button" class="btn btn-danger btn-xs btn-fill delete" data-id="{{$skype->id}}" data-token="{{csrf_token()}}" data-type="skype" data-titledelete="{!! trans('skype.titledelete') !!}" data-textdelete="{!! trans('skype.textdelete') !!}" data-titlesuccess="{!! trans('skype.titlesuccess') !!}" data-url="cliente/formas-de-contato"><i class="fa fa-trash-o"></i></button> {{$skype->contato}}</li>
                                                                @endforeach
                                                                </ul>
                                                              @else
                                                                <spam class="label label-warning">Nenhum Skype cadastrado.</spam>
                                                              @endif
                                                            </div>
                                                          </div>
                                                        </div>

                                                        <div class="col-lg-4">
                                                          <div class="card card-contatos">
                                                            <div class="header"><h4 class="title">Adicionar meio de contato</h4></div>
                                                            <div class="content">
                                                            <form role="form" class="disableonclick" method="POST" action="{{ url ('cliente/formas-de-contato/') }}" accept-charset="UTF-8">
  																														<input type="hidden" name="_method" value="POST">
  																														<input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                              <input type="hidden" name="cliente_id" value="{{$clienteInfo['cliente_id']}}">
                                                              <input type="hidden" name="contato_id" value="{{$contato->id}}">

                                                              <div class="form-group">
                                                                <label for="tipocontato">Tipo de contato:</label>
                                                                <select class="select show-tick form-control" id="tipocontato" name="tipocontato" title="Selecione ..." data-live-search="false" required="true">
                                                                    <option value="email">E-mail</option>
                                                                    <option value="telefone">Telefone</option>
                                                                    <option value="skype">Skype</option>
                                                                </select>
                                                              </div>
                                                              <div class="form-group">
                                                                  <label id="label_tipo_contato" for="contato">Email/Telefone/Skype</label>
                                                                  <input type="text" class="form-control" id="contato" name="contato" value="" required="true" autocomplete="off">
                                                              </div>

                                                              <div class="row">
                                                                  <div class="col-md-12 text-center">
                                                                      <button type="submit" class="btn btn-fill btn-primary disableonclick">
                                                                          <i class="fa fa-plus"></i> Adicionar
                                                                      </button>
                                                                  </div>
                                                              </div>
                                                            </form>
                                                            </div>
                                                          </div>

                                                          @is(['Gestores','Marketing','Especial','Hábito','Liquidez'])
                                                          <div class="card">
                                                            <div class="header text-center">
                                                            <p class="category">{!! trans('contato.sabedoria') !!}</p></div>
                                                            <di class="row">
                                                              @shield('contact.destroy')
                                                              <div class="col-lg-5">
                                                                <div class="content text-center">
                                                                  <form role="form" id="formdeletecontato{{$contato->id}}" class="disableonclick form-delete" method="POST" action="{{ url ('cliente/contatos/'.$contato->id) }}" accept-charset="UTF-8">
                                                                    <input type="hidden" name="_method" value="DELETE">
        																														<input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                                    <input type="hidden" name="cliente_id" value="{{$clienteInfo['cliente_id']}}">
                                                                    <input type="hidden" name="contato_id" value="{{$contato->id}}">
                                                                    <button type="submit" class="btn btn-fill btn-sm btn-danger disableonclick sure" data-form="#formdeletecontato{{$contato->id}}" data-type="contato" data-titledelete="{!! trans('contato.titledelete') !!}" data-textdelete="{!! trans('contato.textdelete') !!}" data-titlesuccess="{!! trans('contato.titledelete_success') !!}" >
                                                                        <i class="fa fa-trash-o"></i> Excluir contato
                                                                    </button>
                                                                  </form>
                                                                </div>
                                                              </div>
                                                              @endshield
                                                              @shield('contact.move')
                                                              <div class="col-lg-5">
                                                                <div class="content text-center">
                                                                  <a href="javascript:void(0)" class="btn btn-fill btn-sm btn-warning" onClick="return abreModal('{{ url ('cliente/contatos/mover/'.$clienteInfo['cliente_id'].'/'.$contato->id) }}', 'Mover contato');" >
                                                                      <i class="fa fa-arrows-alt"></i> Mover contato
                                                                  </a>
                                                                </div>
                                                              </div>
                                                              @endshield
                                                            </di>
                                                          </div>
                                                          @endis
                                                        </div>
                                                      </div>

                                                      <div class="row">
                                                        <div class="col-lg-12">
                                                          <div class="card">
                                                            <div class="header"><h4 class="title">Adicionar produto</h4></div>
                                                              <div class="content">
                                                                <ul class="nav nav-tabs" role="tablist">
                                                                    <li role="presentation" class="active"><a href="#produtos" aria-controls="produtos" role="tab" data-toggle="tab">Adicionar Produto</a></li>
                                                                    <!--<li role="presentation"><a href="#grupoprodutos" aria-controls="grupoprodutos" role="tab" data-toggle="tab">Adicionar Grupo de produtos</a></li>-->
                                                                </ul>
                                                                <div class="tab-content">
                                                                  <div role="tabpanel" class="tab-pane active" id="produtos">
                                                                    @include('crm.cliente.tab_add_produtos')
                                                                  </div>
                                                                  <!--<div role="tabpanel" class="tab-pane" id="grupoprodutos">-->
                                                                    {{-- @include('crm.cliente.tab_add_grupoprodutos') --}}
                                                                  <!--</div>-->
                                                                </div>

                                                              </div>
                                                          </div>
                                                        </div>

                                                        <div class="col-lg-12">
                                                          <h4>Produtos de interesse</h4>
                                                          @if($contato->intermediate_products->count() != 0)
                                                          <table class="table table-responsive" style="margin-top: 1em;">
                                                              <thead>
                                                                  <th>#</th>
                                                                  <th>Tipo de Operação</th>
                                                                  <th>Produto</th>
                                                                  <th>Estados</th>
                                                                  <th>Ações</th>
                                                              </thead>
                                                              <tbody>
                                                              @foreach ($contato->intermediate_products as $produto)
                                                                  <tr id="tr_p{{$produto->contact_id}}-{{$produto->product_id}}-{{$produto->transacao}}">
                                                                      <td>#</td>
                                                                      <td>{{ $produto->transacao }}</td>
                                                                      <td>{{ $produto->product->nome }}</td>
                                                                      <td>{{ $produto->estados }}</td>
                                                                      <td><button type="button" class="btn btn-danger btn-xs btn-fill delete-produto" data-contactid="{{$produto->contact_id}}" data-productid="{{$produto->product_id}}" data-transacao="{{$produto->transacao}}" data-token="{{csrf_token()}}" data-type="produto" data-titledelete="{!! trans('produto.titledelete') !!}" data-textdelete="{!! trans('produto.textdelete') !!}" data-titlesuccess="{!! trans('produto.titlesuccess') !!}" data-url="cliente/produtos"><i class="fa fa-trash-o"></i></button></td>
                                                                  </tr>
                                                              @endforeach
                                                              </tbody>
                                                          </table>
                                                          @else
                                                            <spam class="label label-warning">Nenhum produto de interesse cadastrado.</spam>
                                                          @endif
                                                        </div>
                                                      </div>
                                                    </div> <!-- .panel-body -->
                                                  </div> <!-- .panel-collapse -->
                                                  </div> <!-- .panel -->
                                            @endforeach
                                          @else
                                          <p>Nenhum contato cadastrado!</p>
                                          @endif
                                        </div>
                                    </div>

                                </div>
                              </div>
                            </div>
                          </div> <!-- .tab-pane -->
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $.fn.modal.Constructor.prototype.enforceFocus = function() {};
    </script>
    <script src="{{ asset('lib/js/client.form.js') }}"></script>
    <script>
        var showProductForm = function()
        {
            $("#productInput").show();
        }
    </script>
@endsection
