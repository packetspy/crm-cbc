<form role="form" id="formdelete" class="disableonclick form-horizontal" method="POST" action="{{ url ('cliente/contatos/mover/'.$contato->id) }}" accept-charset="UTF-8">
  <input type="hidden" name="_method" value="PUT">
  <input type="hidden" name="_token" value="{{ csrf_token() }}">
  <input type="hidden" name="contato_id" value="{{$contato->id}}">

<div class="alerta">
    <div class="alert alert-danger" style="display: none" id="erro_input">Preencha ao menos um contato!</div>
</div>

<div class="row">
  <div class="col-xs-12">
    <div class="form-group">
        <b>ID:</b> {{$contato->id}} | <b>Nome:</b> {{$contato->nome}}
    </div>
  </div>

<div class="col-xs-12">
  <div class="form-group">
      {!! Form::label('Mover para:') !!}
      <select class="form-control select" id="cliente_id" name="cliente_id" title="Escolha uma empresa para mover o contato" data-live-search="true" data-size="10" data-width="95%" required="true">
          @foreach($clientes as $cliente)
              <option value="{!! $cliente->id !!}">{!! $cliente->id !!} | {{ str_limit($cliente->empresa, 55) }} </option>
          @endforeach
      </select>
  </div>
</div>
</div>

<div class="clearfix"></div>

<div class="form-group col-sm-12 col-md-12 text-right">
    <button type="submit" class="btn btn-fill btn-primary">
        <i class="fa fa-send"></i> Mover
     </button>
</div>

<div class="clearfix"></div>

</form>

<script>
  $(document).ready(function() {
      $('.select').selectpicker({
        //nothing todo here
      });
  });
</script>
