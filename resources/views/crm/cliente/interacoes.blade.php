@extends('layouts.base')
@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">

            <div class="card">
                <div class="content content-full-width">
                    @include('crm.cliente.tabs')
                    <div class="content">
                        <!-- Tab panes -->
                        <div class="tab-content">
                          <div class="tab-panel active" role="tabpanel" >

                            <div class="row">
                              <div class="col-md-12 col-xs-12 col-sm-12">
                                @include('crm.cliente.info-general')
                              </div>
                            </div>

                              @if(!$interactions->isEmpty())
                              <div class="table-responsive">
                                  <table class="table table-striped">
                                      <thead>
                                      <th>#</th>
                                      <th>Data</th>
                                      <th>Consultor</th>
                                      <th>Anterior</th>
                                      <th>Tipo</th>
                                      <th>Título</th>
                                      <th></th>
                                      </thead>
                                      <tbody>
                                      @foreach($interactions as $interaction)
                                          <tr>
                                              <td><small>{{ $interaction->id }}</small></td>
                                              <td>{{ $interaction->datacriacao }}</td>
                                              <td>{{ $interaction->author->name }}</td>
                                              <td>
                                                @if($interaction->father_id)
                                                    <a href="#" onClick="return abreModal('{{ action('Crm\ClientController@getViewInteracao', [$clienteInfo['cliente_id'], $interaction->father_id]) }}', 'Visualizando interação')" class="text-info">
                                                        <i class="fa fa-eye"></i> #{!! $interaction->father_id !!}
                                                    </a>
                                                @else
                                                    <small class="text-warning">Nenhuma</small>
                                                @endif
                                              </td>
                                              <td>
                                                <span class="label {!! $interaction->tipo_interacao_class !!}"> {!! $interaction->tipo_interacao !!}</span>
                                                @if(!is_null($interaction->children))
                                                    <a href="#" class="label label-success" onClick="return abreModal('{{ action('Crm\ClientController@getViewInteracao', [$clienteInfo['cliente_id'], $interaction->children->id]) }}', 'Visualizando interação')">
                                                        <i class="fa fa-check-circle"></i> Ver resposta
                                                    </a>
                                                @endif
                                              </td>
                                              <td>{{ substr($interaction->titulo, 0, 150) }}...</td>
                                              <td>
                                                <a href="#" onClick="return abreModal('{{ action('Crm\ClientController@getViewInteracao', [$clienteInfo['cliente_id'], $interaction->id]) }}', 'Visualizando interação')" class="btn btn-fill btn-primary btn-xs">
                                                    <i class="fa fa-eye"></i> Detalhes
                                                </a>

                                                @shield('interaction.move')
                                                <a href="#" onClick="return abreModal('{!! action('Crm\ClientController@getMoveInteraction', [$clienteInfo['cliente_id'], $interaction->id]) !!}', 'Mover interação');" class="btn btn-fill btn-xs btn-warning">
                                                    <i class="fa fa-arrows-alt"></i> Mover
                                                </a>
                                                @endshield
                                              </td>
                                          </tr>
                                      @endforeach
                                      </tbody>
                                  </table>
                                    

                                  </div>
                                  <div class="row">
                                    <div class="col-md-12 text-center">
																			{!! $interactions->appends(\Request::except('page'))->render() !!}
																	  </div>
                                  </div>
                                  <div class="clearfix margin-top"></div>
                                  @else
                                  <div class="row">
                                    <div class="col-md-12">
                                      <div class="alert alert-warning">
                                        <i class="fa fa-info"></i>Nenhum historico referente ao cliente cadastrado.
                                      </div>
                                    </div>
                                  </div>
                                  <div class="clearfix margin-top"></div>
                                  @endif
                          </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $.fn.modal.Constructor.prototype.enforceFocus = function() {};
    </script>
    <script src="{{ asset('lib/js/client.form.js') }}"></script>
    <script>
        var showProductForm = function()
        {

            $("#productInput").show();
        }
    </script>
@endsection
