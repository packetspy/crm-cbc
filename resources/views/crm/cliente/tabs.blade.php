<!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="{{ isActiveRoute('cliente/resumo/{resumo}', 'active') }}"><a href="{{ url('/cliente/resumo') }}/{{Request::segment(3)}}"><i class="fa fa-info"></i> Resumo</a></li>
    <li role="presentation" class="{{ isActiveRoute('cliente/dados/{dados}', 'active') }}"><a href="{{ url('/cliente/dados') }}/{{Request::segment(3)}}"><i class="fa fa-pencil"></i> Dados Cadastrais</a></li>
    <li role="presentation" class="{{ isActiveRoute('cliente/contatos/{contatos}', 'active') }}"><a href="{{ url('/cliente/contatos') }}/{{Request::segment(3)}}"><i class="fa fa-users"></i>Contatos</a></li>
    <li role="presentation" class="{{ isActiveRoute('cliente/interacoes/{interacao}', 'active') }}"><a href="{{ url('/cliente/interacoes') }}/{{Request::segment(3)}}"><i class="fa fa-history"></i>Histórico de interações</a></li>
    @shield('payment.index')
    <li role="presentation" class="{{ isActiveRoute('cliente/financeiro/{financeiro}', 'active') }}"><a href="{{ url('/cliente/financeiro') }}/{{Request::segment(3)}}"><i class="fa fa-dollar"></i>Financeiro</a></li>
    @endshield
    <li role="presentation" class="{{ isActiveRoute('cliente/relevancia/{relevancia}', 'active') }}"><a href="{{ url('/cliente/relevancia') }}/{{Request::segment(3)}}"><i class="fa fa-flash"></i>Relevancia</a></li>
</ul>
