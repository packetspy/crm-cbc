@extends('layouts.base')
@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">

            <div class="card">
                <div class="content content-full-width">
                    @include('crm.cliente.tabs')
                    <div class="content">
                        <!-- Tab panes -->
                        <div class="tab-content">

                            <div class="row">
                              <div class="col-md-12 col-xs-12 col-sm-12">
                                @include('crm.cliente.info-general')
                              </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-xs-12 col-sm-12">
                                    <div class="card">
                                        <div class="header">
                                            <h4 class="title">Adicionar Contato</h4>
                                            <p class="category">Você pode adicionar mais informações após salva-lo, exemplo: <b>produto</b>, outros <b>e-mails</b> e <b>telefones</b></p>
                                        </div>
                                        <div class="content">
                                          <form role="form" method="POST" class="form-horizontal" action="{{ url ('cliente/contatos') }}" accept-charset="UTF-8">
                                          <input type="hidden" name="_method" value="POST">
                                          <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                          <input type="hidden" name="cliente_id" value="{{$clienteInfo['cliente_id']}}">
                                          <input type="hidden" name="origem_id" id="origem_id" value="2">

                                          <div class="form-group">
                                            <label for="nome" class="col-sm-2 control-label">Nome</label>
                                            <div class="col-sm-6">
                                              <input type="text" class="form-control" name="nome" id="nome" placeholder="Nome" required="true">
                                            </div>
                                          </div>
                                          <div class="form-group">
                                            <label for="departamento" class="col-sm-2 control-label">Departamento</label>
                                            <div class="col-sm-6">
                                              <input type="text" class="form-control" name="departamento" id="departamento" placeholder="Departamento">
                                            </div>
                                          </div>
                                          <div class="form-group">
                                            <label for="email" class="col-sm-2 control-label">E-mail</label>
                                            <div class="col-sm-6">
                                              <input type="email" class="form-control" name="email" id="email" placeholder="E-mail">
                                            </div>
                                          </div>
                                          <div class="form-group">
                                            <label for="telefone" class="col-sm-2 control-label">Telefone</label>
                                            <div class="col-sm-6">
                                              <input type="text" class="form-control" name="telefone" id="telefone" placeholder="Telefone">
                                            </div>
                                          </div>

                                          <div class="row">
                                              <div class="col-md-6 text-right">
                                                  <button type="submit" class="btn btn-fill btn-success">
                                                      <i class="fa fa-plus"></i> Adicionar
                                                  </button>
                                              </div>
                                          </div>

                                          </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $.fn.modal.Constructor.prototype.enforceFocus = function() {};
    </script>
    <script src="{{ asset('lib/js/client.form.js') }}"></script>
    <script>
        var showProductForm = function()
        {

            $("#productInput").show();
        }
    </script>
@endsection
