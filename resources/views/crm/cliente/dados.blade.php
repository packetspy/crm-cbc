@extends('layouts.base')
@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">

            <div class="card">
                <div class="content content-full-width">
                    @include('crm.cliente.tabs')
                    <div class="content">
                        <!-- Tab panes -->
                        <div class="tab-content">
                          <div class="tab-panel active" role="tabpanel" >

                            <div class="row">
                              <div class="col-md-12 col-xs-12 col-sm-12">
                                @include('crm.cliente.info-general')
                              </div>
                            </div>

                              <form role="form" id="form_dados" method="POST" action="{{ url ('cliente/dados/'.$cliente->id) }}" accept-charset="UTF-8">
																	<input type="hidden" name="_method" value="PUT">
																	<input type="hidden" name="_token" value="{{ csrf_token() }}">
                                @is(['Gestores','Financeiro','Marketing','Especial','Hábito','Liquidez'])
                                  <input type="hidden" id="isResponsavel" name="isResponsavel" value="true">
                                @else
                                  <input type="hidden" id="isResponsavel" name="isResponsavel" value="{{ isResponsavel($cliente->responsavel_id) ? 'true' : 'false' }}">
                                @endis

                                  <div class="row">
                                    @shield('responsavel.edit')
                                      <div class="col-md-3">
                                        <label for="responsavel_id">Consultor Responsavel:</label>
                                        <select class="select show-tick form-control" id="responsavel_id" name="responsavel_id" title="Selecione Consultor..." data-live-search="true">
                                						@foreach($consultores as $consultor)
                                								<option value="{{ $consultor->id }}" {{ $cliente->responsavel_id == $consultor->id ? 'selected="selected"' : '' }}>{{ $consultor->name }}</option>
                                						@endforeach
                                				</select>
                                      </div>
                                    @else
                                      <div class="col-md-3">
                                        <label>Consultor Responsavel:</label><br>
                                        {{$cliente->responsavel ? $cliente->responsavel->name : 'Sem responsável atribuído' }}
                                      </div>
                                    @endshield

                                    @shield('status.edit')
                                      <div class="col-md-3">
                                        <label for="status_id">Situação/Status:</label>
                                        <select class="select show-tick form-control" id="status_id" name="status_id" title="Selecione ..." data-live-search="true" required disabled>
                                						@foreach($status as $status)
                                								<option value="{{ $status->id }}" {{ $cliente->status_id == $status->id ? 'selected="selected"' : '' }}>{{ $status->status }}</option>
                                						@endforeach
                                				</select>
                                      </div>
                                    @else
                                    <div class="col-md-3">
                                      <label>Situação/Status:</label><br>
                                      {{$cliente->status->status}}
                                    </div>
                                    @endshield

                                    @shield('idplataforma.edit')
                                      <div class="col-md-3">
                                        <div class="form-group {{ $errors->has('id_plataforma') ? ' has-error' : '' }}">
                                            <label for="">ID na Plataforma</label>
                                            <input type="text" class="form-control" id="id_plataforma" name="id_plataforma" value="{{$cliente->id_plataforma}}">
                                            @if ($errors->has('id_plataforma'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('id_plataforma') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                      </div>
                                    @else
                                      <div class="col-md-3">
                                        <label>ID Plataforma:</label><br>
                                        {{!$cliente->id_plataforma ? 'Não cadastrado' : $cliente->id_plataforma }}
                                      </div>
                                    @endshield

                                    @shield('origem.edit')
                                      <div class="col-md-3">
                                        <label for="origem_id">Origem:</label>
                                        <select class="select show-tick form-control" id="origem_id" name="origem_id" title="Selecione ..." data-live-search="true" disabled>
                                            @foreach($origens as $origem)
                                                <option value="{{ $origem->id }}" {{ $cliente->origem_id == $origem->id ? 'selected="selected"' : '' }}>{{ $origem->origem }}</option>
                                            @endforeach
                                        </select>
                                      </div>
                                    @else
                                      <div class="col-md-3">
                                        <label>Origem:</label><br>
                                        <span class="form-control">{{$cliente->origem->origem}}</span>
                                      </div>
                                    @endshield
                                  </div>

                                  <div class="row">
                                      <div class="col-md-6">
                                          <div class="form-group {{ $errors->has('empresa') ? ' has-error' : '' }}">
                                              <label for="empresa">Empresa / Razão Social</label><span class="required">*</span>
                                              <input type="text" class="form-control" id="empresa" name="empresa" value="{{$cliente->empresa}}">
                                              @if ($errors->has('empresa'))
                                                  <span class="help-block">
                                                      <strong>{{ $errors->first('empresa') }}</strong>
                                                  </span>
                                              @endif
                                          </div>
                                      </div>
                                      <div class="col-md-6">
                                          <div class="form-group {{ $errors->has('nome') ? ' has-error' : '' }}">
                                              <label for="nome">Nome Fantasia</label>
                                              <input type="text" class="form-control" id="nome" name="nome" value="{{$cliente->nome}}">
                                              @if ($errors->has('nome'))
                                                  <span class="help-block">
                                                      <strong>{{ $errors->first('nome') }}</strong>
                                                  </span>
                                              @endif
                                          </div>
                                      </div>
                                  </div>

                                  <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group {{ $errors->has('tipo_documento') ? ' has-error' : '' }}">
                                            <label for="">Tipo de Empresa</label>
                                            <select class="form-control select show-tick" name="tipoempresa_id" id="tipoempresa_id" title="Selecione o tipo de empresa">
                                                @foreach ($tipoempresa as $tipo)
                                                  @if($tipo->id == $cliente->tipoempresa_id)
                                                    <option selected value="{!! $tipo->id !!}">{!! $tipo->tipo !!}</option>
                                                  @else
                                                   <option value="{!! $tipo->id !!}">{!! $tipo->tipo !!}</option>
                                                  @endif
                                                @endforeach
                                                @if ($errors->has('tipoempresa_id'))
                                                    <span class="help-block"><strong>{{ $errors->first('tipoempresa_id') }}</strong></span>
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group {{ $errors->has('tipo_documento') ? ' has-error' : '' }}">
                                            <label for="">Tipo de Documento</label>
                                            <select class="form-control select show-tick" name="tipo_documento" id="tipo_documento" title="Selecione o tipo de documento">
                                                @foreach ($tipodocumento as $key => $value)
                                                  @if($key == $cliente->tipo_documento)
                                                    <option selected value="{!! $key !!}">{!! $value !!}</option>
                                                  @else
                                                   <option value="{!! $key !!}">{!! $value !!}</option>
                                                  @endif
                                                @endforeach
                                                @if ($errors->has('tipo_documento'))
                                                    <span class="help-block"><strong>{{ $errors->first('tipo_documento') }}</strong></span>
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group {{ $errors->has('documento') ? ' has-error' : '' }}">
                                            <label for="documento">CPF/CNPJ:</label>
                                            <input type="text" class="form-control" id="documento" name="documento" value="{{$cliente->documento}}">
                                            @if ($errors->has('documento'))
                                                <span class="help-block">
                                            <strong>{{ $errors->first('documento') }}</strong>
                                        </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group {{ $errors->has('ie') ? ' has-error' : '' }}">
                                            <label for="empresa">Inscrição Estadual</label>
                                            <input type="text" class="form-control" id="ie" name="ie" value="{{$cliente->ie}}">
                                            @if ($errors->has('ie'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('ie') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                  </div>

                                  <div class="row">
                                    <div class="col-md-3">
                                      <div class="form-group {{ $errors->has('endereco') ? ' has-error' : '' }}">
                                          <label for="endereco">Endereço</label> <a href="https://www.google.com.br/maps/place/{{$cliente->endereco}}+{{$cliente->cidade}}+{{$cliente->estado}}+{{$cliente->cep}}" target="_blank">(<i class="fa fa-auto fa-map-marker"></i> Ver no Google Maps)</a>
                                          <input type="text" class="form-control" id="endereco" name="endereco" value="{{$cliente->endereco}}">
                                          @if ($errors->has('endereco'))
                                              <span class="help-block"><strong>{{ $errors->first('endereco') }}</strong></span>
                                          @endif
                                      </div>
                                    </div>
                                    <div class="col-md-2">
                                      <div class="form-group {{ $errors->has('complemento') ? ' has-error' : '' }}">
                                          <label for="complemento">Complemento</label>
                                          <input type="text" class="form-control" id="complemento" name="complemento" value="{{$cliente->complemento}}">
                                          @if ($errors->has('complemento'))
                                              <span class="help-block"><strong>{{ $errors->first('complemento') }}</strong></span>
                                          @endif
                                      </div>
                                    </div>
                                    <div class="col-md-2">
                                      <div class="form-group {{ $errors->has('bairro') ? ' has-error' : '' }}">
                                          <label for="bairro">Bairro</label>
                                          <input type="text" class="form-control" id="bairro" name="bairro" value="{{$cliente->bairro}}">
                                          @if ($errors->has('bairro'))
                                              <span class="help-block"><strong>{{ $errors->first('bairro') }}</strong></span>
                                          @endif
                                      </div>
                                    </div>
                                    <div class="col-md-2">
                                      <div class="form-group {{ $errors->has('cidade') ? ' has-error' : '' }}">
                                          <label for="cidade">Cidade</label>
                                          <input type="text" class="form-control" id="cidade" name="cidade" value="{{$cliente->cidade}}">
                                          @if ($errors->has('cidade'))
                                              <span class="help-block"><strong>{{ $errors->first('cidade') }}</strong></span>
                                          @endif
                                      </div>
                                    </div>
                                    <div class="col-md-1">
                                      <div class="form-group {{ $errors->has('estado') ? ' has-error' : '' }}">
                                          <label for="estado">Estado</label>
                                          <select class="form-control select show-tick" name="estado" id="estado" title="Selecione o estado" data-live-search="true">
                                              @foreach ($estados as $key => $value)
                                                @if($key == $cliente->estado)
                                                  <option selected value="{!! $key !!}">{!! $key !!}</option>
                                                @else
                                                 <option value="{!! $key !!}">{!! $key !!}</option>
                                                @endif
                                              @endforeach
                                              @if ($errors->has('estado'))
                                                  <span class="help-block"><strong>{{ $errors->first('estado') }}</strong></span>
                                              @endif
                                          </select>
                                      </div>
                                    </div>
                                    <div class="col-md-2">
                                      <div class="form-group {{ $errors->has('cep') ? ' has-error' : '' }}">
                                          <label for="cep">CEP</label>
                                          <input type="text" class="form-control" id="cep" name="cep" value="{{$cliente->cep}}">
                                          @if ($errors->has('cep'))
                                              <span class="help-block"><strong>{{ $errors->first('cep') }}</strong></span>
                                          @endif
                                      </div>
                                    </div>
                                  </div>

                                  <div class="row">

                                      <div class="col-md-6">
                                          <div class="form-group {{ $errors->has('segmento') ? ' has-error' : '' }}">
                                              <label for="">Segmento</label>
                                              <input type="text" class="form-control" id="segmento" name="segmento" value="{{$cliente->segmento}}">
                                              @if ($errors->has('segmento'))
                                                  <span class="help-block">
                                              <strong>{{ $errors->first('segmento') }}</strong>
                                          </span>
                                              @endif
                                          </div>
                                      </div>


                                      <div class="col-md-6">
                                          <div class="form-group {{ $errors->has('site') ? ' has-error' : '' }}">
                                              <label for="site">Site</label>
                                              <input type="text" class="form-control" id="site" name="site" value="{{$cliente->site}}">
                                              @if ($errors->has('site'))
                                                  <span class="help-block">
                                              <strong>{{ $errors->first('site') }}</strong>
                                          </span>
                                              @endif
                                          </div>
                                      </div>
                                  </div>

                                  <div class="row">
                                      <div class="col-md-12 text-right">
                                          <button type="submit" class="btn btn-fill btn-primary">
                                              <i class="fa fa-send"></i> Atualizar dados
                                          </button>
                                      </div>
                                  </div>
                              </form>
                          </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $.fn.modal.Constructor.prototype.enforceFocus = function() {};
    </script>
    <script src="{{ asset('lib/js/client.form.js') }}"></script>
    <script>
        var showProductForm = function()
        {

            $("#productInput").show();
        }
    </script>
@endsection
