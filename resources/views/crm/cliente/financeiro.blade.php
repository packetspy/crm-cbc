@extends('layouts.base')
@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">

            <div class="card">
                <div class="content content-full-width">
                    @include('crm.cliente.tabs')
                    <div class="content">
                        <!-- Tab panes -->
                        <div class="tab-content">
                          <div class="tab-panel active" role="tabpanel" >

                            <div class="row">
                              <div class="col-md-12 col-xs-12 col-sm-12">
                                @include('crm.cliente.info-general')
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-6 col-xs-6 col-sm-6">
                                <!-- incio do form para adicionar novos pagamentos/mensalidades -->
                                @shield('payment.create')
                                <div class="card">
                                  <div class="header">
                                    <h4 class="title">Informações de cobrança</h4>
                                  </div>
                                  <div class="content">
                                  <form role="form" method="POST" action="{{ url ('cliente/financeiro/dados-cobranca/'.$clienteInfo['cliente_id']) }}" accept-charset="UTF-8">
                                    <input type="hidden" name="_method" value="PUT">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="cliente_id" value="{{$clienteInfo['cliente_id']}}">
                                    <input type="hidden" name="dados_id" value="{{$dados_cobranca->id}}">

                                    <div class="row">
                                      <div class="col-md-6">
                                          <div class="form-group {{ $errors->has('valor') ? ' has-error' : '' }}">
                                              <label for="valor">Valor/Mensalidade</label>
                                              <input type="text" class="form-control" id="valor" name="valor" value="{{$dados_cobranca->valor}}">
                                              @if ($errors->has('valor'))
                                                  <span class="help-block"><strong>{{ $errors->first('valor') }}</strong></span>
                                              @endif
                                          </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group {{ $errors->has('vencimento') ? ' has-error' : '' }}">
                                                <label for="vencimento">Vencimento</label>
                                                <input type="number" class="form-control" id="vencimento" name="vencimento" min="1" max="31" autocomplete="off" value="{{$dados_cobranca->vencimento}}">
                                                @if ($errors->has('vencimento'))
                                                    <span class="help-block"><strong>{{ $errors->first('vencimento') }}</strong></span>
                                                @endif
                                            </div>
                                        </div>
                                      </div>

                                      <div class="row">
                                        <div class="col-md-4">
                                          <label for="status_id">Status Financeiro</label>
                                          <select class="select show-tick form-control" id="status_id" name="status_id" title="Selecione ..." data-live-search="true" required>
                                  						@foreach($status as $status)
                                  								<option value="{{ $status->id }}" {{ $dados_cobranca->status_id == $status->id ? 'selected="selected"' : '' }}>{{ $status->status }}</option>
                                  						@endforeach
                                  				</select>
                                        </div>
                                        <div class="col-md-4">
                                          <div class="form-group {{ $errors->has('formapagamento_id') ? ' has-error' : '' }}">
                                              <label for="formapagamento_id">Forma de pagamento</label>
                                              <select class="select form-control" name="formapagamento_id" id="formapagamento_id" title="Escolha ...">
                                                @foreach ($formaspagamentos as $key => $value)
                                                  @if($key == $dados_cobranca->formapagamento_id)
                                                    <option selected value="{!! $key !!}">{!! $value !!}</option>
                                                  @else
                                                   <option value="{!! $key !!}">{!! $value !!}</option>
                                                  @endif
                                                @endforeach
                                                @if ($errors->has('formapagamento_id'))
                                                    <span class="help-block"><strong>{{ $errors->first('formapagamento_id') }}</strong></span>
                                                @endif
                                              </select>
                                          </div>
                                        </div>
                                        <div class="col-md-4">
                                          <div class="form-group {{ $errors->has('recorrencia_id') ? ' has-error' : '' }}">
                                            <label for="recorrencia_id">Recorrência</label>
                                            <select class="select form-control" name="recorrencia_id" id="recorrencia_id" title="Escolha ...">
                                              @foreach ($recorrencia as $key => $value)
                                                @if($key == $dados_cobranca->recorrencia_id)
                                                  <option selected value="{!! $key !!}">{!! $value !!}</option>
                                                @else
                                                 <option value="{!! $key !!}">{!! $value !!}</option>
                                                @endif
                                              @endforeach
                                              @if ($errors->has('recorrencia_id'))
                                                  <span class="help-block"><strong>{{ $errors->first('recorrencia_id') }}</strong></span>
                                              @endif
                                            </select>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12 text-center">
                                            <button type="submit" class="btn btn-fill btn-primary">
                                                <i class="fa fa-refresh"></i> Atualizar Informações
                                            </button>
                                        </div>
                                    </div>
                                  </form>
                                  </div>
                                </div>
                                @endshield
                              </div>

                              <div class="col-md-6 col-xs-6 col-sm-6">
                                <!-- incio do form para adicionar novos pagamentos/mensalidades -->
                                @shield('payment.create')
                                <div class="card">
                                  <div class="header">
                                    <h4 class="title">Endereço de cobrança</h4>
                                  </div>
                                  <div class="content">
                                  <form role="form" method="POST" action="{{ url ('cliente/financeiro/endereco-cobranca/'.$clienteInfo['cliente_id']) }}" accept-charset="UTF-8">
                                    <input type="hidden" name="_method" value="PUT">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="cliente_id" value="{{$clienteInfo['cliente_id']}}">
                                    <input type="hidden" name="endereco_id" value="{{$endereco_cobranca->id}}">

                                    <div class="row">
                                      <div class="col-md-12">
                                        <div class="form-group {{ $errors->has('endereco') ? ' has-error' : '' }}">
                                            <label for="endereco">Endereço</label>
                                            <input type="text" class="form-control" id="endereco" name="endereco" value="{{$endereco_cobranca->endereco}}">
                                            @if ($errors->has('endereco'))
                                                <span class="help-block"><strong>{{ $errors->first('endereco') }}</strong></span>
                                            @endif
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row">
                                      <div class="col-md-6">
                                        <div class="form-group {{ $errors->has('complemento') ? ' has-error' : '' }}">
                                            <label for="complemento">Complemento</label>
                                            <input type="text" class="form-control" id="complemento" name="complemento" value="{{$endereco_cobranca->complemento}}">
                                            @if ($errors->has('complemento'))
                                                <span class="help-block"><strong>{{ $errors->first('complemento') }}</strong></span>
                                            @endif
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group {{ $errors->has('bairro') ? ' has-error' : '' }}">
                                            <label for="bairro">Bairro</label>
                                            <input type="text" class="form-control" id="bairro" name="bairro" value="{{$endereco_cobranca->bairro}}">
                                            @if ($errors->has('bairro'))
                                                <span class="help-block"><strong>{{ $errors->first('bairro') }}</strong></span>
                                            @endif
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row">
                                      <div class="col-md-5">
                                        <div class="form-group {{ $errors->has('cidade') ? ' has-error' : '' }}">
                                            <label for="cidade">Cidade</label>
                                            <input type="text" class="form-control" id="cidade" name="cidade" value="{{$endereco_cobranca->cidade}}">
                                            @if ($errors->has('cidade'))
                                                <span class="help-block"><strong>{{ $errors->first('cidade') }}</strong></span>
                                            @endif
                                        </div>
                                      </div>
                                      <div class="col-md-2">
                                        <div class="form-group {{ $errors->has('estado') ? ' has-error' : '' }}">
                                            <label for="estado">Estado</label>
                                            <select class="form-control select show-tick" name="estado" id="estado" title="Selecione ..." data-live-search="true">
                                                @foreach ($estados as $key => $value)
                                                  @if($key == $endereco_cobranca->estado)
                                                    <option selected value="{!! $key !!}">{!! $key !!}</option>
                                                  @else
                                                   <option value="{!! $key !!}">{!! $key !!}</option>
                                                  @endif
                                                @endforeach
                                                @if ($errors->has('estado'))
                                                    <span class="help-block"><strong>{{ $errors->first('estado') }}</strong></span>
                                                @endif
                                            </select>
                                        </div>
                                      </div>
                                      <div class="col-md-5">
                                        <div class="form-group {{ $errors->has('cep') ? ' has-error' : '' }}">
                                            <label for="cep">CEP</label>
                                            <input type="text" class="form-control" id="cep" name="cep" value="{{$endereco_cobranca->cep}}">
                                            @if ($errors->has('cep'))
                                                <span class="help-block"><strong>{{ $errors->first('cep') }}</strong></span>
                                            @endif
                                        </div>
                                      </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12 text-center">
                                            <button type="submit" class="btn btn-fill btn-primary">
                                                <i class="fa fa-map-marker"></i> Atualizar Endereço
                                            </button>
                                        </div>
                                    </div>

                                  </form>
                                  </div>
                                </div>
                                @endshield
                              </div>
                            </div>

                            <div class="row">
                              <div class="col-md-12 col-xs-12 col-sm-12">
                                <!-- incio do form para adicionar novos pagamentos/mensalidades -->
                                @shield('payment.create')
                                <div class="card">
                                  <div class="header">
                                    <h4 class="title">Observações financeiras</h4>
                                  </div>
                                  <div class="content">
                                  <form role="form" method="POST" action="{{ url ('cliente/financeiro/historico-cobranca/'.$clienteInfo['cliente_id']) }}" accept-charset="UTF-8">
                                    <input type="hidden" name="_method" value="POST">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="cliente_id" value="{{$clienteInfo['cliente_id']}}">
                                      <div class="row">
                                        <div class="col-md-10">
                                            <div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
                                                <textarea class="form-control" id="description" name="description" rows="2" placeholder="Adicionar nova observação"></textarea>
                                                @if ($errors->has('description'))
                                                    <span class="help-block"><strong>{{ $errors->first('description') }}</strong></span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <button type="submit" class="btn btn-fill btn-primary">
                                                <i class="fa fa-plus"></i> Adicionar
                                            </button>
                                        </div>

                                      </div>

                                  </form>
                                  </div>
                                </div>
                                @endshield
                              </div>
                            </div>

                              @if(!$historico_cobranca->isEmpty())
                              <div class="table-responsive">
                                  <table class="table table-striped">
                                      <thead>
                                      <th class="col-md-2">Valor</th>
                                      <th class="col-md-3">Criado em</th>
                                      <th class="col-md-5">Detalhes</th>
                                      </thead>
                                      <tbody>
                                      @foreach($historico_cobranca as $historico)
                                          <tr>
                                              <td>{{ $historico->amount }}</td>
                                              <td>{{ $historico->datacriacao }}</td>
                                              <td>{{ substr($historico->description, 0, 255) }}...</td>
                                          </tr>
                                      @endforeach
                                      </tbody>
                                  </table>

                                    <div class="row">
										<div class="col-md-12 text-center">
											Total de registros: <b>{{ $historico_cobranca->total() }}</b>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12 text-center">
												{!! $historico_cobranca->appends(\Request::except('page'))->render() !!}
										</div>
									</div>

                                  </div>
                                  <div class="clearfix margin-top"></div>
                                  @else
                                  <div class="row">
                                    <div class="col-md-12">
                                      <div class="alert alert-warning">
                                        <i class="fa fa-info"></i>Nenhum pagamento/mensalidade cadastrado.
                                      </div>
                                    </div>
                                  </div>
                                  <div class="clearfix margin-top"></div>
                                  @endif

                          </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
