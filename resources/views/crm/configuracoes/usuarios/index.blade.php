@extends('layouts.base')
@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="card">
                <div class="header">
                    <h4 class="title">{{$titulo}} ({{$usuarios->total()}})</h4>
                    <p class="category">{{$subtitulo}}</p>
                </div>
                <div class="content">
                    <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <th>#</th>
                        <th>Nome</th>
                        <th>E-mail</th>
                        <th>Ação</th>
                        </thead>
                        <tbody>
                        @foreach ($usuarios as $usuario)
                            <tr>
                                <td>{!! $usuario->id !!}</td>
                                <td>{{ $usuario->name }}</td>
                                <td>{{ $usuario->email }}</td>
                                <td>
                                    <a href="{{url('configuracoes/usuarios/'.$usuario->id.'/edit') }}" class="btn btn-fill btn-warning btn-sm">
                                        <i class="fa fa-edit"></i> Editar
                                    </a>
                                    <a href="{{url('configuracoes/usuarios/'.$usuario->id.'/destroy') }}" class="btn btn-fill btn-sm btn-danger">
                                        <i class="fa fa-trash-o"></i> Excluir
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    </div>
                    {!! $usuarios->render() !!}
                </div>
            </div>
        </div>

    </div>

@endsection
