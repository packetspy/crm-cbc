@extends('layouts.base')
@section('content')
   <div class="row">
     <div class="col-md-12 col-xs-12 col-sm-12">
       <div class="card">
           <div class="header">
             <h4 class="title">{{$titulo}}</h4>
           </div>

           <div class="content">
             <form action="{{url('configuracoes/usuarios/'.$usuario->id)}}" method="post" accept-charset="UTF-8">
             <input type="hidden" name="_method" value="PUT">
             <input type="hidden" name="_token" value="{{ csrf_token() }}">
               <div class="row">
                   <div class="col-md-12">
                       <div class="form-group{{ $errors->has('fname') ? ' has-error' : '' }}">
                           <label for="fname" class="col-md-4 control-label">Nome</label>
                           <div class="col-md-6">
                               {!! Form::text('fname', null, ['class' => 'form-control']) !!}
                               @if ($errors->has('fname'))
                                   <span class="help-block">
                                        <strong>{{ $errors->first('fname') }}</strong>
                                    </span>
                               @endif
                           </div>
                       </div>

                       <div class="form-group{{ $errors->has('lname') ? ' has-error' : '' }}">
                           <label for="lname" class="col-md-4 control-label">Sobrenome</label>
                           <div class="col-md-6">
                               {!! Form::text('lname', null, ['class' => 'form-control']) !!}
                               @if ($errors->has('lname'))
                                   <span class="help-block">
                                        <strong>{{ $errors->first('lname') }}</strong>
                                    </span>
                               @endif
                           </div>
                       </div>

                       <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                           <label for="email" class="col-md-4 control-label">E-Mail</label>
                           <div class="col-md-6">
                               {!! Form::email('email', null, ['class' => 'form-control']) !!}

                               @if ($errors->has('email'))
                                   <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                               @endif
                           </div>
                       </div>

                       <div class="form-group{{ $errors->has('role') ? ' has-error' : '' }}">
                           <label for="role" class="col-md-4 control-label">Grupo</label>
                           <div class="col-md-6">
                             @if(isset($user))
                               <select class="select show-tick form-control" id="grupos[]" name="grupos[]" title="Selecione..." data-live-search="true" multiple="true" required="true">
                                  @foreach($grupos as $grupo)
                                    <option value="{{ $grupo->id }}" {{in_array($grupo->id, $user->grupos->lists('id')->toArray()) ? 'selected="selected"' : ''}}>{{ $grupo->name }}</option>
                                  @endforeach
                              </select>
                             @else
                               <select class="select show-tick form-control" id="grupos[]" name="grupos[]" title="Selecione..." data-live-search="true" multiple="true" required="true">
                                  @foreach($grupos as $grupo)
                                      <option value="{{ $grupo->id }}">{{ $grupo->name }}</option>
                                  @endforeach
                              </select>
                             @endif

                               @if ($errors->has('role'))
                                   <span class="help-block">
                                        <strong>{{ $errors->first('role') }}</strong>
                                    </span>
                               @endif
                           </div>
                       </div>

                       <div class="form-group{{ $errors->has('escritorio_id') ? ' has-error' : '' }}">
                           <label for="role" class="col-md-4 control-label">Escritório</label>
                           <div class="col-md-6">
                             @if(isset($user))
                               <select class="select show-tick form-control" id="escritorio_id" name="escritorio_id" title="Selecione..." data-live-search="true" required="true">
                                  @foreach($escritorios as $escritorio)
                                    <option value="{{ $escritorio->id }}" {{$user->escritorio_id == $escritorio->id ? 'selected="selected"' : ''}}>{{ $escritorio->nome }} ({{$escritorio->sigla}})</option>
                                  @endforeach
                              </select>
                             @else
                               <select class="select show-tick form-control" id="escritorio_id" name="escritorio_id" title="Selecione..." data-live-search="true" required="true">
                                  @foreach($escritorios as $escritorio)
                                      <option value="{{ $escritorio->id }}">{{ $escritorio->nome }} ({{$escritorio->sigla}})</option>
                                  @endforeach
                              </select>
                             @endif

                               @if ($errors->has('escritorio_id'))
                                   <span class="help-block">
                                        <strong>{{ $errors->first('escritorio_id') }}</strong>
                                    </span>
                               @endif
                           </div>
                       </div>

                       <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                           <label for="password" class="col-md-4 control-label">Senha</label>
                           <div class="col-md-6">
                               {!! Form::password('password', ['class' => 'form-control']) !!}
                               @if ($errors->has('password'))
                                   <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                               @endif
                           </div>
                       </div>

                       <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                           <label for="password_confirmation" class="col-md-4 control-label">Confirmar senha</label>
                           <div class="col-md-6">
                               {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
                               @if ($errors->has('password_confirmation'))
                                   <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                               @endif
                           </div>
                       </div>
                   </div>
                 </div>
              </form>
           </div>
       </div>
     </div>
   </div>
@endsection
