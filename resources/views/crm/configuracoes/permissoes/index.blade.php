@extends('layouts.base')
@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                  <div class="row">
                    <div class="col-lg-12">
                      <h4 class="title">{{$titulo}} ({{ $permissoes->total() }})</h4>
                      <p class="category">{{$subtitulo}}</p>
                    </div>
                  </div>
                </div>
                <div class="panel-body">

                    <!-- Row -->
                    <table class="table table-responsive" style="margin-top: 1em;">
                        <thead>
                            <th>#</th>
                            <th>Descrição</th>
                            <th>Code</th>
                            <th>Ações</th>
                        </thead>
                        <tbody>
                        @foreach ($permissoes as $permissao)
                            <tr>
                                <td>#{{ $permissao->id }}</td>
                                <td>{{ $permissao->readable_name }}</td>
                                <td>{{ $permissao->name }}</td>
                                <td></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                    <div class="row">
											<div class="col-md-12 text-center">
												Total de registros: <b>{{ $permissoes->total() }}</b>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12 text-center">
													{!! $permissoes->appends(\Request::except('page'))->render() !!}
											</div>
										</div>

                </div>
            </div>
        </div>
    </div>
@endsection
