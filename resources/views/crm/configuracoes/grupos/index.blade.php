@extends('layouts.base')

@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                  <div class="row">
                    <div class="col-lg-12">
                      <h4 class="title">{{$titulo}} ({{ $grupos->total() }})</h4>
                      <p class="category">{{$subtitulo}}</p>
                    </div>
                  </div>
                </div>
                <div class="panel-body">

                    <!-- Row -->
                    <table class="table table-responsive" style="margin-top: 1em;">
                        <thead>
                            <th>#</th>
                            <th>Nome</th>
                            <th>Ações</th>
                        </thead>
                        <tbody>
                        @foreach ($grupos as $grupo)
                            <tr>
                                <td>#{{ $grupo->id }}</td>
                                <td>{{ $grupo->name }}</td>
                                <td></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                    <div class="row">
											<div class="col-md-12 text-center">
												Total de registros: <b>{{ $grupos->total() }}</b>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12 text-center">
													{!! $grupos->appends(\Request::except('page'))->render() !!}
											</div>
										</div>

                </div>
            </div>
        </div>
    </div>
@endsection
