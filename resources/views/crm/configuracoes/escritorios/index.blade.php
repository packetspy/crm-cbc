@extends('layouts.base')

@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                  <div class="row">
                    <div class="col-lg-12">
                      <h4 class="title">{{$titulo}} ({{ $escritorios->total() }})</h4>
                      <p class="category">{{$subtitulo}}</p>
                    </div>
                  </div>
                </div>
                <div class="panel-body">

                    <!-- Row -->
                    <table class="table table-responsive" style="margin-top: 1em;">
                        <thead>
                            <th>#</th>
                            <th>Nome</th>
                            <th>Sigla</th>
                            <th>Ações</th>
                        </thead>
                        <tbody>
                        @foreach ($escritorios as $escritorio)
                            <tr>
                                <td>#{{ $escritorio->id }}</td>
                                <td>{{ $escritorio->nome }}</td>
                                <td>{{ $escritorio->sigla }}</td>
                                <td></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                    <div class="row">
											<div class="col-md-12 text-center">
												Total de registros: <b>{{ $escritorios->total() }}</b>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12 text-center">
													{!! $escritorios->appends(\Request::except('page'))->render() !!}
											</div>
										</div>

                </div>
            </div>
        </div>
    </div>
@endsection
