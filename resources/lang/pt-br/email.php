<?php

return [
	'nenhum_encontrado' 			=> 'Nenhum e-mail cadastrado.',
	'titledelete' 			=> 'Excluir e-mail?',
	'textdelete'	=>	'Tem certeza que deseja excluir este e-mail?',
	'titlesuccess' => 'E-mail excluído!',
];
