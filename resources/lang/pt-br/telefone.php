<?php

return [
	'nenhum_encontrado' 			=> 'Nenhum e-mail cadastrado.',
	'titledelete' 			=> 'Excluir telefone?',
	'textdelete'	=>	'Tem certeza que deseja excluir este telefone?',
	'titlesuccess' => 'Telefone excluído!',
];
