<?php

return [
	'nenhum_encontrado' 			=> 'Nenhum cliente cadastrado.',
	'titledelete' 			=> 'Excluir cliente?',
	'textdelete'	=>	'<b>Certeza?</b> Quer mesmo excluir este Cliente, Contatos, Telefones e E-mails??',
	'titlesuccess' => 'Cliente excluído!',
];
