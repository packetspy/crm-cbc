<?php

return [
	'nenhum_encontrado' 			=> 'Nenhum produto cadastrado.',
	'titledelete' 			=> 'Excluir produto?',
	'textdelete'	=>	'Deseja excluir este produto e todos os estados?',
	'titlesuccess' => 'Produto excluído!',
];
