<?php

return [
	'nenhum_encontrado' 			=> 'Nenhum skype cadastrado.',
	'titledelete' 			=> 'Excluir skype?',
	'textdelete'	=>	'Tem certeza que deseja excluir este skype?',
	'titlesuccess' => 'Skype excluído!',
];
