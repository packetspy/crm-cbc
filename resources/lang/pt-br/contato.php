<?php

return [
	'nenhum_encontrado' 			=> 'Nenhum contato cadastrado.',
	'titledelete' 			=> 'Excluir contato?',
	'textdelete'	=>	'Deseja excluir este contato e todos as formas de contato?',
	'titlesuccess' => 'Contato excluído!',
	'sabedoria'	=>	'Apagar ou Mover contato? Use com sabedoria ;)',
];
