<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Direção CBC',
                'email' => 'crm@cbcnegocios.com.br',
                'password' => '$2y$10$/VAbvtolfgb6kBXYM7P7p.yoqEOfXacAxu5yyhFyULWlq6H2OYQl2',
                'remember_token' => '2lZ93ZGJURt045lvZXeAGrb7i5kozL18HIJHLSDiE8VUYuSTB0aqsSutcsmf',
                'created_at' => '2016-07-18 18:59:13',
                'updated_at' => '2017-01-11 17:01:52',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Alison',
                'email' => 'ex@cluir.com',
                'password' => '$2a$06$efGzZ8no1dGE2.KMIwEtb.omhZ4LEbSjgZ.EZ07KUGKQwUvIsu5P.',
                'remember_token' => 'vV5dsIr1LMlGf31pLPauE5JzheCdUCv2jk0ux1EJdgPWbk0qoZpKIEKyrNS1',
                'created_at' => '2016-07-14 12:32:53',
                'updated_at' => '2016-11-08 18:10:43',
                'deleted_at' => '2016-11-08 18:10:43',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Anderson Tomazeto',
                'email' => 'anderson@cbcnegocios.com.br',
                'password' => '$2a$06$efGzZ8no1dGE2.KMIwEtb.omhZ4LEbSjgZ.EZ07KUGKQwUvIsu5P.',
                'remember_token' => 'uE74dxP1gTrNxpadMnuD5Y2eZtmGsiwTSeq3uCn90japVLfYi9eZ3NslJBBy',
                'created_at' => '2016-07-14 12:33:12',
                'updated_at' => '2017-01-12 16:36:55',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Eder Campos',
                'email' => 'eder.campos@cbcnegocios.com.br',
                'password' => '$2a$06$efGzZ8no1dGE2.KMIwEtb.omhZ4LEbSjgZ.EZ07KUGKQwUvIsu5P.',
                'remember_token' => 'YVDGNpaHClfjSfGzX437TNAaCLH4TAv4zGMShHHSRFm8SWJeLcu0mkrFQ35r',
                'created_at' => '2016-07-14 12:33:35',
                'updated_at' => '2017-02-08 14:47:51',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Eduardo Fogaça',
                'email' => 'eduardo.fogaca@cbcnegocios.com.br',
                'password' => '$2a$06$efGzZ8no1dGE2.KMIwEtb.omhZ4LEbSjgZ.EZ07KUGKQwUvIsu5P.',
                'remember_token' => 'HvbSpDVYmHjdDCFxWPJ1bFCFdcEAtS5hf4lgjRjeEjIMPvndmSB2LjfMj0Qg',
                'created_at' => '2016-07-14 12:33:51',
                'updated_at' => '2016-11-08 18:35:42',
                'deleted_at' => '2016-11-08 18:35:42',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'Igor Evaristo',
                'email' => 'igor.evaristo@cbcnegocios.com.br',
                'password' => '$2a$06$efGzZ8no1dGE2.KMIwEtb.omhZ4LEbSjgZ.EZ07KUGKQwUvIsu5P.',
                'remember_token' => '6DyrPQ2OFF9VmXC65PBNt1B7JlG79lovgYwlRLnvuMgQsN9trhAijYhlsFtJ',
                'created_at' => '2016-07-14 12:34:07',
                'updated_at' => '2016-11-17 15:18:26',
                'deleted_at' => '2016-11-17 15:18:26',
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'Jonathan Santos',
                'email' => 'jonathan@cbcnegocios.com.br',
                'password' => '$2a$06$efGzZ8no1dGE2.KMIwEtb.omhZ4LEbSjgZ.EZ07KUGKQwUvIsu5P.',
                'remember_token' => 'E82Y5FcySHYK9sE7YaxKyEuBr2vvmE0QH7hBxYIzD7dAMul2E3nVmLd3OeFX',
                'created_at' => '2016-07-14 12:34:30',
                'updated_at' => '2017-02-08 11:35:48',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'Junior Rodrigues',
                'email' => 'junior@cbcnegocios.com.br',
                'password' => '$2a$06$efGzZ8no1dGE2.KMIwEtb.omhZ4LEbSjgZ.EZ07KUGKQwUvIsu5P.',
                'remember_token' => 'SmqI8QY5KG4XzVYLbpG6Gs5SIb2Y883yi5ITYfpSYIAraQ7XRJHo0YrlknQY',
                'created_at' => '2016-07-14 12:35:22',
                'updated_at' => '2016-09-08 10:41:43',
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'Karen de Ângelo Ambrósio',
                'email' => 'karen@cbcnegocios.com.br',
                'password' => '$2a$06$efGzZ8no1dGE2.KMIwEtb.omhZ4LEbSjgZ.EZ07KUGKQwUvIsu5P.',
                'remember_token' => 'mvDocCCXxK6zSMqVqeGrd3J45EaO4vYuoZVHXiyQCUWL16khaeAK7dPYJh5X',
                'created_at' => '2016-07-14 12:35:44',
                'updated_at' => '2017-01-24 16:58:19',
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'name' => 'Kelvin Silva',
                'email' => 'kelvin.silva@cbcnegocios.com.br',
                'password' => '$2a$06$efGzZ8no1dGE2.KMIwEtb.omhZ4LEbSjgZ.EZ07KUGKQwUvIsu5P.',
                'remember_token' => '4GdamCFWV8hoUi0TPy99VYqcZguhN6hJKwzunB8lwWgJohRYokdWrawNPZUv',
                'created_at' => '2016-07-14 12:36:11',
                'updated_at' => '2017-01-18 11:22:38',
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'name' => 'Lucas Rodrigues',
                'email' => 'lucas.rodrigues@cbcnegocios.com.br',
                'password' => '$2a$06$efGzZ8no1dGE2.KMIwEtb.omhZ4LEbSjgZ.EZ07KUGKQwUvIsu5P.',
                'remember_token' => 'eUi7SCpecHltfJOh4kF0pEbZOo34FAqlXjXDtL9vSGmQzOGya0pER6WTVGI0',
                'created_at' => '2016-07-14 12:36:31',
                'updated_at' => '2016-11-17 15:18:37',
                'deleted_at' => '2016-11-17 15:18:37',
            ),
            11 => 
            array (
                'id' => 12,
                'name' => 'Luis Fernando Vaz',
                'email' => 'luis.vaz@cbcnegocios.com.br',
                'password' => '$2a$06$efGzZ8no1dGE2.KMIwEtb.omhZ4LEbSjgZ.EZ07KUGKQwUvIsu5P.',
                'remember_token' => 'OxczXDgVBsAFZSETplA6k5pLoBWiNfOlpn8sdxS2cS0W1lRkqtelaxsLpoaT',
                'created_at' => '2016-07-14 12:36:49',
                'updated_at' => '2016-09-12 17:07:54',
                'deleted_at' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'name' => 'Regina Dourado',
                'email' => 'regina.dourado@cbcnegocios.com.br',
                'password' => '$2a$06$efGzZ8no1dGE2.KMIwEtb.omhZ4LEbSjgZ.EZ07KUGKQwUvIsu5P.',
                'remember_token' => 'coVqH3MdmAF7TaoQEbqt7ZCoo5qrCEQHhx3nUEw9rc57kaAMDPdKIe4SqHpg',
                'created_at' => '2016-07-14 12:38:36',
                'updated_at' => '2016-11-21 19:05:43',
                'deleted_at' => NULL,
            ),
            13 => 
            array (
                'id' => 14,
                'name' => 'Rodrigo Augusto do Carmo',
                'email' => 'rodrigo@cbcnegocios.com.br',
                'password' => '$2a$06$efGzZ8no1dGE2.KMIwEtb.omhZ4LEbSjgZ.EZ07KUGKQwUvIsu5P.',
                'remember_token' => 'Element',
                'created_at' => '2016-07-14 12:39:02',
                'updated_at' => '2016-12-09 15:02:04',
                'deleted_at' => '2016-12-09 15:02:04',
            ),
            14 => 
            array (
                'id' => 15,
                'name' => 'Carlos Eduardo Rodrigues',
                'email' => 'rodrigues@cbcnegocios.com.br',
                'password' => '$2a$06$efGzZ8no1dGE2.KMIwEtb.omhZ4LEbSjgZ.EZ07KUGKQwUvIsu5P.',
                'remember_token' => 'fLsPvXijv32fwn42qSIMfsc9GxAsdGnTl8yJcJpoUHcrQpQq57vMvBJEobgs',
                'created_at' => '2016-07-14 12:39:20',
                'updated_at' => '2016-12-02 11:48:25',
                'deleted_at' => NULL,
            ),
            15 => 
            array (
                'id' => 16,
                'name' => 'Sheila Geires',
                'email' => 'sheila@cbcnegocios.com.br',
                'password' => '$2a$06$efGzZ8no1dGE2.KMIwEtb.omhZ4LEbSjgZ.EZ07KUGKQwUvIsu5P.',
                'remember_token' => 'h6jy0uy0qYukvEranBSKeHMqiJyC1YU4b0xUtvmz2j5YPjNeZSIInvQe2TaV',
                'created_at' => '2016-07-14 12:39:33',
                'updated_at' => '2016-10-26 15:11:09',
                'deleted_at' => NULL,
            ),
            16 => 
            array (
                'id' => 17,
                'name' => 'Janaina Melo Lopes',
                'email' => 'janaina.melo@cbcnegocios.com.br',
                'password' => '$2a$06$efGzZ8no1dGE2.KMIwEtb.omhZ4LEbSjgZ.EZ07KUGKQwUvIsu5P.',
                'remember_token' => 'VHE46Rw5FIWYLMX4xbJFnlmVsD5U9GlD9GRRsIij3VXeyigAPZaZGyDgxzli',
                'created_at' => '2016-07-14 14:31:06',
                'updated_at' => '2016-07-20 09:34:09',
                'deleted_at' => NULL,
            ),
            17 => 
            array (
                'id' => 18,
                'name' => 'Renata Farias Failace',
                'email' => 'renata@cbcnegocios.com.br',
                'password' => '$2a$06$efGzZ8no1dGE2.KMIwEtb.omhZ4LEbSjgZ.EZ07KUGKQwUvIsu5P.',
                'remember_token' => 'Element',
                'created_at' => '2016-07-14 14:33:25',
                'updated_at' => '2016-07-14 14:33:25',
                'deleted_at' => NULL,
            ),
            18 => 
            array (
                'id' => 19,
                'name' => 'testapermissao',
                'email' => 'teste@asdasd.com',
                'password' => '$2a$06$efGzZ8no1dGE2.KMIwEtb.omhZ4LEbSjgZ.EZ07KUGKQwUvIsu5P.',
                'remember_token' => 'nTddG9AO6rRq0zzVdQX4m3cJQxAPNqRR2df29jPkA3YqkQMloB5EM9Frofw4',
                'created_at' => '2016-07-18 21:25:06',
                'updated_at' => '2016-11-10 09:43:46',
                'deleted_at' => '2016-11-10 09:43:46',
            ),
            19 => 
            array (
                'id' => 20,
                'name' => 'Tatiane Faria',
                'email' => 'tatiane.faria@cbcnegocios.com.br',
                'password' => '$2a$06$efGzZ8no1dGE2.KMIwEtb.omhZ4LEbSjgZ.EZ07KUGKQwUvIsu5P.',
                'remember_token' => '1Uf5MRsHatZjBEKY8BUrcWJF6dX2ZPdnolpKOaHsfbbF46gxgEdfnJ8rUugO',
                'created_at' => '2016-07-27 09:21:00',
                'updated_at' => '2016-11-25 18:11:52',
                'deleted_at' => NULL,
            ),
            20 => 
            array (
                'id' => 21,
                'name' => 'Melanie Lopes',
                'email' => 'excluir.melanie.lopes@cbcnegocios.com.br',
                'password' => '$2a$06$efGzZ8no1dGE2.KMIwEtb.omhZ4LEbSjgZ.EZ07KUGKQwUvIsu5P.',
                'remember_token' => 'Element',
                'created_at' => '2016-08-01 14:27:20',
                'updated_at' => '2016-11-08 18:34:48',
                'deleted_at' => '2016-11-08 18:34:48',
            ),
            21 => 
            array (
                'id' => 22,
                'name' => 'Andreia Marques',
                'email' => 'andreia.marques@cbcnegocios.com.br',
                'password' => '$2a$06$efGzZ8no1dGE2.KMIwEtb.omhZ4LEbSjgZ.EZ07KUGKQwUvIsu5P.',
                'remember_token' => 'Kezfrl5YekC6LlTGRRTbd8I9OQK9vhrIsoy8RanoU10U13fxI9sGHImJpqf1',
                'created_at' => '2016-08-04 10:19:03',
                'updated_at' => '2017-01-02 17:01:32',
                'deleted_at' => NULL,
            ),
            22 => 
            array (
                'id' => 23,
                'name' => 'Marcos Martinelli',
                'email' => 'marcos.martinelli@cbcnegocios.com.br',
                'password' => '$2a$06$efGzZ8no1dGE2.KMIwEtb.omhZ4LEbSjgZ.EZ07KUGKQwUvIsu5P.',
                'remember_token' => '4YdJY2Y1sKeWhUi1c2YmNGtbHhb3juRwVHuuWimRIURwkASUDtMIFV1ycClf',
                'created_at' => '2016-08-10 17:08:52',
                'updated_at' => '2017-01-24 12:20:50',
                'deleted_at' => NULL,
            ),
            23 => 
            array (
                'id' => 24,
                'name' => 'Gabriela Alvarez',
                'email' => 'gabriela.alvarez@cbcnegocios.com.br',
                'password' => '$2a$06$efGzZ8no1dGE2.KMIwEtb.omhZ4LEbSjgZ.EZ07KUGKQwUvIsu5P.',
                'remember_token' => 'Element',
                'created_at' => '2016-08-15 16:46:49',
                'updated_at' => '2016-09-02 09:29:57',
                'deleted_at' => NULL,
            ),
            24 => 
            array (
                'id' => 25,
                'name' => 'Natan Catach',
                'email' => 'natan@cbcnegocios.com.br',
                'password' => '$2a$06$efGzZ8no1dGE2.KMIwEtb.omhZ4LEbSjgZ.EZ07KUGKQwUvIsu5P.',
                'remember_token' => 'kzsuDXp4EjEU2gW8JqfdFbAubqJK526Df4GOMvkQb9AHBiOeccj2tA2rvawI',
                'created_at' => '2016-09-02 14:48:58',
                'updated_at' => '2017-01-24 15:52:37',
                'deleted_at' => NULL,
            ),
            25 => 
            array (
                'id' => 26,
                'name' => 'Denise Barros',
                'email' => 'denise.barros@cbcnegocios.com.br',
                'password' => '$2a$06$efGzZ8no1dGE2.KMIwEtb.omhZ4LEbSjgZ.EZ07KUGKQwUvIsu5P.',
                'remember_token' => 'cTatozwvsSWgq8bVgnbxRCYM9T9uIZZFuYLtqfSJR4dHqWwbvQDRngvrCsdj',
                'created_at' => '2016-09-08 09:38:05',
                'updated_at' => '2017-01-24 17:34:42',
                'deleted_at' => NULL,
            ),
            26 => 
            array (
                'id' => 27,
                'name' => 'Gabriel Mendes',
                'email' => 'gabriel.mendes@cbcnegocios.com.br',
                'password' => '$2a$06$efGzZ8no1dGE2.KMIwEtb.omhZ4LEbSjgZ.EZ07KUGKQwUvIsu5P.',
                'remember_token' => 'Element',
                'created_at' => '2016-09-19 11:09:15',
                'updated_at' => '2016-09-19 11:14:15',
                'deleted_at' => NULL,
            ),
            27 => 
            array (
                'id' => 28,
                'name' => 'Nicolly Almeida',
                'email' => 'nicolly.almeida@cbcnegocios.com.br',
                'password' => '$2a$06$efGzZ8no1dGE2.KMIwEtb.omhZ4LEbSjgZ.EZ07KUGKQwUvIsu5P.',
                'remember_token' => '30j0uAnQ1MDqUbWLGllhwDP8HRz10Ex1KbElAS5Fp2ntFZrXWhvrnoYzH3Lc',
                'created_at' => '2016-09-19 16:35:33',
                'updated_at' => '2017-01-20 17:15:26',
                'deleted_at' => NULL,
            ),
            28 => 
            array (
                'id' => 29,
                'name' => 'Operacional',
                'email' => 'operacional@cbcnegocios.com.br',
                'password' => '$2a$06$efGzZ8no1dGE2.KMIwEtb.omhZ4LEbSjgZ.EZ07KUGKQwUvIsu5P.',
                'remember_token' => 'Zw4iGbYl2UEmGr5ngBWZ9LyanUXaVqoJ6bjgfzu5sVPSXhCpAaCfZktHOYFT',
                'created_at' => '2016-09-21 16:38:46',
                'updated_at' => '2016-11-10 09:44:01',
                'deleted_at' => '2016-11-10 09:44:01',
            ),
            29 => 
            array (
                'id' => 30,
                'name' => 'Pamella Santos',
                'email' => 'pamella.santos@cbcnegocios.com.br',
                'password' => '$2a$06$efGzZ8no1dGE2.KMIwEtb.omhZ4LEbSjgZ.EZ07KUGKQwUvIsu5P.',
                'remember_token' => 'Element',
                'created_at' => '2016-10-04 18:09:02',
                'updated_at' => '2016-10-04 18:09:02',
                'deleted_at' => NULL,
            ),
            30 => 
            array (
                'id' => 31,
                'name' => 'Murilo Domingues',
                'email' => 'murilo.domingues@cbcnegocios.com.br',
                'password' => '$2a$06$efGzZ8no1dGE2.KMIwEtb.omhZ4LEbSjgZ.EZ07KUGKQwUvIsu5P.',
                'remember_token' => '6mZooLkzwSKq4mOP2dzVfsm3uCpAcSEReCOOeWrNi5a5wpnOoEjunhERvsot',
                'created_at' => '2016-10-05 15:41:34',
                'updated_at' => '2016-10-27 11:04:00',
                'deleted_at' => NULL,
            ),
            31 => 
            array (
                'id' => 32,
                'name' => 'Barbara Navarro',
                'email' => 'barbara.navarro@cbcnegocios.com.br',
                'password' => '$2a$06$efGzZ8no1dGE2.KMIwEtb.omhZ4LEbSjgZ.EZ07KUGKQwUvIsu5P.',
                'remember_token' => 'Z4rYNOTI3Jxmfv9T10esHmPwUN89B0XknMqEgImmJOslRthIJXfGLhEZ8LrP',
                'created_at' => '2016-10-21 10:14:02',
                'updated_at' => '2016-12-14 10:51:18',
                'deleted_at' => NULL,
            ),
            32 => 
            array (
                'id' => 33,
                'name' => 'Leonardo França',
                'email' => 'leonardo.franca@cbcnegocios.com.br',
                'password' => '$2a$06$efGzZ8no1dGE2.KMIwEtb.omhZ4LEbSjgZ.EZ07KUGKQwUvIsu5P.',
                'remember_token' => 'U7ZHhSkWUVk3itZRjYJwmiLj66xvhxvXc0Mwt1mqsFYkLKYSPZ4x3hMqkBbL',
                'created_at' => '2016-11-10 09:45:01',
                'updated_at' => '2017-01-03 11:01:27',
                'deleted_at' => NULL,
            ),
            33 => 
            array (
                'id' => 34,
                'name' => 'Bruno Morgado',
                'email' => 'bruno.morgado@cbcnegocios.com.br',
                'password' => '$2a$06$efGzZ8no1dGE2.KMIwEtb.omhZ4LEbSjgZ.EZ07KUGKQwUvIsu5P.',
                'remember_token' => 'tvxKLTweo36ZV2heFeC3UilSCcMhbSN7VIlx8fQI1X8QLTkD0qr2yVcPcWCs',
                'created_at' => '2016-11-14 12:04:39',
                'updated_at' => '2016-12-14 13:55:56',
                'deleted_at' => NULL,
            ),
            34 => 
            array (
                'id' => 35,
                'name' => 'Breno Casquet',
                'email' => 'breno.casquet@cbcnegocios.com.br',
                'password' => '$2a$06$efGzZ8no1dGE2.KMIwEtb.omhZ4LEbSjgZ.EZ07KUGKQwUvIsu5P.',
                'remember_token' => 'KLwP9PaqevoHEiXuDGFXKc68mbNhDlKXkxapq6zZB2WascafWNF1ips7SUP2',
                'created_at' => '2016-11-14 12:06:09',
                'updated_at' => '2017-01-09 17:30:27',
                'deleted_at' => NULL,
            ),
            35 => 
            array (
                'id' => 36,
                'name' => 'Ivan Wedekin',
                'email' => 'ivan.wedekin@cbcnegocios.com.br',
                'password' => '$2a$06$efGzZ8no1dGE2.KMIwEtb.omhZ4LEbSjgZ.EZ07KUGKQwUvIsu5P.',
                'remember_token' => 'Element',
                'created_at' => '2016-11-18 10:21:58',
                'updated_at' => '2016-11-18 10:21:58',
                'deleted_at' => NULL,
            ),
            36 => 
            array (
                'id' => 37,
                'name' => 'teste_agencia_kmc',
                'email' => 'teste@agenciakmc.com.br',
                'password' => '$2a$06$efGzZ8no1dGE2.KMIwEtb.omhZ4LEbSjgZ.EZ07KUGKQwUvIsu5P.',
                'remember_token' => 'V9tb0zMttai60TzC3k8mCYFtd5zyg1Vk8S49TMlCXANRgXV7LVwj0NHel4Jr',
                'created_at' => '2016-11-30 06:53:51',
                'updated_at' => '2017-01-26 15:00:45',
                'deleted_at' => NULL,
            ),
            37 => 
            array (
                'id' => 38,
                'name' => 'Adolpho Liberman',
                'email' => 'adolpho.liberman@cbcnegocios.com.br',
                'password' => '$2a$06$efGzZ8no1dGE2.KMIwEtb.omhZ4LEbSjgZ.EZ07KUGKQwUvIsu5P.',
                'remember_token' => 'eCKqthk0qrehyWgilJ5xXtAPO9Xd5t3Tj6Ld9XCro0UWV8GrB69MYsSdQKeQ',
                'created_at' => '2017-01-18 09:54:26',
                'updated_at' => '2017-01-19 09:16:18',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}
