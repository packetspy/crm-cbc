<?php

use Illuminate\Database\Seeder;

class S20170608TableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      //tipo empresa
      DB::table('tipoempresa')->insert([
          'tipo' => 'Pessoa Física',
      ]);
      DB::table('tipoempresa')->insert([
          'tipo' => 'Pessoa Jurídica',
      ]);
      DB::table('tipoempresa')->insert([
          'tipo' => 'Produtor Rural',
      ]);
      DB::table('tipoempresa')->insert([
          'tipo' => 'Corretor/Corretora',
      ]);
      DB::table('tipoempresa')->insert([
          'tipo' => 'Empresa estrangeira',
      ]);

      //tipo permissão para origem
      DB::table('permissions')->insert([
          'name' => 'origem.edit',
          'readable_name' => "Permite editar/atualizar o campo 'origem_id' do cliente",
      ]);
      DB::table('roles')->insert([
          'name' => 'Marketing',
      ]);
      DB::table('permission_role')->insert([
          'permission_id' => 32,
          'role_id' => 2,
          'value' => 1,
      ]);
      DB::table('permission_role')->insert([
          'permission_id' => 32,
          'role_id' => 3,
          'value' => 1,
      ]);
      DB::table('permission_role')->insert([
          'permission_id' => 32,
          'role_id' => 5,
          'value' => 1,
      ]);
    }
}
