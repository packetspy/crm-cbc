<?php

use Illuminate\Database\Seeder;

class NotifySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('notification_categories')->insert([
            'name' => 'client.negociofechado',
            'text' => '{from.name} fechou negócio com {extra.empresa}'
        ]);
    }
}
