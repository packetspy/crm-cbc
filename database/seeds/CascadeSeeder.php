<?php

use Illuminate\Database\Seeder;
use Artesaos\Defender\Permission;

class CascadeSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\User::class)
        ->create(["name" => env("NOME_TEST", "Teste")])
        ->each(function($user) {
            $permissions = Artesaos\Defender\Permission::all();
          
            foreach($permissions as $permission)
                $user->attachPermission($permission);



            ClientSeeder::create($user);
            //for($i = 0; $i < 100; $i++)
            //    ProductSeeder::create($user);
            InteractionTypeSeeder::create($user);
        });
    }
}
