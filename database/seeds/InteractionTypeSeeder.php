<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\InteractionType;

class InteractionTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //InteractionTypeSeeder::create(User::find(1));
    }

    public static function create(User $user)
    {
        $interacoes = [
            'entrar_contato', 
            'visitar_cliente', 
            'cadastrar_cliente', 
            'ativar_indicacao',
            'registrar_atendimento',
            'iniciar_contraparte',
            'contraparte'
        ];

        foreach($interacoes as $type) {
            InteractionTypeSeeder::createAnElement($user, $type);
        }

        InteractionTypeSeeder::childrens($user);
    }

    public static function childrens(User $user)
    {
        $father = InteractionTypeSeeder::getFather('Função');

        $types = [
            [
                "father" => "Responder contraparte",
                "childrens" => [
                    "negocio_fechado",
                    "negocio_nao_fechado"
                ]
            ],
            [
                "father" => "Não",
                "childrens" => [
                    "produto_ruim",
                    "negocio_direto",
                    "negocio_cancelado",
                    "preco_ruim"
                ]
            ]
        ];

        foreach($types as $type) {
            $father = InteractionTypeSeeder::getFather($type["father"]);
            foreach($type["childrens"] as $children) {
               InteractionTypeSeeder::createAnElement($user, $children, ['father_id' => $father->id]);
            }
        }
    }

    public static function getFather($name)
    {
        return InteractionType::where("nome", "=", $name)->first();
    }

    public static function createAnElement(User $user, $type, $make=[])
    {
        $user->interaction_types()
             ->save(
                factory(App\Models\InteractionType::class, $type)
                ->make($make)
            );
    }
}
