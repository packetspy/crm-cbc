<?php

use Illuminate\Database\Seeder;
use App\Models\Role;

class RolesTableSeeder extends Seeder
{
  /**
  * Run the database seeds.
  *
  * @return void
  */
  public function run()
  {
    // Role::truncate();

    foreach ([
      [
        'name'      => 'Consultores'
      ],
      [
        'name'      => 'Atendimento'
      ],
      [
        'name'      => 'Gestores'
      ]
      ] as $data) {
        Role::updateOrCreate($data);
      }
    }
  }
