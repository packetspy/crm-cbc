<?php

use Illuminate\Database\Seeder;

use App\Services\ProductService;
use App\Services\ContactService;
use App\Services\UserService;
use App\Services\ClientService;
use Artesaos\Defender\Facades\Defender;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $productService = new ProductService();
        $clientService = new ClientService();
        $contactService = new ContactService();
        $userService = new UserService();
        $calendarService = new \CbcClientes\RegraNegocio\CalendarService();
        $exportService = CbcClientes\RegraNegocio\ExportService::getPermissions();

        $permissions = array_merge(
            $productService->getPermissions(),
            $clientService->getPermissions(),
            $contactService->getPermissions(),
            $userService->getPermissions(),
            $calendarService->getPermissions(),
            $exportService
        );

        foreach ($permissions as $permission)
        {
            if (Defender::permissionExists($permission['id']) == false)
                Defender::createPermission($permission['id'], $permission['name']);
        }
    }
}
