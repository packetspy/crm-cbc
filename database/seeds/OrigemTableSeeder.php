<?php

use Illuminate\Database\Seeder;

class OrigemTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

      DB::table('origem')->insert([
          'origem' => 'Indefinido',
          'created_at' => '2017-05-22 15:00:00'
      ]);
      DB::table('origem')->insert([
          'origem' => 'Acesso Direto',
          'created_at' => '2017-05-22 15:00:00'
      ]);
      DB::table('origem')->insert([
          'origem' => 'Busca orgânica',
          'created_at' => '2017-05-22 15:00:00'
      ]);
      DB::table('origem')->insert([
          'origem' => 'Evento: ExpoMeat',
          'created_at' => '2017-05-22 15:00:00'
      ]);
      DB::table('origem')->insert([
          'origem' => 'FB',
          'created_at' => '2017-05-22 15:00:00'
      ]);
      DB::table('origem')->insert([
          'origem' => 'FB: Lead',
          'created_at' => '2017-05-22 15:00:00'
      ]);
      DB::table('origem')->insert([
          'origem' => 'Display',
          'created_at' => '2017-05-22 15:00:00'
      ]);
      DB::table('origem')->insert([
          'origem' => 'Lead: Jean',
          'created_at' => '2017-05-22 15:00:00'
      ]);
      DB::table('origem')->insert([
          'origem' => 'Parceiro: Biofilica',
          'created_at' => '2017-05-22 15:00:00'
      ]);
      DB::table('origem')->insert([
          'origem' => 'Site: BotaoCadastre-se',
          'created_at' => '2017-05-22 15:00:00'
      ]);
      DB::table('origem')->insert([
          'origem' => 'Site: BotaoNavague',
          'created_at' => '2017-05-22 15:00:00'
      ]);

      DB::table('products')->insert([
          'nome' => 'Soja',
          'user_id' => 1,
          'created_at' => '2017-05-22 15:00:00'
      ]);
      DB::table('products')->insert([
          'nome' => 'Milho',
          'user_id' => 1,
          'created_at' => '2017-05-22 15:00:00'
      ]);
      DB::table('products')->insert([
          'nome' => 'Trigo',
          'user_id' => 1,
          'created_at' => '2017-05-22 15:00:00'
      ]);
      DB::table('products')->insert([
          'nome' => 'Etanol',
          'user_id' => 1,
          'created_at' => '2017-05-22 15:00:00'
      ]);
      DB::table('products')->insert([
          'nome' => 'Açúcar',
          'user_id' => 1,
          'created_at' => '2017-05-22 15:00:00'
      ]);
      DB::table('products')->insert([
          'nome' => 'Sal mineral',
          'user_id' => 1,
          'created_at' => '2017-05-22 15:00:00'
      ]);
      DB::table('products')->insert([
          'nome' => 'Sal proteinado',
          'user_id' => 1,
          'created_at' => '2017-05-22 15:00:00'
      ]);
    }
}
