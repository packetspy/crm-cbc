<?php

use Illuminate\Database\Seeder;

class ProductFromAnotherSystemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    protected $products = [];



    public function run()
    {

        $products = array (
            0 =>
                array (
                    'nome' => 'Milho Transgênico ',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            1 =>
                array (
                    'nome' => 'Milho Convencional',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            2 =>
                array (
                    'nome' => 'Soja Transgênica ',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            3 =>
                array (
                    'nome' => 'Farelo de Soja',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            4 =>
                array (
                    'nome' => 'Soja Convencional',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            5 =>
                array (
                    'nome' => 'Sorgo',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            6 =>
                array (
                    'nome' => 'Algodão em Pluma',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            7 =>
                array (
                    'nome' => 'Milheto',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            8 =>
                array (
                    'nome' => 'Soja Transgênica (Exportação)',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            9 =>
                array (
                    'nome' => 'Milho Transgênico (Exportação)',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            10 =>
                array (
                    'nome' => 'Milho Convencional (Exportação)',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            11 =>
                array (
                    'nome' => 'Farelo de Trigo',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            12 =>
                array (
                    'nome' => 'Trigo em Grãos',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            13 =>
                array (
                    'nome' => 'Soja Convencional (Exportação)',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            14 =>
                array (
                    'nome' => 'Fio 12/1 MALHARIA OEM 100% ALGODÃO MASSA PURA ',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            15 =>
                array (
                    'nome' => 'Fio 150/48 Poliéster Texturizado ',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            16 =>
                array (
                    'nome' => 'Fio 12/1 TECELAGEM OET 100% ALGODÃO MASSA PURA ',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            17 =>
                array (
                    'nome' => 'Aveia ',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            18 =>
                array (
                    'nome' => 'Fio 16/1 MALHARIA OEM 100% ALGODÃO MASSA PURA ',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            19 =>
                array (
                    'nome' => 'Algodão',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            20 =>
                array (
                    'nome' => 'Milho (Semente)',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            21 =>
                array (
                    'nome' => 'Fios de algodão e sintético',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            22 =>
                array (
                    'nome' => 'Soja (Semente)',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            23 =>
                array (
                    'nome' => 'Fio 200/144 Texturizado Ar 100% Poliamida Nylon',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            24 =>
                array (
                    'nome' => 'Fio 20/1 MALHARIA OEM 100% ALGODÃO MASSA PURA ',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            25 =>
                array (
                    'nome' => 'Fio 16/1 TECELAGEM OET 100% ALGODÃO MASSA PURA ',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            26 =>
                array (
                    'nome' => 'Fio 20/1 TECELAGEM  OET  100% ALGODÃO MASSA PURA ',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            27 =>
                array (
                    'nome' => 'Caroço de Algodão',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            28 =>
                array (
                    'nome' => 'Feijão',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            29 =>
                array (
                    'nome' => 'Fio 200/96 Texturizado Ar 100% Poliamida Ny.Mescla',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            30 =>
                array (
                    'nome' => 'Fio 24/1 MALHARIA OEM 100% ALGODÃO MASSA PURA ',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            31 =>
                array (
                    'nome' => 'Farelo de Milho',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            32 =>
                array (
                    'nome' => 'Fio 30/1 100% Viscose OE',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            33 =>
                array (
                    'nome' => 'Farelo de Soja Convencional ',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            34 =>
                array (
                    'nome' => 'Óleo de Soja Degomado',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            35 =>
                array (
                    'nome' => 'Farelo de Soja (Exportação)',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            36 =>
                array (
                    'nome' => 'Fio 30/1 MALHARIA OEM 100% ALGODÃO MASSA PURA ',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            37 =>
                array (
                    'nome' => 'Farelo de Algodão',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            38 =>
                array (
                    'nome' => 'Casca de Soja Peletizada',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            39 =>
                array (
                    'nome' => 'Fio Elastano 20 DEN ',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            40 =>
                array (
                    'nome' => 'Fio 24/1 Pac 65% Poliéster 35% Algodão ',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            41 =>
                array (
                    'nome' => 'Trigo (Exportação) ',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            42 =>
                array (
                    'nome' => 'Fio 30/1 PENTEADO 100% ALGODÃO MASSA PURA ',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            43 =>
                array (
                    'nome' => 'Fio 30/1 Pac 65% Poliéster 35% Algodão ',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            44 =>
                array (
                    'nome' => 'Fio 8/1 TECELAGEM OET 100% ALGODÃO MASSA PURA ',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            45 =>
                array (
                    'nome' => 'Fio 30/1 mescla 88/12',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            46 =>
                array (
                    'nome' => 'Arroz',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            47 =>
                array (
                    'nome' => 'Soja Desativada',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            48 =>
                array (
                    'nome' => 'Fio 8/1 MALHARIA OEM 100% ALGODÃO MASSA PURA ',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            49 =>
                array (
                    'nome' => 'Farelo de Soja Hipro (Exportação)',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            50 =>
                array (
                    'nome' => 'Farinha de Carne e Osso',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            51 =>
                array (
                    'nome' => 'Farelo de Soja Hipro',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            52 =>
                array (
                    'nome' => 'Casca de Soja Moída ',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            53 =>
                array (
                    'nome' => 'Café',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            54 =>
                array (
                    'nome' => 'Fio 30/1 Poliéster Fiado 100% Poliéster  ',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            55 =>
                array (
                    'nome' => 'Soja Integral ',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            56 =>
                array (
                    'nome' => 'Torta Algodão',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            57 =>
                array (
                    'nome' => 'Óleo de Soja Refinado',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            58 =>
                array (
                    'nome' => 'Fio (DUPLICADO) 8/1 TECELAGEM OET 100% ',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            59 =>
                array (
                    'nome' => 'Farinha de Trigo ',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            60 =>
                array (
                    'nome' => 'Óleo de Soja Bruto',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            61 =>
                array (
                    'nome' => 'Polpa cítrica',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            62 =>
                array (
                    'nome' => 'Caroço de Algodão (Exportação)',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            63 =>
                array (
                    'nome' => 'Fio Elastano 40 DEN ',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            64 =>
                array (
                    'nome' => 'Ácido Fosfórico',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            65 =>
                array (
                    'nome' => 'Farelo de Amendoim',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            66 =>
                array (
                    'nome' => 'Germen de Milho',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            67 =>
                array (
                    'nome' => 'Farelo de Soja Semi-Integral',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            68 =>
                array (
                    'nome' => 'Açúcar Cristal',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            69 =>
                array (
                    'nome' => 'Óleo de Soja Degomado (Exportação)',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            70 =>
                array (
                    'nome' => 'Óleo de Algodão Bruto',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            71 =>
                array (
                    'nome' => 'Sebo Bovino',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            72 =>
                array (
                    'nome' => 'Ácido Graxo ',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            73 =>
                array (
                    'nome' => 'Quirera de Milho',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            74 =>
                array (
                    'nome' => 'Cloreto de Potássio - KCl',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            75 =>
                array (
                    'nome' => 'Bagaço de cana',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            76 =>
                array (
                    'nome' => 'Óleo de Soja Refinado (Exportação) ',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            77 =>
                array (
                    'nome' => 'Fio 70/72 Texturizado Full Dull Poliamida Nylon',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            78 =>
                array (
                    'nome' => 'Açúcar VHP',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            79 =>
                array (
                    'nome' => 'Fosfato Monoamônico - MAP',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            80 =>
                array (
                    'nome' => 'Fio Elastano 70 DEN ',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            81 =>
                array (
                    'nome' => 'Aveia (Semente)',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            82 =>
                array (
                    'nome' => 'Açúcar Refinado',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            83 =>
                array (
                    'nome' => 'Fio 30/1PV Mescla 53% Poli. Cru 12% Preto 35% Visc',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            84 =>
                array (
                    'nome' => 'Farelo de Soja Semi- integral (Exportação)',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            85 =>
                array (
                    'nome' => 'Etanol Hidratado Combustivel',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            86 =>
                array (
                    'nome' => 'Feno ',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            87 =>
                array (
                    'nome' => 'Biodiesel',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            88 =>
                array (
                    'nome' => 'Borra de Soja',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            89 =>
                array (
                    'nome' => 'Sorgo (Semente)',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            90 =>
                array (
                    'nome' => 'Capim Sudão',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            91 =>
                array (
                    'nome' => 'Mentol Cristal ',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            92 =>
                array (
                    'nome' => 'Sulfato de Cálcio (gesso agrícola)',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            93 =>
                array (
                    'nome' => 'Fio 75/36 Poliéster Texturizado ',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            94 =>
                array (
                    'nome' => 'Fio 30/1 PV 65% Poliéster 35% Viscose',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            95 =>
                array (
                    'nome' => 'Sebo Bovino Refinado',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            96 =>
                array (
                    'nome' => 'Óleo Vegetal (Recuperado)',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            97 =>
                array (
                    'nome' => 'Amendoim',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            98 =>
                array (
                    'nome' => 'Esterco de Aves ',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            99 =>
                array (
                    'nome' => 'Ácido Cítrico ',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            100 =>
                array (
                    'nome' => 'Etanol Anidro Outros Fins',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            101 =>
                array (
                    'nome' => 'Farinha Animal',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            102 =>
                array (
                    'nome' => 'Óleo De Palma',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            103 =>
                array (
                    'nome' => 'Óleo de Algodão Semi-Refinado',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            104 =>
                array (
                    'nome' => 'Etanol Hidratado Outros Fins',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            105 =>
                array (
                    'nome' => 'Farelo de Algodão (Exportação)',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            106 =>
                array (
                    'nome' => 'Glicerina',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            107 =>
                array (
                    'nome' => 'Farinha de Sangue',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            108 =>
                array (
                    'nome' => 'Big Bag ',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            109 =>
                array (
                    'nome' => 'Etanol Anidro Combustivel',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            110 =>
                array (
                    'nome' => 'Trigo em Grâos (sobre rodas)',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            111 =>
                array (
                    'nome' => 'Açúcar Mascavo ',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            112 =>
                array (
                    'nome' => 'Farinha de Vísceras de Frango',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            113 =>
                array (
                    'nome' => 'Capim Sudão Estribo',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            114 =>
                array (
                    'nome' => 'Laranja',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            115 =>
                array (
                    'nome' => 'Óleo de Milho Bruto',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            116 =>
                array (
                    'nome' => 'Ovo Branco',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            117 =>
                array (
                    'nome' => 'Superfosfato Triplo  TSP',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            118 =>
                array (
                    'nome' => 'Óleo de Vísceras',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            119 =>
                array (
                    'nome' => 'Óleo de Algodão Bruto (Exportação)',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            120 =>
                array (
                    'nome' => 'Açúcar VVHP',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            121 =>
                array (
                    'nome' => 'Farinha de Peixe',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            122 =>
                array (
                    'nome' => 'Glicerina Loira',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            123 =>
                array (
                    'nome' => 'Graxa Suína',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            124 =>
                array (
                    'nome' => 'Fertilizante',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            125 =>
                array (
                    'nome' => 'Óleo de Algodão Semi-Refinado (Exportação)',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            126 =>
                array (
                    'nome' => 'Resíduo de Milho',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            127 =>
                array (
                    'nome' => 'Metanol ',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            128 =>
                array (
                    'nome' => 'Resíduo de Feijão ',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            129 =>
                array (
                    'nome' => 'Ureia',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            130 =>
                array (
                    'nome' => 'Borra Mista',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            131 =>
                array (
                    'nome' => 'Amendoim (Exportação) ',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            132 =>
                array (
                    'nome' => 'Ácido Sulfúrico',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            133 =>
                array (
                    'nome' => 'Etanol Neutro',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            134 =>
                array (
                    'nome' => 'Glicerina Bi-Destilada',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            135 =>
                array (
                    'nome' => 'Etanol Hidratado Combustível (exportação)',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            136 =>
                array (
                    'nome' => 'Glicerina Bruta Vegetal',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            137 =>
                array (
                    'nome' => 'Resíduo de Soja',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            138 =>
                array (
                    'nome' => 'Lecitina de Soja',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            139 =>
                array (
                    'nome' => 'Ovo Vermelho',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            140 =>
                array (
                    'nome' => 'Alho ',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            141 =>
                array (
                    'nome' => 'Farelo SPC',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            142 =>
                array (
                    'nome' => 'Bagaço de Laranja Peletizado ',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            143 =>
                array (
                    'nome' => 'Farelo De Arroz',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            144 =>
                array (
                    'nome' => 'Equipamentos',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            145 =>
                array (
                    'nome' => 'Óleo de Milho Refinado',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            146 =>
                array (
                    'nome' => 'Banha Suína',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            147 =>
                array (
                    'nome' => 'Fosfato Natural Reativo - FNR',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            148 =>
                array (
                    'nome' => 'Ácido Bórico',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            149 =>
                array (
                    'nome' => 'Leite em Pó Integral',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            150 =>
                array (
                    'nome' => 'Enxofre',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            151 =>
                array (
                    'nome' => 'Sacaria de Rafia ',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            152 =>
                array (
                    'nome' => 'Nitrato de Amônio',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            153 =>
                array (
                    'nome' => 'Ácido Nítrico',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            154 =>
                array (
                    'nome' => 'Leite em Pó Desnatado',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            155 =>
                array (
                    'nome' => 'Orgânico',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            156 =>
                array (
                    'nome' => 'Etanol Hidratado Industrial B Grade',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            157 =>
                array (
                    'nome' => 'CALCÁRIO DOLOMÍTICO',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            158 =>
                array (
                    'nome' => 'Sementes de Pastagem ',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            159 =>
                array (
                    'nome' => 'Bagaço de Laranja ',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            160 =>
                array (
                    'nome' => 'Esterco Bovino ',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            161 =>
                array (
                    'nome' => 'Melaço de Soja ',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            162 =>
                array (
                    'nome' => 'Fibrilha ',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            163 =>
                array (
                    'nome' => 'Cacau',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            164 =>
                array (
                    'nome' => 'Briquete de Algodão',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            165 =>
                array (
                    'nome' => 'Água de Coco ',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            166 =>
                array (
                    'nome' => 'Linter de Algodão',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            167 =>
                array (
                    'nome' => 'Fosmais 00-21-00',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            168 =>
                array (
                    'nome' => 'Fosfato Acidulado',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            169 =>
                array (
                    'nome' => 'Linter de Algodão (Exportação)',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            170 =>
                array (
                    'nome' => 'Fosfato Natural Reativo  -FNR',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            171 =>
                array (
                    'nome' => 'Cloreto de Potássio',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            172 =>
                array (
                    'nome' => 'Soda Caústica ',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            173 =>
                array (
                    'nome' => 'Farinha de Penas',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            174 =>
                array (
                    'nome' => 'Suco de Uva ',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            175 =>
                array (
                    'nome' => 'Farelo De Girassol',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            176 =>
                array (
                    'nome' => 'Ácido Clorídrico ',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            177 =>
                array (
                    'nome' => 'Girassol ',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            178 =>
                array (
                    'nome' => 'Castanha do Pará (Pedaço)',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            179 =>
                array (
                    'nome' => 'Suco Tropical Concentrado ',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            180 =>
                array (
                    'nome' => 'Castanha do Pará (Granulada)',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            181 =>
                array (
                    'nome' => 'Fertifaz 03-17-00',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            182 =>
                array (
                    'nome' => 'Farelo de Casca de Arroz',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            183 =>
                array (
                    'nome' => 'Refinazil',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            184 =>
                array (
                    'nome' => 'Açai (Polpa) ',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            185 =>
                array (
                    'nome' => 'Fertilizante Folhas Completo ',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            186 =>
                array (
                    'nome' => 'Melaço de cana',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            187 =>
                array (
                    'nome' => 'Óleo de Girassol Refinado',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            188 =>
                array (
                    'nome' => 'Milho de Pipoca ',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            189 =>
                array (
                    'nome' => 'Hidróxido de Sódio ',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            190 =>
                array (
                    'nome' => 'Óleo de Peixe',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            191 =>
                array (
                    'nome' => 'Fertilizante Sais ',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            192 =>
                array (
                    'nome' => 'Óleo de Girassol Bruto',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            193 =>
                array (
                    'nome' => 'Óleo de Amendoim Bruto',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            194 =>
                array (
                    'nome' => 'Óleo De Girassol Alto Oleico Refinado',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            195 =>
                array (
                    'nome' => 'Óleo De Girassol Alto Oleico Bruto',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            196 =>
                array (
                    'nome' => 'Semente de Girassol',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            197 =>
                array (
                    'nome' => 'Palmito Processado',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            198 =>
                array (
                    'nome' => 'Batata',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            199 =>
                array (
                    'nome' => ' Oleína animal',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            200 =>
                array (
                    'nome' => 'Fosfato Decantado',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            201 =>
                array (
                    'nome' => 'Amônia Anidra ',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            202 =>
                array (
                    'nome' => 'Palmito in Natura',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            203 =>
                array (
                    'nome' => 'Tomate',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            204 =>
                array (
                    'nome' => 'Suplementos',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            205 =>
                array (
                    'nome' => 'Borra de Enxofre',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            206 =>
                array (
                    'nome' => 'Fertilizante Folhas 06-06-08',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            207 =>
                array (
                    'nome' => 'Tangerina Ponkan',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            208 =>
                array (
                    'nome' => 'Farelo/Torta De Mamona',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            209 =>
                array (
                    'nome' => 'Fosfato Precipitado',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            210 =>
                array (
                    'nome' => 'Quirera De Arroz',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            211 =>
                array (
                    'nome' => 'Fosmais 00-19-00',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            212 =>
                array (
                    'nome' => 'Fertilizante Folhas K 50',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            213 =>
                array (
                    'nome' => 'Glicerina Bruta ( EXCLUIR)',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            214 =>
                array (
                    'nome' => 'Fertilizante Folhas 20-10-10',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            215 =>
                array (
                    'nome' => 'Fertilizante Folhas 20-05-08 ',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            216 =>
                array (
                    'nome' => 'Chia ',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            217 =>
                array (
                    'nome' => 'Mandioca',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            218 =>
                array (
                    'nome' => 'Óleo De Babaçu Bruto',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            219 =>
                array (
                    'nome' => 'Medicamentos',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            220 =>
                array (
                    'nome' => 'Sulfato de Magnésio',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            221 =>
                array (
                    'nome' => 'Quirera de Soja',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            222 =>
                array (
                    'nome' => 'Hemoglobina em Pó',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            223 =>
                array (
                    'nome' => 'Farelo De Canola',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            224 =>
                array (
                    'nome' => 'Óleo De Arroz Bruto',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            225 =>
                array (
                    'nome' => 'Cloreto de Cálcio',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            226 =>
                array (
                    'nome' => 'Gergelim ',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            227 =>
                array (
                    'nome' => 'CALCÁRIO CÍTICO',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            228 =>
                array (
                    'nome' => 'Ureia Técnica ',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            229 =>
                array (
                    'nome' => 'Óleo De Peixe (Ômega 3)',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            230 =>
                array (
                    'nome' => 'Óleo De Arroz Dicerado Degomado',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            231 =>
                array (
                    'nome' => 'Farelo De Palmiste',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            232 =>
                array (
                    'nome' => 'Farinha de Trigo Especial Panificação',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            233 =>
                array (
                    'nome' => 'Hemoglobina',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            234 =>
                array (
                    'nome' => 'Óleo de Canola Refinado',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            235 =>
                array (
                    'nome' => 'Fertilizante Folhas 10-10-10',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            236 =>
                array (
                    'nome' => 'Farinha de Osso Calcinada',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            237 =>
                array (
                    'nome' => 'Plasma em Pó',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            238 =>
                array (
                    'nome' => 'Farinha de Trigo Especial para Macarrão',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            239 =>
                array (
                    'nome' => 'Amido ',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            240 =>
                array (
                    'nome' => 'Bezerro',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            241 =>
                array (
                    'nome' => 'Óleo de Canola Bruto',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            242 =>
                array (
                    'nome' => 'Farinha de Trigo Especial Massa Fresca',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            243 =>
                array (
                    'nome' => 'Cacau em Pó ',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            244 =>
                array (
                    'nome' => 'Óleo De Salmão',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            245 =>
                array (
                    'nome' => 'Farinha de Trigo Inter. Biscoitos Diversos',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            246 =>
                array (
                    'nome' => 'Touro',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            247 =>
                array (
                    'nome' => 'Fertilizante Foliar ',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            248 =>
                array (
                    'nome' => 'Novilha',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            249 =>
                array (
                    'nome' => 'Vaca ',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            250 =>
                array (
                    'nome' => 'Capa de Fardo de Algodão',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            251 =>
                array (
                    'nome' => 'Boi Magro',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            252 =>
                array (
                    'nome' => 'Barrilha',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            253 =>
                array (
                    'nome' => 'Garrote',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            254 =>
                array (
                    'nome' => 'Ácido Acético',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            255 =>
                array (
                    'nome' => 'Acetato de Etila',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            256 =>
                array (
                    'nome' => 'Aproach Prima L',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            257 =>
                array (
                    'nome' => 'Fécula de Mandioca ',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            258 =>
                array (
                    'nome' => 'DDGS',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            259 =>
                array (
                    'nome' => 'Farinha de Trigo Comum para Macarrâo',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            260 =>
                array (
                    'nome' => 'Tangerina Dekopon',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            261 =>
                array (
                    'nome' => 'Alfafa',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            262 =>
                array (
                    'nome' => 'Polisorbato',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            263 =>
                array (
                    'nome' => 'Xileno',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            264 =>
                array (
                    'nome' => 'BIM 750 BR ',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            265 =>
                array (
                    'nome' => 'Actara',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            266 =>
                array (
                    'nome' => 'Belt',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            267 =>
                array (
                    'nome' => 'Aguarrás Mineral ',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            268 =>
                array (
                    'nome' => ' Óleo de Babaçu - PKO',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
            269 =>
                array (
                    'nome' => 'Cantus 500 Mg',
                    'descricao' => 'Não informado',
                    'ativo' => 1,
                    'user_id' => 1,
                ),
        );

        $products_to_save = [];
        foreach ($products as $product)
        {

            $newProduct = [];
            $newProduct["nome"] = $product["nome"];
            $newProduct["ativo"] = $product["ativo"];
            $newProduct["user_id"] = $product["user_id"];
            $newProduct["created_at"] = date('Y-m-d H:i:s');
            $newProduct["updated_at"] = date('Y-m-d H:i:s');

            $products_to_save[] = $newProduct;
        }

        App\Models\Product::insert($products_to_save);
    }

}
