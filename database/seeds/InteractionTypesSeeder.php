<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class InteractionTypesSeeder extends Seeder
{

	private $interationTypes = [];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
			$interationTypesDB = App\Models\InteractionType::get()->toArray();


        $interationTypesNew = [
            0 => [

                'nome' => "Cliente não tem interesse",
                'exibir_formulario'=>0,
                'ativo'=>1,
                'user_id'=>1,
                'created_at'=>Carbon::now(),
                'data_obrigatoria'=>0,
                'observacao_obrigatoria'=>0,
                'funcao'=>1,
                'exibe_apenas_contraparte'=>1

            ],
            1 => [
                'nome' => "Cliente não está apto",
                'exibir_formulario'=>0,
                'ativo'=>1,
                'user_id'=>1,
                'created_at'=>Carbon::now(),
                'data_obrigatoria'=>0,
                'observacao_obrigatoria'=>0,
                'funcao'=>1,
                'exibe_apenas_contraparte'=>1
            ],
						2 => [
                'nome' => "Cliente direcionado",
                'exibir_formulario'=>0,
                'ativo'=>1,
                'user_id'=>1,
                'created_at'=>Carbon::now(),
                'data_obrigatoria'=>0,
                'observacao_obrigatoria'=>0,
                'funcao'=>1,
                'exibe_apenas_contraparte'=>1
            ],
						3 => [
                'nome' => "Cliente direcionado teste",
                'exibir_formulario'=>0,
                'ativo'=>1,
                'user_id'=>1,
                'created_at'=>Carbon::now(),
                'data_obrigatoria'=>0,
                'observacao_obrigatoria'=>0,
                'funcao'=>1,
                'exibe_apenas_contraparte'=>1
            ]
        ];

				// dd($interationTypesNew);

				$interactionExist = 0;
				$i = 0;

				foreach($interationTypesNew as $interaction)
				{
					foreach($interationTypesDB as $interactionDB)
					{
						if($interactionDB['nome'] == $interaction['nome'])
						{
							$interactionExist = 1;
						}
					}

					if($interactionExist == 0)
					{
						App\Models\InteractionType::create($interationTypesNew[$i]);
					}
					else {
						$interactionExist = 0;
					}

					$i++;

				}


    }
}
