<?php

use Illuminate\Database\Seeder;

class S20170614TableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      //tipo empresa
      DB::table('escritorios')->insert([
          'nome' => 'Matriz São Paulo',
          'sigla' => 'M-SP',
      ]);
      DB::table('escritorios')->insert([
          'nome' => 'Filial Belo Horizonte',
          'sigla' => 'F-BH',
      ]);

      DB::statement("UPDATE clients SET escritorio_id = 1");
      DB::statement("UPDATE users SET escritorio_id = 1");

      DB::table('permission_role')->insert([
          'permission_id' => 16,
          'role_id' => 1,
          'value' => 1,
      ]);
    }
}
