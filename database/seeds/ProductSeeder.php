<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
    }

    public static function create(User $user)
    {
        $user->products()->save(factory(App\Models\Product::class)->make());
    }
}
