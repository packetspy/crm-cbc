<?php

use Illuminate\Database\Seeder;

class S20170620TableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Defender::createRole('Hábito');
      Defender::createRole('Liquidez');

      // Associa usuarios ao grupo Hábito
      DB::table('role_user')->insert([
          ['user_id' => 22, 'role_id' => 6],
          ['user_id' => 43, 'role_id' => 6],
          ['user_id' => 44, 'role_id' => 6],
          ['user_id' => 16, 'role_id' => 6],
          ['user_id' => 42, 'role_id' => 6],
          ['user_id' => 17, 'role_id' => 6],
          ['user_id' => 40, 'role_id' => 6],
          ['user_id' => 41, 'role_id' => 6],
      ]);

      // Associa usuarios ao grupo Liquidez
      DB::table('role_user')->insert([
          ['user_id' => 15, 'role_id' => 7],
          ['user_id' => 9, 'role_id' => 7],
          ['user_id' => 8, 'role_id' => 7],
          ['user_id' => 32, 'role_id' => 7],
          ['user_id' => 35, 'role_id' => 7],
      ]);

      // Associa usuarios ao grupo Marketing
      DB::table('role_user')->insert([
          ['user_id' => 7, 'role_id' => 5],
          ['user_id' => 26, 'role_id' => 5],
          ['user_id' => 27, 'role_id' => 5],
      ]);

      //Limpa tabele de Roles X Permission
      DB::table('permission_role')->truncate();
      //Readiciona permissoes para as roles
      DB::table('permission_role')->insert([
          //Permissoes para grupo Gestoress
          ['permission_id' => 1, 'role_id' => 3, 'value' => 1],
          ['permission_id' => 2, 'role_id' => 3, 'value' => 1],
          ['permission_id' => 3, 'role_id' => 3, 'value' => 1],
          ['permission_id' => 4, 'role_id' => 3, 'value' => 1],
          ['permission_id' => 5, 'role_id' => 3, 'value' => 1],
          ['permission_id' => 6, 'role_id' => 3, 'value' => 1],
          ['permission_id' => 7, 'role_id' => 3, 'value' => 1],
          ['permission_id' => 8, 'role_id' => 3, 'value' => 1],
          ['permission_id' => 9, 'role_id' => 3, 'value' => 1],
          ['permission_id' => 10, 'role_id' => 3, 'value' => 1],
          ['permission_id' => 11, 'role_id' => 3, 'value' => 1],
          ['permission_id' => 12, 'role_id' => 3, 'value' => 1],
          ['permission_id' => 13, 'role_id' => 3, 'value' => 1],
          ['permission_id' => 14, 'role_id' => 3, 'value' => 1],
          ['permission_id' => 15, 'role_id' => 3, 'value' => 1],
          ['permission_id' => 16, 'role_id' => 3, 'value' => 1],
          ['permission_id' => 17, 'role_id' => 3, 'value' => 1],
          ['permission_id' => 18, 'role_id' => 3, 'value' => 1],
          ['permission_id' => 19, 'role_id' => 3, 'value' => 1],
          ['permission_id' => 20, 'role_id' => 3, 'value' => 1],
          ['permission_id' => 21, 'role_id' => 3, 'value' => 1],
          ['permission_id' => 22, 'role_id' => 3, 'value' => 1],
          ['permission_id' => 23, 'role_id' => 3, 'value' => 1],
          ['permission_id' => 24, 'role_id' => 3, 'value' => 1],
          ['permission_id' => 25, 'role_id' => 3, 'value' => 1],
          ['permission_id' => 26, 'role_id' => 3, 'value' => 1],
          ['permission_id' => 27, 'role_id' => 3, 'value' => 1],
          ['permission_id' => 28, 'role_id' => 3, 'value' => 1],
          ['permission_id' => 29, 'role_id' => 3, 'value' => 1],
          ['permission_id' => 30, 'role_id' => 3, 'value' => 1],
          ['permission_id' => 31, 'role_id' => 3, 'value' => 1],
          ['permission_id' => 32, 'role_id' => 3, 'value' => 1],
          ['permission_id' => 33, 'role_id' => 3, 'value' => 1],

          //Permissoes para grupo 'Direitos Especiais'
          ['permission_id' => 3, 'role_id' => 4, 'value' => 1],
          ['permission_id' => 5, 'role_id' => 4, 'value' => 1],
          ['permission_id' => 18, 'role_id' => 4, 'value' => 1],
          ['permission_id' => 19, 'role_id' => 4, 'value' => 1],
          ['permission_id' => 24, 'role_id' => 4, 'value' => 1],
          ['permission_id' => 32, 'role_id' => 4, 'value' => 1],
          ['permission_id' => 33, 'role_id' => 4, 'value' => 1],

          //Permissoes para grupo Marketing
          ['permission_id' => 1, 'role_id' => 5, 'value' => 1],
          ['permission_id' => 2, 'role_id' => 5, 'value' => 1],
          ['permission_id' => 3, 'role_id' => 5, 'value' => 1],
          ['permission_id' => 4, 'role_id' => 5, 'value' => 1],
          ['permission_id' => 9, 'role_id' => 5, 'value' => 1],
          ['permission_id' => 10, 'role_id' => 5, 'value' => 1],
          ['permission_id' => 16, 'role_id' => 5, 'value' => 1],
          ['permission_id' => 17, 'role_id' => 5, 'value' => 1],
          ['permission_id' => 18, 'role_id' => 5, 'value' => 1],
          ['permission_id' => 19, 'role_id' => 5, 'value' => 1],
          ['permission_id' => 23, 'role_id' => 5, 'value' => 1],
          ['permission_id' => 29, 'role_id' => 5, 'value' => 1],
          ['permission_id' => 30, 'role_id' => 5, 'value' => 1],
          ['permission_id' => 31, 'role_id' => 5, 'value' => 1],
          ['permission_id' => 32, 'role_id' => 5, 'value' => 1],

          //Permissoes para grupo Hábito
          ['permission_id' => 1, 'role_id' => 6, 'value' => 1],
          ['permission_id' => 2, 'role_id' => 6, 'value' => 1],
          ['permission_id' => 4, 'role_id' => 6, 'value' => 1],
          ['permission_id' => 6, 'role_id' => 6, 'value' => 1],
          ['permission_id' => 7, 'role_id' => 6, 'value' => 1],
          ['permission_id' => 9, 'role_id' => 6, 'value' => 1],
          ['permission_id' => 10, 'role_id' => 6, 'value' => 1],
          ['permission_id' => 16, 'role_id' => 6, 'value' => 1],
          ['permission_id' => 29, 'role_id' => 6, 'value' => 1],
          ['permission_id' => 30, 'role_id' => 6, 'value' => 1],
          ['permission_id' => 31, 'role_id' => 6, 'value' => 1],

          //Permissoes para grupo Liquidez
          ['permission_id' => 1, 'role_id' => 7, 'value' => 1],
          ['permission_id' => 2, 'role_id' => 7, 'value' => 1],
          ['permission_id' => 4, 'role_id' => 7, 'value' => 1],
          ['permission_id' => 6, 'role_id' => 7, 'value' => 1],
          ['permission_id' => 7, 'role_id' => 7, 'value' => 1],
          ['permission_id' => 9, 'role_id' => 7, 'value' => 1],
          ['permission_id' => 10, 'role_id' => 7, 'value' => 1],
          ['permission_id' => 16, 'role_id' => 7, 'value' => 1],
          ['permission_id' => 29, 'role_id' => 7, 'value' => 1],
          ['permission_id' => 30, 'role_id' => 7, 'value' => 1],
          ['permission_id' => 31, 'role_id' => 7, 'value' => 1],
      ]);
    }
}
