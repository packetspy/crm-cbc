<?php

use Illuminate\Database\Seeder;
use App\Models\User as User;
use App\Models\Client;

class ClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
    }

    public static function create(User $user)
    {
        foreach(['cpf', 'cnpj'] as $type)
            $user->clients()->save(factory(App\Models\Client::class, $type)->make());
    }
}
