<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveCamposClients extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('clients', function(Blueprint $table)
      {
        $table->dropColumn('telefone');
        $table->dropColumn('email');
        $table->dropColumn('celular');
        $table->dropColumn('cliente_na_plataforma');
        $table->dropColumn('ativo');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
