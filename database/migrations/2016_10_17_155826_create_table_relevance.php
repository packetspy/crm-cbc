<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableRelevance extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('relevance', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('id_plataforma')->unsigned()->nullable();
          $table->integer('indicacoes')->unsigned()->nullable();
          $table->integer('contraindicacoes')->unsigned()->nullable();
          $table->integer('negocios')->unsigned()->nullable();

          $table->timestamps();
          $table->softDeletes();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('relevance');
    }
}
