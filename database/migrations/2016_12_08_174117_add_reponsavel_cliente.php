<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReponsavelCliente extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::table('clients', function (Blueprint $table) {
           $table->integer('responsavel_id')->after('user_id')->unsigned()->nullable();
       });
     }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('clients', function(Blueprint $table)
      {
          $table->dropColumn('responsavel_id');
      });
    }
}
