<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFinanceiroToStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('status', function (Blueprint $table) {
        $table->integer('financeiro')->after('status')->nullable();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('status', function(Blueprint $table)
      {
          $table->dropColumn('financeiro');
      });
    }
}
