<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterClientsChangeColumnNotNull extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clients', function (Blueprint $table) {
            $table->integer('tipo_documento')->nullable()->change();
            $table->string('segmento')->nullable()->change();
            $table->string('nome')->nullable()->change();
            $table->string('empresa')->nullable()->change();
            $table->boolean('cliente_na_plataforma')->nullable()->change();
            $table->boolean('ativo')->nullable()->default(1)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clients', function (Blueprint $table) {
            //
        });
    }
}
