<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterInteractionTypeAddColumnDadosObrigatorios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('interaction_types', function (Blueprint $table) {
            $table->boolean('data_obrigatoria')->nullable();
            $table->boolean('observacao_obrigatoria')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('interaction_types', function (Blueprint $table) {
            $table->removeColumn('data_obrigatoria');
            $table->removeColumn('observacao_obrigatoria');
        });
    }
}
