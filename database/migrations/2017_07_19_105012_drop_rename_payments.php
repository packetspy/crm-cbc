<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropRenamePayments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('payments', function (Blueprint $table) {
        $table->dropForeign('payments_client_id_foreign');
        $table->dropForeign('payments_user_id_foreign');
        $table->dropForeign('payments_payment_type_id_foreign');
      });
      Schema::dropIfExists('payment_types');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
