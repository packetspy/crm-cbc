<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSoftdeleteUsersContacts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('contacts', function (Blueprint $table) {
          $table->softDeletes()->after('updated_at');
      });

      Schema::table('users', function (Blueprint $table) {
          $table->softDeletes();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('contacts', function(Blueprint $table)
      {
          $table->dropColumn('deleted_at');
      });

      Schema::table('users', function(Blueprint $table)
  		{
  		    $table->dropColumn('deleted_at');
  		});
    }
}
