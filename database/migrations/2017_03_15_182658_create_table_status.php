<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      //Create table Status
      Schema::create('status', function(Blueprint $table)
      {
        $table->engine = 'InnoDB';
        $table->increments('id')->unsigned();
        $table->string('status',100);
        $table->integer('order');
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('status');
    }
}
