<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterInteractionTypesAddColumnHabilitaContraparte extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('interaction_types', function (Blueprint $table) {
            $table->boolean('exibe_apenas_contraparte')->nullable()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('interaction_types', function (Blueprint $table) {
            $table->dropColumn('exibe_apenas_contraparte');
        });
    }
}
