<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterContacts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('contacts', function(Blueprint $table)
      {
        $table->dropColumn('contatos');
        $table->string('cpf',25)->after('nome')->nullable();
        $table->string('perfil_plataforma',50)->after('cpf')->nullable();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
