<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterInteractionTypeColumnExibeCarrinho extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('interaction_types', function (Blueprint $table) {
            $table->renameColumn('exibe_carrinho', 'exibir_formulario');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('interaction_types', function (Blueprint $table) {
            $table->renameColumn('exibir_formulario', 'exibe_carrinho');
        });
    }
}
