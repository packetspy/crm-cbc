<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablesToPayment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      /*Schema::create('payment_types', function (Blueprint $table) {
          $table->increments('id');
          $table->string('type',100)->nullable();

          $table->integer('user_id')->unsigned();
          $table->foreign('user_id')->references('id')->on('users');

          $table->timestamps();
          $table->softDeletes();
      });*/

      Schema::create('payments', function (Blueprint $table) {
          $table->increments('id');
          $table->decimal('amount',10,2)->nullable();
          $table->boolean('free');
          $table->string('day',4)->nullable();
          $table->mediumText('description')->nullable();

          $table->integer('payment_type_id')->unsigned();
          $table->foreign('payment_type_id')->references('id')->on('payment_types');

          $table->integer('client_id')->unsigned();
          $table->foreign('client_id')->references('id')->on('clients');

          $table->integer('user_id')->unsigned();
          $table->foreign('user_id')->references('id')->on('users');

          $table->timestamps();
          $table->softDeletes();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payments', function (Blueprint $table) {
          $table->dropForeign('payments_client_id_foreign');
          $table->dropForeign('payments_user_id_foreign');
          $table->dropForeign('payments_payment_type_id_foreign');
        });

        Schema::dropIfExists('payment_types');
        Schema::dropIfExists('payments');
    }
}
