<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AjustesCobranca extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      /*Schema::table('payments', function (Blueprint $table) {
        $table->dropForeign('payments_payment_type_id_foreign');
      });

      Schema::dropIfExists('payment_types');*/

      Schema::create('cobranca', function (Blueprint $table) {
          $table->increments('id');
          $table->decimal('valor',10,2)->nullable();
          $table->string('vencimento',4)->nullable();

          $table->integer('status_id')->unsigned();
          $table->foreign('status_id')->references('id')->on('status');

          $table->integer('formapagamento_id')->unsigned();
          $table->integer('recorrencia_id')->unsigned();

          $table->string('endereco',255)->nullable();
          $table->string('complemento',150)->nullable();
          $table->string('bairro',50)->nullable();
          $table->string('cidade',150)->nullable();
          $table->string('estado',5)->nullable();
          $table->string('cep',12)->nullable();

          $table->integer('client_id')->unsigned();
          $table->foreign('client_id')->references('id')->on('clients');

          $table->integer('user_id')->unsigned();
          $table->foreign('user_id')->references('id')->on('users');

          $table->timestamps();
          $table->softDeletes();
      });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('cobranca', function (Blueprint $table) {
        $table->dropForeign('cobranca_status_id_foreign');
        $table->dropForeign('cobranca_client_id_foreign');
        $table->dropForeign('cobranca_user_id_foreign');
      });

      Schema::dropIfExists('cobranca');
    }
}
