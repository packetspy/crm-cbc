<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AdicionaColunasEnderecoClientes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('clients', function (Blueprint $table) {
          $table->string('endereco',255)->after('empresa')->nullable();
          $table->string('complemento',150)->after('endereco')->nullable();
          $table->string('cidade',150)->after('complemento')->nullable();
          $table->string('estado',5)->after('cidade')->nullable();
          $table->string('cep',12)->after('estado')->nullable();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('clients', function (Blueprint $table) {
          $table->dropColumn('votes');
          $table->dropColumn('endereco');
          $table->dropColumn('complemento');
          $table->dropColumn('cidade');
          $table->dropColumn('estado');
          $table->dropColumn('cep');
      });
    }
}
