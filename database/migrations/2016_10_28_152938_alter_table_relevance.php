<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableRelevance extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('relevance', function (Blueprint $table) {
          $table->date('data_referencia')->after('negocios');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('relevance', function (Blueprint $table) {
          $table->dropColumn('data_referencia');
      });
    }
}
