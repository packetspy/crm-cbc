<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableHistroricoOperacao extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('historico_operacao', function(Blueprint $table)
      {
        $table->engine = 'InnoDB';
        $table->increments('id')->unsigned();
        $table->integer('id_operacao')->nullable();
        $table->integer('id_oferta_indicacao')->nullable();
        $table->string('tipo')->nullable();
        $table->string('situacao',100)->nullable();
        $table->string('produto')->nullable();
        $table->integer('id_empresa_plataforma')->nullable();
        $table->string('razao_empresa')->nullable();
        $table->integer('id_usuario_plataforma')->nullable();
        $table->string('nome_usuario')->nullable();
        $table->dateTime('data_inclusao')->nullable();
        $table->dateTime('data_alteracao')->nullable();
        $table->dateTime('data_validade')->nullable();
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('historico_operacao');
    }
}
