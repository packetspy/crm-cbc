<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterInteractionTypesAddColumnSubInteractionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('interaction_types', function (Blueprint $table) {
            $table->integer('father_id')
                  ->unsigned()
                  ->nullable();

            $table->foreign('father_id')
                  ->references('id')
                  ->on('interaction_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('interaction_types', function (Blueprint $table) {
            //$table->dropColumn('father_id');
        });
    }
}
