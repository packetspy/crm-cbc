<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableHistoricoLogin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('historico_login', function(Blueprint $table)
      {
        $table->engine = 'InnoDB';
        $table->increments('id')->unsigned();
        $table->integer('id_empresa_plataforma')->nullable();
        $table->integer('id_usuario_plataforma')->nullable();
        $table->string('email')->nullable();
        $table->dateTime('data_acesso')->nullable();
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('historico_login');
    }
}
