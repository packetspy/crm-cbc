<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIeTipoempresaDataplataforma extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('escritorios', function(Blueprint $table)
      {
        $table->engine = 'InnoDB';
        $table->increments('id')->unsigned();
        $table->string('nome',100);
        $table->string('sigla',10);
        $table->timestamps();
      });

      Schema::create('tipoempresa', function(Blueprint $table)
      {
        $table->engine = 'InnoDB';
        $table->increments('id')->unsigned();
        $table->string('tipo',100);
        $table->timestamps();
      });

      Schema::table('clients', function (Blueprint $table) {
        $table->string('ie',50)->after('documento')->nullable();
        $table->integer('escritorio_id')->after('responsavel_id')->unsigned()->nullable();
        $table->integer('tipoempresa_id')->after('tipo_documento')->unsigned()->nullable();
        $table->dateTime('datacadastro_plataforma')->after('id_plataforma')->nullable();
      });

      Schema::table('contacts', function (Blueprint $table) {
        $table->dateTime('datacadastro_plataforma')->after('id_plataforma')->nullable();
      });

      Schema::table('users', function (Blueprint $table) {
        $table->integer('escritorio_id')->after('remember_token')->unsigned()->nullable();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('escritorios');
      Schema::dropIfExists('tipoempresa');
      Schema::table('clients', function(Blueprint $table)
      {
          $table->dropColumn('ie');
          $table->dropColumn('escritorio_id');
          $table->dropColumn('tipoempresa_id');
          $table->dropColumn('datacadastro_plataforma');
      });
      Schema::table('contacts', function(Blueprint $table)
      {
          $table->dropColumn('datacadastro_plataforma');
      });

      Schema::table('users', function(Blueprint $table)
      {
          $table->dropColumn('escritorio_id');
      });
    }
}
