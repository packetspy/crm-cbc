<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnStatusId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('clients', function (Blueprint $table) {
          $table->integer('status_id')->after('responsavel_id')->unsigned()->nullable();

          $table->foreign("status_id")
               ->on('status')
              ->references('id');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('clients', function(Blueprint $table)
      {
        $table->dropForeign('clients_status_id_foreign');
        $table->dropColumn('status_id');
      });
    }
}
