<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableHistoricoLead extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('historico_lead', function(Blueprint $table)
    {
      $table->engine = 'InnoDB';
      $table->increments('id')->unsigned();
      $table->integer('client_id')->nullable();
      $table->integer('user_id')->nullable();
      $table->integer('responsavel_id')->nullable();
      $table->integer('escritorio_id')->nullable();
      $table->integer('status_id')->nullable();
      $table->integer('origem_id')->nullable();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
      Schema::dropIfExists('historico_lead');
  }
}
