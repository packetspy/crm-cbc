<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableAddOrigemId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('origem', function (Blueprint $table) {
          $table->increments('id');
          $table->string("origem", 100);
          $table->timestamps();
      });
      Schema::table('clients', function (Blueprint $table) {
          $table->integer('origem_id')->after('status_id')->unsigned()->default(1);
          //$table->foreign("origem_id")->on('origem')->references('id');
      });
      Schema::table('contacts', function (Blueprint $table) {
          $table->integer('origem_id')->after('client_id')->unsigned()->default(1);
          //$table->foreign("origem_id")->on('origem')->references('id');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('clients', function(Blueprint $table)
      {
        //$table->dropForeign('clients_origem_id_foreign');
        $table->dropColumn('origem_id');
      });

      Schema::table('contacts', function(Blueprint $table)
      {
        //$table->dropForeign('contacts_origem_id_foreign');
        $table->dropColumn('origem_id');
      });

      Schema::drop('origem');
    }
}
