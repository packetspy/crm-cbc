<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/


$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => env('EMAIL_LOGIN_TEST', null),
        'password' => bcrypt(env('PASSWORD_TEST', '123')),
        'remember_token' => env('PASSWORD_HASH', '123'),
    ];
});

$factory->define(App\Models\Client::class,function(Faker\Generator $faker){
    $faker->addProvider(new Faker\Provider\pt_BR\Person($faker));
    $faker->addProvider(new Faker\Provider\pt_BR\PhoneNumber($faker));

    return [
        'nome'      => "José Ricardo",
        'email'     => $faker->safeEmail,
        'empresa'  => $faker->name,//"Teste",
        'segmento' => 'teste',
        'cliente_na_plataforma' => true,
        'telefone' => $faker->phoneNumber,
        'celular'  => $faker->cellphoneNumber,
        'ativo'    => true
    ];
});



$factory->defineAs(App\Models\Client::class, "cpf", function(Faker\Generator $faker) use($factory) {
    $faker->addProvider(new Faker\Provider\pt_BR\Person($faker));
    return array_merge($factory->raw(App\Models\Client::class), [
            'documento' => $faker->cpf, 
            'tipo_documento' => 1
        ]
    );
});

$factory->defineAs(App\Models\Client::class, "cnpj", function(Faker\Generator $faker) use ($factory) {
    $faker->addProvider(new Faker\Provider\pt_BR\Company($faker));

    return array_merge($factory->raw(App\Models\Client::class), [
        'documento' => $faker->cnpj, 
        'tipo_documento' => 2
    ]);
});

$factory->define(App\Models\Product::class, function(Faker\Generator $faker){
    $faker->addProvider(new Faker\Provider\Lorem($faker));
    return [
        'nome' => $faker->name,
        'descricao' => $faker->text(100),
        'valor' => rand(1, 1000),
        'ativo' => true
    ];
});

$factory->define(App\Models\InteractionType::class, function(){
    return [
        'ativo'                  => true, 
        'exibir_formulario'      => true,
        'father_id'              => null,
        'observacao_obrigatoria' => false,
        'data_obrigatoria'       => false,
        'funcao'                 => true,
        'exibe_apenas_contraparte' => false
    ];
});

$factory->defineAs(App\Models\InteractionType::class, 'funcao', function() use ($factory) {
    return array_merge($factory->raw(App\Models\InteractionType::class), [
        'nome' => 'Função'
    ]);
});

$factory->defineAs(App\Models\InteractionType::class, 'entrar_contato', function() use ($factory) {
    return array_merge($factory->raw(App\Models\InteractionType::class), [
        'nome' => 'Entrar em contato',
        'exibir_formulario' => false
    ]);
});

$factory->defineAs(App\Models\InteractionType::class, 'visitar_cliente', function() use ($factory) {
    return array_merge($factory->raw(App\Models\InteractionType::class), [
        'nome' => 'Visitar cliente'
    ]);
});

$factory->defineAs(App\Models\InteractionType::class, 'cadastrar_cliente', function() use ($factory) {
    return array_merge($factory->raw(App\Models\InteractionType::class), [
        'nome' => 'Cadastrar cliente'
    ]);
});

$factory->defineAs(App\Models\InteractionType::class, 'ativar_indicacao', function() use ($factory) {
    return array_merge($factory->raw(App\Models\InteractionType::class), [
        'nome' => 'Ativar Indicação' 
    ]);
});


$factory->defineAs(App\Models\InteractionType::class, 'iniciar_contraparte', function() use ($factory) {
    return array_merge($factory->raw(App\Models\InteractionType::class), [
       // 'nome' => 'Iniciar contraparte',
        'nome' => 'Achar contraparte',
        'funcao' => true,
        'exibe_apenas_contraparte' => true,
        'exibir_formulario' => false
    ]);
});


$factory->defineAs(App\Models\InteractionType::class, 'contraparte', function() use ($factory) {
    return array_merge($factory->raw(App\Models\InteractionType::class), [
      //  'nome' => 'Achar contraparte',
        'nome' => 'Responder contraparte',
        'funcao' => false
    ]);
});

$factory->defineAs(App\Models\InteractionType::class, 'negocio_fechado', function() use ($factory) {
    return array_merge($factory->raw(App\Models\InteractionType::class), [
        'nome' => 'Negócio fechado',
        'funcao' => false
    ]);
});

$factory->defineAs(App\Models\InteractionType::class, 'negocio_nao_fechado', function() use ($factory) {
    return array_merge($factory->raw(App\Models\InteractionType::class), [
        'nome' => 'Não',
        'observacao_obrigatoria' => true
    ]);
});

$factory->defineAs(App\Models\InteractionType::class, 'produto_ruim', function() use ($factory) {
    return array_merge($factory->raw(App\Models\InteractionType::class), [
        'nome' => 'Produto ruím',
        'observacao_obrigatoria' => true
    ]);
});

$factory->defineAs(App\Models\InteractionType::class, 'negocio_direto', function() use ($factory) {
    return array_merge($factory->raw(App\Models\InteractionType::class), [
        'nome' => 'Negócio direto',
    ]);
});

$factory->defineAs(App\Models\InteractionType::class, 'negocio_cancelado', function() use ($factory) {
    return array_merge($factory->raw(App\Models\InteractionType::class), [
        'nome' => 'Negócio cancelado',
        'observacao_obrigatoria' => true
    ]);
});
    
$factory->defineAs(App\Models\InteractionType::class, 'preco_ruim', function() use ($factory) {
    return array_merge($factory->raw(App\Models\InteractionType::class), [
        'nome' => 'Preço ruím',
        'observacao_obrigatoria' => true
    ]);
});



$factory->defineAs(App\Models\InteractionType::class, 'registrar_atendimento', function() use ($factory) {
    return array_merge($factory->raw(App\Models\InteractionType::class), [
        'nome' => 'Registrar atendimento'
    ]);
});
